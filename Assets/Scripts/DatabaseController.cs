using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Controller {
    public class DatabaseController : MonoBehaviour
    {
        [Header("Managers")]
        [SerializeField]
        private SceneController sceneController;
        [SerializeField]
        private TMP_Text input;
        // Start is called before the first frame update

        private void Awake()
        {
            if (sceneController == null)
            {
                sceneController = FindObjectOfType<SceneController>();
            }
            //by adam

        }
        public void changeDB()
        {
            sceneController.DataManager.changeConnection(input.text);
        }
    }
}



