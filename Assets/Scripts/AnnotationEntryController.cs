using System.Threading.Tasks;
using Microsoft.MixedReality.Toolkit.UI;
using MRTK.Tutorials.AzureCloudServices.Scripts.Domain;
using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;
using TMPro;
using UnityEngine;
//author: Adam Hilario, adam.hilario@ucdconnect.ie
//based on prefabs from Microsoft
namespace MAAPAnnotate.Controller
{
    public class AnnotationEntryController : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private SceneController sceneController;
        [SerializeField]
        private GameObject searchObjectPanel = default;
        [SerializeField]
        private AnnotationController annotationEditPanel = default;
        [SerializeField]
        private AnnotationCardViewController annotationCardPrefab = default;
        [Header("UI Elements")]
        [SerializeField]
        private ButtonConfigHelper submitButtonConfigHelper = default;
        [SerializeField]
        private TMP_Text hintLabel = default;
        [SerializeField]
        private TMP_InputField inputField = default;
        [SerializeField]
        private string loadingText = "Please wait...";
        [SerializeField]
        private Interactable[] buttons = default;

        private bool isInSearchMode;

        private void Awake()
        {
            if (sceneController == null)
            {
                sceneController = FindObjectOfType<SceneController>();
            }
        }

        private void OnEnable()
        {
            inputField.text = "";
        }

        public void SetSearchMode(bool searchModeActive)
        {
            isInSearchMode = searchModeActive;
            submitButtonConfigHelper.MainLabelText = isInSearchMode ? "Search Object" : "Set Object";
        }

        public async void SubmitQuery()
        {
            if (string.IsNullOrWhiteSpace(inputField.text))
            {
                hintLabel.SetText("Please type in a name.");
                hintLabel.gameObject.SetActive(true);
                return;
            }

            if (!sceneController.DataManager.IsReady)
            {
                hintLabel.SetText("No connection to the database!");
                hintLabel.gameObject.SetActive(true);
                return;
            }

            SetButtonsInteractiveState(false);
            var project = await FindObject(inputField.text);
            if (project != null)
            {
                /*
                searchObjectPanel.SetActive(false);
                var objectCard = Instantiate(annotationCardPrefab, transform.position, transform.rotation);
                objectCard.Init(project);
                objectCard.sendMessage("Database already contains an annotation by this name. Saving now will overwrite it.");
                */
                Debug.Log("object found");
                searchObjectPanel.SetActive(false);
                annotationEditPanel.gameObject.SetActive(true);
                annotationEditPanel.Init(project);
                
                annotationEditPanel.sendMessage("Database already contains an annotation by this name. Saving now will overwrite it.");
                
            }
            else
            {
                project = await CreateObject(inputField.text);
                if (project != null)
                {
                    searchObjectPanel.SetActive(false);
                    annotationEditPanel.gameObject.SetActive(true);
                    annotationEditPanel.Init(project);
                }
            }
            SetButtonsInteractiveState(true);
        }

        private async Task<TrackedObject> FindObject(string searchName)
        {
            hintLabel.SetText(loadingText);
            hintLabel.gameObject.SetActive(true);
            var projectFromDb = await sceneController.DataManager.FindTrackedObjectByName(searchName);
            if (projectFromDb == null)
            {
                hintLabel.SetText($"No object found with the name '{searchName}'.");
                return null;
            }

            hintLabel.gameObject.SetActive(false);
            return projectFromDb;
        }

        private async Task<TrackedObject> CreateObject(string searchName)
        {
            hintLabel.SetText(loadingText);
            hintLabel.gameObject.SetActive(true);
            var trackedObject = await sceneController.DataManager.FindTrackedObjectByName(searchName);
            if (trackedObject == null)
            {
                trackedObject = new TrackedObject(searchName);
                var success = await sceneController.DataManager.UploadOrUpdate(trackedObject);
                if (!success)
                {
                    return null;
                }

                await sceneController.DataManager.UploadOrUpdate(trackedObject);
            }

            hintLabel.gameObject.SetActive(false);
            return trackedObject;
        }

        private void SetButtonsInteractiveState(bool state)
        {
            foreach (var interactable in buttons)
            {
                interactable.IsEnabled = state;
            }
        }
    }
}
