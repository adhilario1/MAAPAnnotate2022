using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//author: Adam Hilario, adam.hilario@ucdconnect.ie
public class LightController : MonoBehaviour
{
    public PinchSlider slider;
    public Light lamp;
    // Start is called before the first frame update
    void Start()
    {
        lamp.intensity = .5f;
    }

    // Update is called once per frame
    void Update()
    {
        lamp.intensity = slider.SliderValue;
    }
}
