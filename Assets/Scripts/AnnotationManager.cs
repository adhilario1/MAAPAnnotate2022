using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Manager
{
    public class AnnotationManager : MonoBehaviour
    {
        // Start is called before the first frame update
        private Stack<GameObject> currentAnnotations = new Stack<GameObject>();
        private Stack<GameObject> undoneAnnotations = new Stack<GameObject>();

        public void addObject(GameObject newObj)
        {
            currentAnnotations.Push(newObj);
        }

        public void undo()
        {
            if (currentAnnotations.Count == 0) return;

            GameObject temp = currentAnnotations.Pop();
            temp.gameObject.SetActive(false);
            undoneAnnotations.Push(temp);
        }

        public void redo()
        {
            //eventually create pop-up message saying annotations are up to date
            if (undoneAnnotations.Count == 0) return;

            GameObject temp = undoneAnnotations.Pop();
            Debug.Log(temp);
            temp.gameObject.SetActive(true);
            currentAnnotations.Push(temp);
        }

        public void clear()
        {
            while (currentAnnotations.Count > 0)
            {
                GameObject temp = currentAnnotations.Pop();
                Destroy(temp);
            }
            while (undoneAnnotations.Count > 0)
            {
                GameObject temp = undoneAnnotations.Pop();
                Destroy(temp);
            }
        }
    }
}
