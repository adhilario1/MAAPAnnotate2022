using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Utilities.Serialisation
{
    [System.Serializable]
    public class SerialisedVector3
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        public SerialisedVector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public SerialisedVector3(Vector3 v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
        }
    }
}

