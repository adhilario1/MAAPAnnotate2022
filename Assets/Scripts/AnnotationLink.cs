using MAAPAnnotate.Controller;
using MRTK.Tutorials.AzureCloudServices.Scripts.Domain;
using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Utilities.Menus
{
    public class AnnotationLink : MonoBehaviour
    {
        public string annotationName { private get; set; }
        public SceneController sceneController;
        public AnnotationController annotationController;


        public async void load()
        {
            var projectFromDb = await sceneController.DataManager.FindTrackedObjectByName(annotationName);
            TrackedObject trackedObject = (TrackedObject)projectFromDb;
            if (trackedObject.AnnotationBlobName != null)
            {
                var annotationData = await sceneController.DataManager.DownloadBlob(trackedObject.AnnotationBlobName);
                Debug.Log(trackedObject.AnnotationBlobName);
                annotationController.readFromBinary(annotationData);
            }
        }
    }
}

