using Microsoft.MixedReality.Toolkit.Examples.Demos;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

using MAAPAnnotate.Manager;

//original author: Johanna Barbier
//ported to MRTK by: Adam Hilario, adam.hilario@ucdconnect.ie
public class Pen : MonoBehaviour, IMixedRealityInputHandler
{
    #region controllers
    public PinchSlider redSlider;
    public PinchSlider greenSlider;
    public PinchSlider blueSlider;
    public PinchSlider opacitySlider;
    public PinchSlider widthSlider;
    #endregion

    #region Private Properties

    AnnotationManager am = null;
    /// GENERAL PROP
    private float timer;
    private GameObject lineCollection;
    /// <summary>
    /// Pointer Properties
    /// </summary>

    private struct PointerData
    {
        public IMixedRealityPointer pointer;
        private Vector3 initialGrabPointInPointer;

        public PointerData(IMixedRealityPointer pointer, Vector3 worldGrabPoint) : this()
        {
            this.pointer = pointer;
            this.initialGrabPointInPointer = Quaternion.Inverse(pointer.Rotation) * (worldGrabPoint - pointer.Position);
        }

        public bool IsNearPointer => pointer is IMixedRealityNearPointer;

        /// Returns the grab point on the manipulated object in world space
        public Vector3 GrabPoint => (pointer.Rotation * initialGrabPointInPointer) + pointer.Position;
    }

    private Dictionary<uint, PointerData> pointerIdToPointerMap = new Dictionary<uint, PointerData>();

    /// <summary>
    /// line properties
    /// </summary>

    private List<Vector3> currentPoints = new List<Vector3>();
    private RaycastHit hit;
    private Vector3 handPosition = Vector3.zero;
    //vector to add to handPosition to get replacedHandPosition
    private Vector3 replacingHand = Vector3.zero;
    //the back of the drawing ray, drawOriginPosition -> replacedHandPosition -> 3DModel
    private Vector3 drawOriginPosition = Vector3.zero;
    private Vector3 replacedHandPosition = Vector3.zero;
    //list with all lines
    //private List<LineRenderer> lineList = new List<LineRenderer>();
    private IMixedRealityPointer currentPointer = null;
    private IMixedRealityInputSource currentInputSource = null;
    private InputEventData currentInputEventData = null;
    private uint currentInputSourceId;

    private bool isInputUp = true;

    //public LineRenderer lineStyle;
    private GameObject newLine;
    private LineRenderer currentLine;

    private Color currColor = Color.red;
    private Color prevColor = Color.red;

    private Vector3 originalScale = Vector3.zero;
    private Vector3 originalDrawCursorScale = Vector3.zero;

    private int cpt1 = 0; //to name the line for "previous annotation"
    private int cpt2 = 0;

    #endregion

    #region PUBLIC PROPERTIES

    //GENERAL
    public float delay;

    //public line properties
    //public float delay; //for timer
    public float lineWidth;
    float ColorR;
    float ColorG;
    float ColorB;
    public float lineOpacity;

    /// <summary>
    /// DEPRECATED
    ///
    /// //Cursor appearing and following the hand when drawing
    /// public GameObject drawingCursor;
    /// //Main cursor of the scene
    /// public InteractiveMeshCursor mainCursor;
    /// </summary>

    #endregion

    #region Private Methods

    private PointerData GetFirstPointer()
    {
        // We may be able to do this without allocating memory.
        // Moving to a method for later investigation.
        return pointerIdToPointerMap.Values.First();
    }

    private Vector3 GetPointersGrabPoint()
    {
        Vector3 sum = Vector3.zero;
        int count = 0;
        foreach (var p in pointerIdToPointerMap.Values)
        {
            sum += p.GrabPoint;
            count++;
        }
        return sum / System.Math.Max(1, count);
    }

    private Vector3[] GetHandPositionArray()
    {
        var handPositionMap = new Vector3[pointerIdToPointerMap.Count];
        int index = 0;
        foreach (var item in pointerIdToPointerMap)
        {
            handPositionMap[index++] = item.Value.pointer.Position;
        }
        return handPositionMap;
    }

    private bool IsNearDrawing()
    {
        foreach (var item in pointerIdToPointerMap)
        {
            if (item.Value.IsNearPointer)
            {
                return true;
            }
        }
        return false;
    }

    private void fetchColor()
    { 
        ///DEPRECTED
        /// originalDrawCursorScale = drawingCursor.transform.localScale;
        currColor = new Color(redSlider.SliderValue, greenSlider.SliderValue, blueSlider.SliderValue, opacitySlider.SliderValue);
    }

    #endregion

    #region Public Methods

    private Renderer TargetRenderer;

    public void OnSliderUpdatedRed(SliderEventData eventData)
    {
        if ((currentLine != null))
        {
            currentLine.material.color = new Color(eventData.NewValue, currentLine.sharedMaterial.color.g, currentLine.sharedMaterial.color.b);
        }
    }

    public void OnSliderUpdatedGreen(SliderEventData eventData)
    {
        
        if (currentLine != null)
        {
            currentLine.material.color = new Color(currentLine.sharedMaterial.color.r, eventData.NewValue, currentLine.sharedMaterial.color.b);
        }
    }

    public void OnSliderUpdateBlue(SliderEventData eventData)
    {
        
        if ((currentLine != null))
        {
            currentLine.material.color = new Color(currentLine.sharedMaterial.color.r, currentLine.sharedMaterial.color.g, eventData.NewValue);
        }
    }

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        am = GameObject.Find("Annotation Collection").GetComponent<AnnotationManager>();
        timer = delay;
    }

    void Awake()
    {
        //save the original scale in order to change the line renderer width depending on it
        originalScale = this.transform.localScale;
        currColor = new Color(0f, 1f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        //throw new System.NotImplementedException();
        // bit shift the index of the layer to get a bit mask
        int layerMask = 1 << 8; // the layer 8 is the kern layer


        if (!isInputUp)
        {
            Debug.DrawLine(drawOriginPosition, drawOriginPosition + (replacedHandPosition - drawOriginPosition) * 2);

            IMixedRealityPointer pointer = currentInputEventData.InputSource.Pointers.First();
            //get current inputsource Position information if we can

            //if (pointer.Controller.IsPositionAvailable)
            //{
            handPosition = pointer.Position;
            //Debug.Log("Hand Position " + handPosition);
            //replacing hand close to the user gaze direction to avoid occlusion issues
            replacedHandPosition = handPosition + replacingHand;

            RaycastHit gazeHitResult;
            if (Physics.Raycast(
                    currentInputSource.Pointers.First().Rays.First().Origin,
                    currentInputSource.Pointers.First().Rays.First().Direction,
                    out gazeHitResult,
                    20.0f,
                    layerMask))
            {
                // If the Raycast has succeeded and hit a hologram
                // hitInfo's point represents the position being gazed at
                // hitInfo's collider GameObject represents the hologram being gazed at

                /// <summary>
                /// This is where the points after the origin is added
                /// Users may not wish to, or have the hardware capailities,
                /// to store every point, so the delay adjusts which frame a point is stored
                /// </summary>
                timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    //Physics.Raycast()
                    //to avoid a point repetitions
                    if (pointer.Result != null)
                    {
                        if (currentPoints.Count != 0 && currentPoints[currentPoints.Count - 1] != pointer.Result.Details.Point)
                        {
                            currentPoints.Add(pointer.Result.Details.Point + pointer.Result.Details.Normal * 0.0015f);
                        }
                    }
                    
                    
                }
            }

            
            //drawing the line
            if (currentPoints.Count != 0)
            {
                //setting the points of the line one by one
                //using this instead of setPositions because it's more logical to use list here
                currentLine.positionCount = currentPoints.Count;
                for (int i = 0; i < currentPoints.Count; i++)
                {
                    currentLine.SetPosition(i, currentPoints[i]);
                }
            }
            

        }
    }

    #region PORTED METHODS FROM PREVIOUS VERSION

    void OnDisable()
    {
        /*
        //hide all the line renderer created
        for (int i = 0; i < lineList.Count; i++)
        {
            lineList[i].gameObject.SetActive(false);
        }
        */
        if (cpt2 == 0)
        {
            //no line added
            if (cpt1 != 0)
            {
                cpt1--;
            }
        }
        cpt2 = 0; //reinitialize cpt2
    }

    void OnEnable()
    {
        cpt1++;
    }

    #endregion

    #region INPUT HANDLER

    public void OnInputDown(InputEventData eventData)
    {

        uint id = eventData.InputSource.Pointers.First().PointerId;

        //check if the source down detected is the hand

        if (eventData.InputSource.SourceType != InputSourceType.Hand)
        {
            return;
        }

        //Debug.Log("Input Down");

        //start a new line
        cpt2++;
        newLine = new GameObject();
        am.addObject(newLine);
        newLine.AddComponent<MeshCollider>();
        newLine.AddComponent<SliderChangeColor>();
        currentLine = newLine.AddComponent<LineRenderer>();
        //currentLine = Instantiate(lineStyle, this.transform, true);
        currentLine.name = "line" + cpt1.ToString() + "_" + cpt2.ToString();
        
        currentLine.material = new Material(Shader.Find("Sprites/Default"));

        fetchColor();
        currentLine.startColor = currColor;
        currentLine.endColor = currColor;
        float newLineWidth = widthSlider.SliderValue * .1f;
        currentLine.startWidth = newLineWidth;
        currentLine.endWidth = newLineWidth;

        currentLine.transform.parent = GameObject.Find("Drawings").transform;
        

        //add the newly created line ID to the list
        //lineList.Add(currentLine);
        currentLine.positionCount = 0;
        currentPoints.Clear();

        //save this eventData (the user hand) as currentInputSource
        currentInputSource = eventData.InputSource;
        currentInputEventData = eventData;
        currentInputSourceId = eventData.SourceId;

        isInputUp = false;

        // Add self as a modal input handler, to get all inputs during the manipulation
        //InputManager.Instance.PushModalInputHandler(gameObject);

        //point hit by gaze ray
        int layerMask = 1 << 8;
        RaycastHit gazeHitResult;
        if (Physics.Raycast(
                Camera.main.transform.position,
                Camera.main.transform.forward,
                out gazeHitResult,
                20.0f,
                layerMask))
        {

            drawOriginPosition = eventData.InputSource.Pointers.First().Result.Details.Point;

            //add the first point of the current line
            currentPoints.Add(eventData.InputSource.Pointers.First().Result.Details.Point + eventData.InputSource.Pointers.First().Result.Details.Normal * 0.0015f);
        }
    }

    public void OnInputUp(InputEventData eventData)
    {
        uint id = eventData.InputSource.Pointers.First().PointerId;
        /*
        if (pointerIdToPointerMap.ContainsKey(id))
        {
            pointerIdToPointerMap.Remove(id);
        }
        */
        //Debug.Log("Input Up");
        isInputUp = true;

        currentLine.positionCount = currentPoints.Count;
        currentLine.SetPositions(currentPoints.ToArray());
      
        /// DEPRECATED
        /// drawingCursor.SetActive(false);
        /// mainCursor.SetVisibility(true);
        currentInputSource = null;
    }

    #endregion
    
}
