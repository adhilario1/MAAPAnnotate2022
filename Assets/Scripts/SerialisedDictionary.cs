using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Utilities.Serialisation
{
    [System.Serializable]
    public class SerialisedDictionary : SerialisedObject
    {
        public typeFlag _typeFlag = typeFlag.DICTIONARY;
        public Dictionary<string, string> _dictionary { get; }

        public SerialisedDictionary(Dictionary <string, string> dictionary)
        {
            name = "Annotation Dictionary";
            _dictionary = dictionary;
        }
    }
}

