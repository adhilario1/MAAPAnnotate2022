 using MAAPAnnotate.Manager;
using Microsoft.MixedReality.Toolkit.Experimental.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//author: Adam Hilario, adam.hilario@ucdconnect.ie

namespace MAAPAnnotate.Controller
{
    public class ViewerController : MonoBehaviour
    {
        #region private properties
        
        private int AnnotationID;
        private TMP_Text messageLabel;
        private int currPlaceInDictionary;
        private string currKey;
        #endregion

        #region public components
        public Dictionary<string, string> annotationDictionary { get; private set; }
        public AnnotationManager annotationManager;
        public TMP_InputField title;
        public TMP_InputField body;
        public MRTKUGUIInputField searchBar;
        //public InputField deleteNote;
        public GameObject template;
        public TMP_Text indicator;
        public TMP_Text displayTitle;
        public TMP_Text displayBody;

        #endregion

        // Start is called before the first frame update
        void Start()
        {
            annotationDictionary = new Dictionary<string, string>();
            AnnotationID = 0;
            currPlaceInDictionary = 0;
        }

        public void load(Dictionary<string, string> dictionary)
        {
            annotationDictionary = dictionary;
            indicator.text = "Page: 1 / " + annotationDictionary.Count().ToString();
            displayTitle.text = annotationDictionary.Keys.First();
            displayBody.text = annotationDictionary.Values.First();

        }
        public void send()
        {
            if (string.IsNullOrWhiteSpace(title.text) || string.IsNullOrEmpty(title.text))  title.text = "Annotation_" + AnnotationID;
            
            if (annotationDictionary.ContainsKey(title.text.ToLower())){
                messageLabel.text = "An Annotation Currently exists with this title";
                return;
            }
            if (string.IsNullOrWhiteSpace(body.text) || string.IsNullOrEmpty(body.text))
            {
                messageLabel.text = "body cannot be null";
            }
            annotationDictionary.Add(title.text.ToLower(), body.text);
            AnnotationID++;
            currPlaceInDictionary = annotationDictionary.Count - 1;
            currKey = title.text;
            displayTitle.text = title.text;
            displayBody.text = body.text;
            indicator.text = "Page: " + (currPlaceInDictionary+1).ToString() + " / " + annotationDictionary.Count.ToString();
            title.text = null;
            body.text = null;
            Debug.Log(currPlaceInDictionary);
        }

        public void next()
        {
            Debug.Log(currPlaceInDictionary);
            currPlaceInDictionary++;
            if (currPlaceInDictionary > annotationDictionary.Count-1) currPlaceInDictionary = annotationDictionary.Count-1;
            currKey = annotationDictionary.Keys.ElementAt(currPlaceInDictionary);
            Debug.Log(currKey);
            displayTitle.text = currKey;
            string temp;
            annotationDictionary.TryGetValue(currKey, out temp);
            displayBody.text = temp;
            indicator.text = "Page: " + (currPlaceInDictionary+1).ToString() + " / " + annotationDictionary.Count.ToString();
            
        }

        public void back()
        {
            currPlaceInDictionary--;
            if (currPlaceInDictionary < 0) currPlaceInDictionary = 0;
            currKey = annotationDictionary.Keys.ElementAt(currPlaceInDictionary);
            displayTitle.text = currKey;
            Debug.Log(currKey);
            string temp;
            annotationDictionary.TryGetValue(currKey, out temp);
            displayBody.text = temp;
            indicator.text = "Page: " + (currPlaceInDictionary + 1).ToString() + " / " + annotationDictionary.Count.ToString();
        }

        public void search()
        {
            if (int.TryParse(searchBar.text, out int result))
            {
                searchByIndex(result);
            }
            else
            {
                searchByName(searchBar.text);
            }
        }

        public void delete()
        {
            AnnotationID--;
        }
        private void searchByIndex(int index)
        {
            if (index > annotationDictionary.Count)
            {
                messageLabel.text = "Index out of bounds";
                return;
            }
            currKey = annotationDictionary.Keys.ElementAt(index-1);
            displayTitle.text = currKey;
            string temp;
            annotationDictionary.TryGetValue(currKey, out temp);
            displayBody.text = temp;
            indicator.text = "Page: " + (currPlaceInDictionary + 1).ToString() + " / " + annotationDictionary.Count.ToString();
        }

        private void searchByName(string name)
        {
            string temp;
            
            if (annotationDictionary.TryGetValue(name.ToLower(), out temp))
            {
                displayTitle.text = name;
                displayBody.text = temp;
            } else
            {
                messageLabel.text = name + " not found.";
            }
            int count = 0;
            foreach (string key in annotationDictionary.Keys)
            {
                if (name.ToLower().Equals(key))
                {
                    currPlaceInDictionary = count;
                    break;
                }
                else count++;
            }
            
            indicator.text = "Page: "+(currPlaceInDictionary + 1).ToString() + " / " + annotationDictionary.Count.ToString();
        }
    }
}

