using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Utilities.Serialisation
{
    [System.Serializable]
    public class SerialisedColor
    {
        [SerializeField]
        private float r;
        [SerializeField]
        private float g;
        [SerializeField]
        private float b;
        [SerializeField]
        private float a;

        public SerialisedColor(float r, float g, float b, float a)
        {
            this.r = r; 
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public SerialisedColor(Color color)
        {
            this.r = color.r;
            this.g = color.g;
            this.b = color.b;
            this.a = color.a;
        }

        public Color toColor()
        {
            return new Color(r, g, b, a);
        }
    }
}
