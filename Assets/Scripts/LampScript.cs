﻿using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//author: Johanna Barbier, Johanna.Barbier@eleves.ec-nantes.fr, 08/2017

//Handle the lamp activation
public class LampScript : MonoBehaviour,
                          IMixedRealityPointerHandler
{
    [Tooltip("Object the lamp will look at (needs a MeshRenderer)")]
    public GameObject targetObject;
    [Tooltip("Light connected to lamp")]
    public Light lampLight;

    private Vector3 originPosition;
    private Quaternion originDirection;
    private MeshRenderer m_renderer = null;
    private Material material = null;
    private bool lampOn = false;

    void Start()
    {

        originPosition = this.transform.position;
        originDirection = this.transform.rotation;
    }
    void Awake()
    {
        
        if (targetObject != null)
        {
            m_renderer = targetObject.GetComponentInChildren<MeshRenderer>();
        }
        material = this.GetComponent<Renderer>().material;
        material.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        Debug.Log("inputClicked");
        
        lampOn = true;
        material.color = new Color(1.000f, 0.969f, 0.559f, 1.000f); //pale yellow
        lampLight.enabled = true;
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        if (lampOn)
        {
            //this.transform.LookAt(m_renderer.bounds.center);
        }
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        //return to original location
        lampOn = false;
        material.color = Color.white;
        lampLight.enabled = false;
        this.transform.position = originPosition;
        this.transform.rotation = originDirection;
    }

    // Use this for initialization
    #region UNUSED METHODS

    public void OnPointerClicked(MixedRealityPointerEventData eventData) { }

    #endregion
}
