
using UnityEngine;
using UnityEngine.UI;

//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Utilities.Serialisation
{
    [System.Serializable]
    public class Serialised3DObject : SerialisedObject
    {
        public float px, py, pz;
        public float sx, sy, sz;
        public float rx, ry, rz, rw;

        private SerialisedVector3[] vertices;
        public SerialisedColor color { get; }
        private string text;

        public Serialised3DObject(GameObject go)
        {
            this.name = go.name;
            this.px = go.transform.position.x;
            this.py = go.transform.position.y;
            this.pz = go.transform.position.z;
            this.sx = go.transform.localScale.x;
            this.sy = go.transform.localScale.y;
            this.sz = go.transform.localScale.z;
            this.rx = go.transform.rotation.x;
            this.ry = go.transform.rotation.y;
            this.rz = go.transform.rotation.z;
            this.rw = go.transform.rotation.w;

            if (go.gameObject.TryGetComponent(typeof(LineRenderer), out Component line))
            {
                LineRenderer lineRenderer = (LineRenderer)line;
                //lineRenderer.GetPositions
                this.type = typeFlag.LINE;

                this.vertices = new SerialisedVector3[lineRenderer.positionCount];
                this.color = new SerialisedColor(lineRenderer.startColor);

                for (int i = 0; i < lineRenderer.positionCount; i++)
                {
                    vertices[i] = new SerialisedVector3(lineRenderer.GetPosition(i));
                }
            } else
            {
                this.type = typeFlag.ARTIFACT;
            }
        }

        public bool TryReturnVertices(out SerialisedVector3[] pos)
        {
            if (this.type == typeFlag.LINE)
            {
                pos = vertices;
                return true;
            }
            else
            {
                pos = null;
                return false;
            }
        }

        public bool TryReturnText(out string text)
        {
            if (this.type == typeFlag.LINE)
            {
                text = this.text;
                return true;
            }
            else
            {
                text = null;
                return false;
            }
        }

        public Vector3 ReturnPosition()
        {
            return new Vector3(px, py, pz);
        }

        public Vector3 ReturnLocalScale()
        {
            return new Vector3(sx, sy, sz);
        }

        public Quaternion ReturnRotation()
        {
            return new Quaternion(rx, ry, rz, rw);
        }

        public GameObject toGameObject()
        {
            GameObject go = new GameObject();
            go.transform.position = ReturnPosition();
            go.transform.rotation = ReturnRotation();
            go.transform.localScale = ReturnLocalScale();
            go.name = this.name;
            return go;
        }
    }

    
}

