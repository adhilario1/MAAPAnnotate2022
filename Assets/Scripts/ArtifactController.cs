using MAAPAnnotate.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//author: Adam Hilario, adam.hilario@ucdconnect.ie
namespace MAAPAnnotate.Controller
{
    public class ArtifactController : MonoBehaviour
    {
        #region component
        public AnnotationManager annotationManager;
        public GameObject stone;
        #endregion

        #region private attributes
        private Vector3 orig_pos, orig_locScale;
        private Quaternion orig_rot;
        #endregion

        private void Start()
        {
            orig_pos = stone.transform.position;
            orig_rot = stone.transform.rotation;
            orig_locScale = stone.transform.localScale;
        }

        public void Reset()
        {
            annotationManager.clear();
            stone.transform.position = orig_pos;
            stone.transform.rotation = orig_rot;
            stone.transform.localScale = orig_locScale;
        }
    }
}

