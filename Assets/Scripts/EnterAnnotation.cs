using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;
using MAAPAnnotate.UI;
using MAAPAnnotate.Manager;
using Microsoft.MixedReality.Toolkit.UI;

//author: Adam Hilario, adam.hilario@ucdconnect.ie based on MRTK prefab
public class EnterAnnotation : MonoBehaviour
{
    
    public InputField input;
    public InputField deleteNote;
    public MAAPScrollableListPopulator listPopulator;
    public AnnotationManager annotationManager;
    public GameObject template;

    private List<string> annotationList = new List<string>();
    GameObject placeHolder;
    private int AnnotationID = 0;
    private string text;

    void Start()
    {
        placeHolder = GameObject.Find("TempText");
    }

    public void send()
    {
        Debug.Log(template.name);
        if (string.IsNullOrWhiteSpace(input.text) || string.IsNullOrEmpty(input.text)) return;
        string text = input.text;
        annotationList.Add(text);
        listPopulator.AddItem(text);

        /*
        if (annotationList.Count == 0 && GameObject.Find("TempText") != null)
        {
            placeHolder.SetActive(false);
        }
        */
        ///<summary>
        ///duplicating the template instead of creating a 
        ///new game object due to a bug (or feature?) of 
        ///Unity where new text objects are really big and 
        ///really far away in world space.
        /// </summary>
        /*
        string temp = input.text+"\n\n";
        annotationList.Add(temp);
        text = string.Empty;
        for (int i = 0; i < annotationList.Count; i++)
        {
            text += "" + (i+1) + ". " + annotationList[i];
        }
        template.GetComponent<Text>().text = text;
        */
        input.text = null;
        
    }

    void delete(string notesToBeDeleted) 
    {
        if (string.IsNullOrWhiteSpace(deleteNote.text) || string.IsNullOrEmpty(input.text)) return;

        string temp = Regex.Replace(deleteNote.text, "[^ 0-9]", "");
        if (string.IsNullOrWhiteSpace(temp) || string.IsNullOrEmpty(temp)) return;

        Debug.Log(temp);

        string[] tokens = temp.Split(' ');

        foreach (string token in tokens)
        {
            annotationList.RemoveAt(Int32.Parse(token.Trim()) - 1);
        }

        text = string.Empty;
        for (int i = 0; i < annotationList.Count; i++)
        {
            text += "" + (i + 1) + ". " + annotationList[i];
        }
        template.GetComponent<Text>().text = text;
        input.text = null;
    }
}
