﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void HolographicRemoteConnect::Connect()
extern void HolographicRemoteConnect_Connect_m0E82B1FF6F081D4C9F7EE426A8E51844928DDB57 (void);
// 0x00000002 System.Void HolographicRemoteConnect::OnGUI()
extern void HolographicRemoteConnect_OnGUI_m12662133938EFE0D369C2A1447A4FC7EF577FDCB (void);
// 0x00000003 System.Void HolographicRemoteConnect::.ctor()
extern void HolographicRemoteConnect__ctor_mB947007991C533B059F3CF272D4258357D3C743D (void);
// 0x00000004 System.Void ToggleButton::ToggleClipping()
extern void ToggleButton_ToggleClipping_mE7203AADB2A4CF3154E82054854E46BCD045DFF5 (void);
// 0x00000005 System.Void ToggleButton::.ctor()
extern void ToggleButton__ctor_m7051B7DCF4943D16DFD85C6F6C518B9C8A230957 (void);
// 0x00000006 System.Void ToggleSpatialMap::ToggleSpatialMaps()
extern void ToggleSpatialMap_ToggleSpatialMaps_mACF9F6644BB819CE7FA4FB9D2CC938F024A9D4C4 (void);
// 0x00000007 System.Boolean ToggleSpatialMap::get_IsObserverRunning()
extern void ToggleSpatialMap_get_IsObserverRunning_m1F6EE04DA7C52A3432E6CAD588C7F843B414E4FE (void);
// 0x00000008 System.Void ToggleSpatialMap::.ctor()
extern void ToggleSpatialMap__ctor_m14E2950855F302A84CE45C185C2A3F119341FF39 (void);
// 0x00000009 System.Void ViewButtonControl::Start()
extern void ViewButtonControl_Start_m1A5D56852C6C4B3F8D764FF2FE325349421C3D74 (void);
// 0x0000000A System.Void ViewButtonControl::NextModel()
extern void ViewButtonControl_NextModel_m97F19332A0B5EE3D419B9F6A950E3631334BB469 (void);
// 0x0000000B System.Void ViewButtonControl::PreviousModel()
extern void ViewButtonControl_PreviousModel_m31396945DBF6B181921DBEC0A6EF2F3E9E8DF796 (void);
// 0x0000000C System.Void ViewButtonControl::.ctor()
extern void ViewButtonControl__ctor_mADD68095E9D500AF804D34D38B39D8E95543A6DA (void);
// 0x0000000D System.Void AnnotationManager::Start()
extern void AnnotationManager_Start_m749C00A8299BB2B177ECA91A4CA23656B1D70401 (void);
// 0x0000000E System.Void AnnotationManager::Update()
extern void AnnotationManager_Update_m71BEB92C391788841A97642E4BAD1829ABB255C0 (void);
// 0x0000000F System.Void AnnotationManager::addObject(UnityEngine.GameObject)
extern void AnnotationManager_addObject_m7D581E1AC7CE5B7F62251F77AE726698E74704A7 (void);
// 0x00000010 System.Void AnnotationManager::undo()
extern void AnnotationManager_undo_m7C088695AD7CE2509B5B14D8484CFDA80012529C (void);
// 0x00000011 System.Void AnnotationManager::redo()
extern void AnnotationManager_redo_m0642B2A3A4615F8640C5B678B48EFC50AA6EB62D (void);
// 0x00000012 System.Void AnnotationManager::clear()
extern void AnnotationManager_clear_m601B99D5364071127D0E0EC12B62B088F8679F58 (void);
// 0x00000013 System.Void AnnotationManager::.ctor()
extern void AnnotationManager__ctor_m38A19A7AD9A7D25152FA607261741ABA0171DC23 (void);
// 0x00000014 System.Void EnterAnnotation::Start()
extern void EnterAnnotation_Start_m520E5E498DD580937EE9A0080B0A5FD723AA70EE (void);
// 0x00000015 System.Void EnterAnnotation::Update()
extern void EnterAnnotation_Update_m56F9DC5E2075E54C49ED4F5636A698C1C68F2F87 (void);
// 0x00000016 System.Void EnterAnnotation::send()
extern void EnterAnnotation_send_m4339E7F4D4D4E23970F996877F65BB3DD5E8C855 (void);
// 0x00000017 System.Void EnterAnnotation::delete(System.String)
extern void EnterAnnotation_delete_mD0CE9185B5C7ED4DB10D6E9ED202DA105F70710A (void);
// 0x00000018 System.Void EnterAnnotation::.ctor()
extern void EnterAnnotation__ctor_mA350D08990EA3F23F131E8CE4015925BCDB9A006 (void);
// 0x00000019 System.Void Info::Start()
extern void Info_Start_m8F84B2622094B7EB30853C075A94AC4FC34CA561 (void);
// 0x0000001A System.Void Info::Update()
extern void Info_Update_mE90E589BC0A55D94B1D8F0BE1DB3C122799FB8BD (void);
// 0x0000001B System.Void Info::.ctor()
extern void Info__ctor_m2837F74E1391252CA7D29C3BECD03D7F097BFC84 (void);
// 0x0000001C System.Void LampScript::Start()
extern void LampScript_Start_m4D798F0F5C1AE071D5BF76BF6E7B6CB9100E5A3F (void);
// 0x0000001D System.Void LampScript::Awake()
extern void LampScript_Awake_m23E31B6FF97E19213B872BEE21A778C574AF0F45 (void);
// 0x0000001E System.Void LampScript::Update()
extern void LampScript_Update_m719DF24899E86771D3B3FF2509E08B1DEA111789 (void);
// 0x0000001F System.Void LampScript::OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LampScript_OnPointerDown_mD452AFB94094716B0FB8AFDB76FBEF34F036FBA1 (void);
// 0x00000020 System.Void LampScript::OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LampScript_OnPointerDragged_mC9BB7FBA2515E96696AEE18CB28CE9993E5E2FB0 (void);
// 0x00000021 System.Void LampScript::OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LampScript_OnPointerUp_mB0A44926916559829E8523DFCCCDD44264E9BAF4 (void);
// 0x00000022 System.Void LampScript::OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LampScript_OnPointerClicked_mB210DA889C50C59CAC75C75BCB7955D092BEBC0D (void);
// 0x00000023 System.Void LampScript::.ctor()
extern void LampScript__ctor_m8211CD082E2F2149F7AFF06213E69B320EFB798C (void);
// 0x00000024 System.String OneDriveFilePicker::get_ClientID()
extern void OneDriveFilePicker_get_ClientID_m67A0ABAE34E87A17802B390454BFEC9D9ECA73EA (void);
// 0x00000025 System.Void OneDriveFilePicker::set_ClientID(System.String)
extern void OneDriveFilePicker_set_ClientID_mE4297097A3235352E7059E6ADA70F886DF59AC0E (void);
// 0x00000026 System.String OneDriveFilePicker::get_ClientSecret()
extern void OneDriveFilePicker_get_ClientSecret_m7CBCCE155886291E1CEA0D4955543636F77DFA3E (void);
// 0x00000027 System.Void OneDriveFilePicker::set_ClientSecret(System.String)
extern void OneDriveFilePicker_set_ClientSecret_mA5362556AC0329C6F9CC4C2904E50E7F8F5ABDD3 (void);
// 0x00000028 System.Void OneDriveFilePicker::Start()
extern void OneDriveFilePicker_Start_m4A1B7A79E25B571B8AE49B99FEAA3465C9F97503 (void);
// 0x00000029 System.Void OneDriveFilePicker::Update()
extern void OneDriveFilePicker_Update_m43F63720C9965013AAF9C5EC71054D03CC09074D (void);
// 0x0000002A System.Void OneDriveFilePicker::.ctor()
extern void OneDriveFilePicker__ctor_mECECA642ABE9FCBE30478385224EB69D8D0E90BB (void);
// 0x0000002B System.Void Organize3DModelWindow::Awake()
extern void Organize3DModelWindow_Awake_m29F0EA566B920914AF94135BAF8C8688E4F97E36 (void);
// 0x0000002C System.Void Organize3DModelWindow::Update()
extern void Organize3DModelWindow_Update_mB5717B76100567BB5399DC861D0FC56A3E52B0A8 (void);
// 0x0000002D System.Void Organize3DModelWindow::ReorganizeWindow()
extern void Organize3DModelWindow_ReorganizeWindow_mF66C510D87B556160250432FA00E7C349837BE90 (void);
// 0x0000002E System.Void Organize3DModelWindow::.ctor()
extern void Organize3DModelWindow__ctor_mEFC9A19FFABA57E1848E0DF35D3B796CD0BEEADD (void);
// 0x0000002F Pen/PointerData Pen::GetFirstPointer()
extern void Pen_GetFirstPointer_m38D7BEAA88A679B62ADBEF5607A8D60723A16E6D (void);
// 0x00000030 UnityEngine.Vector3 Pen::GetPointersGrabPoint()
extern void Pen_GetPointersGrabPoint_m1E6CE5428163BD131BFB8FEC85536B006BB5DFBA (void);
// 0x00000031 UnityEngine.Vector3[] Pen::GetHandPositionArray()
extern void Pen_GetHandPositionArray_m26A2FEB2B0D182F7D34EF47EAD17ABF7D16056DD (void);
// 0x00000032 System.Boolean Pen::IsNearDrawing()
extern void Pen_IsNearDrawing_mB72D7988D9604907FD1A27464D59EB6CAB54932E (void);
// 0x00000033 System.Void Pen::OnSliderUpdatedRed(Microsoft.MixedReality.Toolkit.UI.SliderEventData)
extern void Pen_OnSliderUpdatedRed_m8AB790688DB4E7197FEA51E023E3028E1AE28EDC (void);
// 0x00000034 System.Void Pen::OnSliderUpdatedGreen(Microsoft.MixedReality.Toolkit.UI.SliderEventData)
extern void Pen_OnSliderUpdatedGreen_m544D0EEF7373D02EC5894EC78555EADE7B84317A (void);
// 0x00000035 System.Void Pen::OnSliderUpdateBlue(Microsoft.MixedReality.Toolkit.UI.SliderEventData)
extern void Pen_OnSliderUpdateBlue_m76BA607E567F68D26C77B58CC01DA280C38EDF70 (void);
// 0x00000036 System.Void Pen::Start()
extern void Pen_Start_mF06F77F9C9D8058B5D9CC0E261F86EF230403B07 (void);
// 0x00000037 System.Void Pen::Awake()
extern void Pen_Awake_m6E1AC355EBE45ED706B6589F661BD6EB690C8065 (void);
// 0x00000038 System.Void Pen::Update()
extern void Pen_Update_mB2FD1E9B4484A8BAFFE8D2A61FAABE8C45CC3884 (void);
// 0x00000039 System.Void Pen::OnDisable()
extern void Pen_OnDisable_m73B1826C68A6B91F72632B5D3E950C0F9C8546E3 (void);
// 0x0000003A System.Void Pen::OnEnable()
extern void Pen_OnEnable_m39A5047DB41A3925CA354B540ED19CE620EE409D (void);
// 0x0000003B System.Void Pen::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void Pen_OnInputDown_m0145D6532AC810106F79483FCC3F2EBF6555E836 (void);
// 0x0000003C System.Void Pen::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void Pen_OnInputUp_m3591BDDEA6EBB35BD4E1783028CFC6B80724781C (void);
// 0x0000003D System.Void Pen::.ctor()
extern void Pen__ctor_mBF2019946D0DEF1A0C0A1289EEB295C8B2E68E92 (void);
// 0x0000003E System.Void Pen/PointerData::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.Vector3)
extern void PointerData__ctor_m1C270542C6E8CA3F5536722DAC86B2EECC3B2AD0 (void);
// 0x0000003F System.Boolean Pen/PointerData::get_IsNearPointer()
extern void PointerData_get_IsNearPointer_mCEC5F5FC9A255DFAFA14CA5A4B5ED7251FCEE37F (void);
// 0x00000040 UnityEngine.Vector3 Pen/PointerData::get_GrabPoint()
extern void PointerData_get_GrabPoint_m6C4BF165B3C043AEB832099DE2033B8204473B61 (void);
// 0x00000041 System.Void ResetStone::Awake()
extern void ResetStone_Awake_mB5691D559DE49F2A4F1BE1A8F11602DD4A1049D7 (void);
// 0x00000042 System.Void ResetStone::Reset()
extern void ResetStone_Reset_m41924A86A15431A5EA3170398232332BDFA7295A (void);
// 0x00000043 System.Void ResetStone::.ctor()
extern void ResetStone__ctor_mCD5906B12EE6CFF8CC6F242D9640CF837D7E99BD (void);
// 0x00000044 System.Void ReverseNormals::Start()
extern void ReverseNormals_Start_m52BF288A38C6B3796AFC5D501210C7C062E581C4 (void);
// 0x00000045 System.Void ReverseNormals::.ctor()
extern void ReverseNormals__ctor_mB5B3AD5724E884C1D0DD0FC8EF08B54715D9E308 (void);
// 0x00000046 System.Void ReverseTriangles::Start()
extern void ReverseTriangles_Start_m6170CB3FC8D2A201B001FD935185EB4FF53B22D0 (void);
// 0x00000047 System.Void ReverseTriangles::.ctor()
extern void ReverseTriangles__ctor_mDE7E3B9D413151BF6B0DF61AE83C35C4B9B1CB09 (void);
// 0x00000048 System.Void SaveAnnotation::Start()
extern void SaveAnnotation_Start_m2EFD9530DBEAD74A6D475D690D53DF39599A868C (void);
// 0x00000049 System.Void SaveAnnotation::Update()
extern void SaveAnnotation_Update_m97596240EDF64F1ED15CF8272B16F2C957DD0A4D (void);
// 0x0000004A System.Void SaveAnnotation::save()
extern void SaveAnnotation_save_m809A2D47C1C91D209D1AFEB18ABEC2FE4EE9F759 (void);
// 0x0000004B System.Void SaveAnnotation::load()
extern void SaveAnnotation_load_m2D989A1A8776024D36DEC68A0FE3E32FBC55E66B (void);
// 0x0000004C SaveAnnotation/ioFlag SaveAnnotation::writeToBinary()
extern void SaveAnnotation_writeToBinary_mC5F3B1A657A04939345A358D3CF74CF2C90BDBE7 (void);
// 0x0000004D SaveAnnotation/ioFlag SaveAnnotation::readFromBinary(System.String)
extern void SaveAnnotation_readFromBinary_m7B966C9512BFCD6A6C12857BBFDD81366F593E7B (void);
// 0x0000004E System.Void SaveAnnotation::changeAccessMode(System.Int32)
extern void SaveAnnotation_changeAccessMode_m32015E5062322C69DDD6A557BD70D1E36F342052 (void);
// 0x0000004F System.Int32 SaveAnnotation::getAccessMode()
extern void SaveAnnotation_getAccessMode_m16B456FCBF7E23370494248A6E1EC77E11DE15FA (void);
// 0x00000050 System.Void SaveAnnotation::.ctor()
extern void SaveAnnotation__ctor_mDD846578093B824E2495ECF371397E2172AE926D (void);
// 0x00000051 System.Void SavableObject::.ctor(UnityEngine.GameObject)
extern void SavableObject__ctor_mF621B4053DB71CB5BA86720BADE136B0AEDD4D79 (void);
// 0x00000052 System.Boolean SavableObject::TryReturnVertices(Vector3Ser[]&)
extern void SavableObject_TryReturnVertices_mD5ADCF3B52594A5689C5FF67FEE1B535C798D7E6 (void);
// 0x00000053 System.Boolean SavableObject::TryReturnText(System.String&)
extern void SavableObject_TryReturnText_mE12A84DF51A9251DFBA56AA6D7A7AF7AA3F17545 (void);
// 0x00000054 UnityEngine.Vector3 SavableObject::ReturnPosition()
extern void SavableObject_ReturnPosition_m63FB8AFB5B4815EA85AFC1322BF6FE2D2AFAA509 (void);
// 0x00000055 UnityEngine.Vector3 SavableObject::ReturnLocalScale()
extern void SavableObject_ReturnLocalScale_m2C48189FFE50C9D7895261178E8D215AFC81F670 (void);
// 0x00000056 UnityEngine.Quaternion SavableObject::ReturnRotation()
extern void SavableObject_ReturnRotation_mD7A3303077F46DBF5ECE07EFFEE5E34DE2FABC2D (void);
// 0x00000057 System.Single Vector3Ser::get_x()
extern void Vector3Ser_get_x_m6F1973AA868052BC30D1D6683E58694D41EBD422 (void);
// 0x00000058 System.Void Vector3Ser::set_x(System.Single)
extern void Vector3Ser_set_x_mEB455F2E8464EB93AA74D7AA4A46A4D5624B9774 (void);
// 0x00000059 System.Single Vector3Ser::get_y()
extern void Vector3Ser_get_y_m4D7B84975201BCA452B7E1E2BE88FEAADACADEA3 (void);
// 0x0000005A System.Void Vector3Ser::set_y(System.Single)
extern void Vector3Ser_set_y_mD7E6AF4B7E3996879CDDE9F26C9B62B3A06E6685 (void);
// 0x0000005B System.Single Vector3Ser::get_Z()
extern void Vector3Ser_get_Z_m2AEED0B4E5E71055FD6859D604CEB64A73ECBFDA (void);
// 0x0000005C System.Void Vector3Ser::set_Z(System.Single)
extern void Vector3Ser_set_Z_m2833E66EE3859E429B260E8854F2E0C027B1F196 (void);
// 0x0000005D System.Void Vector3Ser::.ctor(System.Single,System.Single,System.Single)
extern void Vector3Ser__ctor_mD21F0FF3C4F00FDBE9CCC635FB4E5FD26B4C7EE6 (void);
// 0x0000005E System.Void Vector3Ser::.ctor(UnityEngine.Vector3)
extern void Vector3Ser__ctor_m94B96C75D8958CC7383AD3EC832E8796C508184A (void);
// 0x0000005F System.Void SelectingArea::Awake()
extern void SelectingArea_Awake_mC9135DB37D9D0909E5C5763EF28AFC920047A44D (void);
// 0x00000060 System.Void SelectingArea::Start()
extern void SelectingArea_Start_mBF942E7ED5E1EADED98B9B0A9DED0687193EBA24 (void);
// 0x00000061 System.Void SelectingArea::Update()
extern void SelectingArea_Update_m64988F7F12EEDBA5B87B7CC5CE799192730D7CC7 (void);
// 0x00000062 System.Void SelectingArea::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void SelectingArea_OnInputDown_mDCE512801A313E8B63D2E1ECFF7A80BA7D8B1175 (void);
// 0x00000063 System.Void SelectingArea::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void SelectingArea_OnInputUp_mD49F9ED49A237440F76E10EFEDE923AD26287A85 (void);
// 0x00000064 System.Void SelectingArea::RectangleSelection()
extern void SelectingArea_RectangleSelection_mCEC2B39856EB98301C263041426CF015DF07CF36 (void);
// 0x00000065 System.Void SelectingArea::RectangleProjection()
extern void SelectingArea_RectangleProjection_m1F5BD603EF4547AEC24A6113C45B979251C374B1 (void);
// 0x00000066 System.Void SelectingArea::UpdateQuadVertices()
extern void SelectingArea_UpdateQuadVertices_m02A7581E815635FDC029C040533A4BB95862FC36 (void);
// 0x00000067 System.Void SelectingArea::FlipQuadNormals()
extern void SelectingArea_FlipQuadNormals_m69859F329B91EAEA4C3DA03DCF4B675CE3D3C149 (void);
// 0x00000068 System.Void SelectingArea::HideMeshObjList()
extern void SelectingArea_HideMeshObjList_mD930A9D269208EA9579C4C218D1499A0D39B35A9 (void);
// 0x00000069 System.Void SelectingArea::DestroyMeshObjList()
extern void SelectingArea_DestroyMeshObjList_mA7B89B6DAEBEA92420AE9AF5313F3B4A53FCBBBA (void);
// 0x0000006A System.Void SelectingArea::HideQuadList()
extern void SelectingArea_HideQuadList_mF113801CCAA6EDFFEAA4B98AF14DD49845280AB2 (void);
// 0x0000006B System.Void SelectingArea::SetOption(System.Int32)
extern void SelectingArea_SetOption_m44713811A1570A7DF164C58D4E0FE263EEFD8CAA (void);
// 0x0000006C System.Void SelectingArea::OnDisable()
extern void SelectingArea_OnDisable_m269205A0ABCC2674ADE32C892A7357F494CF15E9 (void);
// 0x0000006D System.Void SelectingArea::OnEnable()
extern void SelectingArea_OnEnable_mEBF13F742648492A1BE449E4884878B06443BDBD (void);
// 0x0000006E System.Void SelectingArea::OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void SelectingArea_OnSourceDetected_mAF5C3740DA8FF3116FF6F96438E00CB97F40405E (void);
// 0x0000006F System.Void SelectingArea::OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void SelectingArea_OnSourceLost_m4F50969E80F6B874AF55D15AFF1DA1498671B683 (void);
// 0x00000070 System.Void SelectingArea::.ctor()
extern void SelectingArea__ctor_mBE5C0F7E48F5672012D77D48F0D437C9CE69DBC8 (void);
// 0x00000071 System.Void Test::OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void Test_OnPointerClicked_mF212E2D8C44BC4BBB3356ECC564F45FFCA5959A3 (void);
// 0x00000072 System.Void Test::OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void Test_OnPointerDown_m8A79B9EA26E0CD31A2A4C5CC5C7F8E47ACB52BB2 (void);
// 0x00000073 System.Void Test::OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void Test_OnPointerDragged_mED001DD316C6C2A0A8702D422E802AFD4912AD75 (void);
// 0x00000074 System.Void Test::OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void Test_OnPointerUp_m82519CE3B9AA909CEDD4094C9C2C8D9256A64D4E (void);
// 0x00000075 System.Void Test::Start()
extern void Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8 (void);
// 0x00000076 System.Void Test::Update()
extern void Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0 (void);
// 0x00000077 System.Void Test::.ctor()
extern void Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C (void);
// 0x00000078 System.Void UploadNewModel::Start()
extern void UploadNewModel_Start_m609E149586BB178ADDE88F7E912A7FB4EDBC245D (void);
// 0x00000079 System.Void UploadNewModel::Update()
extern void UploadNewModel_Update_m4B6BDF4888EC73970771632DA2B8FBECB87CD654 (void);
// 0x0000007A System.Void UploadNewModel::.ctor()
extern void UploadNewModel__ctor_m42A2740E1E9772D70040A9F33DFAC60371BDFE29 (void);
// 0x0000007B System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000007C System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000007D System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000007E System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000007F System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x00000080 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x00000081 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x00000082 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000083 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000084 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000085 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000086 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000087 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000088 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000089 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x0000008A System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x0000008B System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x0000008C System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x0000008D System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x0000008E TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x0000008F System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x00000090 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x00000091 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x00000092 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000093 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000094 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000095 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000096 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000097 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000098 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000099 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x0000009A System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x0000009B System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x0000009C System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000009D System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x0000009E System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000009F System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x000000A0 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x000000A1 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x000000A2 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x000000A3 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x000000A4 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x000000A5 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x000000A6 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x000000A7 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x000000A8 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x000000A9 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x000000AA System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x000000AB System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x000000AC System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x000000AD System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x000000AE System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x000000AF System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x000000B0 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x000000B1 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x000000B2 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x000000B3 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x000000B4 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000000B5 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000000B6 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000000B7 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x000000B8 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x000000B9 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x000000BA System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x000000BB System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x000000BC System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x000000BD System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x000000BE System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x000000BF System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x000000C0 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x000000C1 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x000000C2 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x000000C3 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x000000C4 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x000000C5 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x000000C6 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x000000C7 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x000000C8 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x000000C9 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x000000CA System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x000000CB System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x000000CC System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x000000CD System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x000000CE System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x000000CF System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x000000D0 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x000000D1 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x000000D2 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x000000D3 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x000000D4 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x000000D5 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x000000D6 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x000000D7 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x000000D8 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x000000D9 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x000000DA System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x000000DB System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x000000DC System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x000000DD System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x000000DE System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x000000DF System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x000000E0 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x000000E1 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x000000E2 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x000000E3 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x000000E4 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x000000E5 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x000000E6 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x000000E7 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x000000E8 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x000000E9 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x000000EA System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x000000EB System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x000000EC System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x000000ED System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x000000EE System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x000000EF System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x000000F0 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x000000F1 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x000000F2 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x000000F3 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x000000F4 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x000000F5 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x000000F6 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x000000F7 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x000000F8 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x000000F9 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x000000FA System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x000000FB System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x000000FC System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x000000FD System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000000FE System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000000FF System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x00000100 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x00000101 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x00000102 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x00000103 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x00000104 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x00000105 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x00000106 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x00000107 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x00000108 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x00000109 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x0000010A System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x0000010B System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x0000010C System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x0000010D System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x0000010E System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x0000010F System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x00000110 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x00000111 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x00000112 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x00000113 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x00000114 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x00000115 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x00000116 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x00000117 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000118 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000119 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x0000011A System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x0000011B System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x0000011C System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x0000011D System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x0000011E System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x0000011F System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x00000120 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x00000121 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x00000122 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x00000123 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x00000124 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x00000125 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x00000126 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x00000127 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000128 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000129 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x0000012A System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x0000012B System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x0000012C System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x0000012D System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x0000012E System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x0000012F System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x00000130 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x00000131 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x00000132 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x00000133 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x00000134 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x00000135 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x00000136 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x00000137 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x00000138 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x00000139 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x0000013A System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x0000013B System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x0000013C System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x0000013D System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x0000013E System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x0000013F System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x00000140 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x00000141 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x00000142 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x00000143 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x00000144 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x00000145 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x00000146 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x00000147 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x00000148 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x00000149 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x0000014A System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x0000014B System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x0000014C System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x0000014D System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x0000014E System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x0000014F System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x00000150 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x00000151 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x00000152 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x00000153 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x00000154 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x00000155 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x00000156 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x00000157 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000158 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000159 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x0000015A System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x0000015B System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x0000015C System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x0000015D System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x0000015E System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x0000015F System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x00000160 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x00000161 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x00000162 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x00000163 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000164 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000165 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000166 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000167 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000168 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000169 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x0000016A System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x0000016B System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x0000016C System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x0000016D System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x0000016E System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000016F System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x00000170 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x00000171 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x00000172 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x00000173 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000174 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000175 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000176 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000177 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000178 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000179 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x0000017A System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x0000017B UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x0000017C System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x0000017D System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x0000017E System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000017F System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x00000180 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x00000181 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x00000182 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x00000183 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x00000184 System.Single HoloToolkit.Unity.TextToSpeechManager::BytesToFloat(System.Byte,System.Byte)
extern void TextToSpeechManager_BytesToFloat_m00198F96D71C22BD29F8572E4FBFACFCF381C585 (void);
// 0x00000185 System.Int32 HoloToolkit.Unity.TextToSpeechManager::BytesToInt(System.Byte[],System.Int32)
extern void TextToSpeechManager_BytesToInt_m635AD055DBD2569EF8698DAB3B13BB67EAEE0F7F (void);
// 0x00000186 UnityEngine.AudioClip HoloToolkit.Unity.TextToSpeechManager::ToClip(System.String,System.Single[],System.Int32,System.Int32)
extern void TextToSpeechManager_ToClip_m70FDE0CE26C25CD3D7692B2B1E6EAFEED2CDCC79 (void);
// 0x00000187 System.Single[] HoloToolkit.Unity.TextToSpeechManager::ToUnityAudio(System.Byte[],System.Int32&,System.Int32&)
extern void TextToSpeechManager_ToUnityAudio_mEAF3F4F39A74BD95F7600721944E6A4BC7A428CC (void);
// 0x00000188 System.Void HoloToolkit.Unity.TextToSpeechManager::LogSpeech(System.String)
extern void TextToSpeechManager_LogSpeech_m36EBC522D8011A6B2092A605170BC2E01D686F2C (void);
// 0x00000189 System.Void HoloToolkit.Unity.TextToSpeechManager::PlaySpeech(System.String,System.Func`1<Windows.Foundation.IAsyncOperation`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream>>)
extern void TextToSpeechManager_PlaySpeech_m54C2EC6DFB4B80FF67BFA594419367DFE4F1F9B1 (void);
// 0x0000018A System.Void HoloToolkit.Unity.TextToSpeechManager::Start()
extern void TextToSpeechManager_Start_m112AFD8569A086B85BE0358B1CD03282B1CEC7E2 (void);
// 0x0000018B System.Void HoloToolkit.Unity.TextToSpeechManager::SpeakSsml(System.String)
extern void TextToSpeechManager_SpeakSsml_m30BDFD8475DC679C2CD2DBF69A76C932ECAA1971 (void);
// 0x0000018C System.Void HoloToolkit.Unity.TextToSpeechManager::SpeakText(System.String)
extern void TextToSpeechManager_SpeakText_mEA9C9E2437A4BCC11C90A0F157C0535FDBFB9878 (void);
// 0x0000018D UnityEngine.AudioSource HoloToolkit.Unity.TextToSpeechManager::get_AudioSource()
extern void TextToSpeechManager_get_AudioSource_m85FF350D00BBA44EA643B29502B2F3F0A0C0301E (void);
// 0x0000018E System.Void HoloToolkit.Unity.TextToSpeechManager::set_AudioSource(UnityEngine.AudioSource)
extern void TextToSpeechManager_set_AudioSource_m1708481C623D2387572B4FED2C17422FA8ACA37E (void);
// 0x0000018F HoloToolkit.Unity.TextToSpeechVoice HoloToolkit.Unity.TextToSpeechManager::get_Voice()
extern void TextToSpeechManager_get_Voice_mEC5E487B2E6216A7760077CF7E4F7908AD309C27 (void);
// 0x00000190 System.Void HoloToolkit.Unity.TextToSpeechManager::set_Voice(HoloToolkit.Unity.TextToSpeechVoice)
extern void TextToSpeechManager_set_Voice_m2C0F68DDE982C6B88240491F53EE00138F71D8E9 (void);
// 0x00000191 System.Void HoloToolkit.Unity.TextToSpeechManager::.ctor()
extern void TextToSpeechManager__ctor_mA610D3650F7A25852D7AF89C3C12616A13618CD3 (void);
// 0x00000192 System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m18BBE49444A95B46397D4E57BCDAE33346323459 (void);
// 0x00000193 System.Threading.Tasks.Task HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_0::<PlaySpeech>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CPlaySpeechU3Eb__0_m7AD83D3004206869B0B6CE54154797FD57AF24CE (void);
// 0x00000194 System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_0/<<PlaySpeech>b__0>d::MoveNext()
extern void U3CU3CPlaySpeechU3Eb__0U3Ed_MoveNext_m604395E2D0545ABA13308F844EF82D1E3359B46E (void);
// 0x00000195 System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_0/<<PlaySpeech>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CPlaySpeechU3Eb__0U3Ed_SetStateMachine_mFD91BAA9F1F1A2294AA3CCF16050C660FC13F7F0 (void);
// 0x00000196 System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_1::.ctor()
extern void U3CU3Ec__DisplayClass9_1__ctor_m5D8327F509E5E02F836E1563D1C19ED98FB9C474 (void);
// 0x00000197 System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_1::<PlaySpeech>b__1()
extern void U3CU3Ec__DisplayClass9_1_U3CPlaySpeechU3Eb__1_m2A87E639A3140FD6D4BEC8AFB8C4B3EB5F74FED3 (void);
// 0x00000198 System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_2::.ctor()
extern void U3CU3Ec__DisplayClass9_2__ctor_mB55DC1141A110A010701A5BA87B3E965C26B0F91 (void);
// 0x00000199 System.Boolean HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass9_2::<PlaySpeech>b__2(Windows.Media.SpeechSynthesis.VoiceInformation)
extern void U3CU3Ec__DisplayClass9_2_U3CPlaySpeechU3Eb__2_m0871A2DCFB641E9BE77F2D22FC262DA7D4895BFB (void);
// 0x0000019A System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mF01AA4E90CB37150BD05E1A7FFC12D40E0E1C1A7 (void);
// 0x0000019B Windows.Foundation.IAsyncOperation`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream> HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass11_0::<SpeakSsml>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CSpeakSsmlU3Eb__0_mD2F84B12192A5644CC5E98EA87E7B2B8AE6115B3 (void);
// 0x0000019C System.Void HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mAC0A19C259154E1C1FDB8BC0E8FF9FF3DD966FE1 (void);
// 0x0000019D Windows.Foundation.IAsyncOperation`1<Windows.Media.SpeechSynthesis.SpeechSynthesisStream> HoloToolkit.Unity.TextToSpeechManager/<>c__DisplayClass12_0::<SpeakText>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CSpeakTextU3Eb__0_mF73BE698BBE002423F86876103ACCDF207A82E56 (void);
// 0x0000019E Microsoft.MixedReality.Toolkit.Dwell.DwellHandler Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::get_DwellHandler()
extern void BaseDwellSample_get_DwellHandler_mE7E88483FEEF32A0EC8E2A14677CC3BAD02878FE (void);
// 0x0000019F System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::set_DwellHandler(Microsoft.MixedReality.Toolkit.Dwell.DwellHandler)
extern void BaseDwellSample_set_DwellHandler_mA7ED0780EA864E6E8E437DD6A109D80D333A0923 (void);
// 0x000001A0 System.Boolean Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::get_IsDwelling()
extern void BaseDwellSample_get_IsDwelling_mF8BA92D92DCEEA358F1D504417350886800D3BC5 (void);
// 0x000001A1 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::set_IsDwelling(System.Boolean)
extern void BaseDwellSample_set_IsDwelling_mB339F111599B576E0C353619B6F03DC1CAA33139 (void);
// 0x000001A2 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::Awake()
extern void BaseDwellSample_Awake_m92902301B904E0A84BC2DDE8111358B1B8E6FF83 (void);
// 0x000001A3 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellStarted_m4BC95AE386B073E97646AD8D75D9299800ED3515 (void);
// 0x000001A4 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellIntended(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellIntended_mE225ABD345ABEB4EC4879283516B35C6AC5E381A (void);
// 0x000001A5 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellCanceled_mB4EE479879225BCAD03DFC16A41ADB42F31A395D (void);
// 0x000001A6 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellCompleted_m0278883E0090F2ADCC9D2B648609C6A9DAF04E68 (void);
// 0x000001A7 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::ButtonExecute()
extern void BaseDwellSample_ButtonExecute_m9DF3D058CCA03D2955F3EEB9CC797A73CFF52802 (void);
// 0x000001A8 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::.ctor()
extern void BaseDwellSample__ctor_mF07F35663FCDF204D2A4D270BF679694B015B4F0 (void);
// 0x000001A9 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::Update()
extern void InstantDwellSample_Update_m020095FE6938C0C5E0F61BC122B19040FB9E3BD4 (void);
// 0x000001AA System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void InstantDwellSample_DwellCompleted_mD57B332DD31239D1ABEE52DBBAEDFC0584046DC2 (void);
// 0x000001AB System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::ButtonExecute()
extern void InstantDwellSample_ButtonExecute_mA7083DD44C7E00376F73CB8D285B1244B4D6D032 (void);
// 0x000001AC System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::.ctor()
extern void InstantDwellSample__ctor_mF63A380F57BADD53FB5D6AD43F7CD6CA74265C27 (void);
// 0x000001AD System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::Awake()
extern void ListItemDwell_Awake_m68AC969200A596B60F891C22FA9D4FD2E2677F4A (void);
// 0x000001AE System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::Update()
extern void ListItemDwell_Update_m717C89E13D412F11580BDE4AB02CC1AF1601FA5F (void);
// 0x000001AF System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ListItemDwell_DwellCompleted_mF27529880A038665A5F68D5E0A9C48D4CC6DC33B (void);
// 0x000001B0 System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::ButtonExecute()
extern void ListItemDwell_ButtonExecute_mE27FB4CD8CDC2DA550C17054B648E696DBBB839E (void);
// 0x000001B1 System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::.ctor()
extern void ListItemDwell__ctor_mEF18F64091788C951EAAADADB90B8091F0BD6854 (void);
// 0x000001B2 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::Update()
extern void ToggleDwellSample_Update_m30A06B48D82AD4F220CDF15B7D1BC397AA712C9E (void);
// 0x000001B3 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellIntended(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellIntended_m9A839EB5BE2707ABC9C9F070E134DD79187E75FD (void);
// 0x000001B4 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellStarted_m32D126A396C497A7330E814F4004F7A769ED46DA (void);
// 0x000001B5 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellCanceled_m277D291308981DBED6AB4C39127AEDAE29742CB4 (void);
// 0x000001B6 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellCompleted_m57F979B2EFF69A91918344ABA3709A4A0BFBFCA4 (void);
// 0x000001B7 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::ButtonExecute()
extern void ToggleDwellSample_ButtonExecute_m7F9003C0CE7B5E3E3A66DF8B8AA219CBACFBE628 (void);
// 0x000001B8 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::.ctor()
extern void ToggleDwellSample__ctor_m7ED34BCBD44C2D0DC9DDBB65956554AC89C58331 (void);
// 0x000001B9 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnEnable()
extern void GestureTester_OnEnable_m0140D4F607D27D42F445714D070CC7EADA275E24 (void);
// 0x000001BA System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureStarted(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureStarted_m769D9B5F7D3FA69B08CD2396FE4E8EB9C4CE9041 (void);
// 0x000001BB System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureUpdated_m2CF619D7BD37384BC7765A1AFC43FF355613A234 (void);
// 0x000001BC System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector3>)
extern void GestureTester_OnGestureUpdated_mEA2E33387D62C4D1B919C2C4D9C6AD1163B5CB70 (void);
// 0x000001BD System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCompleted(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureCompleted_m857D1FEB9276A76A9D1A872BEABB8B0337D79194 (void);
// 0x000001BE System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCompleted(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector3>)
extern void GestureTester_OnGestureCompleted_m18D1D6159094D9C15E3EE5EBAC03B0214051B2E8 (void);
// 0x000001BF System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCanceled(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureCanceled_mEC678CBBBC1E312636C51D78A83EA8E293680C3F (void);
// 0x000001C0 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::SetIndicator(UnityEngine.GameObject,System.String,UnityEngine.Material)
extern void GestureTester_SetIndicator_mC10E281F42604E9478C157E2E1D6A10045789FDA (void);
// 0x000001C1 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::SetIndicator(UnityEngine.GameObject,System.String,UnityEngine.Material,UnityEngine.Vector3)
extern void GestureTester_SetIndicator_mCE66488AB17DA7410AD184940079B68B013EDD63 (void);
// 0x000001C2 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::ShowRails(UnityEngine.Vector3)
extern void GestureTester_ShowRails_m503612C0788C264B5250059F23F4F267F6662C78 (void);
// 0x000001C3 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::HideRails()
extern void GestureTester_HideRails_mEF63845796950C93EF3A59BB7E9D9EB6B82192B9 (void);
// 0x000001C4 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::.ctor()
extern void GestureTester__ctor_mCA9059AE04A957A48F5E848BB37BC4FB1A5AD782 (void);
// 0x000001C5 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::Awake()
extern void GrabTouchExample_Awake_m193BA2448772C129BA391D7666A5373CA98EAD17 (void);
// 0x000001C6 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GrabTouchExample_OnInputDown_m07A209D9218A662FEFDE282E5CCCCC92D78DDADA (void);
// 0x000001C7 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GrabTouchExample_OnInputUp_m628ACEE16B43D23EBDD71F97D768E936B0FF2B7E (void);
// 0x000001C8 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputPressed(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Single>)
extern void GrabTouchExample_OnInputPressed_m45CCFEA21313A542F9FD03150F3C83521A3CB242 (void);
// 0x000001C9 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnPositionInputChanged(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector2>)
extern void GrabTouchExample_OnPositionInputChanged_m548EBF13250031CF2857C36BB10AD640954CFE03 (void);
// 0x000001CA System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchCompleted_m4B01CAC3AAA8B2BD773161831871894E71DBB926 (void);
// 0x000001CB System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchStarted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchStarted_m36E8794CB581EDCB902B0FDE20671D70F42A221F (void);
// 0x000001CC System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchUpdated_m94E2D7C86FF55ACB515079E5958902AAD67BB04B (void);
// 0x000001CD System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::.ctor()
extern void GrabTouchExample__ctor_m6FFF0E748673903DFA2072F856F569ECDC1BBF6A (void);
// 0x000001CE System.Void Microsoft.MixedReality.Toolkit.Examples.LeapMotionOrientationDisplay::.ctor()
extern void LeapMotionOrientationDisplay__ctor_mC9E9FDEE213D10636277EF57AAA73C69CA0A9DA1 (void);
// 0x000001CF System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnEnable()
extern void RotateWithPan_OnEnable_mFA342F4128FB0B7B1FBB2AF91ACAD08BF1B34AA1 (void);
// 0x000001D0 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnDisable()
extern void RotateWithPan_OnDisable_m606981F1E173DE557743B647BAFCA2F3E2404600 (void);
// 0x000001D1 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanEnded(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanEnded_m293831F1B00757AFB383BD53E6D94BD08A42FB6E (void);
// 0x000001D2 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanning(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanning_m1E07E3B6FB54BA55684D6ED5E5D9A2CBC431C0A3 (void);
// 0x000001D3 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanStarted(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanStarted_mEC903265CA88C185DB163CEBF2EFD0A7BA224FE9 (void);
// 0x000001D4 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::.ctor()
extern void RotateWithPan__ctor_mA18E5BAD40CDA091610D697BE9FD1C8636A68EDE (void);
// 0x000001D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::Start()
extern void WidgetElasticDemo_Start_m3AA84DAFCBDA72AF1EEB77B0069CB15753D196C5 (void);
// 0x000001D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::Update()
extern void WidgetElasticDemo_Update_m243D8FCC1B9E729D737357722C43C7AAE7CEB34E (void);
// 0x000001D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::ToggleInflate()
extern void WidgetElasticDemo_ToggleInflate_mCE86AF4A69EEBEA32A437BA152C589D4EFE6EE8B (void);
// 0x000001D8 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::DeflateCoroutine()
extern void WidgetElasticDemo_DeflateCoroutine_m84135534DD3C09B645B81B0EB6115FDD02C00429 (void);
// 0x000001D9 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::InflateCoroutine()
extern void WidgetElasticDemo_InflateCoroutine_mE2F383E15019773883AF45EF7DD49AA449F4875B (void);
// 0x000001DA System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::.ctor()
extern void WidgetElasticDemo__ctor_m8B8A9189D8C89D4FB77AF954F2FAE39FC1BBE1AB (void);
// 0x000001DB System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::.cctor()
extern void WidgetElasticDemo__cctor_m15492C9A3999ECA43B8235AC74CC6990646903DE (void);
// 0x000001DC System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::.ctor(System.Int32)
extern void U3CDeflateCoroutineU3Ed__17__ctor_mEA2390B9684B886A451971C951655213D65AC906 (void);
// 0x000001DD System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.IDisposable.Dispose()
extern void U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_mFCA12467EBE6761B70E1E53B3C4A840AF292F943 (void);
// 0x000001DE System.Boolean Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::MoveNext()
extern void U3CDeflateCoroutineU3Ed__17_MoveNext_m08BE00565057C7244AF5B0F58AE8F00CBB46D80B (void);
// 0x000001DF System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C6CC0497CBBFA4475663EABF571C620908EE095 (void);
// 0x000001E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.IEnumerator.Reset()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m95CFD6D6D284C2903E685D5BFF75638564042683 (void);
// 0x000001E1 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_mAD2FAE910FD557443AFE0E15488E50AF6CA1A7C5 (void);
// 0x000001E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::.ctor(System.Int32)
extern void U3CInflateCoroutineU3Ed__18__ctor_m12D0CEBD462022A3EC020D476F2F86769928A257 (void);
// 0x000001E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.IDisposable.Dispose()
extern void U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_mFC8253327295702721A20B6545F95CF3BB337974 (void);
// 0x000001E4 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::MoveNext()
extern void U3CInflateCoroutineU3Ed__18_MoveNext_mB98E20FF4A5938A765AD1CCB0E7E4A37E685C3DF (void);
// 0x000001E5 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E4B6452DAA034D58A0F8BE4B5AB52D8191B084D (void);
// 0x000001E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_m2D65C6C7F8392C468C35F5054DF01AF0F916C3F1 (void);
// 0x000001E7 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_mBD973DB790EE2644898B112F81BEB648D33F3EC9 (void);
// 0x000001E8 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabLarge()
extern void DialogExampleController_get_DialogPrefabLarge_mDB86B8120B06D2EB8A7F4F48C14AAABE00D807D4 (void);
// 0x000001E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabLarge(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabLarge_mB896606829DAC0F184B850C2676DA2ACC7ACEF2E (void);
// 0x000001EA UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabMedium()
extern void DialogExampleController_get_DialogPrefabMedium_m63245472CCF878280A7C9B4BC7F59512E227711B (void);
// 0x000001EB System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabMedium(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabMedium_m7D86E5FCE17FE4B50B3BC9C521784FFACB8FF5BC (void);
// 0x000001EC UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabSmall()
extern void DialogExampleController_get_DialogPrefabSmall_mD7AC4C762F1C70ACABE36E07BBEF87F03D0F51B1 (void);
// 0x000001ED System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabSmall(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabSmall_m66E13D24F52C672ADAEDFFA8FC355CFF5DD268FE (void);
// 0x000001EE System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogLarge()
extern void DialogExampleController_OpenConfirmationDialogLarge_m32B34312C88F143A9C0601D520F2A9884E661E24 (void);
// 0x000001EF System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogLarge()
extern void DialogExampleController_OpenChoiceDialogLarge_m199DF164ABF6B47C96EB6883FA97961208D9308B (void);
// 0x000001F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogMedium()
extern void DialogExampleController_OpenConfirmationDialogMedium_m3FDEDB6E1CFAB8D85FB08D07E2467A5ED08A9DD8 (void);
// 0x000001F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogMedium()
extern void DialogExampleController_OpenChoiceDialogMedium_mB24EE11A661367692654F1C799DCFD507A3E2253 (void);
// 0x000001F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogSmall()
extern void DialogExampleController_OpenConfirmationDialogSmall_m7F576C35E0CF36914C7863728E3F2C10E062077E (void);
// 0x000001F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogSmall()
extern void DialogExampleController_OpenChoiceDialogSmall_m2B360094918BC60C0DB3498CE354E172F6D0E5AE (void);
// 0x000001F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OnClosedDialogEvent(Microsoft.MixedReality.Toolkit.UI.DialogResult)
extern void DialogExampleController_OnClosedDialogEvent_m0CDAC35415EEB0E0B16E12CB070FC02AD90E1E67 (void);
// 0x000001F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::.ctor()
extern void DialogExampleController__ctor_m02DEAEF5905B7D243CFEE40C6D25C654831734AC (void);
// 0x000001F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Awake()
extern void BoundaryVisualizationDemo_Awake_mADF2C333750B0965A7C0E530803818305B3C27AB (void);
// 0x000001F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Start()
extern void BoundaryVisualizationDemo_Start_mF2856E7037862C0E0074967C845A7CA5800B1C03 (void);
// 0x000001F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Update()
extern void BoundaryVisualizationDemo_Update_mF2C8117468A4E28EE51EC3777EA0FE0004018783 (void);
// 0x000001F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnEnable()
extern void BoundaryVisualizationDemo_OnEnable_m6C14109111EBFCD295E5A123FE40A8E8C34ABC26 (void);
// 0x000001FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnDisable()
extern void BoundaryVisualizationDemo_OnDisable_m102AF5706F009FFF389AB0E0839265E6CF03424F (void);
// 0x000001FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnBoundaryVisualizationChanged(Microsoft.MixedReality.Toolkit.Boundary.BoundaryEventData)
extern void BoundaryVisualizationDemo_OnBoundaryVisualizationChanged_m09A05D153DE8DF4957C740438360566A9791DBB9 (void);
// 0x000001FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::AddMarkers()
extern void BoundaryVisualizationDemo_AddMarkers_mC6C2F70AC796A12C938B38212C97BE9EDA496453 (void);
// 0x000001FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::.ctor()
extern void BoundaryVisualizationDemo__ctor_m60F00512A5599ACBA2666128B81FA3F1AD10092F (void);
// 0x000001FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DebugTextOutput::SetTextWithTimestamp(System.String)
extern void DebugTextOutput_SetTextWithTimestamp_mDF010EB22994E08515BD0D7168C5FD8A2A79D6C0 (void);
// 0x000001FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DebugTextOutput::.ctor()
extern void DebugTextOutput__ctor_mBA36C4F1E296266F1F198C5A87A3F2EA9AE9E201 (void);
// 0x00000200 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Awake()
extern void DemoTouchButton_Awake_m1AF45B8901FCF6F00A529AEFCAE5506AE7555E02 (void);
// 0x00000201 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m84EAE1A02F16D7B72C51B5CD236826E8839104BC (void);
// 0x00000202 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mC005178DA61C7FE367B2962D91B018E38C0DFB08 (void);
// 0x00000203 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m18B30B55011CD005213C0317A3180051D2D6A26C (void);
// 0x00000204 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m1A0CD1D270C21807A0F2910702FB9D7D18B1BB39 (void);
// 0x00000205 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::.ctor()
extern void DemoTouchButton__ctor_m8C7E042730F36A15DCA22444DC001BC632B961CF (void);
// 0x00000206 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Start()
extern void HandInteractionTouch_Start_mEA26CFA5F9C0670533A9D81F9599ACAC2E884FBF (void);
// 0x00000207 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchCompleted_m3D6DF00714DBC2BA327BD969D6E2AA2655AAD43A (void);
// 0x00000208 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchStarted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchStarted_m5CBDAF9E02F3D13A8138610576D56581B02624B0 (void);
// 0x00000209 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_m2566E12443FEB048F8BD99C6B8FE9366834D5FCB (void);
// 0x0000020A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::.ctor()
extern void HandInteractionTouch__ctor_m95795EEB965D94E5EE65E4F18F8D1ADE174F6E10 (void);
// 0x0000020B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouchRotate::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouchRotate_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_m5635032533A00F90E61210644F3CFBEA14F4BA9C (void);
// 0x0000020C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouchRotate::.ctor()
extern void HandInteractionTouchRotate__ctor_m04F2A1112ABDEFB18CC1D8CB81232B06D7AD8622 (void);
// 0x0000020D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LaunchUri::Launch(System.String)
extern void LaunchUri_Launch_mC659D1BDD2E134A84AA1791D4D44C87A2A901962 (void);
// 0x0000020E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LaunchUri::.ctor()
extern void LaunchUri__ctor_m32487E2888A148FCAD613C50117270B1293E3431 (void);
// 0x0000020F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LeapCoreAssetsDetector::Start()
extern void LeapCoreAssetsDetector_Start_mC7AE99208F21C227403657D0886ACA11D32C2732 (void);
// 0x00000210 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LeapCoreAssetsDetector::.ctor()
extern void LeapCoreAssetsDetector__ctor_m4AEEC0EBF67469344098FF87A216AC35B67DCEC5 (void);
// 0x00000211 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::ChangeTrackedTargetTypeHead()
extern void SolverTrackedTargetType_ChangeTrackedTargetTypeHead_m2B0CD7B51F5FA879B77DCDD61DEB5D9C63E7A037 (void);
// 0x00000212 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::ChangeTrackedTargetTypeHandJoint()
extern void SolverTrackedTargetType_ChangeTrackedTargetTypeHandJoint_m4B00304A18536F7663D2657FC0D960D1100754B5 (void);
// 0x00000213 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::.ctor()
extern void SolverTrackedTargetType__ctor_m8CC435EED303E982EE5530B33180A75965B19D15 (void);
// 0x00000214 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::Start()
extern void TetheredPlacement_Start_m6CACD480BBE24C0EB87A6E89715F20893DD5AAE8 (void);
// 0x00000215 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::LateUpdate()
extern void TetheredPlacement_LateUpdate_m2B4BE1BD4E712C73D57EED7018B72DB1CC99B124 (void);
// 0x00000216 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::LockSpawnPoint()
extern void TetheredPlacement_LockSpawnPoint_mA32ABA5BBD3753BB351C15CF983B486EC5518236 (void);
// 0x00000217 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::.ctor()
extern void TetheredPlacement__ctor_mDD6A080D7A8DF6DC83BD6C7C89CF27CF289CFE09 (void);
// 0x00000218 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::Awake()
extern void ToggleBoundingBox_Awake_m20C0BB56F14913BCB69B995A4A2D6E8508802224 (void);
// 0x00000219 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::ToggleBoundingBoxActiveState()
extern void ToggleBoundingBox_ToggleBoundingBoxActiveState_m4CA2880B8D2C11ED0EFDD0C9F38ACF6D70B35977 (void);
// 0x0000021A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::.ctor()
extern void ToggleBoundingBox__ctor_mF49E02C24C50FB0472BBBD80B9A6E247BC036D11 (void);
// 0x0000021B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::Start()
extern void DisablePointersExample_Start_m3E31DF9B8F38D17AF1AEDEC7B1E05AD3C198DDE9 (void);
// 0x0000021C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::ResetExample()
extern void DisablePointersExample_ResetExample_m9E6E34DCE84AC4EFD9A17098B529A5DCCC1AC529 (void);
// 0x0000021D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::Update()
extern void DisablePointersExample_Update_m13BB16503BC11195A9CF914C7CE6FFAFC58BA763 (void);
// 0x0000021E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::SetToggleHelper(Microsoft.MixedReality.Toolkit.UI.Interactable,System.String,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
// 0x0000021F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::.ctor()
extern void DisablePointersExample__ctor_m7E6F097FC701D4F2E642999EEEF1988A8DDB88C7 (void);
// 0x00000220 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Rotator::Rotate()
extern void Rotator_Rotate_mCAACB33C1CA545A2E38FCB30EB19EC90678C8CB4 (void);
// 0x00000221 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Rotator::.ctor()
extern void Rotator__ctor_m4F79EFA6E5945343F8F4DA70EEBAF97E267FBE03 (void);
// 0x00000222 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::Update()
extern void InputDataExample_Update_m0F1B0CDFAB7DAF0C26D2A1726BEC5D4857CE1ECD (void);
// 0x00000223 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::Start()
extern void InputDataExample_Start_m5EAC869FDF9D300F62E609A3FA495879F17FF3BC (void);
// 0x00000224 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::.ctor()
extern void InputDataExample__ctor_m883220AABF5DABB410458C4A1A4DB6279C82FB14 (void);
// 0x00000225 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::SetIsDataAvailable(System.Boolean)
extern void InputDataExampleGizmo_SetIsDataAvailable_m095B653F634FB98B0D54A2B09F36F010C42BD730 (void);
// 0x00000226 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::Update()
extern void InputDataExampleGizmo_Update_m66CEFC1AE337DD44EB4E64D2255FCA3EF4ED206D (void);
// 0x00000227 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::.ctor()
extern void InputDataExampleGizmo__ctor_m073E47C354EB42BCABFCC24E2310A21118A685A1 (void);
// 0x00000228 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SpawnOnPointerEvent::Spawn(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void SpawnOnPointerEvent_Spawn_mEE71AF33FC68D78B16CC5214545522B34B69694E (void);
// 0x00000229 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SpawnOnPointerEvent::.ctor()
extern void SpawnOnPointerEvent__ctor_mC540FBBA46ADB0EC808F31F01AAA48AE3E3C8491 (void);
// 0x0000022A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnEnable()
extern void PrimaryPointerHandlerExample_OnEnable_m6FDDA02E110E76093918206D568068FB5BE436F8 (void);
// 0x0000022B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void PrimaryPointerHandlerExample_OnPrimaryPointerChanged_m379D9C08E09FA6DE5B20CF5FBB4FAE6BECA5985B (void);
// 0x0000022C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnDisable()
extern void PrimaryPointerHandlerExample_OnDisable_m90A94E8D78E3DBC461BBDC45D6438E3366D811C2 (void);
// 0x0000022D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::.ctor()
extern void PrimaryPointerHandlerExample__ctor_m97D53B12F721E0EAFA962463B8A1AE9A64E7160F (void);
// 0x0000022E Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_ScrollView()
extern void ScrollableListPopulator_get_ScrollView_m042238B5B001927FDD8C67EB26015C9E641665A7 (void);
// 0x0000022F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_ScrollView(Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection)
extern void ScrollableListPopulator_set_ScrollView_m7FA0A4D292E3A1AF78A9D3B4A76C7AF2791F2675 (void);
// 0x00000230 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_DynamicItem()
extern void ScrollableListPopulator_get_DynamicItem_mD96CFFBB028D28F379C71B65514B96E259B431BB (void);
// 0x00000231 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_DynamicItem(UnityEngine.GameObject)
extern void ScrollableListPopulator_set_DynamicItem_m1C76D9E63F971C530F4DF29081D801F553C8BFA1 (void);
// 0x00000232 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_NumItems()
extern void ScrollableListPopulator_get_NumItems_m73437AA1C4B5CC09418930DB87CD6A5D92C94B17 (void);
// 0x00000233 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_NumItems(System.Int32)
extern void ScrollableListPopulator_set_NumItems_m33015E2D6684E547C9C90ACC22F582EAE86CA93E (void);
// 0x00000234 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_LazyLoad()
extern void ScrollableListPopulator_get_LazyLoad_m055FEF55C1ADAFC1E12A0AF4EAA3CAEA321B9003 (void);
// 0x00000235 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_LazyLoad(System.Boolean)
extern void ScrollableListPopulator_set_LazyLoad_m9463C287A6C9BAF30FD266F116D9CE7FFD17F9BA (void);
// 0x00000236 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_ItemsPerFrame()
extern void ScrollableListPopulator_get_ItemsPerFrame_mDBB98A7F2205F3E4B2DB72CFE053F23335FD94FE (void);
// 0x00000237 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_ItemsPerFrame(System.Int32)
extern void ScrollableListPopulator_set_ItemsPerFrame_mE58DDF07ECB10546F4D464FE256EDEC7CA5D0BAA (void);
// 0x00000238 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_Loader()
extern void ScrollableListPopulator_get_Loader_m8D4D089B552F12567D862F7851561143F2B3963C (void);
// 0x00000239 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_Loader(UnityEngine.GameObject)
extern void ScrollableListPopulator_set_Loader_mD782F54965CEDEA3414FF58DFFAE272977E8CFCE (void);
// 0x0000023A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::OnEnable()
extern void ScrollableListPopulator_OnEnable_mA5F8A1BAE41B1060D05E53498E872315A88F2131 (void);
// 0x0000023B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::MakeScrollingList()
extern void ScrollableListPopulator_MakeScrollingList_mAD71C9D7BF987ED7B6D689E54B577243BC63F532 (void);
// 0x0000023C System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::UpdateListOverTime(UnityEngine.GameObject,System.Int32)
extern void ScrollableListPopulator_UpdateListOverTime_mF520005989CE5A4FB04B9A18072DB1A7A32680F2 (void);
// 0x0000023D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::MakeItem(UnityEngine.GameObject)
extern void ScrollableListPopulator_MakeItem_mDB70A887590A8252C685F2230B8F7B721F781E49 (void);
// 0x0000023E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::.ctor()
extern void ScrollableListPopulator__ctor_m504433E97D003B122568DF23225038D109ECCD84 (void);
// 0x0000023F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::.ctor(System.Int32)
extern void U3CUpdateListOverTimeU3Ed__33__ctor_mD6DC4198EA19B70B22CFB394251F3584E4638F88 (void);
// 0x00000240 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.IDisposable.Dispose()
extern void U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_m96767201EB44DE2678760CEC09A37615EDF9C67D (void);
// 0x00000241 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::MoveNext()
extern void U3CUpdateListOverTimeU3Ed__33_MoveNext_m9F34E80A591109FCBC9F2BBCF24AB9CD6040FB05 (void);
// 0x00000242 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50FE87AB5366443CF64C7D0D0170CCD6F07D8695 (void);
// 0x00000243 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.IEnumerator.Reset()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_mCB10BA6CFE80B992125294E8B2970BE7214CD81C (void);
// 0x00000244 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_mE153FE28CE0FD6B843D064D1D40662D94E46A417 (void);
// 0x00000245 Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::get_ScrollView()
extern void ScrollablePagination_get_ScrollView_m37EBC4CA0A2B6B1306B474F6AA7FE6310480FF07 (void);
// 0x00000246 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::set_ScrollView(Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection)
extern void ScrollablePagination_set_ScrollView_m7BA82F6CE9207BA4F003FA07F246D60A4E62CEF3 (void);
// 0x00000247 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::ScrollByTier(System.Int32)
extern void ScrollablePagination_ScrollByTier_m265EDEA535DFAA8FC0FF4F864586E0F6C7D538C2 (void);
// 0x00000248 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::.ctor()
extern void ScrollablePagination__ctor_m368543C1E56F5ACF4334F4CB2A5E4D0600A34475 (void);
// 0x00000249 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::Start()
extern void HideTapToPlaceLabel_Start_mE89B6110A5F19CD9E9C64C9860F2E300E57AAE38 (void);
// 0x0000024A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::AddTapToPlaceListeners()
extern void HideTapToPlaceLabel_AddTapToPlaceListeners_m2A5A470FA7A269DF07BAB1CEB3D6EC0EF59446C9 (void);
// 0x0000024B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::.ctor()
extern void HideTapToPlaceLabel__ctor_m8B8EA7B412E3EA9C8D9E297306273DA51E20AE7A (void);
// 0x0000024C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::<AddTapToPlaceListeners>b__3_0()
extern void HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_mF9C234F125FD4A2CBF763DFFE1B15B0E90023749 (void);
// 0x0000024D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::<AddTapToPlaceListeners>b__3_1()
extern void HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m02F105131E5262BB8991FE6D7F077ABD81F89528 (void);
// 0x0000024E Microsoft.MixedReality.Toolkit.Utilities.TrackedObjectType Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::get_TrackedType()
extern void SolverExampleManager_get_TrackedType_m4DABEE0D8106E2502E426C0367F15A628B5803B5 (void);
// 0x0000024F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::set_TrackedType(Microsoft.MixedReality.Toolkit.Utilities.TrackedObjectType)
extern void SolverExampleManager_set_TrackedType_mD8D843F03F2C7064F77F8C7220A84FFFD30336DF (void);
// 0x00000250 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::Awake()
extern void SolverExampleManager_Awake_m6087680ED6CCA8589EED17BB1DF368424AB28702 (void);
// 0x00000251 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedHead()
extern void SolverExampleManager_SetTrackedHead_m82AAA89811D9E07D26FE7244466E4A26D7800F60 (void);
// 0x00000252 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedController()
extern void SolverExampleManager_SetTrackedController_m9687D772AFE232B76A9A5E37E71AD9C5510C128B (void);
// 0x00000253 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedHands()
extern void SolverExampleManager_SetTrackedHands_m363BDD043A03F58F728CD522EFFC3E39F7B58DEA (void);
// 0x00000254 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedCustom()
extern void SolverExampleManager_SetTrackedCustom_mC7023779CF83B9858CF3C41A2D853695B449D53F (void);
// 0x00000255 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetRadialView()
extern void SolverExampleManager_SetRadialView_m9964B0FEA35B75F55900E06EA434238280B4E406 (void);
// 0x00000256 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetOrbital()
extern void SolverExampleManager_SetOrbital_m4DD257DA9CE338F170691B77C9D7FD60F339A4B3 (void);
// 0x00000257 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetSurfaceMagnetism()
extern void SolverExampleManager_SetSurfaceMagnetism_m225A0CA340934D421A6830A0BF89E2155AAA199D (void);
// 0x00000258 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::AddSolver()
// 0x00000259 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::RefreshSolverHandler()
extern void SolverExampleManager_RefreshSolverHandler_m7593ADE5877D611339AB4BF944BF6DB5B78E8D99 (void);
// 0x0000025A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::DestroySolver()
extern void SolverExampleManager_DestroySolver_m982039E6B7C5EF861AB2B140ABD46BEFB73106DE (void);
// 0x0000025B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::.ctor()
extern void SolverExampleManager__ctor_mF5C0C58F5C1D6876E4A4F9BDBFA78CE295E29BBD (void);
// 0x0000025C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ClearSpatialObservations::ToggleObservers()
extern void ClearSpatialObservations_ToggleObservers_m9FC49F6F59044E6C31118F6A477DECC3EE60C7C8 (void);
// 0x0000025D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ClearSpatialObservations::.ctor()
extern void ClearSpatialObservations__ctor_m0D7B5227EEC0F97CDD8E1F4F02F7A457123734B5 (void);
// 0x0000025E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ShowPlaneFindingInstructions::Start()
extern void ShowPlaneFindingInstructions_Start_m5F47C24A1CEAFACC9A1B9F5978771509FADA294D (void);
// 0x0000025F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ShowPlaneFindingInstructions::.ctor()
extern void ShowPlaneFindingInstructions__ctor_m9D9CAFC3AC7EC35325562355292941860F81BC8B (void);
// 0x00000260 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnEnable()
extern void BoundingBoxExampleTest_OnEnable_m9295F0AD2432A82F8F1789D1255C5F6A67EE9F1C (void);
// 0x00000261 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnDisable()
extern void BoundingBoxExampleTest_OnDisable_m02195DEA798C73BFE83917C7A3FD39444763071B (void);
// 0x00000262 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::Start()
extern void BoundingBoxExampleTest_Start_mF3A4F299620D7F8FCE98348C4C80609331E71587 (void);
// 0x00000263 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::SetStatus(System.String)
extern void BoundingBoxExampleTest_SetStatus_mD8CA641DA51CFC1BE8F4D7210B4D909E43B437D0 (void);
// 0x00000264 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::Sequence()
extern void BoundingBoxExampleTest_Sequence_m2815993AD477DD94A3F3022481E7071C91A7BFAC (void);
// 0x00000265 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::DebugDrawObjectBounds(UnityEngine.Bounds)
extern void BoundingBoxExampleTest_DebugDrawObjectBounds_m63B2C326983FD65B4326BB3DB39C4CA871D75321 (void);
// 0x00000266 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::WaitForSpeechCommand()
extern void BoundingBoxExampleTest_WaitForSpeechCommand_mCCD711FC1BABDFF3BAE9DEC26BC51B2995A3B110 (void);
// 0x00000267 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void BoundingBoxExampleTest_OnSpeechKeywordRecognized_m7F093F58ECF69047FE0783DFE1AADF29037D96A0 (void);
// 0x00000268 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::.ctor()
extern void BoundingBoxExampleTest__ctor_m0EF5184A995D83B82467C2EE19B858808659C502 (void);
// 0x00000269 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::<Sequence>b__12_0(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_m5401FB3A85CC9416A3A0F53641AE1710458D7138 (void);
// 0x0000026A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::<Sequence>b__12_1(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_mD5787D43CEEAC5B444A4014F7A594ECFCF841A4A (void);
// 0x0000026B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::.ctor(System.Int32)
extern void U3CSequenceU3Ed__12__ctor_mA663B54F4980B4C13C28ECB85AA899FC07F85317 (void);
// 0x0000026C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.IDisposable.Dispose()
extern void U3CSequenceU3Ed__12_System_IDisposable_Dispose_mFC94D94B07EF3F2C1247D365E0D84769024ED144 (void);
// 0x0000026D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::MoveNext()
extern void U3CSequenceU3Ed__12_MoveNext_m68C6D02B10634D65142180FD6B8F3D47F414193E (void);
// 0x0000026E System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B985A513FD5EFE77C262DC64681E5E2C4B0AC4B (void);
// 0x0000026F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m5FBFDB842A09B18EA25BB9D3FC0E50B76D987347 (void);
// 0x00000270 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mB9BB258F4FE7467F169CB5DA57A9705EB24076E8 (void);
// 0x00000271 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::.ctor(System.Int32)
extern void U3CWaitForSpeechCommandU3Ed__14__ctor_m19EBB56BB6F7A447FAD350D93775EEE2212125D8 (void);
// 0x00000272 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.IDisposable.Dispose()
extern void U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_mC2F9F1E1C5F34227ED0B6D32BFF5D071A398D7AC (void);
// 0x00000273 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::MoveNext()
extern void U3CWaitForSpeechCommandU3Ed__14_MoveNext_m45BDD1E7A696EB2A0767B31AA28D9C171F6847D9 (void);
// 0x00000274 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28F4EB16558BB5E01DAFF2FDAE29BEFCC976CD83 (void);
// 0x00000275 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m1482C0A7977C2AD6641EE11C52B6096C7F3C2D0E (void);
// 0x00000276 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m38FCB3DA2EFBA5A05689FD0DDA9FEC89210F01BB (void);
// 0x00000277 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnEnable()
extern void BoundsControlRuntimeExample_OnEnable_m32043787E11B8ABCBBEFF1E9404451B69D7C93FD (void);
// 0x00000278 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnDisable()
extern void BoundsControlRuntimeExample_OnDisable_m07B769690C8482614E64D0FCF8225F7DE2767D3C (void);
// 0x00000279 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::Start()
extern void BoundsControlRuntimeExample_Start_m7E51A67593A7B5E72815A22E551473B1234E9B34 (void);
// 0x0000027A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::SetStatus(System.String)
extern void BoundsControlRuntimeExample_SetStatus_m3DC9EF2E593D96E794BE9786018B6EDAA45C20D0 (void);
// 0x0000027B System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::Sequence()
extern void BoundsControlRuntimeExample_Sequence_m5D3B4D8884CAA38FDD9CB04C6AAE923ECDC57F55 (void);
// 0x0000027C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::DebugDrawObjectBounds(UnityEngine.Bounds)
extern void BoundsControlRuntimeExample_DebugDrawObjectBounds_m39B257E66BFCA6B53BBCA143E00A44C671C9B390 (void);
// 0x0000027D System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::WaitForSpeechCommand()
extern void BoundsControlRuntimeExample_WaitForSpeechCommand_m92D8C8953D2F8B338F8F4AC8F835C23CD91382BC (void);
// 0x0000027E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void BoundsControlRuntimeExample_OnSpeechKeywordRecognized_m951D513FE1437B071A9C868CE0BF181101208BAF (void);
// 0x0000027F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::.ctor()
extern void BoundsControlRuntimeExample__ctor_m6CB3D841CFC8900DA38BAC8E41DA9178F907E4F8 (void);
// 0x00000280 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::<Sequence>b__12_0(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_mD25E64FA74BA6AA310D821E423EF22DF0AA85BC7 (void);
// 0x00000281 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::<Sequence>b__12_1(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_m742CEECC7C111C331DE17B3599E20CF5540B2A58 (void);
// 0x00000282 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::.ctor(System.Int32)
extern void U3CSequenceU3Ed__12__ctor_m55B247941B692847AA4CCC0ED16980FC2D03B94C (void);
// 0x00000283 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.IDisposable.Dispose()
extern void U3CSequenceU3Ed__12_System_IDisposable_Dispose_mD723CAC7F92786F4DAF791EB47907BF11835D2B9 (void);
// 0x00000284 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::MoveNext()
extern void U3CSequenceU3Ed__12_MoveNext_mFA3FBE98FF0A6A0C9FDB3CBD283DFA58F749409C (void);
// 0x00000285 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m660BA921571B846752FE727B7A59A91889CDB038 (void);
// 0x00000286 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m6E69902A58F67293E421398F6D3DD7983F2B50A9 (void);
// 0x00000287 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_m44C927A6B3A701C153035A23FB7A42B60814A648 (void);
// 0x00000288 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::.ctor(System.Int32)
extern void U3CWaitForSpeechCommandU3Ed__14__ctor_m7EC3A4ABDBCACE6D9B0E5916C6D6A6F9FE2DA7A1 (void);
// 0x00000289 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.IDisposable.Dispose()
extern void U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_m30FFC6585ADBF2325EAD45119CF8D3010F6A6DBE (void);
// 0x0000028A System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::MoveNext()
extern void U3CWaitForSpeechCommandU3Ed__14_MoveNext_m084B1D7816E8485A5382284DC422D017CC1A49E8 (void);
// 0x0000028B System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3332944F5E4DB1D4F67ABC6396679665BF2074AA (void);
// 0x0000028C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m23D9D167B9C254D9E0EDE56BD05605CB1A1EFC8E (void);
// 0x0000028D System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m2749F8A7420AC313C2106276C2E3B1EE028354A0 (void);
// 0x0000028E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::NextLayout()
extern void GridObjectLayoutControl_NextLayout_m23BB04DC59B670FB446E11A42213A88C4B12694B (void);
// 0x0000028F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::PreviousLayout()
extern void GridObjectLayoutControl_PreviousLayout_m24F8056EEF08DA248D846AA1AF83C9D5C7D8F73F (void);
// 0x00000290 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::RunTest()
extern void GridObjectLayoutControl_RunTest_m4D7483AE0ADD69566A77D63C5E84B7AB5D38ABEE (void);
// 0x00000291 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::Start()
extern void GridObjectLayoutControl_Start_mF244041C4BC701356A043DE288B5293FE4236A14 (void);
// 0x00000292 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::UpdateUI()
extern void GridObjectLayoutControl_UpdateUI_m63B641F14890EE88A96E81C708F94388C513533E (void);
// 0x00000293 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::TestAnchors()
extern void GridObjectLayoutControl_TestAnchors_mD647505E82D2241C5F965A41A1B445A013D7D3B3 (void);
// 0x00000294 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::PrintGrid(Microsoft.MixedReality.Toolkit.Utilities.GridObjectCollection,System.String,System.String,System.Text.StringBuilder)
extern void GridObjectLayoutControl_PrintGrid_m4F6EE3C292819EB66185ACE0210CCFCF4F7EAAA2 (void);
// 0x00000295 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::.ctor()
extern void GridObjectLayoutControl__ctor_mF0E73D9A1FB611F7954EABD5C7E31FC53A6F313E (void);
// 0x00000296 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::.ctor(System.Int32)
extern void U3CTestAnchorsU3Ed__7__ctor_m3B3F2D3E9732092D37D550372E6A197CD5B82428 (void);
// 0x00000297 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.IDisposable.Dispose()
extern void U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m34BE0906298167ED5412589AF1475A38E94545F8 (void);
// 0x00000298 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::MoveNext()
extern void U3CTestAnchorsU3Ed__7_MoveNext_m59D0733A17890C0A4FAFC8BB78529243D345A531 (void);
// 0x00000299 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::<>m__Finally1()
extern void U3CTestAnchorsU3Ed__7_U3CU3Em__Finally1_m573F2C47858941BBB4E65BFC96F53E47800B953B (void);
// 0x0000029A System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23425D4DA6043B0BACFB553ECE773D481B7F6F03 (void);
// 0x0000029B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_m122EFF9A2DDD3862957246285AD5706F44F99D63 (void);
// 0x0000029C System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_mD4E8A7309758EF3897AC9EB48789686F179BA575 (void);
// 0x0000029D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::Start()
extern void ChangeManipulation_Start_mD6003C257B8C32C1F10208C2D2C3524550F388AE (void);
// 0x0000029E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::Update()
extern void ChangeManipulation_Update_mB6CD2E3249852087DD12E5C6F3DEBE2450958F44 (void);
// 0x0000029F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::TryStopManipulation()
extern void ChangeManipulation_TryStopManipulation_m321451715A9445BA4F35C5E111C52C5C61FD0089 (void);
// 0x000002A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::.ctor()
extern void ChangeManipulation__ctor_mBB2442734228CE7BF6A47B503F66D5F7856292E5 (void);
// 0x000002A1 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_FrontBounds()
extern void ReturnToBounds_get_FrontBounds_m7A47E013948C72481150EEF1AEB6222585121045 (void);
// 0x000002A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_FrontBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_FrontBounds_m5CD7964F5726000F6B3CB5143A0B3C97692C3234 (void);
// 0x000002A3 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_BackBounds()
extern void ReturnToBounds_get_BackBounds_m3CC5E50F406B99E2C3F5D847CCDA293044E0D59C (void);
// 0x000002A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_BackBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_BackBounds_m0C11D4EE3EEEE05D52EC5012E263CB1782A6A3CF (void);
// 0x000002A5 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_LeftBounds()
extern void ReturnToBounds_get_LeftBounds_mF9E268BBA59DA87DCDB531DDF62DB4916D358F2B (void);
// 0x000002A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_LeftBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_LeftBounds_m428D0FD67CC5433434830EAE5F354C5450B45C9A (void);
// 0x000002A7 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_RightBounds()
extern void ReturnToBounds_get_RightBounds_m68B51E70BAB055CC8337EC329E7E311704629370 (void);
// 0x000002A8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_RightBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_RightBounds_m05152BFD550D63A25BE2AB4F619EFF96883801B6 (void);
// 0x000002A9 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_BottomBounds()
extern void ReturnToBounds_get_BottomBounds_m835748A162A673F3D99801C4394F3E529F9C8A81 (void);
// 0x000002AA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_BottomBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_BottomBounds_m6CF789953842D6403EAB1F4D678AAB487294144F (void);
// 0x000002AB UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_TopBounds()
extern void ReturnToBounds_get_TopBounds_mD66F106483D03BA32D6F66C92B5C729E8CB98381 (void);
// 0x000002AC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_TopBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_TopBounds_mF88FD05B07BE1C5B2B8EBC68B0460A6F675190BB (void);
// 0x000002AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::Start()
extern void ReturnToBounds_Start_m75EC0D3A42A356FAA123868E4D2E44FC384D7215 (void);
// 0x000002AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::Update()
extern void ReturnToBounds_Update_m08B29ED9C4F8C510F5C100503CBD0947C0A5B4F8 (void);
// 0x000002AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::.ctor()
extern void ReturnToBounds__ctor_m6296DD3E33E6BA87D261D981EFFBAF0D3AB24ED4 (void);
// 0x000002B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickBar()
extern void ProgressIndicatorDemo_OnClickBar_mE249485554831AF9642197827B5597C69F09CB06 (void);
// 0x000002B1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickRotating()
extern void ProgressIndicatorDemo_OnClickRotating_m9EC74D04852B93C4991F3891E4E2032012BF8043 (void);
// 0x000002B2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickOrbs()
extern void ProgressIndicatorDemo_OnClickOrbs_m2CC5B22649A43CB5BB3A684ED0E1A260C95D4B17 (void);
// 0x000002B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::HandleButtonClick(Microsoft.MixedReality.Toolkit.UI.IProgressIndicator)
extern void ProgressIndicatorDemo_HandleButtonClick_mB8DADCD9DA6F66028B8C789D2D22014B4D02556E (void);
// 0x000002B4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnEnable()
extern void ProgressIndicatorDemo_OnEnable_mEC7E4C409CD5992D87A70E61FF8B0D5F4EEA9864 (void);
// 0x000002B5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::Update()
extern void ProgressIndicatorDemo_Update_m4AB1D7597BD053CFD0B7525FC6DC1A6B8E4B9251 (void);
// 0x000002B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OpenProgressIndicator(Microsoft.MixedReality.Toolkit.UI.IProgressIndicator)
extern void ProgressIndicatorDemo_OpenProgressIndicator_mEA02AB6401CCE2F4B69BB6E25ADCD75E0F67ADB2 (void);
// 0x000002B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::.ctor()
extern void ProgressIndicatorDemo__ctor_m1BB04733ABF3D2B03BFB47B2208505F73FDCAAD7 (void);
// 0x000002B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<HandleButtonClick>d__14::MoveNext()
extern void U3CHandleButtonClickU3Ed__14_MoveNext_mF720161ECDEF41103FA127C6530AD8437E8EA6A6 (void);
// 0x000002B9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<HandleButtonClick>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleButtonClickU3Ed__14_SetStateMachine_m5B82CD99AA2F16295DDEC5460785119016DA29FD (void);
// 0x000002BA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<OpenProgressIndicator>d__17::MoveNext()
extern void U3COpenProgressIndicatorU3Ed__17_MoveNext_m1BD23385324B99F28EA5A32496561FA695B5F61A (void);
// 0x000002BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<OpenProgressIndicator>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COpenProgressIndicatorU3Ed__17_SetStateMachine_mA9B346FEC4EADA24B0E0784703212DFFD1215ACF (void);
// 0x000002BC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SliderLunarLander::OnSliderUpdated(Microsoft.MixedReality.Toolkit.UI.SliderEventData)
extern void SliderLunarLander_OnSliderUpdated_m5EF016138B6DC527F44B05F56FBF57A06BE4B5BF (void);
// 0x000002BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SliderLunarLander::.ctor()
extern void SliderLunarLander__ctor_m16C9648E3A00E0CD8CEDCA1DB4119FB7C6F48026 (void);
// 0x000002BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.MixedRealityCapabilityDemo::Start()
extern void MixedRealityCapabilityDemo_Start_m4C359D1DEEB760A211B692325FAC59EA77879A8F (void);
// 0x000002BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.MixedRealityCapabilityDemo::.ctor()
extern void MixedRealityCapabilityDemo__ctor_m8F7A48E8A9FC779C6899C89A8198ADF3F5A15E7B (void);
// 0x000002C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::Update()
extern void ReadingModeSceneBehavior_Update_m2565D68981EF0DA269C19A09A9885EF1B907559A (void);
// 0x000002C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::EnableReadingMode()
extern void ReadingModeSceneBehavior_EnableReadingMode_mC34A0D03AA61CB6DB7A43831361FA3BE247CD051 (void);
// 0x000002C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::DisableReadingMode()
extern void ReadingModeSceneBehavior_DisableReadingMode_m02077800F845467CEE761AAD7457361268D9FA2F (void);
// 0x000002C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::.ctor()
extern void ReadingModeSceneBehavior__ctor_m384062F15608B93A080381545A638FFE62ED6C0E (void);
// 0x000002C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Awake()
extern void ColorTap_Awake_m5B0748BC43CB2EB1A25F9B91961C2A3411A17DFE (void);
// 0x000002C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusEnter(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m75AAB5DB726346536886AA399F0F41770808F465 (void);
// 0x000002C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusExit(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m9343BF91A62EAB87D67344D703054B93247E26CD (void);
// 0x000002C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m6577294DB1F606E9C531688D70DFEBD18D2B34FD (void);
// 0x000002C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m39879CF0E9D84DC65C5DBBB913B6849F487F17F3 (void);
// 0x000002C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m2907579CFD6051BA5FE627A8500B8BBE11D5CAA4 (void);
// 0x000002CA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m7724C137D555DA8BDE49890ED09645FBCEFE7EA8 (void);
// 0x000002CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::.ctor()
extern void ColorTap__ctor_m3B0B18CE2B657CAA5E91505CAE68AAA339F725AE (void);
// 0x000002CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGazeGazeProvider::Update()
extern void FollowEyeGazeGazeProvider_Update_mB689FFA9AA489E4A7F3A53B977562DE423BA31BB (void);
// 0x000002CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGazeGazeProvider::.ctor()
extern void FollowEyeGazeGazeProvider__ctor_m702A0A5EF6D725D4AA103018B5FF96A03369E6CC (void);
// 0x000002CE Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_EyeSaccadeProvider()
extern void PanZoomBase_get_EyeSaccadeProvider_m811BB49108CA3819FB0F6FFF76148B3C33EEC455 (void);
// 0x000002CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Initialize()
// 0x000002D0 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ComputePanSpeed(System.Single,System.Single,System.Single)
// 0x000002D1 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomDir(System.Boolean)
// 0x000002D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomIn()
// 0x000002D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOut()
// 0x000002D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdatePanZoom()
// 0x000002D5 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateCursorPosInHitBox()
// 0x000002D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Start()
extern void PanZoomBase_Start_m9E1A94CE9A9DE0DC66689391455028074550284A (void);
// 0x000002D7 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_CustomColliderSizeOnLookAt()
extern void PanZoomBase_get_CustomColliderSizeOnLookAt_mE6219C39F17861C551D4431C3DA809807613ABB6 (void);
// 0x000002D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::AutoPan()
extern void PanZoomBase_AutoPan_mF4225157DFC50748761B21C33894C055ADE2B32A (void);
// 0x000002D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanHorizontally(System.Single)
extern void PanZoomBase_PanHorizontally_mC28F0AD9A7708DE29AC7AF306AAA9594C549FC4E (void);
// 0x000002DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanVertically(System.Single)
extern void PanZoomBase_PanVertically_m4997AC2B8EC987057C66FAA7E33F980A598203D4 (void);
// 0x000002DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::EnableHandZoom()
extern void PanZoomBase_EnableHandZoom_mE8AC9DF3B7B0A5576377B3ABF926D10CC88016FE (void);
// 0x000002DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::DisableHandZoom()
extern void PanZoomBase_DisableHandZoom_mF676B88558C48ED63E369211266850E8DC52B2E2 (void);
// 0x000002DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomStart(System.Boolean)
extern void PanZoomBase_ZoomStart_m60CC810675822C9F3175636C87C84C8F26FF44D1 (void);
// 0x000002DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomInStart()
extern void PanZoomBase_ZoomInStart_m59DB8482763052590B41D1C71063FDE5CE469857 (void);
// 0x000002DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOutStart()
extern void PanZoomBase_ZoomOutStart_m8E0E25B9B8296E8C703CD6E775D30834F2522314 (void);
// 0x000002E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomStop()
extern void PanZoomBase_ZoomStop_m62575FEDCC8B7F8C1FC5ACB5714F46702ACAC3B8 (void);
// 0x000002E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationStart()
extern void PanZoomBase_NavigationStart_mFE247FC7AFA1E31C03BD9D823F65FBD4063175EE (void);
// 0x000002E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationStop()
extern void PanZoomBase_NavigationStop_mC82C14FDAFF691F5F55754AE1FC716094EA023C4 (void);
// 0x000002E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationUpdate(UnityEngine.Vector3)
extern void PanZoomBase_NavigationUpdate_mDC9595D123232E6A6CD134FFCDEDC467F6908CE5 (void);
// 0x000002E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Update()
extern void PanZoomBase_Update_m4DEC4FB6B13010D0042171BA9CAC61326FC762B5 (void);
// 0x000002E5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::SetSkimProofUpdateSpeed(System.Single)
extern void PanZoomBase_SetSkimProofUpdateSpeed_m87E3C4DAF595EE1768A8A7575A8CFDC7D075EDEB (void);
// 0x000002E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ResetNormFixator()
extern void PanZoomBase_ResetNormFixator_m536160EB4A351BD28CC7B1813A39F495A4C27EFC (void);
// 0x000002E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::IncrementNormFixator()
extern void PanZoomBase_IncrementNormFixator_m5C54FAE13E9A82DD0D0B84B667CFEBC00F257C52 (void);
// 0x000002E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ResetScroll_OnSaccade()
extern void PanZoomBase_ResetScroll_OnSaccade_m2B772378EC95AE787C31ECB9606B6A53D70A3975 (void);
// 0x000002E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::LateUpdate()
extern void PanZoomBase_LateUpdate_m4F49BCB26913084A61B0A3616C3EF198268AD800 (void);
// 0x000002EA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::AutomaticGazePanning()
extern void PanZoomBase_AutomaticGazePanning_mE5E8E8DE77A0EB31349F1988D07114CD9B63260B (void);
// 0x000002EB UnityEngine.BoxCollider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_MyCollider()
extern void PanZoomBase_get_MyCollider_mD6AA9AD640EA709EE208933FAE25421FFD043979 (void);
// 0x000002EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::set_MyCollider(UnityEngine.BoxCollider)
extern void PanZoomBase_set_MyCollider_mFD875A8827CECB4B8E8B96C20BB85E468A554CB8 (void);
// 0x000002ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanUpDown(System.Single)
extern void PanZoomBase_PanUpDown_mFBA766EAC54C966F185642F32923E73B1EE1DF18 (void);
// 0x000002EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanLeftRight(System.Single)
extern void PanZoomBase_PanLeftRight_m216BDB263C0735562E809E9C8EDDEB2179656DD8 (void);
// 0x000002EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateZoom()
extern void PanZoomBase_UpdateZoom_mA77F02A03DAA6B6017F5F5A5373F3864AC6F70E2 (void);
// 0x000002F0 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::LimitScaling(UnityEngine.Vector2)
extern void PanZoomBase_LimitScaling_mA79A4B2BEB4FF523475A80C865468E08AFCF2ED3 (void);
// 0x000002F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomIn_Timed()
extern void PanZoomBase_ZoomIn_Timed_mAC6F4E7A38D4E0D08131ACF0023C61412080A341 (void);
// 0x000002F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOut_Timed()
extern void PanZoomBase_ZoomOut_Timed_m98575816334CC9F7BC55B964326A9BC965CC22E8 (void);
// 0x000002F3 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomAndStop(System.Boolean)
extern void PanZoomBase_ZoomAndStop_m3BBADDBDF41664DC7702B155D7F4F2C1F7417A21 (void);
// 0x000002F4 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateValues(T&,T)
// 0x000002F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::StartFocusing()
extern void PanZoomBase_StartFocusing_m357A96792947A2C46EEC402C81EFAAED58016A9B (void);
// 0x000002F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::StopFocusing()
extern void PanZoomBase_StopFocusing_m2D85E400C402C6E750AA3B5D7F07E8721A753A78 (void);
// 0x000002F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m8A735A80147B96EBB033BE84963D2AD7514A4F5C (void);
// 0x000002F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m683370431A263429C1E1CFE19F9C7A0D005494EB (void);
// 0x000002F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m9FA5D7496D5BA0B7DFD0E9074BD990DDD6747BA1 (void);
// 0x000002FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m5F5417379D572CD2DD0F9D7829FEB52BD100B960 (void);
// 0x000002FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusEnter(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m6BBAAE5223655643AD7FCC4AF75CF1612F4C134C (void);
// 0x000002FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusExit(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m6DA4F77E4CD2345C1976F5A949C058F13EE52AE8 (void);
// 0x000002FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m3A5CECC1DC75E173CDAF359D4A4965DEBAB7C26C (void);
// 0x000002FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_mF19020B6E1661B1EDCCA800A083101D2C107E63D (void);
// 0x000002FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler.OnHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>>)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_mAE7541398B00185D9D197E51EDB0D0BAE38D4D35 (void);
// 0x00000300 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::.ctor()
extern void PanZoomBase__ctor_m44859B817973C6500079E081A36AB5B59B2ED690 (void);
// 0x00000301 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::.ctor(System.Int32)
extern void U3CZoomAndStopU3Ed__78__ctor_m30C53A8A39CC3FBAE8A279C57EC3FDC2C55706B7 (void);
// 0x00000302 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.IDisposable.Dispose()
extern void U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_mBAEE47B4402D4A15A9019E8BE21DF0E709776BBE (void);
// 0x00000303 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::MoveNext()
extern void U3CZoomAndStopU3Ed__78_MoveNext_mABBEFDB098EC2BC20D7F0A2C666A9E54C58CF892 (void);
// 0x00000304 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C5C426516CF1CC7EF5EF725543F34C316B3137F (void);
// 0x00000305 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.IEnumerator.Reset()
extern void U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_m0C9556653FCC60711B49BCB31FCB46DAE13556DD (void);
// 0x00000306 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.IEnumerator.get_Current()
extern void U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_mBD9E41C773B5305A57FE6361E5A74B5AFFAF3137 (void);
// 0x00000307 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::get_IsValid()
extern void PanZoomBaseRectTransf_get_IsValid_m33B1D98FB5A97CA8EDB7E85444BBD530A3395DC1 (void);
// 0x00000308 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::Initialize()
extern void PanZoomBaseRectTransf_Initialize_m2E0F2FF44E985D4D27786CD42BB995C72E313A83 (void);
// 0x00000309 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ComputePanSpeed(System.Single,System.Single,System.Single)
extern void PanZoomBaseRectTransf_ComputePanSpeed_m88D9336E7A56D2C3651E559771E7D8F6F68EF964 (void);
// 0x0000030A System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomDir(System.Boolean)
extern void PanZoomBaseRectTransf_ZoomDir_m6B2CAA4BC7E141464367FDA33A0AF625233F0CFE (void);
// 0x0000030B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomIn()
extern void PanZoomBaseRectTransf_ZoomIn_mF634DFB0F1F818BB3C0EA4CC322B10C84D91E77A (void);
// 0x0000030C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomOut()
extern void PanZoomBaseRectTransf_ZoomOut_mCE0AC547E432E2302CD89C36293CB6157F21CA8B (void);
// 0x0000030D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::UpdatePanZoom()
extern void PanZoomBaseRectTransf_UpdatePanZoom_m6CDE014A4584C41AA97410460CACE1B786139EFC (void);
// 0x0000030E UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::LimitPanning()
extern void PanZoomBaseRectTransf_LimitPanning_m8626D4A0DB5642C15DFD4BEA2A4A7BF38A045842 (void);
// 0x0000030F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomInOut_RectTransform(System.Single,UnityEngine.Vector2)
extern void PanZoomBaseRectTransf_ZoomInOut_RectTransform_mF5CE843B79AA6E60FCB1C2BA2EDB2057AFA8514B (void);
// 0x00000310 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::UpdateCursorPosInHitBox()
extern void PanZoomBaseRectTransf_UpdateCursorPosInHitBox_m1495886A2942E11C4A69A502613510BAD9DF7661 (void);
// 0x00000311 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::.ctor()
extern void PanZoomBaseRectTransf__ctor_m5791DAA60859F5378DFFFF789E5A6C383D67F3B3 (void);
// 0x00000312 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::get_TextureShaderProperty()
extern void PanZoomBaseTexture_get_TextureShaderProperty_m72B81821F9738602E98B87BA525103FDFC5D629A (void);
// 0x00000313 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::set_TextureShaderProperty(System.String)
extern void PanZoomBaseTexture_set_TextureShaderProperty_m7415E1D1C6BC45D5F29876E22346F31C70EA5F15 (void);
// 0x00000314 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::get_IsValid()
extern void PanZoomBaseTexture_get_IsValid_mDB1F9C9DFE43502BAFA02EB4A8F6238EA10FDC61 (void);
// 0x00000315 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::Initialize()
extern void PanZoomBaseTexture_Initialize_m76DEB5F9F75B20748CD0585038E0AD02C0DCAB4F (void);
// 0x00000316 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::Initialize(System.Single)
extern void PanZoomBaseTexture_Initialize_m044FFA8F9C6FB6334E7AACB3CFF1410E45103989 (void);
// 0x00000317 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ComputePanSpeed(System.Single,System.Single,System.Single)
extern void PanZoomBaseTexture_ComputePanSpeed_mB591FEDAD310D0EB7F16B2384A613D096013C8CE (void);
// 0x00000318 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::UpdatePanZoom()
extern void PanZoomBaseTexture_UpdatePanZoom_mAB9D05E4C43D9DF69D2CA654BF38A6625720B777 (void);
// 0x00000319 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomDir(System.Boolean)
extern void PanZoomBaseTexture_ZoomDir_m2748177E06D502EA36BA162B23144217D720BE1A (void);
// 0x0000031A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomIn()
extern void PanZoomBaseTexture_ZoomIn_mF5AED4DAF9BB5E2AA1597339893567BCB88CE52A (void);
// 0x0000031B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomOut()
extern void PanZoomBaseTexture_ZoomOut_mF30A71FC3E55C0EAA0E267F69896D454545B8CC1 (void);
// 0x0000031C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomInOut(System.Single,UnityEngine.Vector2)
extern void PanZoomBaseTexture_ZoomInOut_m903899DA63F9408FF245CB268C1B32AF26C934EA (void);
// 0x0000031D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::UpdateCursorPosInHitBox()
extern void PanZoomBaseTexture_UpdateCursorPosInHitBox_m2E96ABA642771F86E30182D9A56781BC1AFC36A3 (void);
// 0x0000031E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::.ctor()
extern void PanZoomBaseTexture__ctor_mB335A71BD80791CDD2D580D3FFE2D47E958333FB (void);
// 0x0000031F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::Start()
extern void PanZoomRectTransf_Start_m9C9F5CB03E2EC4CCA21AC7CC7FF1EC1B1E5EB348 (void);
// 0x00000320 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::Update()
extern void PanZoomRectTransf_Update_mA3707EBA4508008C84CACD8DCAF353FD70F621BB (void);
// 0x00000321 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::.ctor()
extern void PanZoomRectTransf__ctor_m1E3DADE797DC6C487D2899868ABF6C80D9F0DA5F (void);
// 0x00000322 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::Start()
extern void PanZoomTexture_Start_m38B9EA010636DAC2FD2D03F84921848C51C778B0 (void);
// 0x00000323 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::Update()
extern void PanZoomTexture_Update_m51BFB8F50E32FA32CA0748AB670E1623C19AD0A6 (void);
// 0x00000324 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::.ctor()
extern void PanZoomTexture__ctor_m4A80E1EC5003A94A339DC3D9DD7EE6EAA972CAF6 (void);
// 0x00000325 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::Start()
extern void ScrollRectTransf_Start_mB3FEAD54E13A4583692759A19C373AF30646B292 (void);
// 0x00000326 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::UpdatePivot()
extern void ScrollRectTransf_UpdatePivot_m1E27424744FA1F3AFD878F5B72574BC00A28951E (void);
// 0x00000327 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::Update()
extern void ScrollRectTransf_Update_m06F0A161CCDFD80537096DAAE1D646B81E73F1F1 (void);
// 0x00000328 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::.ctor()
extern void ScrollRectTransf__ctor_mD10FCDB4DB284D45DC16882D579B30F02C20CFB5 (void);
// 0x00000329 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::Start()
extern void ScrollTexture_Start_m5B690489FCAD1D61DCC57AE7976F921593EA3FC8 (void);
// 0x0000032A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::Update()
extern void ScrollTexture_Update_m4C24E9A0029B0B6E4D4F536A37DF071613B5B1FE (void);
// 0x0000032B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::.ctor()
extern void ScrollTexture__ctor_mE2FDA788487523755B18303B864B706E8D763982 (void);
// 0x0000032C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::Start()
extern void TargetMoveToCamera_Start_m941551F3BE41FB020F73E171F21193BAC90492B1 (void);
// 0x0000032D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::Update()
extern void TargetMoveToCamera_Update_mDD41B84708CD03E20CF1933B9BD9EEA975C7EE6E (void);
// 0x0000032E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::OnEyeFocusStop()
extern void TargetMoveToCamera_OnEyeFocusStop_m7DE5069C043EFBEBA5F28838019EBCABAB3256D3 (void);
// 0x0000032F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::OnSelect()
extern void TargetMoveToCamera_OnSelect_m160D3D80A06B651B06C6CF55722EE165324DCE4C (void);
// 0x00000330 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::TransitionToUser()
extern void TargetMoveToCamera_TransitionToUser_m3D3A2F7C149CAB8A5BBD48548DF0D32622D79534 (void);
// 0x00000331 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::ReturnHome()
extern void TargetMoveToCamera_ReturnHome_m993554E6685A17671618F47DA3A0DD6582537385 (void);
// 0x00000332 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::TransitionToCamera()
extern void TargetMoveToCamera_TransitionToCamera_m864B7147454F17CFDF626C6CA7F9DF6483095C0F (void);
// 0x00000333 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::.ctor()
extern void TargetMoveToCamera__ctor_m04D8CE0BD97A138AAAE45883147D359C0394EA5C (void);
// 0x00000334 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Awake()
extern void GrabReleaseDetector_Awake_m5C16858474773B506742617DD012141E79BB5209 (void);
// 0x00000335 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mF8D3E2F68F5D5B4BAFE287FFF371FA34E2F37591 (void);
// 0x00000336 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mE2BB94F79D6328FA0053BC2173ABC82EF521602A (void);
// 0x00000337 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB11638EB582D5D5E3AE4EBAECD12F2BA4D9E8D67 (void);
// 0x00000338 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m4C9F19BB3B5F61B5E5D377A020958CB60AA5EF2E (void);
// 0x00000339 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::.ctor()
extern void GrabReleaseDetector__ctor_m6AA5BA7C598B58ECD098DD13A8895C0C75D5DCC7 (void);
// 0x0000033A Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_EyeTrackingProvider()
extern void MoveObjByEyeGaze_get_EyeTrackingProvider_mA8DBE1CD875F04E83D11DBAEAD0619706F0E9B26 (void);
// 0x0000033B System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrX()
extern void MoveObjByEyeGaze_get_ConstrX_mD0A87209424A466F68A765EB622DF7A3284D5F2B (void);
// 0x0000033C System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrY()
extern void MoveObjByEyeGaze_get_ConstrY_mECE865983ABA30D469541DAB01F5E456468B4844 (void);
// 0x0000033D System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrZ()
extern void MoveObjByEyeGaze_get_ConstrZ_mCF15E8138318027BE7C2F5EC820DD125B495F1A4 (void);
// 0x0000033E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Start()
extern void MoveObjByEyeGaze_Start_m0AF6953E3915D39941DF3B6DD1F41F1EF116BA37 (void);
// 0x0000033F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Update()
extern void MoveObjByEyeGaze_Update_mDE77D9A6EBB6F00F104C949B0EDDDFAD07AB9F76 (void);
// 0x00000340 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m13BFBECF659FFA9C3EAC18225003A925AC582E81 (void);
// 0x00000341 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler.OnHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>>)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_mCCBA974FF15D8329AA1AFA12D330DA1542195D4B (void);
// 0x00000342 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m8FB158C1817A4CA659C9E1BAA630A2EB50100A42 (void);
// 0x00000343 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m85728852918C79AF823C8FA3C27B7493DC31B2A6 (void);
// 0x00000344 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m71AC0138D46C10488FA9AFD05A11626E49244C16 (void);
// 0x00000345 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mBD17757DABDB930BB1AF49CDD67AC8C205625B9F (void);
// 0x00000346 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m725AF569AADECB6F1D8528C04FC47125DB003C4C (void);
// 0x00000347 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HandDrag_Start()
extern void MoveObjByEyeGaze_HandDrag_Start_m00794FAEDB4F0760C1A2E52AA1EE17102E962AFA (void);
// 0x00000348 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HandDrag_Stop()
extern void MoveObjByEyeGaze_HandDrag_Stop_m3FDBE6C977EAB91577034E8D3090834EE7669E26 (void);
// 0x00000349 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsLookingAwayFromTarget()
extern void MoveObjByEyeGaze_IsLookingAwayFromTarget_m2EE6761557E0B665C94D46912A835AC6797E11A8 (void);
// 0x0000034A System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsLookingAwayFromPreview()
extern void MoveObjByEyeGaze_IsLookingAwayFromPreview_m40607FA8770A5597BD45EE0D5D2FB4353917FAD7 (void);
// 0x0000034B System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsDestinationPlausible()
extern void MoveObjByEyeGaze_IsDestinationPlausible_m4F10BABC537B5440871B8AB858BDE56FD34B1F2D (void);
// 0x0000034C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::GetValidPlacemLocation(UnityEngine.GameObject)
extern void MoveObjByEyeGaze_GetValidPlacemLocation_m3090052CDA5C67A17DCB861DFB314FD2F8067DC8 (void);
// 0x0000034D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ActivatePreview()
extern void MoveObjByEyeGaze_ActivatePreview_mA89566A6C348FC868D20FCAA3937230A3AD9A947 (void);
// 0x0000034E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DeactivatePreview()
extern void MoveObjByEyeGaze_DeactivatePreview_m6DA3A23F2DDD010487FD7DE2AD33634BECD7C3F6 (void);
// 0x0000034F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DragAndDrop_Start()
extern void MoveObjByEyeGaze_DragAndDrop_Start_mC1BDB8D6A6470B6869FDAF597B4A2A69BEC59228 (void);
// 0x00000350 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DragAndDrop_Finish()
extern void MoveObjByEyeGaze_DragAndDrop_Finish_m4D7FAF9AACD1065019620CBFD7F28568739F8FDC (void);
// 0x00000351 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::RelativeMoveUpdate(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_RelativeMoveUpdate_m1CE0A59A37F580D56041DB6400E6601B5BDEB491 (void);
// 0x00000352 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Angle_InitialGazeToCurrGazeDir()
extern void MoveObjByEyeGaze_Angle_InitialGazeToCurrGazeDir_mD2B898C78C5DFFAC5F818F39266624B56703423E (void);
// 0x00000353 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Angle_ToCurrHitTarget(UnityEngine.GameObject)
extern void MoveObjByEyeGaze_Angle_ToCurrHitTarget_m48A42A21185624CF74BB5B7D96FAE0D0C1743DBD (void);
// 0x00000354 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HeadIsInMotion()
extern void MoveObjByEyeGaze_HeadIsInMotion_m9A1B8381BDF396403C2AB2B82E6C48BAEE2964FB (void);
// 0x00000355 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::MoveTargetBy(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_MoveTargetBy_m2BC0BECA50693B6BA0AE5DAA959BDEDC8158B1ED (void);
// 0x00000356 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::UpdateSliderTextOutput()
extern void MoveObjByEyeGaze_UpdateSliderTextOutput_mBDD1AB6AB3C368F0E15867BD02F0D7638AFA14A2 (void);
// 0x00000357 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ConstrainMovement()
extern void MoveObjByEyeGaze_ConstrainMovement_m8D13C5B3045F23CF0F6FF87EA809FFC0EE073CF4 (void);
// 0x00000358 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::OnDrop_SnapToClosestDecimal()
extern void MoveObjByEyeGaze_OnDrop_SnapToClosestDecimal_m255DD9EA82DA6856163D140513C804838A5FFBAC (void);
// 0x00000359 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::MoveTargetTo(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_MoveTargetTo_mBDBABE716B44243CBF76D61016EFB0BEDBB23DB0 (void);
// 0x0000035A System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ShouldObjBeWarped(System.Single,System.Single,System.Boolean)
extern void MoveObjByEyeGaze_ShouldObjBeWarped_mC565CF4B3EB836AE7068A41E01F49227E2A8603B (void);
// 0x0000035B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m9C319765FB5E879EC16AAE33BC0BB3DF1F9CA34A (void);
// 0x0000035C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::.ctor()
extern void MoveObjByEyeGaze__ctor_mE90ED6991F03A2DF3400DEBADB45F5CBF3573F5F (void);
// 0x0000035D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::.cctor()
extern void MoveObjByEyeGaze__cctor_m39C2BE838E485A6CCBB57FCE21B5E7B974C83A47 (void);
// 0x0000035E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SnapTo::.ctor()
extern void SnapTo__ctor_m28F482C53D1C3BF487E8CC24295477B656FB1B58 (void);
// 0x0000035F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TransportToRespawnLocation::OnTriggerEnter(UnityEngine.Collider)
extern void TransportToRespawnLocation_OnTriggerEnter_mAB4066F48FB8BE6AD812407A97D9A7F1C235D0A8 (void);
// 0x00000360 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TransportToRespawnLocation::.ctor()
extern void TransportToRespawnLocation__ctor_mB11A32C5C32FBFF544F8C5E973DB64A10EF209A8 (void);
// 0x00000361 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::Start()
extern void TriggerZonePlaceObjsWithin_Start_m82741DCB01D9758DA02F743446DD06B1381AA137 (void);
// 0x00000362 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::OnTriggerEnter(UnityEngine.Collider)
extern void TriggerZonePlaceObjsWithin_OnTriggerEnter_m00DE1A1057B04E3F308AEEA04BB19B8E22AF2DF4 (void);
// 0x00000363 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::OnTriggerExit(UnityEngine.Collider)
extern void TriggerZonePlaceObjsWithin_OnTriggerExit_m9D73E6A37966B80907C6372CF02ED65BCA73B274 (void);
// 0x00000364 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::CheckForCompletion()
extern void TriggerZonePlaceObjsWithin_CheckForCompletion_mF362892B6AF50C461D36D5913995E9A3D1BE5165 (void);
// 0x00000365 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::TargetDropped()
extern void TriggerZonePlaceObjsWithin_TargetDropped_m1E654BA879B9359ABBB4D48A641F28A31BFB470B (void);
// 0x00000366 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::AreWeThereYet()
extern void TriggerZonePlaceObjsWithin_AreWeThereYet_m672574735FF19F1AA08E0FB6DE7A884766E215F8 (void);
// 0x00000367 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::.ctor()
extern void TriggerZonePlaceObjsWithin__ctor_mA1E439FA78080A4B4173257A47114957ECBCB3FE (void);
// 0x00000368 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::Start()
extern void HitBehaviorDestroyOnSelect_Start_m48487AAC00C5461B68D16F5D1DC33763F3A49026 (void);
// 0x00000369 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::SetUpAudio()
extern void HitBehaviorDestroyOnSelect_SetUpAudio_m2E2C109048C9CDE854B53591CC3659406EB49887 (void);
// 0x0000036A System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::PlayAudioOnHit(UnityEngine.AudioClip)
extern void HitBehaviorDestroyOnSelect_PlayAudioOnHit_mDBD5D04684169F9C9008C88E1C4AAF2BD3D32E77 (void);
// 0x0000036B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::PlayAnimationOnHit()
extern void HitBehaviorDestroyOnSelect_PlayAnimationOnHit_m7A76B49083DBF3C240D9E8EFD2FE2D0127E18C2F (void);
// 0x0000036C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::TargetSelected()
extern void HitBehaviorDestroyOnSelect_TargetSelected_m84D6E172E557643F9E927C08C9E35DC1CB88B473 (void);
// 0x0000036D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::HandleTargetGridIterator()
extern void HitBehaviorDestroyOnSelect_HandleTargetGridIterator_mE4FBB3310B9D8B0949CC11B78D0EFAC1141309E6 (void);
// 0x0000036E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::.ctor()
extern void HitBehaviorDestroyOnSelect__ctor_mC583F57875912AE72E1AD608DFDD72D187DE63A0 (void);
// 0x0000036F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.RotateWithConstSpeedDir::RotateTarget()
extern void RotateWithConstSpeedDir_RotateTarget_m9808D6B937D1E3D1C8172F7EDFB8A1F14BADB5F4 (void);
// 0x00000370 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.RotateWithConstSpeedDir::.ctor()
extern void RotateWithConstSpeedDir__ctor_mF88607AA9D1A217C680F212360BE902DAFA2AE34 (void);
// 0x00000371 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::Start()
extern void TargetGroupCreatorRadial_Start_m831613E6B5C6F5A063AD16D1746AD4095EFE8DD2 (void);
// 0x00000372 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::Update()
extern void TargetGroupCreatorRadial_Update_m440B0DBC7C39FB38F6969A73C4BE695AA55602BA (void);
// 0x00000373 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::HideTemplates()
extern void TargetGroupCreatorRadial_HideTemplates_mE5F73C348AD52A91413B1A72BF767DDBECF00893 (void);
// 0x00000374 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::CreateNewTargets_RadialLayout()
extern void TargetGroupCreatorRadial_CreateNewTargets_RadialLayout_m2AB6A8EA5BBE6BFBEC3C81A0D2C15F9447CAD4FD (void);
// 0x00000375 UnityEngine.GameObject[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::get_InstantiatedObjects()
extern void TargetGroupCreatorRadial_get_InstantiatedObjects_mFE13FF0C02937405C0385E28B269500F644B50C0 (void);
// 0x00000376 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::GetRandomTemplate()
extern void TargetGroupCreatorRadial_GetRandomTemplate_m0546AE27D46912D6D5C3CC9CEC55F12FC0084984 (void);
// 0x00000377 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::InstantiateRadialLayoutedTarget(System.Single,System.Single,System.Int32)
extern void TargetGroupCreatorRadial_InstantiateRadialLayoutedTarget_m8F2AC71FE3DC54D5DAA8EB0EAD18ADC51E31A2D0 (void);
// 0x00000378 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::KeepConstantVisAngleTargetSize()
extern void TargetGroupCreatorRadial_KeepConstantVisAngleTargetSize_m79688D8259703160D79A8270288B4A80D4C4E10F (void);
// 0x00000379 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::KeepFacingTheCamera()
extern void TargetGroupCreatorRadial_KeepFacingTheCamera_m337F48BC581563B43B4C65BC2796D1BA844CA374 (void);
// 0x0000037A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::.ctor()
extern void TargetGroupCreatorRadial__ctor_m44939C31AD440025D5C852D1A460B7815E9D7B4C (void);
// 0x0000037B UnityEngine.Color Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_HighlightColor()
extern void TargetGroupIterator_get_HighlightColor_m2DEE097D7D4A652ECDCB9E7B2B79EA228CA02528 (void);
// 0x0000037C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::add_OnAllTargetsSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_add_OnAllTargetsSelected_m6C0E9568229293FD6865AD8A9A664EBBAF4B8297 (void);
// 0x0000037D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::remove_OnAllTargetsSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_remove_OnAllTargetsSelected_m0058693A19333EAEC032003269F6F94335FD2F0C (void);
// 0x0000037E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::add_OnTargetSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_add_OnTargetSelected_m5E6C941090BB244AAA18AA580E9A70957860C5D6 (void);
// 0x0000037F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::remove_OnTargetSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_remove_OnTargetSelected_mDB22A4E5586BAF0909DEBA3D7D40A43FF822FC4C (void);
// 0x00000380 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Start()
extern void TargetGroupIterator_Start_m17A656D6BEE047C14129DAF9A657DB9961AB24ED (void);
// 0x00000381 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Update()
extern void TargetGroupIterator_Update_m4E12751AF44610E8165268695065489677F0105F (void);
// 0x00000382 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ResetAmountOfTries()
extern void TargetGroupIterator_ResetAmountOfTries_m3AED4FD554CFD322237C012A5C84D946554EC5BE (void);
// 0x00000383 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ResetIterator()
extern void TargetGroupIterator_ResetIterator_m6C1F315D4955A371E2E63A131454F4777AD16097 (void);
// 0x00000384 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ProceedToNextTarget()
extern void TargetGroupIterator_ProceedToNextTarget_mEB2B71CC30D84070933D99615F6EE94C4636A327 (void);
// 0x00000385 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_CurrentTarget()
extern void TargetGroupIterator_get_CurrentTarget_m11CC2AE3EF318E3E889EE1E5482D9539FC93FD1A (void);
// 0x00000386 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_PreviousTarget()
extern void TargetGroupIterator_get_PreviousTarget_m36CBD481ACB1DB69CFCD4620994B1F76160F50F0 (void);
// 0x00000387 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_CurrentTargetIsValid()
extern void TargetGroupIterator_get_CurrentTargetIsValid_mFBBF420EBF95662187F671E51FE2EA79246C2571 (void);
// 0x00000388 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::SaveOriginalColors()
extern void TargetGroupIterator_SaveOriginalColors_m46119C0C8851B04FCBAB56682C07E3F8AC1B13FF (void);
// 0x00000389 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HighlightTarget()
extern void TargetGroupIterator_HighlightTarget_m6F02D4547C8C4458A14DAD0A3397918BD6B4409D (void);
// 0x0000038A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HighlightTarget(UnityEngine.Color)
extern void TargetGroupIterator_HighlightTarget_mD8EB8C3D2D854B6867FD611F8F2449B9C6882E3A (void);
// 0x0000038B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ShowVisualMarkerForCurrTarget()
extern void TargetGroupIterator_ShowVisualMarkerForCurrTarget_m1DDB110FF0E39C3763404403E8EE78A836EC0119 (void);
// 0x0000038C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HideVisualMarkerForCurrTarget()
extern void TargetGroupIterator_HideVisualMarkerForCurrTarget_mFC89BA7B36CB12929DA2AC4CD47C6EC2CB078E85 (void);
// 0x0000038D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::UnhighlightTarget()
extern void TargetGroupIterator_UnhighlightTarget_m7157393A06F3650FB072141E84BDBF1CC6B6559E (void);
// 0x0000038E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ShowHighlights()
extern void TargetGroupIterator_ShowHighlights_m3EC739F509C5BDB6F467FBEE993EA35458F560F1 (void);
// 0x0000038F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HideHighlights()
extern void TargetGroupIterator_HideHighlights_m5308D071A4D64FDB57D4DFF626A442CCCC1CC68C (void);
// 0x00000390 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Fire_OnAllTargetsSelected()
extern void TargetGroupIterator_Fire_OnAllTargetsSelected_m90C6833C574F27FF3CE79379C5915C2B4D37F7A6 (void);
// 0x00000391 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Fire_OnTargetSelected()
extern void TargetGroupIterator_Fire_OnTargetSelected_mBE3AC9A807BE7FFED530CA72571137D15494EA58 (void);
// 0x00000392 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mADECE26B73ABFA292D0374C9B1765058C75EA14F (void);
// 0x00000393 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mB5372602D56F3983B471A4B3515A74DE67C83E16 (void);
// 0x00000394 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m7B2CEB7A6F30E03E08180D8733BC24F391AC5336 (void);
// 0x00000395 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m9F92C02C2C9DD924A24CEC1236A8BDFB359147F6 (void);
// 0x00000396 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::.ctor()
extern void TargetGroupIterator__ctor_m614A02C7BDA69CED830D7D9764CB1CC8287130FF (void);
// 0x00000397 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::.ctor(System.Object,System.IntPtr)
extern void TargetGroupEventHandler__ctor_m997C89086AD15A00E869AB6E434D653AF34D993E (void);
// 0x00000398 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::Invoke()
extern void TargetGroupEventHandler_Invoke_mBEB04FD68BFAE54AA1D98E5277F2B9EB9DF0096A (void);
// 0x00000399 System.IAsyncResult Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern void TargetGroupEventHandler_BeginInvoke_m46C2DAF130C84AB4AA790519A6CAA0E64051AFE1 (void);
// 0x0000039A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::EndInvoke(System.IAsyncResult)
extern void TargetGroupEventHandler_EndInvoke_m7164986017E581F23F4E12B26827CED82D85D441 (void);
// 0x0000039B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::ShowIt()
extern void ToggleGameObject_ShowIt_mA31F1D58FFEC19F10F822CDB729FE86289106F49 (void);
// 0x0000039C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::HideIt()
extern void ToggleGameObject_HideIt_mD60E92D7F00FCA4D1FB249BE161A082FE532C477 (void);
// 0x0000039D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::ShowIt(System.Boolean)
extern void ToggleGameObject_ShowIt_m232C17535A6619725980E6432B560F98D5398303 (void);
// 0x0000039E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::.ctor()
extern void ToggleGameObject__ctor_mB5EC9D7E66468ED63D2AC3905482D496340D072D (void);
// 0x0000039F Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_EyeTarget()
extern void DrawOnTexture_get_EyeTarget_mA70E643C1E86AA742B828A1F17671FA9FD295DFB (void);
// 0x000003A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::Start()
extern void DrawOnTexture_Start_m327596B8F4A0E2AAC2053C3E901AB84FD9B0B723 (void);
// 0x000003A1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::OnLookAt()
extern void DrawOnTexture_OnLookAt_m540C4800E116A89CABCFDBBB35DA58433141C55A (void);
// 0x000003A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAtThisHitPos(UnityEngine.Vector3)
extern void DrawOnTexture_DrawAtThisHitPos_m047A7C6A53B8D4ECEE1DEDCC8F3DA70212912A1C (void);
// 0x000003A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt(UnityEngine.Vector2,UnityEngine.Color)
extern void DrawOnTexture_DrawAt_m5F0F192DB617A2E7FB522CAE89E3F657827491E5 (void);
// 0x000003A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt2(UnityEngine.Vector2,System.Int32,System.Single)
extern void DrawOnTexture_DrawAt2_m8B027C59B579518ABA1E3FF3527570DCBE08178F (void);
// 0x000003A5 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt(UnityEngine.Vector2)
extern void DrawOnTexture_DrawAt_mACF2F8ADBF4397769AE19F1349A1ABCAE5E27D09 (void);
// 0x000003A6 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::ComputeHeatmapAt(UnityEngine.Vector2,System.Boolean,System.Boolean)
extern void DrawOnTexture_ComputeHeatmapAt_m6C6D602F8B92BD20208D4B693DF4B22300DB86F0 (void);
// 0x000003A7 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::ComputeHeatmapColorAt(UnityEngine.Vector2,UnityEngine.Vector2,System.Nullable`1<UnityEngine.Color>&)
extern void DrawOnTexture_ComputeHeatmapColorAt_m383E4A8E46C915FE697F45980D05DA752A22324D (void);
// 0x000003A8 UnityEngine.Renderer Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_MyRenderer()
extern void DrawOnTexture_get_MyRenderer_m78CD25F04403DACC07F3F486F7F308E729A110C7 (void);
// 0x000003A9 UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_MyDrawTexture()
extern void DrawOnTexture_get_MyDrawTexture_m9DE426485BF5D05377FE28682895668DBC96438B (void);
// 0x000003AA System.Nullable`1<UnityEngine.Vector2> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::GetCursorPosInTexture(UnityEngine.Vector3)
extern void DrawOnTexture_GetCursorPosInTexture_mFF3065FFFA16BE80B82D3A79F3920ED73E9785A4 (void);
// 0x000003AB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::.ctor()
extern void DrawOnTexture__ctor_mF8891432993C7EF93E82C8A6257542466CBA73A9 (void);
// 0x000003AC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::.ctor(System.Int32)
extern void U3CDrawAtU3Ed__19__ctor_mABD515A84774524FDB73B1E4F727A8D187845FE7 (void);
// 0x000003AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.IDisposable.Dispose()
extern void U3CDrawAtU3Ed__19_System_IDisposable_Dispose_mD855D631D537C8C972022C07A4E6B86F20694767 (void);
// 0x000003AE System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::MoveNext()
extern void U3CDrawAtU3Ed__19_MoveNext_m4DF6A2F7745D70FFF3ADBE96578F3012CCF60ACC (void);
// 0x000003AF System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60FBFD67550BBF502D2B84D09BF924A6F53A72B3 (void);
// 0x000003B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m519DAF7D703E82BD40D6C88FD166E9C92FF15E54 (void);
// 0x000003B1 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_mC139E877DB3EE6F74DB6CA4BA39C6255D8D43451 (void);
// 0x000003B2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::.ctor(System.Int32)
extern void U3CComputeHeatmapAtU3Ed__20__ctor_m53B7C7F16531AC26E5DEAFA91C43B854C9B32579 (void);
// 0x000003B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.IDisposable.Dispose()
extern void U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m16413974FA759817A38994519D529BFBC814F259 (void);
// 0x000003B4 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::MoveNext()
extern void U3CComputeHeatmapAtU3Ed__20_MoveNext_mE1FE991D3205CB8680BCB0E361355BE082D9E8B2 (void);
// 0x000003B5 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FC8124B6EBADC8E4051496092E545DD8444561B (void);
// 0x000003B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.IEnumerator.Reset()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_m40447F82651458D01A92A2E255E914D430A93DD1 (void);
// 0x000003B7 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_m5756DA265E1D39955B13A9266C169C371F832D8F (void);
// 0x000003B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Awake()
extern void OnSelectVisualizerInputController_Awake_m82957B06EAF6AFAACEB6F8E37EDFFCB9C8C4F9EB (void);
// 0x000003B9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::OnTargetSelected()
extern void OnSelectVisualizerInputController_OnTargetSelected_m0AA0FBCF573A8A7FEF253392A2090399361408B4 (void);
// 0x000003BA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mD3DFF9EE88EF810D9AFDF1078E642D64A9AF2A54 (void);
// 0x000003BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m21554712FD0EA7E6403AB8687E260FDE66F5389F (void);
// 0x000003BC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB4E80D6D0A9DBF4C386568CE24A880CC23E555A8 (void);
// 0x000003BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mCEEA16E41BDF811BABCAC1035A30D4A14B31F4A2 (void);
// 0x000003BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::.ctor()
extern void OnSelectVisualizerInputController__ctor_m6FE60D81C1E8051770AB91546E5ACEFBF3FDCE34 (void);
// 0x000003BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::Start()
extern void ParticleHeatmap_Start_m05347379F9A7FA3C65876A9D3EFCC6BB527127FB (void);
// 0x000003C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::SetParticle(UnityEngine.Vector3)
extern void ParticleHeatmap_SetParticle_m559A29E8081760B3E56D5CF75EF3167AED1EC353 (void);
// 0x000003C1 System.Nullable`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::GetPositionOfParticle(System.Int32)
extern void ParticleHeatmap_GetPositionOfParticle_m14896739BD05E1DABD3BAF93AF7862E2E0D37D54 (void);
// 0x000003C2 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::DetermineNormalizedIntensity(UnityEngine.Vector3,System.Single)
extern void ParticleHeatmap_DetermineNormalizedIntensity_m54771F152B1FFA5AA1DEB19D255B28A19049E3E2 (void);
// 0x000003C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::UpdateColorForAllParticles()
extern void ParticleHeatmap_UpdateColorForAllParticles_mAE24C104FF6FABFA70C731D2245D18E7C3469B3D (void);
// 0x000003C4 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::saturate(System.Single)
extern void ParticleHeatmap_saturate_m0D63575121242EFAF3ED8383642568A5942EB146 (void);
// 0x000003C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::DisplayParticles()
extern void ParticleHeatmap_DisplayParticles_mDA6F6C2342CC9D48B7B51D8C7915221313EFE828 (void);
// 0x000003C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::ShowHeatmap()
extern void ParticleHeatmap_ShowHeatmap_m46CAA1254ACB043F9DCC54D3B926585693AA3D0D (void);
// 0x000003C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::HideHeatmap()
extern void ParticleHeatmap_HideHeatmap_m2E469EA0C52AF24C0D6A74EB4BEBD2413DC318CA (void);
// 0x000003C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::.ctor()
extern void ParticleHeatmap__ctor_m15C54986BE73F77665A1DE903C50BE4969D8794B (void);
// 0x000003C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmapParticleData::.ctor()
extern void ParticleHeatmapParticleData__ctor_m6CB919346735511FC3D5429615FB65C10B1E2A8C (void);
// 0x000003CA Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::get_Instance()
extern void AudioFeedbackPlayer_get_Instance_m912630AC6CAD66FD421E7A52FDCB1CF948D3C2D9 (void);
// 0x000003CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::set_Instance(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer)
extern void AudioFeedbackPlayer_set_Instance_m48FD2B8B89EFF6DD59A9C457F3B06AE8920ABE6A (void);
// 0x000003CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::Start()
extern void AudioFeedbackPlayer_Start_mE32ABA1C89FD78754589D045C6136903EE27CA42 (void);
// 0x000003CD UnityEngine.AudioSource Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::SetupAudioSource(UnityEngine.GameObject)
extern void AudioFeedbackPlayer_SetupAudioSource_m6FED5C5810DC8568F04C63A43A3210375074B42B (void);
// 0x000003CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::PlaySound(UnityEngine.AudioClip)
extern void AudioFeedbackPlayer_PlaySound_m0F80FA8C3D86D06B605514F298C4C19D83C60FFC (void);
// 0x000003CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::.ctor()
extern void AudioFeedbackPlayer__ctor_mBE6DE1EA39695BCE2978149DDC785A49DEDD73BB (void);
// 0x000003D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGaze::Update()
extern void FollowEyeGaze_Update_m1CA67FC6955B8BA18760175CCBBD1BC5160A47BB (void);
// 0x000003D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGaze::.ctor()
extern void FollowEyeGaze__ctor_mDC6451236CF451885296C0097AF6ADBB2A3EE783 (void);
// 0x000003D2 UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::get_MyTextMesh()
extern void SpeechVisualFeedback_get_MyTextMesh_m7809A802D36E4B4051C4D500C85207818961DB2A (void);
// 0x000003D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::UpdateTextMesh(System.String)
extern void SpeechVisualFeedback_UpdateTextMesh_m8A492269EEF70E7D0FED953F4D4518D7F6F1C66B (void);
// 0x000003D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::ShowVisualFeedback(System.String)
extern void SpeechVisualFeedback_ShowVisualFeedback_mE9575158229425BD3C15A304C7F2FF609A31CA1B (void);
// 0x000003D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::Update()
extern void SpeechVisualFeedback_Update_m1B57AF94099A602F2190E739438FECF17F4ABC33 (void);
// 0x000003D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void SpeechVisualFeedback_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m1AF24F9AFBD6141EA377FD55A35B7AC8BDEF8944 (void);
// 0x000003D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::.ctor()
extern void SpeechVisualFeedback__ctor_mC968FA1A631A51E110C4B2BFD2A72C83EA2AD164 (void);
// 0x000003D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Start()
extern void BlendOut_Start_m92CF6BB3F1E50CE30E0AACD8200A87A5FAD2DC42 (void);
// 0x000003D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::InitialSetup()
extern void BlendOut_InitialSetup_m341E859D5277A1CA5462A59B63554D91399CB2AD (void);
// 0x000003DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Engage()
extern void BlendOut_Engage_m7F24EFE9BC7AB7BDD871CEF103DD1D9F9A734876 (void);
// 0x000003DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Disengage()
extern void BlendOut_Disengage_mE1A6575BD4D343D8B469783C37EA89F374D00E1A (void);
// 0x000003DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::DwellSucceeded()
extern void BlendOut_DwellSucceeded_m11AFA8D81D0F3B2CF1B2328114F61BE8EE9B1501 (void);
// 0x000003DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Update()
extern void BlendOut_Update_m30ED869C44C58A92929404F54FB3FBE1453794BE (void);
// 0x000003DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::SlowlyBlendOut()
extern void BlendOut_SlowlyBlendOut_m87AFE227ECAC3D57480F21E5D708B8AEF4DD3513 (void);
// 0x000003DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::ChangeTransparency(System.Single)
extern void BlendOut_ChangeTransparency_mAFF086F037D07D718B0A820D4DF8118300AD790B (void);
// 0x000003E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::ChangeTransparency(System.Single,System.String)
extern void BlendOut_ChangeTransparency_m359E3957A3492831C4D1CFD0C2BC81F04E4C83EB (void);
// 0x000003E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Materials_BlendOut(UnityEngine.Material[],System.Single,System.String)
extern void BlendOut_Materials_BlendOut_m8F3BD3FC54BD74C7FACAC5DB6DE5170C5D6DBE6E (void);
// 0x000003E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::.ctor()
extern void BlendOut__ctor_m66AC9A8DC165FADA57242E3A19D8A5938417283D (void);
// 0x000003E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Start()
extern void ChangeSize_Start_m8FE6A1A724C46AEA65727FE899C5166F395FF788 (void);
// 0x000003E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Engage()
extern void ChangeSize_Engage_m40C66E8A2C8F410BCEEBE9CDE9AB07D4737214FE (void);
// 0x000003E5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Disengage()
extern void ChangeSize_Disengage_m4000A6294C3B0278DE8846829525F32A85532FDB (void);
// 0x000003E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::InitialSetup()
extern void ChangeSize_InitialSetup_m654A4784D159CE6D364E921E49DC9D7943DC39FE (void);
// 0x000003E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Update()
extern void ChangeSize_Update_m2F8853498A0F35A83B9699597C1D661CC979669B (void);
// 0x000003E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::OnLookAt_IncreaseTargetSize()
extern void ChangeSize_OnLookAt_IncreaseTargetSize_m563F8A6B0E0EEDC5DC35D8C7AEFE6B010C374CC1 (void);
// 0x000003E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::OnLookAway_ReturnToOriginalTargetSize()
extern void ChangeSize_OnLookAway_ReturnToOriginalTargetSize_m2BB092B93CC1386F1864FEEEB30CD88F85814150 (void);
// 0x000003EA System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::AreWeThereYet(UnityEngine.Vector3)
extern void ChangeSize_AreWeThereYet_mD19A19E9D31445E02AE02B9CC91B4255D35F6AAF (void);
// 0x000003EB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::.ctor()
extern void ChangeSize__ctor_m24FA050091DBD07218344EE79A049EFC146C1492 (void);
// 0x000003EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Start()
extern void FaceUser_Start_m745571D26DDBBF85366F4E4A997CF41F2B0EA5B2 (void);
// 0x000003ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::InitialSetup()
extern void FaceUser_InitialSetup_mB8E86391DFB496D0FD176D1D182028F31F7A74F4 (void);
// 0x000003EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Update()
extern void FaceUser_Update_mFD6A52D6A65CE218B2224506D44C6A438AF3280C (void);
// 0x000003EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::TurnToUser(UnityEngine.Vector3,UnityEngine.Vector3)
extern void FaceUser_TurnToUser_m1D9591AF2DF395937766D9193BB47F35D39E23A1 (void);
// 0x000003F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::ReturnToOriginalRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void FaceUser_ReturnToOriginalRotation_m9DB7C5783B52550F1F93261336C6AC387B27A13D (void);
// 0x000003F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Engage()
extern void FaceUser_Engage_mC609B46E080445B5CA409968ED931065A9CD13C8 (void);
// 0x000003F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Disengage()
extern void FaceUser_Disengage_mD76051E077D7D0E3D6DB46C41AC711CBCCD17A8A (void);
// 0x000003F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::.ctor()
extern void FaceUser__ctor_m0ACF4D51A5A138C8322B1C867D6EAD5DB9115927 (void);
// 0x000003F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Awake()
extern void KeepFacingCamera_Awake_m68EDDD927545FB3316A5A0CF6C8249F3F021B713 (void);
// 0x000003F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Start()
extern void KeepFacingCamera_Start_m32A6B37DE55EDDEB03E22062A4B739DEBBDF6605 (void);
// 0x000003F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Update()
extern void KeepFacingCamera_Update_m1D90A01AC83754B6CE5B31F85EB57A9601043C94 (void);
// 0x000003F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::.ctor()
extern void KeepFacingCamera__ctor_m6C7432F67BB02A7EEF9D8D541B577EAAB1B4DEAA (void);
// 0x000003F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadScene()
extern void LoadAdditiveScene_LoadScene_m4C2D7DEE921E17B9FD6C243FDE12ADB8AA34A3F8 (void);
// 0x000003F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadScene(System.String)
extern void LoadAdditiveScene_LoadScene_m5E35643C373870B340B243FA3D4A6ECA9CA24146 (void);
// 0x000003FA System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadNewScene(System.String)
extern void LoadAdditiveScene_LoadNewScene_m41B80A9D1B71E2BC35FC564D347578C5D79CD0B1 (void);
// 0x000003FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::.ctor()
extern void LoadAdditiveScene__ctor_mA807C178CB329689965D38022A55023EC8020BBF (void);
// 0x000003FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::.cctor()
extern void LoadAdditiveScene__cctor_mCEA48A9111093FC445F0C0BECC58D0924E781B42 (void);
// 0x000003FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::.ctor(System.Int32)
extern void U3CLoadNewSceneU3Ed__6__ctor_m1331A95D8499FADDB82F2FB52E9AE3FDD5006176 (void);
// 0x000003FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.IDisposable.Dispose()
extern void U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_mCE27DCE29F01564C1FC0C47B33E186218792D0B8 (void);
// 0x000003FF System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::MoveNext()
extern void U3CLoadNewSceneU3Ed__6_MoveNext_m85E3DA53F286661F66C6AEB06260A43105CBC82B (void);
// 0x00000400 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m282DC92EF12B29B3CB54A878B4B0962B4299E57B (void);
// 0x00000401 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_m5893EC4779342E70D038F6799047B868C1477831 (void);
// 0x00000402 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m81939C50D638BDAC85C6AADAE36D762E5A80F4C1 (void);
// 0x00000403 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveWithCamera::Update()
extern void MoveWithCamera_Update_mF5E2FF364275E6F437E0132BD2D920CC46B42896 (void);
// 0x00000404 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveWithCamera::.ctor()
extern void MoveWithCamera__ctor_mC86F01FF3D2CD80E6573D2A00932FB3B2F98C980 (void);
// 0x00000405 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::Start()
extern void OnLoadStartScene_Start_mA65E479C82FFC13659F92122E8EABC789347163F (void);
// 0x00000406 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::LoadOnDevice()
extern void OnLoadStartScene_LoadOnDevice_m9EE1C8972C73480CB013D787E7A0FBF65CAEC004 (void);
// 0x00000407 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::LoadNewScene()
extern void OnLoadStartScene_LoadNewScene_m69315DF288B92BD307BC86F1F1CB6B2FE58675C3 (void);
// 0x00000408 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::.ctor()
extern void OnLoadStartScene__ctor_mA09AB0E4DDFA73414D6F3DBF232FF59E55963D26 (void);
// 0x00000409 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::OnEyeFocusStay()
extern void OnLookAtRotateByEyeGaze_OnEyeFocusStay_mA2D0FEE0C229342825D2FA6D5D57E75619AF1EF1 (void);
// 0x0000040A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::RotateHitTarget()
extern void OnLookAtRotateByEyeGaze_RotateHitTarget_m635E6052BA2FE5C416C8FA6F18CC1F73E5B62EE0 (void);
// 0x0000040B System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::ClampAngleInDegree(System.Single,System.Single,System.Single)
extern void OnLookAtRotateByEyeGaze_ClampAngleInDegree_mB0B27DE6617523C3867E77C728B26413EBC29742 (void);
// 0x0000040C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::.ctor()
extern void OnLookAtRotateByEyeGaze__ctor_m54F302C5AB29F2E03479C35BEE7E25D96E72488A (void);
// 0x0000040D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Start()
extern void DwellSelection_Start_m26131C5585A6D086A1789BC898FE0FAA0BE31306 (void);
// 0x0000040E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::EtTarget_OnTargetSelected(System.Object,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs)
extern void DwellSelection_EtTarget_OnTargetSelected_m8920976D57D2E531DCAD538FAAA85E1CB5E0655A (void);
// 0x0000040F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::EnableDwell()
extern void DwellSelection_EnableDwell_mFFADB227DEF696C5676CE33FAC44CCFCC01A8ABE (void);
// 0x00000410 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::DisableDwell()
extern void DwellSelection_DisableDwell_mDB0594528DF6DF35D1FCD85EFEA14BB0C57BBA08 (void);
// 0x00000411 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_UseDwell()
extern void DwellSelection_get_UseDwell_mCFAE17F0BD28FCABD3E3540FC5076FFA7AF99A23 (void);
// 0x00000412 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStart()
extern void DwellSelection_OnEyeFocusStart_m86466E84D200B34BE3CF4EA88F216152AB9D3B23 (void);
// 0x00000413 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStay()
extern void DwellSelection_OnEyeFocusStay_mE1F92A433993D8AE4A00FF81252C20FC781F2FE1 (void);
// 0x00000414 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStop()
extern void DwellSelection_OnEyeFocusStop_m9C4B6F635BCCAA5893CB822F1109EED149D961ED (void);
// 0x00000415 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::StartDwellFeedback()
extern void DwellSelection_StartDwellFeedback_m39C04C7A743F8D75A6B3AD9443EA0681CE29E732 (void);
// 0x00000416 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::ResetDwellFeedback()
extern void DwellSelection_ResetDwellFeedback_m2B9461B09996D61AD7BE6F4EDF0C06AD063709AC (void);
// 0x00000417 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_SpeedSizeChangePerSecond()
extern void DwellSelection_get_SpeedSizeChangePerSecond_m1DDDEBA6D680AEB9888D7E8FA5F9730DABAD03BE (void);
// 0x00000418 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Update()
extern void DwellSelection_Update_mEC9951AC65D973629CCE691DDC366E99B5A09CDF (void);
// 0x00000419 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::ClampVector3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void DwellSelection_ClampVector3_m6AE9B31FEF019D17D347A735A55FE656ADDA97CA (void);
// 0x0000041A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::UpdateTransparency(System.Single)
extern void DwellSelection_UpdateTransparency_m109B9047A0BAD7FF2D2B2A73AF2F36C6EB003E30 (void);
// 0x0000041B System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::LerpTransparency(System.Single,System.Single,System.Single)
extern void DwellSelection_LerpTransparency_mD84EDBED6528B6BC1E07F5E253EDF238995F0D13 (void);
// 0x0000041C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mA77998B3632503CCE1D5E910E57CC7446942D25B (void);
// 0x0000041D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m6B284F3161920A7949248683FF61206DD3482F81 (void);
// 0x0000041E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m297EE8AAFB38D8A3CC061A7115F818F63EF38A72 (void);
// 0x0000041F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mFC64E1A1C837F0D5CC4EB7A108018A9BE892DC04 (void);
// 0x00000420 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_TextureShaderProperty()
extern void DwellSelection_get_TextureShaderProperty_mF6D0878E318EE601DBB12AE7289EDDCDC7644477 (void);
// 0x00000421 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::set_TextureShaderProperty(System.String)
extern void DwellSelection_set_TextureShaderProperty_m8ABDC9C9BF8A2B0E4C46013494E3D06245EFCD2E (void);
// 0x00000422 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::.ctor()
extern void DwellSelection__ctor_m00D9CBAD7C19D27B475424B3C87ECF0C5C9EF158 (void);
// 0x00000423 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::.cctor()
extern void DwellSelection__cctor_mA736173BF55B05663938C21F429617AD3FF0F5BE (void);
// 0x00000424 Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::get_HitTarget()
extern void TargetEventArgs_get_HitTarget_m2A5AE1ACE5D805D810A47F1C50FF564DF2DF44F0 (void);
// 0x00000425 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::set_HitTarget(Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void TargetEventArgs_set_HitTarget_mD19716DFCABC5AE632B8EA4992BCC31832DEC023 (void);
// 0x00000426 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::.ctor(Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void TargetEventArgs__ctor_m139BCF25211B31F194CF5BEB82B4A9F2E743AB69 (void);
// 0x00000427 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeRenderMode::ChangeRenderModes(UnityEngine.Material,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeRenderMode/BlendMode)
extern void ChangeRenderMode_ChangeRenderModes_mFDC2BA9202CCECEC488C7FF4145C2829DE0DECF1 (void);
// 0x00000428 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DoNotRender::Start()
extern void DoNotRender_Start_m5FDAFABF8A2B7012D56CA1AEDF1E069FEB39BA66 (void);
// 0x00000429 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DoNotRender::.ctor()
extern void DoNotRender__ctor_m59717DCA45151107257DA1C0BD6630CB8AFEB0D7 (void);
// 0x0000042A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeCalibrationChecker::Update()
extern void EyeCalibrationChecker_Update_m79C82B57C73B837B39319DB3AE3707ABDC65E805 (void);
// 0x0000042B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeCalibrationChecker::.ctor()
extern void EyeCalibrationChecker__ctor_mA349C3C484C1921EE4E333D2DC0D5ACCFCC4040F (void);
// 0x0000042C Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::get_Instance()
extern void KeepThisAlive_get_Instance_mFE6D6461311FC9AA2644F7FB8E9F9B78382274CD (void);
// 0x0000042D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::set_Instance(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive)
extern void KeepThisAlive_set_Instance_m7E6EE81F37E81A5B03B89ED652060479A762A5EB (void);
// 0x0000042E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::Awake()
extern void KeepThisAlive_Awake_m32D4F5CD19AA286255F77D998112ECC295038FDD (void);
// 0x0000042F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::Start()
extern void KeepThisAlive_Start_mAA40CC746F4DAEE258ED3AD10F4725F95EBC05A2 (void);
// 0x00000430 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::.ctor()
extern void KeepThisAlive__ctor_m7AE743E0FC87ED0EC8136743410B0E8D726721D2 (void);
// 0x00000431 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::get_Instance()
extern void StatusText_get_Instance_mA4DAF9B004FDE3BCD2D57ADD050177514247218F (void);
// 0x00000432 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Awake()
extern void StatusText_Awake_m9F66EB7110483420B375A31126502F3255124B79 (void);
// 0x00000433 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Start()
extern void StatusText_Start_mD14E96259C7D7006A86055C331F56DA8C4173FE0 (void);
// 0x00000434 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Log(System.String,System.Boolean)
extern void StatusText_Log_m22A9134C95F17165C1020AEF6A62F37DDD4C1873 (void);
// 0x00000435 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::.ctor()
extern void StatusText__ctor_mEE38C8F214A04919137FBDB5919C35367ED47CFA (void);
// 0x00000436 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetValidFilename(System.String)
extern void EyeTrackingDemoUtils_GetValidFilename_m1769969410FB8F47232D227A80EECC173D6D0C2E (void);
// 0x00000437 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetFullName(UnityEngine.GameObject)
extern void EyeTrackingDemoUtils_GetFullName_mB06CD362F096B1E8612A26D9896514DBB725BC9B (void);
// 0x00000438 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetFullName(UnityEngine.GameObject,System.Boolean&)
extern void EyeTrackingDemoUtils_GetFullName_m6011F94619750174835C6731606608D64BDBDEC2 (void);
// 0x00000439 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::Normalize(System.Single,System.Single,System.Single)
extern void EyeTrackingDemoUtils_Normalize_m7102A77A1A6F2050B1C4CF4837156BA3DD92BDFC (void);
// 0x0000043A T[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::RandomizeListOrder(T[])
// 0x0000043B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::VisAngleInDegreesToMeters(UnityEngine.Vector3,System.Single)
extern void EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m1576FDB9B2194F38340A2365F6725CD4400A22BB (void);
// 0x0000043C System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::VisAngleInDegreesToMeters(System.Single,System.Single)
extern void EyeTrackingDemoUtils_VisAngleInDegreesToMeters_mB4B9167DC5777679DF86C4845FBFA4E2E7B02188 (void);
// 0x0000043D System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::LoadNewScene(System.String)
extern void EyeTrackingDemoUtils_LoadNewScene_mD937808E2CA495D5F29C629129C2C6DE9263E28A (void);
// 0x0000043E System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::LoadNewScene(System.String,System.Single)
extern void EyeTrackingDemoUtils_LoadNewScene_mE6E7B24DACB3B93997AABE189E3589D7B4170F6A (void);
// 0x0000043F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeColor(UnityEngine.GameObject,UnityEngine.Color,System.Nullable`1<UnityEngine.Color>&,System.Boolean)
extern void EyeTrackingDemoUtils_GameObject_ChangeColor_mBAD6F4E89DE1D680B3F98E4CD4C4679B7C96233C (void);
// 0x00000440 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeTransparency(UnityEngine.GameObject,System.Single)
extern void EyeTrackingDemoUtils_GameObject_ChangeTransparency_m68980832260767045305D6F6146BAEC882E2B311 (void);
// 0x00000441 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeTransparency(UnityEngine.GameObject,System.Single,System.Single&)
extern void EyeTrackingDemoUtils_GameObject_ChangeTransparency_m184502037227044035227EE9BB674B3E988A1F3F (void);
// 0x00000442 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::Renderers_ChangeTransparency(UnityEngine.Renderer[],System.Single,System.Single&)
extern void EyeTrackingDemoUtils_Renderers_ChangeTransparency_m65E324A122674266F7BD8C5751F65C75E400EF8F (void);
// 0x00000443 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::.ctor(System.Int32)
extern void U3CLoadNewSceneU3Ed__8__ctor_m3A2F1CBA96F1179B92E3A52E1EE44A2B193FF1FA (void);
// 0x00000444 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.IDisposable.Dispose()
extern void U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_m58345FC11B56BAD0D15DB69FD494D264F254761B (void);
// 0x00000445 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::MoveNext()
extern void U3CLoadNewSceneU3Ed__8_MoveNext_mD2AE7E7882DB08130868AE0361D333F82D66F86A (void);
// 0x00000446 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5817BBEAE74A29AEE51F5AEC7E7376B119D031D2 (void);
// 0x00000447 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_m781F3CC9710C2418B6244439728EA2E856C8F44E (void);
// 0x00000448 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_m8C387795FF25C2D3F4383BC593F25C59A3F8A9C9 (void);
// 0x00000449 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::Start()
extern void OnLookAtShowHoverFeedback_Start_m059CD02F7D337D91E93EA98EA7159895E33149B2 (void);
// 0x0000044A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::Update()
extern void OnLookAtShowHoverFeedback_Update_mE004FD57BC941EC83BC5FC6B449824944FAA05BC (void);
// 0x0000044B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnDestroy()
extern void OnLookAtShowHoverFeedback_OnDestroy_m2C3FA5CC8D81FC5A98A86FBA45C408A21595923B (void);
// 0x0000044C System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::NormalizedInterest_Dwell()
extern void OnLookAtShowHoverFeedback_NormalizedInterest_Dwell_mF67325411E93CB4B80609505D0BAADC1AF7D1475 (void);
// 0x0000044D System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::NormalizedDisinterest_LookAway()
extern void OnLookAtShowHoverFeedback_NormalizedDisinterest_LookAway_mCDB9AFB247A9B64E16A8D37BA734A17EFE5AC273 (void);
// 0x0000044E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::DestroyLocalFeedback()
extern void OnLookAtShowHoverFeedback_DestroyLocalFeedback_mCBEEB10FC81A23D56E5887260C6C95C7582C375F (void);
// 0x0000044F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_m518894BE3D5E8CFABE4914BA3EE15A6CBBF6A930 (void);
// 0x00000450 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback_Overlay(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_Overlay_mCF240FED25D2FF7DBD82E8581641502580B649F0 (void);
// 0x00000451 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback_Highlight(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_Highlight_mFDE4E6960674DD10936AFBFFE174BAF6E04FF0B7 (void);
// 0x00000452 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::TransitionAdjustedInterest(System.Single)
extern void OnLookAtShowHoverFeedback_TransitionAdjustedInterest_m1A09B57AB20D30A67E2CB04EDCB627E12201A014 (void);
// 0x00000453 System.Double Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::get_LookAwayTimeInMs()
extern void OnLookAtShowHoverFeedback_get_LookAwayTimeInMs_mC97D3065EF61567B9DE4D2F95DE54ADABFE98C56 (void);
// 0x00000454 System.Double Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::get_DwellTimeInMs()
extern void OnLookAtShowHoverFeedback_get_DwellTimeInMs_m04854C77D70088945F4297294AE4F10A70F4F6D2 (void);
// 0x00000455 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnLookAtStop()
extern void OnLookAtShowHoverFeedback_OnLookAtStop_mC1B7AB41559D84E40BC8E39B4442A5B28BDAB8CA (void);
// 0x00000456 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnLookAtStart()
extern void OnLookAtShowHoverFeedback_OnLookAtStart_m00A48B6842673BFCF201BCA1BA0E6FCEC9ED8F0C (void);
// 0x00000457 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::SaveOriginalColor()
extern void OnLookAtShowHoverFeedback_SaveOriginalColor_m3D9AED9FBF4DBA54885F4B39F9DA2B3FE3178193 (void);
// 0x00000458 UnityEngine.Color[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::GetColorsByProperty(System.String,UnityEngine.Renderer[])
extern void OnLookAtShowHoverFeedback_GetColorsByProperty_m454147565927415A1C6CFEF157FB85D518AAD53B (void);
// 0x00000459 UnityEngine.Color Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::BlendColors(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void OnLookAtShowHoverFeedback_BlendColors_mA7110F451ACD334FA6B36D8A52E352A1F2BFB067 (void);
// 0x0000045A System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::divBlendColor(System.Single,System.Single,System.Single)
extern void OnLookAtShowHoverFeedback_divBlendColor_mCAD946E68F7D4570F18AF6F5598A79DD4D3D1E94 (void);
// 0x0000045B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::.ctor()
extern void OnLookAtShowHoverFeedback__ctor_mA02E007E932886BA028CCADFF7DE0999EB6DBC35 (void);
// 0x0000045C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task>)
extern void AsyncHelpers_RunSync_m749F9636EFAD43F9F8F564D79351D869A4A72D04 (void);
// 0x0000045D T Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task`1<T>>)
// 0x0000045E System.Exception Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::get_InnerException()
extern void ExclusiveSynchronizationContext_get_InnerException_mF62B2FC120AAA96FDFAB07DCA41B1FDCF2930D44 (void);
// 0x0000045F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::set_InnerException(System.Exception)
extern void ExclusiveSynchronizationContext_set_InnerException_mBDAC674E7236860DE7EE3A2B36A9CFFCD21E5680 (void);
// 0x00000460 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Send_mECC1E7E76598E502E9F3BFCFE01A7343189C3E1D (void);
// 0x00000461 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Post_m626611A2FD0A186362E88D01FCB2F44650B079DA (void);
// 0x00000462 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::EndMessageLoop()
extern void ExclusiveSynchronizationContext_EndMessageLoop_mDBA1461B9A1A176909AB4849C1D5F2AEE21F3F8A (void);
// 0x00000463 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::BeginMessageLoop()
extern void ExclusiveSynchronizationContext_BeginMessageLoop_m3F18EC869AB291241D7F81C1486909145A170563 (void);
// 0x00000464 System.Threading.SynchronizationContext Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::CreateCopy()
extern void ExclusiveSynchronizationContext_CreateCopy_mAB38AC92A4462D1EC4102F7ABD78C7D22B5C03A1 (void);
// 0x00000465 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::.ctor()
extern void ExclusiveSynchronizationContext__ctor_m55B3E14A95501A29CB0BCF55B3CD4E0920EAE217 (void);
// 0x00000466 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::<EndMessageLoop>b__9_0(System.Object)
extern void ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m9BE0BB978BCBC49997EAF9109511A0A0EC52C213 (void);
// 0x00000467 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mAD5C24DE56C36DB5CEF2C89CAE901EA304E40153 (void);
// 0x00000468 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0::<RunSync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mF614B85D7EC692BA75E582FF3216638A145288E5 (void);
// 0x00000469 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::MoveNext()
extern void U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m4D4B2A2F50874C4C173492BC496BD868518AD19E (void);
// 0x0000046A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m0B908A49592D587220B0D4F5EDF8DC82DCCC2431 (void);
// 0x0000046B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1::.ctor()
// 0x0000046C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1::<RunSync>b__0(System.Object)
// 0x0000046D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::MoveNext()
// 0x0000046E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x0000046F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SetUserName(System.String)
extern void BasicInputLogger_SetUserName_m0F18AE9CAB8E242D62BE322EBC8304D045E650C8 (void);
// 0x00000470 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SetSessionDescr(System.String)
extern void BasicInputLogger_SetSessionDescr_m85B3A0308F86431BBDC10609410D007D2A14764C (void);
// 0x00000471 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_LogDirectory()
extern void BasicInputLogger_get_LogDirectory_mEC6DB4DC9AFC7F80A48E46018DB9421D6603F651 (void);
// 0x00000472 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::GetHeader()
// 0x00000473 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::GetFileName()
// 0x00000474 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::CreateNewLogFile()
extern void BasicInputLogger_CreateNewLogFile_mEE3B1C07FA9A4B6F75F9F3C1A7754A2373DC62DD (void);
// 0x00000475 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::CheckIfInitialized()
extern void BasicInputLogger_CheckIfInitialized_mEC57E59C70E58D9C116C234657CD1724CA187DC7 (void);
// 0x00000476 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::ResetLog()
extern void BasicInputLogger_ResetLog_mA7BC0B6794FEFFB09DB51DBFC80FD308E7600E9B (void);
// 0x00000477 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FormattedTimeStamp()
extern void BasicInputLogger_get_FormattedTimeStamp_m644D10667B8CB4AC4A38BA9CAF9233FB3720C19A (void);
// 0x00000478 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::AddLeadingZeroToSingleDigitIntegers(System.Int32)
extern void BasicInputLogger_AddLeadingZeroToSingleDigitIntegers_mE8EF6E463EA00E4831D81FAF72950A3491751F04 (void);
// 0x00000479 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::Append(System.String)
extern void BasicInputLogger_Append_m3BB3B3F37660A66253AB486ECF859FFF71BBA9D0 (void);
// 0x0000047A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::LoadLogs()
extern void BasicInputLogger_LoadLogs_m263523BC5198887EB7ABE226A4F4CD1330B241DC (void);
// 0x0000047B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SaveLogs()
extern void BasicInputLogger_SaveLogs_m4C965A36341E67DE7F0D1B1F143C6B386FFC18E3 (void);
// 0x0000047C System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_Filename()
extern void BasicInputLogger_get_Filename_m8564B5352FFA1F2BD9A741A34CEE88EC21BE5BF6 (void);
// 0x0000047D System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FilenameWithTimestamp()
extern void BasicInputLogger_get_FilenameWithTimestamp_m4BDC98552F2129CCCEAA73513F81864A03D1B80E (void);
// 0x0000047E System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FilenameNoTimestamp()
extern void BasicInputLogger_get_FilenameNoTimestamp_m49EDFC8CBE04395ACB778668B53C0552E3D4775F (void);
// 0x0000047F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::OnDestroy()
extern void BasicInputLogger_OnDestroy_mB7B71D726184C6E314CE53C7F4CACA4A674AB707 (void);
// 0x00000480 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::.ctor()
extern void BasicInputLogger__ctor_m13449FB20D73D923A5035E07077BCD4DBCD64566 (void);
// 0x00000481 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::MoveNext()
extern void U3CCreateNewLogFileU3Ed__14_MoveNext_m51610C8380D963ECD36549D7396D660051DC203D (void);
// 0x00000482 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateNewLogFileU3Ed__14_SetStateMachine_mE9AF39239F16491AA8140EA2885F104078421DE0 (void);
// 0x00000483 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::MoveNext()
extern void U3CLoadLogsU3Ed__21_MoveNext_mBFBDA1A9170A471CB4AB057033A5665AF89420E1 (void);
// 0x00000484 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadLogsU3Ed__21_SetStateMachine_m52CD7696C92235AB638D02D5D50AD1F4E2FD8CE0 (void);
// 0x00000485 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::MoveNext()
extern void U3CSaveLogsU3Ed__22_MoveNext_m2DBD29F6521D3A72E1288518B9ACE7E6BFCB9D3C (void);
// 0x00000486 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveLogsU3Ed__22_SetStateMachine_m08AA69B7E317EF2D112C6AD190A55B92EC99840D (void);
// 0x00000487 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CustomAppend(System.String)
extern void CustomInputLogger_CustomAppend_m034F789A2E14846879796B9D27CF62BAB8C5C49D (void);
// 0x00000488 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CreateNewLog()
extern void CustomInputLogger_CreateNewLog_m47BF3C4F791E26F603003A079B76B6DEB0EA3385 (void);
// 0x00000489 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::StartLogging()
extern void CustomInputLogger_StartLogging_mEAEF562633A9EBE87A561E52D91FD13861C582AA (void);
// 0x0000048A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::StopLoggingAndSave()
extern void CustomInputLogger_StopLoggingAndSave_m5132B60BBE50403150C4FE95C00C3747AA950241 (void);
// 0x0000048B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CancelLogging()
extern void CustomInputLogger_CancelLogging_mA8701DBBABCFEC2EC042B6EECE331332BCC7E654 (void);
// 0x0000048C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::.ctor()
extern void CustomInputLogger__ctor_mF55E765FD4C24BA1114E38DBEF1ED070FAF6FC2F (void);
// 0x0000048D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::Start()
extern void InputPointerVisualizer_Start_m89871394B7B69221AAA2D03C23680E21E1C58C2F (void);
// 0x0000048E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVisualizations()
extern void InputPointerVisualizer_ResetVisualizations_m6847E8783C70355193DF17FFA8C91A770910864E (void);
// 0x0000048F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::InitPointClouds(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap,System.Int32)
extern void InputPointerVisualizer_InitPointClouds_m973CB9C4AC370704992BC1CB7FD87D224E736D88 (void);
// 0x00000490 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::InitVisArrayObj(UnityEngine.GameObject[]&,UnityEngine.GameObject,System.Int32)
extern void InputPointerVisualizer_InitVisArrayObj_m6F50ED1ADEF2BDAD3959AC69EE330B92F0B0A9BF (void);
// 0x00000491 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVis()
extern void InputPointerVisualizer_ResetVis_m0C48AACCD9DA5F64DBBDE76E8ED62E60D158FA70 (void);
// 0x00000492 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVis(UnityEngine.GameObject[]&)
extern void InputPointerVisualizer_ResetVis_m9665A3FE91EA6F6F40FD6A25F148B970BDEB8EB4 (void);
// 0x00000493 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetPointCloudVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&)
extern void InputPointerVisualizer_ResetPointCloudVis_mFBC9BA65DC1E64D253D3D7D6205E54A7882609E8 (void);
// 0x00000494 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer/VisModes)
extern void InputPointerVisualizer_SetActive_DataVis_m0C93E38A1CAEBD3107C1C1EF9DC9E137EC507B86 (void);
// 0x00000495 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void InputPointerVisualizer_SetActive_DataVis_mD35978CCB6F81F65B815FDD5D878B953A3C71BAE (void);
// 0x00000496 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(UnityEngine.GameObject[]&,System.Boolean)
extern void InputPointerVisualizer_SetActive_DataVis_m5BEE4C203346C18456E8BBC88348B8364399B8F7 (void);
// 0x00000497 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_PointCloudVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,System.Boolean)
extern void InputPointerVisualizer_SetActive_PointCloudVis_m18DC4A1818F41A0E13EFE40E30E58D4C10D238DC (void);
// 0x00000498 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateDataVisPos(UnityEngine.GameObject[]&,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateDataVisPos_m5A994AAA9DDAFE8E82505188929A0C19AFFE9ECF (void);
// 0x00000499 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateVis_PointCloud(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateVis_PointCloud_m4FE5899CB908CC587AC1316855A4C0FC22342C30 (void);
// 0x0000049A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateConnectorLines(UnityEngine.GameObject[]&,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateConnectorLines_m3861799033ED9C1F184E88B179A54B44DB2485DD (void);
// 0x0000049B System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::GetPrevLineIndex(System.Int32,System.Int32)
extern void InputPointerVisualizer_GetPrevLineIndex_m7DB648F08CBA65FDCEADB33CD4FD3E883CD2F59B (void);
// 0x0000049C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateDataVis(UnityEngine.Ray)
extern void InputPointerVisualizer_UpdateDataVis_m8411A7A7997785E0E7C895A21D77D6D25AC597BA (void);
// 0x0000049D System.Nullable`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::PerformHitTest(UnityEngine.Ray)
extern void InputPointerVisualizer_PerformHitTest_m2A09568D6836438E8BCFD1F783BCA97C98A3B1AF (void);
// 0x0000049E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::Update()
extern void InputPointerVisualizer_Update_m91DA053457D0F5E0137A59CCE4CD9F73C0315E2B (void);
// 0x0000049F System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::IsDwelling()
extern void InputPointerVisualizer_IsDwelling_m32FED22023C69C94BB66DA356F8C1597694BFFE6 (void);
// 0x000004A0 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::get_AmountOfSamples()
extern void InputPointerVisualizer_get_AmountOfSamples_mB43F7C198CF1FA1EA69C889E1DB9263766EA2F12 (void);
// 0x000004A1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::set_AmountOfSamples(System.Int32)
extern void InputPointerVisualizer_set_AmountOfSamples_m087B7ACAD2EB276230D5771B8D82402D80C5A0D4 (void);
// 0x000004A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ToggleAppState()
extern void InputPointerVisualizer_ToggleAppState_m22D601F3A252368492D61DA8E7C3FC51DF4799A1 (void);
// 0x000004A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::PauseApp()
extern void InputPointerVisualizer_PauseApp_mDDEC107A59C29B0B2159CF3024BB95DB1D6A3E16 (void);
// 0x000004A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UnpauseApp()
extern void InputPointerVisualizer_UnpauseApp_m5AFBB94CCBAB9A39367E3BF516AC06D4CC70FD07 (void);
// 0x000004A5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetAppState(System.Boolean)
extern void InputPointerVisualizer_SetAppState_m30B822699BA7E422DDC27CE328B73125FCEDE1F3 (void);
// 0x000004A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::.ctor()
extern void InputPointerVisualizer__ctor_m2BCE260DCEF580704F2C8480DB6775667239E23C (void);
// 0x000004A7 System.String[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::GetHeaderColumns()
extern void LogStructure_GetHeaderColumns_m0047E48DBE6BB223DE37365C81DF1F732ACAC695 (void);
// 0x000004A8 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::GetData(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void LogStructure_GetData_m4B4479666DE3093EB2D2F5FAFFA46E99A77E3FD1 (void);
// 0x000004A9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::.ctor()
extern void LogStructure__ctor_m0182A0D086BD5CCFAF13309374DAFF68F4C79DFA (void);
// 0x000004AA Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::get_EyeTrackingProvider()
extern void LogStructureEyeGaze_get_EyeTrackingProvider_m94D080F7DF046AFD77E75061F8FEA8D902373D41 (void);
// 0x000004AB System.String[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::GetHeaderColumns()
extern void LogStructureEyeGaze_GetHeaderColumns_mC038607A40573425E7DA6BC2D27199BDF6B724D0 (void);
// 0x000004AC System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::GetData(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void LogStructureEyeGaze_GetData_m67095F0D44011A0E990603891CF4D7737B3FE70A (void);
// 0x000004AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::.ctor()
extern void LogStructureEyeGaze__ctor_m89EAC1BCCBE3C3A8B288FBD6D4F83938D75B9F83 (void);
// 0x000004AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Start()
extern void UserInputPlayback_Start_m54AB6E8D8253EC21DB6E8524B04A0BBC91BEF7D4 (void);
// 0x000004AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ResetCurrentStream()
extern void UserInputPlayback_ResetCurrentStream_mBD6BAE2CF723C8C32AABD93EF19BE24D7DA2B52D (void);
// 0x000004B0 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_Load()
extern void UserInputPlayback_UWP_Load_m2B55367B83F51DF8C569F21506D151C0E80973C0 (void);
// 0x000004B1 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_LoadNewFile(System.String)
extern void UserInputPlayback_UWP_LoadNewFile_m7796B983149F6B8E3AEBFFC316F60F8395AA65F7 (void);
// 0x000004B2 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_FileExists(System.String,System.String)
extern void UserInputPlayback_UWP_FileExists_mB5CAD0835C681AC363FE9A4B5330CDC6121B8BAB (void);
// 0x000004B3 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_ReadData(Windows.Storage.StorageFile)
extern void UserInputPlayback_UWP_ReadData_mB31485383089D9766D0B24E497A9B9F37E3E5E35 (void);
// 0x000004B4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadNewFile(System.String)
extern void UserInputPlayback_LoadNewFile_m01CB44DDF2B697A82D2EE077DE93C28FFF5F850E (void);
// 0x000004B5 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::TryParseStringToVector3(System.String,System.String,System.String,System.Boolean&)
extern void UserInputPlayback_TryParseStringToVector3_m666C74C9F35DA534F7E5451BF6DA5B3110CB4C5F (void);
// 0x000004B6 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ParseStringToFloat(System.String)
extern void UserInputPlayback_ParseStringToFloat_m0050255DE52420E8DE57E2C035EA0C1B691EA9F9 (void);
// 0x000004B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Load()
extern void UserInputPlayback_Load_m46C9F6D4927F66AD61CB6A0F549F379FE0ED8746 (void);
// 0x000004B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadInEditor()
extern void UserInputPlayback_LoadInEditor_mF677C262C5F4C51ACEC7C6591E3BDADBF11A2C9C (void);
// 0x000004B9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadInUWP()
extern void UserInputPlayback_LoadInUWP_m7A8AA7608180B430BE699238462FC3452C81AEEE (void);
// 0x000004BA System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_FileName()
extern void UserInputPlayback_get_FileName_m8D74BE21E7A22F5B0E1FD238B85BA9B173209F1B (void);
// 0x000004BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::set_IsPlaying(System.Boolean)
extern void UserInputPlayback_set_IsPlaying_m972CCD61E1228F96DEC35B7A32AD4D82F247B439 (void);
// 0x000004BC System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_IsPlaying()
extern void UserInputPlayback_get_IsPlaying_mCCC5F1149ECAFACBEB06A7264006EC59DD4824BA (void);
// 0x000004BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Play()
extern void UserInputPlayback_Play_mC59A6B44FFE6CC1F14594D2850AE8CEC9498368B (void);
// 0x000004BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Pause()
extern void UserInputPlayback_Pause_m0B6D744ACAC9ABA0E6A1E99735B65809D7BC37EC (void);
// 0x000004BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Clear()
extern void UserInputPlayback_Clear_m5ACE3831BC2D633217DF9B15F40E0F23A07E2ADE (void);
// 0x000004C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::SpeedUp()
extern void UserInputPlayback_SpeedUp_mCC7E61525B0C8E111BE6CB59FD928E54E65BC032 (void);
// 0x000004C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::SlowDown()
extern void UserInputPlayback_SlowDown_m494CD1A1B515238F34835B8D708ECB1BE357E372 (void);
// 0x000004C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowAllAndFreeze()
extern void UserInputPlayback_ShowAllAndFreeze_mFDB6D7F4BADF206989A90E065884A8F59C4314CB (void);
// 0x000004C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowAllAndFreeze(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void UserInputPlayback_ShowAllAndFreeze_m5DC6645984B47D78E3F82E812ADE8AF779C3B845 (void);
// 0x000004C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowHeatmap()
extern void UserInputPlayback_ShowHeatmap_mD9B93CC89FDDA7275FCF3AF3DF0FE24EDA6680B8 (void);
// 0x000004C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadingStatus_Hide()
extern void UserInputPlayback_LoadingStatus_Hide_m757A84635080D2B7A78B6B4174F9E561DD4BE108 (void);
// 0x000004C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadingStatus_Show()
extern void UserInputPlayback_LoadingStatus_Show_mBBC3FC8557AF8603331E13D732274344377BB043 (void);
// 0x000004C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateLoadingStatus(System.Int32,System.Int32)
extern void UserInputPlayback_UpdateLoadingStatus_m78A0F2B88C23D5545FE0BFDC8416542F523EBF13 (void);
// 0x000004C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Log(System.String)
extern void UserInputPlayback_Log_m8139F7014EC270F6AB273857867644964D8DE42D (void);
// 0x000004C9 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::PopulateHeatmap()
extern void UserInputPlayback_PopulateHeatmap_m637AA234900CF056E60B7043CF62C0EA2611101A (void);
// 0x000004CA System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateStatus(System.Single)
extern void UserInputPlayback_UpdateStatus_mCDE75363C04014A6F1C1AE6600B5C9011AD381B1 (void);
// 0x000004CB System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::AddToCounter(System.Single)
extern void UserInputPlayback_AddToCounter_mB9134E39F625B862A75A763532D0931B59F08C04 (void);
// 0x000004CC System.Nullable`1<UnityEngine.Ray> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::GetEyeRay(System.String[])
extern void UserInputPlayback_GetEyeRay_m8B68E9E5EBE2F05410292AAF1B6623B36A6013AD (void);
// 0x000004CD System.Nullable`1<UnityEngine.Ray> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::GetRay(System.String,System.String,System.String,System.String,System.String,System.String)
extern void UserInputPlayback_GetRay_mFBEF18D743487F92C604E39B22234F78B4CB9C9A (void);
// 0x000004CE System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_DataIsLoaded()
extern void UserInputPlayback_get_DataIsLoaded_mE578EA092A33EA8E79DA30578A2E1F189BDFAA07 (void);
// 0x000004CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateTimestampForNextReplay(System.String[])
extern void UserInputPlayback_UpdateTimestampForNextReplay_m406E3B154B4AEF5ED5633C84E220D12119BB5161 (void);
// 0x000004D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateEyeGazeSignal(System.String[],Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateEyeGazeSignal_mE7C51CDF1A73150A61F1BFBC8FC05001C7B0EA56 (void);
// 0x000004D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateHeadGazeSignal(System.String[],Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateHeadGazeSignal_m9B3F6951772CF3D598BBF27C0438813CCF952AFD (void);
// 0x000004D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateTargetingSignal(System.String,System.String,System.String,System.String,System.String,System.String,UnityEngine.Ray,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateTargetingSignal_m9BB1C4774454102B34D25E4FA65C44C4A8675A8E (void);
// 0x000004D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Update()
extern void UserInputPlayback_Update_m19FB263ECBBA12D19B548FF3A994B445C35A9048 (void);
// 0x000004D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::PlayNext()
extern void UserInputPlayback_PlayNext_mBAD27CCB948DE35AABF9E38BD6DEF1081A00A9C4 (void);
// 0x000004D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::.ctor()
extern void UserInputPlayback__ctor_m378AB66FB7D5934D1533F4A8DBB325B2DA3BCA3D (void);
// 0x000004D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::.cctor()
extern void UserInputPlayback__cctor_m3C7EBF9D548CEB4CEAFF08D9A24B128BBFEE317F (void);
// 0x000004D7 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::<ShowAllAndFreeze>b__34_0()
extern void UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_m582F19E7B0B2E5B68B1203A7189A0EF2C6D10117 (void);
// 0x000004D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::MoveNext()
extern void U3CUWP_LoadU3Ed__12_MoveNext_mF50633445CA445C15EB96BCBFEE6B0D0EAE5750B (void);
// 0x000004D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_LoadU3Ed__12_SetStateMachine_m591DA891C7D71B6BE2A2BEF8B43C3085CA166AD0 (void);
// 0x000004DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::MoveNext()
extern void U3CUWP_LoadNewFileU3Ed__13_MoveNext_mB09A0EF138A0B6388ACD97A9551D04E6A72180F2 (void);
// 0x000004DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m0662CBA1196DF3F7A847D11CC714071670225D45 (void);
// 0x000004DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::MoveNext()
extern void U3CUWP_FileExistsU3Ed__14_MoveNext_m45168326C1D3F26DA0853F2BB2770F5A2175DA0C (void);
// 0x000004DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_FileExistsU3Ed__14_SetStateMachine_m0E7C98FD2BBBB9620A93E4779B89C2437C450B25 (void);
// 0x000004DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::MoveNext()
extern void U3CUWP_ReadDataU3Ed__15_MoveNext_mC1061B1A4D9E4ECF16413DCB85C41EC0C215C381 (void);
// 0x000004DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_ReadDataU3Ed__15_SetStateMachine_m254C3E9028A12B79C84A1945BE7B7D5A04D7B183 (void);
// 0x000004E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::MoveNext()
extern void U3CLoadInUWPU3Ed__21_MoveNext_m43043EEB6C1601ECF81ABB1FD6933A79D82F134B (void);
// 0x000004E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadInUWPU3Ed__21_SetStateMachine_m7F5529D96FD69AE974D6B8E3417740D88B06C266 (void);
// 0x000004E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::.ctor(System.Int32)
extern void U3CPopulateHeatmapU3Ed__42__ctor_mD7F558F041990420DF91A544D2EC0E4A12569DF7 (void);
// 0x000004E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.IDisposable.Dispose()
extern void U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m968703DCCF8F30F6E5AB40CA817BD294700AE49C (void);
// 0x000004E4 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::MoveNext()
extern void U3CPopulateHeatmapU3Ed__42_MoveNext_m5A43B1C0A8D1E446A94BB3B299D35C562E9580B2 (void);
// 0x000004E5 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEBA75C228FBBCD232FA4C0E03AE58697C7893E34 (void);
// 0x000004E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.IEnumerator.Reset()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m7222AD3D3A78DFF63D494B95D7A11907B60AE9F4 (void);
// 0x000004E7 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_m3D1E82CFFE98A8F15C08C42151C37B3270E0FB93 (void);
// 0x000004E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::.ctor(System.Int32)
extern void U3CUpdateStatusU3Ed__43__ctor_mE161EBBFEF609B0F65CD0CE15945C1D1EC6FC4AF (void);
// 0x000004E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.IDisposable.Dispose()
extern void U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_m52E11C9DDCEC6F3629A428A7ADB9CAE81F1470BB (void);
// 0x000004EA System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::MoveNext()
extern void U3CUpdateStatusU3Ed__43_MoveNext_m5B4572E9557830322F0748570F651C40F42400FB (void);
// 0x000004EB System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8074B2EB191D1B18FBBB93DD55185C12E4317327 (void);
// 0x000004EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.IEnumerator.Reset()
extern void U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_m0AD9A16C4686B50EC28E28CFFF3BF5F2D0D3E5A0 (void);
// 0x000004ED System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_mAAE632C13CA80355BC2F8C7B0F9E61C892EFFE1D (void);
// 0x000004EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::.ctor(System.Int32)
extern void U3CAddToCounterU3Ed__44__ctor_mC4B3BCCCF949C0BAF5F66E437C38954E177E500E (void);
// 0x000004EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.IDisposable.Dispose()
extern void U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mD3303648584768F0BE84E38A10E6D2358B72D511 (void);
// 0x000004F0 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::MoveNext()
extern void U3CAddToCounterU3Ed__44_MoveNext_m453A54E1D482866BDDF7602492A710C1F2D31256 (void);
// 0x000004F1 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EC981668509CD31D32AB48230F40F16415C6C34 (void);
// 0x000004F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.IEnumerator.Reset()
extern void U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_mF63C30885BB8BF2D12A452E0152B216737529A33 (void);
// 0x000004F3 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_mE233AE8D42A08801E4AC5CCD7721E66B1C2BCC1C (void);
// 0x000004F4 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::get_Instance()
extern void UserInputRecorder_get_Instance_m2AD896F101FD5D1165B98E57394696664DD53833 (void);
// 0x000004F5 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetHeader()
extern void UserInputRecorder_GetHeader_m2B8F52FC9EDDE9B4DF786A0D4BC2461A355F8AD0 (void);
// 0x000004F6 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetData_Part1()
extern void UserInputRecorder_GetData_Part1_m8F9987DF7BD3BB6F4F60C50A886C2A4456251C26 (void);
// 0x000004F7 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::MergeObjArrays(System.Object[],System.Object[])
extern void UserInputRecorder_MergeObjArrays_mBDA409BEF5E657E4AFB2478AB03F8F80902859FC (void);
// 0x000004F8 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetFileName()
extern void UserInputRecorder_GetFileName_m24FCA6F95DE59856189C807AAB833B3A29CABFE1 (void);
// 0x000004F9 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::LimitStringLength(System.String,System.Int32)
extern void UserInputRecorder_LimitStringLength_mDEF0CC2CF9570097CDB352C5C4817ED8EC0374D3 (void);
// 0x000004FA System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetStringFormat(System.Object[])
extern void UserInputRecorder_GetStringFormat_m1A3616A837821EECFE95837D2A7EE963CD35A516 (void);
// 0x000004FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::UpdateLog(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void UserInputRecorder_UpdateLog_m019FA69FB87AA0CB3EF0E911720FC538A1227571 (void);
// 0x000004FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::CustomAppend(System.String)
extern void UserInputRecorder_CustomAppend_m063047DFEEBB9318F93AE1C92B8FE0AB39F90C80 (void);
// 0x000004FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::UpdateLog()
extern void UserInputRecorder_UpdateLog_m2187053D96641A9B676B2B2BFE675494EB6338F9 (void);
// 0x000004FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::Update()
extern void UserInputRecorder_Update_mDCA18C5D8F130482E4EF8E9FE594C9AEF3FCF19E (void);
// 0x000004FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::OnDestroy()
extern void UserInputRecorder_OnDestroy_m6C6D8A14BE0BA04AAF807C6B86FE4CE38CE812E4 (void);
// 0x00000500 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::.ctor()
extern void UserInputRecorder__ctor_m523C9771D6DD426B36AE3502B802F067D412D707 (void);
// 0x00000501 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::PlayAudio(UnityEngine.AudioClip)
extern void UserInputRecorderFeedback_PlayAudio_m855CCDDA5C75FA7234431D3B41DD944CB88826D6 (void);
// 0x00000502 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::UpdateStatusText(System.String)
extern void UserInputRecorderFeedback_UpdateStatusText_m6D5FFFF743C7FD9A0DB40F1E803DC4A3CDE90D3D (void);
// 0x00000503 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::ResetStatusText()
extern void UserInputRecorderFeedback_ResetStatusText_mC6A8C2050421A7BA49164F7BFE12B2497CEA39D8 (void);
// 0x00000504 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::Update()
extern void UserInputRecorderFeedback_Update_m7B60199FF60C5FAD16C61F02359A9628323D76EE (void);
// 0x00000505 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StartRecording()
extern void UserInputRecorderFeedback_StartRecording_m29FE7A7D706928AA6283B26EC8951ACE28FCD6C1 (void);
// 0x00000506 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StopRecording()
extern void UserInputRecorderFeedback_StopRecording_m52FF35ADB862C4B98960D9D3B95034E415ACEC7D (void);
// 0x00000507 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::LoadData()
extern void UserInputRecorderFeedback_LoadData_m8C4ACCC0F62BAD4CD38112AE376BB8B582F97036 (void);
// 0x00000508 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StartReplay()
extern void UserInputRecorderFeedback_StartReplay_m55E941EED25826F76FF0145E94CDBD8D95356876 (void);
// 0x00000509 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::PauseReplay()
extern void UserInputRecorderFeedback_PauseReplay_m4665C9A3DF1BD59CFEF1C26A9552E71CD4022541 (void);
// 0x0000050A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::.ctor()
extern void UserInputRecorderFeedback__ctor_m7784EA7A5D02EE62EDD8F542CE2E7B1394DED1E0 (void);
// 0x0000050B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::Start()
extern void UserInputRecorderUIController_Start_mAAE1DAF1092EF41454A58BB933FE4CABD32F041D (void);
// 0x0000050C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StartRecording()
extern void UserInputRecorderUIController_StartRecording_m7DBB3F9EF8D6964695E027F4AA22811DD5A7A668 (void);
// 0x0000050D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StopRecording()
extern void UserInputRecorderUIController_StopRecording_mB4B352D3D43090FF21A0D84F0CEF74224243D546 (void);
// 0x0000050E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::RecordingUI_Reset(System.Boolean)
extern void UserInputRecorderUIController_RecordingUI_Reset_mBC9DD0DDA89A8E0E413ABE854391D9ADA802C1E3 (void);
// 0x0000050F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::LoadData()
extern void UserInputRecorderUIController_LoadData_m379CC243B823D3A636AEC87FD377DE38FC7E3F8E (void);
// 0x00000510 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::ReplayUI_SetActive(System.Boolean)
extern void UserInputRecorderUIController_ReplayUI_SetActive_m21E6B70283000B375DF81F4DD2DC18C594C80496 (void);
// 0x00000511 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StartReplay()
extern void UserInputRecorderUIController_StartReplay_m3132F5241D80529B61E6CE1505AD21BDEC40A590 (void);
// 0x00000512 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::PauseReplay()
extern void UserInputRecorderUIController_PauseReplay_m8D7242E8CB657CD67D23BBF5EEE61FAD8F274C5C (void);
// 0x00000513 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::ResetPlayback(System.Boolean,System.Boolean)
extern void UserInputRecorderUIController_ResetPlayback_mCD89B5E8CEFF741885F7E7590DEF482C53D263A5 (void);
// 0x00000514 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::.ctor()
extern void UserInputRecorderUIController__ctor_m915DA99929AE13AAC143C1DA673BC4F236164B1B (void);
// 0x00000515 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_AutoStart()
extern void TapToPlace_get_AutoStart_m6374D48DC107466BB0FAAB1480ECB1236365E1BF (void);
// 0x00000516 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_AutoStart(System.Boolean)
extern void TapToPlace_set_AutoStart_m489E97811C6AAA422BB49DB22E2BC28F5D77F9B9 (void);
// 0x00000517 System.Single Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_DefaultPlacementDistance()
extern void TapToPlace_get_DefaultPlacementDistance_m760304B5AD2203B6BDB8AE81C1AD8104C824B68B (void);
// 0x00000518 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_DefaultPlacementDistance(System.Single)
extern void TapToPlace_set_DefaultPlacementDistance_m85427D0C199F34B01527D736F154EDE174ECBD29 (void);
// 0x00000519 System.Single Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_MaxRaycastDistance()
extern void TapToPlace_get_MaxRaycastDistance_m56A592BB8CB3961CFCB8EF4020D9ED1498428A72 (void);
// 0x0000051A System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_MaxRaycastDistance(System.Single)
extern void TapToPlace_set_MaxRaycastDistance_m1DEB527C7ED530D08C18F246FE45EF3D75250570 (void);
// 0x0000051B System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_IsBeingPlaced()
extern void TapToPlace_get_IsBeingPlaced_m9DB86AC090F7A1F2CB11EE1EC10C4A80EA797614 (void);
// 0x0000051C System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_IsBeingPlaced(System.Boolean)
extern void TapToPlace_set_IsBeingPlaced_m7991270BCF77F77018715BF41AA87339E250F25F (void);
// 0x0000051D System.Single Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_SurfaceNormalOffset()
extern void TapToPlace_get_SurfaceNormalOffset_m058966A0B6194ACF7C0CDA0C347D98E35AEC32D5 (void);
// 0x0000051E System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_SurfaceNormalOffset(System.Single)
extern void TapToPlace_set_SurfaceNormalOffset_m3040E9A012EE9E635E3E13AA3DE7566AAB9B5DDC (void);
// 0x0000051F System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_KeepOrientationVertical()
extern void TapToPlace_get_KeepOrientationVertical_m8A6B6FE6208E10FA41E732E6EECB843E46ED1B46 (void);
// 0x00000520 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_KeepOrientationVertical(System.Boolean)
extern void TapToPlace_set_KeepOrientationVertical_m25AC11588192589566C071B9865D5C55521EA6D0 (void);
// 0x00000521 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_RotateAccordingToSurface()
extern void TapToPlace_get_RotateAccordingToSurface_m610A8CC1409498021D3B58FF579391BE847A768D (void);
// 0x00000522 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_RotateAccordingToSurface(System.Boolean)
extern void TapToPlace_set_RotateAccordingToSurface_m00231C60960FAE5D7CEAD8C40156ED9A7FB9D841 (void);
// 0x00000523 UnityEngine.LayerMask[] Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_MagneticSurfaces()
extern void TapToPlace_get_MagneticSurfaces_m24C05D9D7C221A38B7C133F05685F3507BEB5539 (void);
// 0x00000524 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_MagneticSurfaces(UnityEngine.LayerMask[])
extern void TapToPlace_set_MagneticSurfaces_m03443E0FBD8716769975C733816F93AA1E5F717B (void);
// 0x00000525 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_DebugEnabled()
extern void TapToPlace_get_DebugEnabled_m3370364D2A1AA51040B12394F879707588FB56E2 (void);
// 0x00000526 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_DebugEnabled(System.Boolean)
extern void TapToPlace_set_DebugEnabled_m5243049BFB7D75138F08D79C03CEC953B8F1B387 (void);
// 0x00000527 UnityEngine.Events.UnityEvent Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_OnPlacingStarted()
extern void TapToPlace_get_OnPlacingStarted_mE442436C9D044BEAE8A3905C665AF1B5A4A69A27 (void);
// 0x00000528 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_OnPlacingStarted(UnityEngine.Events.UnityEvent)
extern void TapToPlace_set_OnPlacingStarted_mD19936891BC2EE2D83FEAB438C7D37385C1DA639 (void);
// 0x00000529 UnityEngine.Events.UnityEvent Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_OnPlacingStopped()
extern void TapToPlace_get_OnPlacingStopped_m9B20DA76E41E89A92582347949AB3EDBEF6F81CF (void);
// 0x0000052A System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_OnPlacingStopped(UnityEngine.Events.UnityEvent)
extern void TapToPlace_set_OnPlacingStopped_m9175D2A804141BE4610C8F995D4780B576433188 (void);
// 0x0000052B System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_GameObjectLayer()
extern void TapToPlace_get_GameObjectLayer_m0C8950ACEA68495F9E599696726F3451A677E666 (void);
// 0x0000052C System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::set_GameObjectLayer(System.Int32)
extern void TapToPlace_set_GameObjectLayer_mCBD515CFBBD695DB2C4606CBF73AB655A376BAAC (void);
// 0x0000052D System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::get_IsColliderPresent()
extern void TapToPlace_get_IsColliderPresent_mC141EFA6674C2A135EE71BCA649003DE7217F6D8 (void);
// 0x0000052E System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::Start()
extern void TapToPlace_Start_mAD0CE835E0DA29F65D7CB175CD8D6F96C947DCD4 (void);
// 0x0000052F System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::OnDisable()
extern void TapToPlace_OnDisable_mD0731FDCE887BAF862D0E44A24ADD5C3E76F2BEC (void);
// 0x00000530 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::StartPlacement()
extern void TapToPlace_StartPlacement_m0639FFE90B9BD36EEE7222E0311FE1EA0371861A (void);
// 0x00000531 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::StopPlacement()
extern void TapToPlace_StopPlacement_mC3E17C2C0F11F3979549F5A4975177A2CFAAE8BB (void);
// 0x00000532 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::SolverUpdate()
extern void TapToPlace_SolverUpdate_mF4388FF02A60E041E48F041B75193C284475924F (void);
// 0x00000533 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::PerformRaycast()
extern void TapToPlace_PerformRaycast_mFE63B48B6C14EAA721A71F8E5016930B75361112 (void);
// 0x00000534 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::SetPosition()
extern void TapToPlace_SetPosition_m1832DD81E1B1C03D489A66B3F180A932C446F1EC (void);
// 0x00000535 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::SetRotation()
extern void TapToPlace_SetRotation_mDC415B841C82DC8E65105EAF7741F15B4D99EF3F (void);
// 0x00000536 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TapToPlace_OnPointerDown_mA8A5020D73A119D587B3CE17FF4BD27622397849 (void);
// 0x00000537 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TapToPlace_OnPointerDragged_m57F0EF0B94CF71A0A476DA2B0E9CB9A87BEEF4BC (void);
// 0x00000538 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TapToPlace_OnPointerUp_m5260088AE86E65B710ACA659768879CD4B2DE46E (void);
// 0x00000539 System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TapToPlace_OnPointerClicked_m1C985F0522359DD0155434C92DC94735678CAF04 (void);
// 0x0000053A System.Void Microsoft.MixedReality.Toolkit.Utilities.Solvers.TapToPlace::.ctor()
extern void TapToPlace__ctor_mA0591E8902EA48A093E28ACD5C8CDA4322FD6B6A (void);
// 0x0000053B System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::Start()
extern void DemoSceneUnderstandingController_Start_m81FDB4ECD9DEADE2E0FC9897B69E7CC2B448612C (void);
// 0x0000053C System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnEnable()
extern void DemoSceneUnderstandingController_OnEnable_m14DC558B9F0BE230E0073833B1FCC838268AC695 (void);
// 0x0000053D System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnDisable()
extern void DemoSceneUnderstandingController_OnDisable_m8BE11CF03CC02E7558C89CE70D243BD7F13FB7EB (void);
// 0x0000053E System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnDestroy()
extern void DemoSceneUnderstandingController_OnDestroy_m83101B507334B0F9A6C55039D8295E4230E897B4 (void);
// 0x0000053F System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationAdded(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationAdded_mFB2D1F9863A3452BBD366DA97E96FA2C6BE7A376 (void);
// 0x00000540 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationUpdated(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationUpdated_m80D1C5B6342BCE4E18D5D1D5A7FA3948BDD2D773 (void);
// 0x00000541 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationRemoved(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationRemoved_mD1253B1190BEB10F3C5145E37C03119494888B47 (void);
// 0x00000542 System.Collections.Generic.IReadOnlyDictionary`2<System.Int32,Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject> Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::GetSceneObjectsOfType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_GetSceneObjectsOfType_m3630FDF67DBB49AA6E5413E41797E2BEA418FEA7 (void);
// 0x00000543 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::UpdateScene()
extern void DemoSceneUnderstandingController_UpdateScene_m7E7A36484F24DDB8B5C2BFC3D9509C2A0E6A38E8 (void);
// 0x00000544 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::SaveScene()
extern void DemoSceneUnderstandingController_SaveScene_m4C006E2A5AEDC23A35389220B26D4BEDA95E0171 (void);
// 0x00000545 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ClearScene()
extern void DemoSceneUnderstandingController_ClearScene_mC54D363F795FF530800BA1FCC9567D8EFA7E5F39 (void);
// 0x00000546 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleAutoUpdate()
extern void DemoSceneUnderstandingController_ToggleAutoUpdate_m0F3331D3ED4D877783FBF814ABE6E708E01FAB9F (void);
// 0x00000547 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleOcclusionMask()
extern void DemoSceneUnderstandingController_ToggleOcclusionMask_mC667E7FBF9697AB95CA2A6026CBA48EC985F3E05 (void);
// 0x00000548 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleGeneratePlanes()
extern void DemoSceneUnderstandingController_ToggleGeneratePlanes_m2150D76D96C4D7F7CE63DC1445647D96ED74EE41 (void);
// 0x00000549 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleGenerateMeshes()
extern void DemoSceneUnderstandingController_ToggleGenerateMeshes_m4FAAA3BBD333AF28936E2DB67F2DC6826201B6A7 (void);
// 0x0000054A System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleFloors()
extern void DemoSceneUnderstandingController_ToggleFloors_mE5352EF8BD112A46B8B30F1F70481DDD9F9533D9 (void);
// 0x0000054B System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleWalls()
extern void DemoSceneUnderstandingController_ToggleWalls_m61FD7A872F156D121F0E10B53C48C06AA409CCE8 (void);
// 0x0000054C System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleCeilings()
extern void DemoSceneUnderstandingController_ToggleCeilings_mA9EDD05F95228284EEDC3AB3DD5BAFA7AE334EF0 (void);
// 0x0000054D System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::TogglePlatforms()
extern void DemoSceneUnderstandingController_TogglePlatforms_mDC7989260ED46FE37A03D33D966A0C4E0050382D (void);
// 0x0000054E System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleInferRegions()
extern void DemoSceneUnderstandingController_ToggleInferRegions_m2BA28C4F24F87653E2C18FFE0F93F55A0D214F7B (void);
// 0x0000054F System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleWorld()
extern void DemoSceneUnderstandingController_ToggleWorld_m3A285FA70CCDA72B1FE62D77A0009D365FE36749 (void);
// 0x00000550 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleBackground()
extern void DemoSceneUnderstandingController_ToggleBackground_m1518A97736F9E28F43D9E0214EC0CF7E157597B5 (void);
// 0x00000551 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleCompletelyInferred()
extern void DemoSceneUnderstandingController_ToggleCompletelyInferred_m2EAD63DCCA93B6C27D2AAF4F84DDCA7484C6E22D (void);
// 0x00000552 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::InitToggleButtonState()
extern void DemoSceneUnderstandingController_InitToggleButtonState_m9D71A4D589CB4355266C744108757E6ADAAD5E00 (void);
// 0x00000553 UnityEngine.Color Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ColorForSurfaceType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_ColorForSurfaceType_m80B7E4172D409DBB702726C8CD00800D72720EA1 (void);
// 0x00000554 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ClearAndUpdateObserver()
extern void DemoSceneUnderstandingController_ClearAndUpdateObserver_m917617CA6286CDD85F6DA7BF7F6302D8904ADAD6 (void);
// 0x00000555 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleObservedSurfaceType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_ToggleObservedSurfaceType_mAF3CB636F2F664BC777F0B00FC36DF35255D4CCA (void);
// 0x00000556 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::.ctor()
extern void DemoSceneUnderstandingController__ctor_m7545F95E952FA57B2471CEAF16B2FD73FE6356C4 (void);
// 0x00000557 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::Start()
extern void JoystickSliders_Start_m8969386B0FDCD0C0DCC536AD5949623F29D5E642 (void);
// 0x00000558 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::CalculateValues()
extern void JoystickSliders_CalculateValues_m4AFB14141CDF0BDA48F2EF76343FBDB4A4F6CE64 (void);
// 0x00000559 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::UpdateSliderValues()
extern void JoystickSliders_UpdateSliderValues_m3138DC151B9D426A294DB62C5C09DD1A57A02A63 (void);
// 0x0000055A System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::.ctor()
extern void JoystickSliders__ctor_m8A84CA57812CB0EF3AC3906C51EF4C1AAFD82F86 (void);
// 0x0000055B System.Void Microsoft.MixedReality.Toolkit.Experimental.SurfacePulse.HideOnDevice::Start()
extern void HideOnDevice_Start_m542FED820F24D18D743A5484A82AF9D550586DEC (void);
// 0x0000055C System.Void Microsoft.MixedReality.Toolkit.Experimental.SurfacePulse.HideOnDevice::.ctor()
extern void HideOnDevice__ctor_mFE24A0B9BA58C70A3CA5102222781A5FCA2F7169 (void);
// 0x0000055D System.Boolean Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_Visible()
extern void MixedRealityKeyboard_get_Visible_mD6D5981F52A35FDEEE326E55F11DAA1DF70F0661 (void);
// 0x0000055E System.String Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_Text()
extern void MixedRealityKeyboard_get_Text_mA9138CAF80EE85CC1D1FC71303896CAD356C5B28 (void);
// 0x0000055F System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::set_Text(System.String)
extern void MixedRealityKeyboard_set_Text_m568E4AF62B73D6CD049B702337B8D025AEA6AC69 (void);
// 0x00000560 System.Int32 Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_CaretIndex()
extern void MixedRealityKeyboard_get_CaretIndex_m00F252F2BB6AE73648A82EBC90A16D41FA7002CF (void);
// 0x00000561 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::set_CaretIndex(System.Int32)
extern void MixedRealityKeyboard_set_CaretIndex_mD68B44C9DAA9C7E5EDC293934393016EA7A710C8 (void);
// 0x00000562 UnityEngine.Events.UnityEvent Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_OnShowKeyboard()
extern void MixedRealityKeyboard_get_OnShowKeyboard_mC560D247AEB95AB8EBCA90F7C04987D736C162AF (void);
// 0x00000563 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::set_OnShowKeyboard(UnityEngine.Events.UnityEvent)
extern void MixedRealityKeyboard_set_OnShowKeyboard_m97EC0A3FA11E1E2DA42D11427406D61AF6B07F5A (void);
// 0x00000564 UnityEngine.Events.UnityEvent Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_OnCommitText()
extern void MixedRealityKeyboard_get_OnCommitText_m9706EC7A8CE10D3BA5C7E7A9F643976D191973CB (void);
// 0x00000565 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::set_OnCommitText(UnityEngine.Events.UnityEvent)
extern void MixedRealityKeyboard_set_OnCommitText_m54FDECFC19733A7EC87B59EBBA689687712154D6 (void);
// 0x00000566 UnityEngine.Events.UnityEvent Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_OnHideKeyboard()
extern void MixedRealityKeyboard_get_OnHideKeyboard_mFD4F7F5504734655D2A7A1F32101C4E3AA9AC923 (void);
// 0x00000567 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::set_OnHideKeyboard(UnityEngine.Events.UnityEvent)
extern void MixedRealityKeyboard_set_OnHideKeyboard_m45A28986C9B7145C788A55631710224FDDEDD88B (void);
// 0x00000568 Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard/KeyboardState Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::get_State()
extern void MixedRealityKeyboard_get_State_mD009D2CA7777921952ED86F627F7A749ADE23A63 (void);
// 0x00000569 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::set_State(Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard/KeyboardState)
extern void MixedRealityKeyboard_set_State_mA543947C28F00EC13277B59034A9E785928FC109 (void);
// 0x0000056A System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::Start()
extern void MixedRealityKeyboard_Start_m88FD2A265450BF27E747EEE54E2A8FE0141ECE20 (void);
// 0x0000056B System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::Update()
extern void MixedRealityKeyboard_Update_m41BF0D668900C471CA2DCE24FF2AE856BAA3E4CD (void);
// 0x0000056C System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::ShowKeyboard(System.String,System.Boolean)
extern void MixedRealityKeyboard_ShowKeyboard_m0B6057E5146F139F388668BF36985C30BB6FF23B (void);
// 0x0000056D System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::HideKeyboard()
extern void MixedRealityKeyboard_HideKeyboard_m520EC46393E05E1F7944A4E3EA772E5085F218E0 (void);
// 0x0000056E System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::ClearKeyboardText()
extern void MixedRealityKeyboard_ClearKeyboardText_mBB6A2AADFB8E4EAF94EB1346DE2F98CBC7A94140 (void);
// 0x0000056F System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::UpdateText()
extern void MixedRealityKeyboard_UpdateText_m99E10E97C2229FC8C6EE1CABED5DAD3D6D141D5B (void);
// 0x00000570 System.Boolean Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::IsPreviewCaretAtEnd()
extern void MixedRealityKeyboard_IsPreviewCaretAtEnd_m3C97CEBFAD0776FA3E9B917135C277D0988553B6 (void);
// 0x00000571 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::MovePreviewCaretToEnd()
extern void MixedRealityKeyboard_MovePreviewCaretToEnd_mF3DF68995332558504103EE1748D9772BFAFD6A0 (void);
// 0x00000572 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::OnKeyboardHiding()
extern void MixedRealityKeyboard_OnKeyboardHiding_m6566D649C21604B8D0E4198A9714F9E14E090EDD (void);
// 0x00000573 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::OnKeyboardShowing()
extern void MixedRealityKeyboard_OnKeyboardShowing_m22EBFA6BE5AC53D0BA67B13FB11DE1849707433A (void);
// 0x00000574 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::.ctor()
extern void MixedRealityKeyboard__ctor_m69B4A1D0E77CEDE81D5ED44AD7085D93D33E3DA9 (void);
// 0x00000575 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::<Start>b__30_0()
extern void MixedRealityKeyboard_U3CStartU3Eb__30_0_m02624AAD7D376D7E696607910BEF2426546988C2 (void);
// 0x00000576 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::<Start>b__30_1(Windows.UI.ViewManagement.InputPane,Windows.UI.ViewManagement.InputPaneVisibilityEventArgs)
extern void MixedRealityKeyboard_U3CStartU3Eb__30_1_m14A785D4B43E4DFB192D5C1BC7E32A95FE45C58D (void);
// 0x00000577 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::<Start>b__30_2(Windows.UI.ViewManagement.InputPane,Windows.UI.ViewManagement.InputPaneVisibilityEventArgs)
extern void MixedRealityKeyboard_U3CStartU3Eb__30_2_m09BFFD7D8210B409C5DA6CD1294549C48B908681 (void);
// 0x00000578 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::<ShowKeyboard>b__32_0()
extern void MixedRealityKeyboard_U3CShowKeyboardU3Eb__32_0_m5F497EB9FFD36998D8FB21F8F2C111441F772DE8 (void);
// 0x00000579 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.MixedRealityKeyboard::<HideKeyboard>b__33_0()
extern void MixedRealityKeyboard_U3CHideKeyboardU3Eb__33_0_m70561532B63A742608CD100C6B5CAA9C399A81F8 (void);
// 0x0000057A System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void KeyboardTest_OnPointerDown_m53BF9AE3E12D88F545CA0EE40BC901D42333538E (void);
// 0x0000057B System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::UpdateText(System.String)
extern void KeyboardTest_UpdateText_m3B7597ED54941BC90F04A2415030C27AABE54C38 (void);
// 0x0000057C System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::DisableKeyboard(System.Object,System.EventArgs)
extern void KeyboardTest_DisableKeyboard_mE36CE6548B2BE770D02E35525E38C5EB286E4188 (void);
// 0x0000057D System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::.ctor()
extern void KeyboardTest__ctor_m0BC979C0C7D1A61D003EEA44E4361BEC9F684106 (void);
// 0x0000057E System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4 (void);
// 0x0000057F SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807 (void);
// 0x00000580 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7 (void);
// 0x00000581 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D (void);
// 0x00000582 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07 (void);
// 0x00000583 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1 (void);
// 0x00000584 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA (void);
// 0x00000585 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269 (void);
// 0x00000586 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2 (void);
// 0x00000587 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47 (void);
// 0x00000588 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053 (void);
// 0x00000589 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA (void);
// 0x0000058A System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Childs()
extern void JSONNode_get_Childs_m62FABDFCC2A9EE8029721B2B42B55DBF17FDA3A4 (void);
// 0x0000058B System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChilds()
extern void JSONNode_get_DeepChilds_mD9ACDC378594A79ED2F10B1816BF27077004B25B (void);
// 0x0000058C System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98 (void);
// 0x0000058D System.String SimpleJSON.JSONNode::ToString(System.String)
extern void JSONNode_ToString_m7FCA0DFCAB4289D983E8FB21216993980AF4CCB7 (void);
// 0x0000058E System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA (void);
// 0x0000058F System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75 (void);
// 0x00000590 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB (void);
// 0x00000591 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1 (void);
// 0x00000592 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656 (void);
// 0x00000593 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5 (void);
// 0x00000594 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D (void);
// 0x00000595 System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F (void);
// 0x00000596 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829 (void);
// 0x00000597 SimpleJSON.JSONClass SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m965C37E386EEF32FF6EE5C59DE5743F11D58B9E8 (void);
// 0x00000598 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2 (void);
// 0x00000599 System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668 (void);
// 0x0000059A System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F (void);
// 0x0000059B System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE (void);
// 0x0000059C System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF (void);
// 0x0000059D System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834 (void);
// 0x0000059E System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68 (void);
// 0x0000059F SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076 (void);
// 0x000005A0 System.Void SimpleJSON.JSONNode::Serialize(System.IO.BinaryWriter)
extern void JSONNode_Serialize_m4E6645099B140A079610B4F644E4A9E978778CAE (void);
// 0x000005A1 System.Void SimpleJSON.JSONNode::SaveToStream(System.IO.Stream)
extern void JSONNode_SaveToStream_m5F95DFA988259F086D8C657151B8FCB5EBB4AEF7 (void);
// 0x000005A2 System.Void SimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_m939EB2E9267698F54AD64849D96E4ADFCCF5A00A (void);
// 0x000005A3 System.Void SimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m66A1234959DDCEE44D9000255F86A8FD32EC87BC (void);
// 0x000005A4 System.String SimpleJSON.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mF1BB008CE575D288A986676FD23806E62C98D539 (void);
// 0x000005A5 System.Void SimpleJSON.JSONNode::SaveToFile(System.String)
extern void JSONNode_SaveToFile_m7B15985908548F36FEB6654CFBDA7D5E1ABB1839 (void);
// 0x000005A6 System.String SimpleJSON.JSONNode::SaveToBase64()
extern void JSONNode_SaveToBase64_m55A648ED569B7E22511D0E71DA5A1348E1E908DC (void);
// 0x000005A7 SimpleJSON.JSONNode SimpleJSON.JSONNode::Deserialize(System.IO.BinaryReader)
extern void JSONNode_Deserialize_m38C70CC7D80AEDBD4ADF9618BBC161224C3F45E1 (void);
// 0x000005A8 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m50BB9E1D951C7444EFE1FE570894E5A92BA35275 (void);
// 0x000005A9 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m6D59AB50712DF46BE7D37DB5EDB8EDD7C0B6D04D (void);
// 0x000005AA SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_mBC74C1DE9C65EF47BA738413E2140F1370A9F433 (void);
// 0x000005AB SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromStream(System.IO.Stream)
extern void JSONNode_LoadFromStream_m490074C19DC1E5CFA73842C6839BED6DA62F12BE (void);
// 0x000005AC SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromFile(System.String)
extern void JSONNode_LoadFromFile_m7FC0A7565712C07EF7B7954586381E50A193B9B9 (void);
// 0x000005AD SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBase64(System.String)
extern void JSONNode_LoadFromBase64_mF1CFF8D5D2484CC31D32A39A8F88C95E46AD8644 (void);
// 0x000005AE System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E (void);
// 0x000005AF System.Void SimpleJSON.JSONNode/<get_Childs>d__17::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__17__ctor_mD2F92AF443A79C6319D6675D9B8EB54C3F69AF70 (void);
// 0x000005B0 System.Void SimpleJSON.JSONNode/<get_Childs>d__17::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__17_System_IDisposable_Dispose_m1843C95CC9C3F00F24A341C006AF9481FB408A93 (void);
// 0x000005B1 System.Boolean SimpleJSON.JSONNode/<get_Childs>d__17::MoveNext()
extern void U3Cget_ChildsU3Ed__17_MoveNext_m62DB18AFF29AF270D32CB504C0677A61E0E56369 (void);
// 0x000005B2 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Childs>d__17::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m446EABB222302FCA20A65E6FC3EDE4CA67F00309 (void);
// 0x000005B3 System.Void SimpleJSON.JSONNode/<get_Childs>d__17::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_Reset_m20DEC2764F0917653F7DBE54394410D0CCF75538 (void);
// 0x000005B4 System.Object SimpleJSON.JSONNode/<get_Childs>d__17::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_get_Current_mE68F3F2E789E20E3919A83F45CADD50C4BC63D72 (void);
// 0x000005B5 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Childs>d__17::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF6FF6762A1746B8536A34B80AB04F7FB4574226F (void);
// 0x000005B6 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Childs>d__17::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mEBCE9E490A3EF4D9FD87A5E4724B9A0666DC818B (void);
// 0x000005B7 System.Void SimpleJSON.JSONNode/<get_DeepChilds>d__19::.ctor(System.Int32)
extern void U3Cget_DeepChildsU3Ed__19__ctor_mBEE3D8BB361728473C66F7871C19838BDE8E6EE2 (void);
// 0x000005B8 System.Void SimpleJSON.JSONNode/<get_DeepChilds>d__19::System.IDisposable.Dispose()
extern void U3Cget_DeepChildsU3Ed__19_System_IDisposable_Dispose_mC6F8AFB4A4716135BE8A9252CE2B620E7F3CA0DD (void);
// 0x000005B9 System.Boolean SimpleJSON.JSONNode/<get_DeepChilds>d__19::MoveNext()
extern void U3Cget_DeepChildsU3Ed__19_MoveNext_mE1419F074852413368A8E1D3DD8C68A76F7F6814 (void);
// 0x000005BA System.Void SimpleJSON.JSONNode/<get_DeepChilds>d__19::<>m__Finally1()
extern void U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally1_m3C13EB580FB1D6EB0A781B9785EAD11BFFDCD4C0 (void);
// 0x000005BB System.Void SimpleJSON.JSONNode/<get_DeepChilds>d__19::<>m__Finally2()
extern void U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally2_m913DD85500791510F835734C52BF1FCE6E70D365 (void);
// 0x000005BC SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChilds>d__19::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6055AD945E93ADFCFD74E87F8B1B348038A3B65A (void);
// 0x000005BD System.Void SimpleJSON.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_Reset_mE4AA0EB22642B477A594C4B2814DCCBD19470621 (void);
// 0x000005BE System.Object SimpleJSON.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_get_Current_mD8FB3E7E3C35922C4DCA32785C2F6CB2432BFBB8 (void);
// 0x000005BF System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChilds>d__19::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m03E96284C0131C15D5289DE63DE3BBE2652AF42A (void);
// 0x000005C0 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChilds>d__19::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerable_GetEnumerator_m5789A070C8140458EBC69890EF2E314B803EA170 (void);
// 0x000005C1 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC (void);
// 0x000005C2 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF (void);
// 0x000005C3 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B (void);
// 0x000005C4 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE (void);
// 0x000005C5 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B (void);
// 0x000005C6 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685 (void);
// 0x000005C7 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9 (void);
// 0x000005C8 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E (void);
// 0x000005C9 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Childs()
extern void JSONArray_get_Childs_mAC9E9800F9F99F137D2032D7B9B00A2E8E4FB323 (void);
// 0x000005CA System.Collections.IEnumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m6C84A7480387B68D91988514017653738819E7B2 (void);
// 0x000005CB System.String SimpleJSON.JSONArray::ToString()
extern void JSONArray_ToString_mA4E44724197E6EBDED42F5648AEBEE6DAF95DA2E (void);
// 0x000005CC System.String SimpleJSON.JSONArray::ToString(System.String)
extern void JSONArray_ToString_m96E4DCAF1E5B0C88D6B73CC58488B47405D2F9C5 (void);
// 0x000005CD System.Void SimpleJSON.JSONArray::Serialize(System.IO.BinaryWriter)
extern void JSONArray_Serialize_m742EF25F05EB47F6DF6910A21044C6DE62FFDD03 (void);
// 0x000005CE System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512 (void);
// 0x000005CF System.Void SimpleJSON.JSONArray/<get_Childs>d__13::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__13__ctor_m21A583D968F53AE41E2FA06917ABAC9D9699473E (void);
// 0x000005D0 System.Void SimpleJSON.JSONArray/<get_Childs>d__13::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__13_System_IDisposable_Dispose_m5A1086E94D62110F2271E60FB85840059984A7DA (void);
// 0x000005D1 System.Boolean SimpleJSON.JSONArray/<get_Childs>d__13::MoveNext()
extern void U3Cget_ChildsU3Ed__13_MoveNext_mF0AD3C64CE7A11C3EFEA0D40EBD8F1A3095AA4B6 (void);
// 0x000005D2 System.Void SimpleJSON.JSONArray/<get_Childs>d__13::<>m__Finally1()
extern void U3Cget_ChildsU3Ed__13_U3CU3Em__Finally1_m143EA669B23A1936BC3B1E1701A20A58B09B860F (void);
// 0x000005D3 SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Childs>d__13::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD4786DBC563CD88FDF86558BD112197AD43674FC (void);
// 0x000005D4 System.Void SimpleJSON.JSONArray/<get_Childs>d__13::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_Reset_m8A61E7D4389FF479518A7AA2379FB8D8514B9EA2 (void);
// 0x000005D5 System.Object SimpleJSON.JSONArray/<get_Childs>d__13::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_get_Current_m82EF5BD3F486BBF4F56C7D6AB3268AB3F3783DFC (void);
// 0x000005D6 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Childs>d__13::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m9AC9E6F2235ACB8E42AA6B2928B5BDE3B999B3AB (void);
// 0x000005D7 System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Childs>d__13::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__13_System_Collections_IEnumerable_GetEnumerator_mB16BA5F771503FAA010363CC7F565CDA477FC056 (void);
// 0x000005D8 System.Void SimpleJSON.JSONArray/<GetEnumerator>d__14::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__14__ctor_m9443943452B220F583808D225CB05DBC67AE4EAB (void);
// 0x000005D9 System.Void SimpleJSON.JSONArray/<GetEnumerator>d__14::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__14_System_IDisposable_Dispose_m4BD1094EA16FCAFCAE7FBA46A90E495346239B2E (void);
// 0x000005DA System.Boolean SimpleJSON.JSONArray/<GetEnumerator>d__14::MoveNext()
extern void U3CGetEnumeratorU3Ed__14_MoveNext_mB002FEFA4F25E85F7ABBAACE45DE15123B43E647 (void);
// 0x000005DB System.Void SimpleJSON.JSONArray/<GetEnumerator>d__14::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__14_U3CU3Em__Finally1_m7A8C62B8E3949D722715DCDF5B35CE140219DA89 (void);
// 0x000005DC System.Object SimpleJSON.JSONArray/<GetEnumerator>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1824EC362F1755F5DF4066F6A9E481F50253905 (void);
// 0x000005DD System.Void SimpleJSON.JSONArray/<GetEnumerator>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_Reset_m96F051F06E4D2972A326277E2A0B8902F0C4DDB9 (void);
// 0x000005DE System.Object SimpleJSON.JSONArray/<GetEnumerator>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_get_Current_mEE886034A8F9C19F494478CA1E9159ABEB428F4D (void);
// 0x000005DF SimpleJSON.JSONNode SimpleJSON.JSONClass::get_Item(System.String)
extern void JSONClass_get_Item_mEBC27C74AB8C3DEEB9E243952B0F1EA2C25528D7 (void);
// 0x000005E0 System.Void SimpleJSON.JSONClass::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONClass_set_Item_m7D109D31520EF4D4CB27B22778883393CDCBA33F (void);
// 0x000005E1 SimpleJSON.JSONNode SimpleJSON.JSONClass::get_Item(System.Int32)
extern void JSONClass_get_Item_mDF11AC8ACFE3A539B55000FC2A6671ACEB5B15C5 (void);
// 0x000005E2 System.Void SimpleJSON.JSONClass::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONClass_set_Item_mDFCA44AC3C44FDE1E776D4A295431C72022208C3 (void);
// 0x000005E3 System.Int32 SimpleJSON.JSONClass::get_Count()
extern void JSONClass_get_Count_m4B5121347CACB0517E4E461729AC144764FBCE06 (void);
// 0x000005E4 System.Void SimpleJSON.JSONClass::Add(System.String,SimpleJSON.JSONNode)
extern void JSONClass_Add_m5C0424C984499D48E81F00373DDBAA5CC295155B (void);
// 0x000005E5 SimpleJSON.JSONNode SimpleJSON.JSONClass::Remove(System.String)
extern void JSONClass_Remove_m34FAC69B637A22128801233A3A8F466A0B944D40 (void);
// 0x000005E6 SimpleJSON.JSONNode SimpleJSON.JSONClass::Remove(System.Int32)
extern void JSONClass_Remove_mABE15123ABACCEFC0B331A16E8DC8B0B079E2B73 (void);
// 0x000005E7 SimpleJSON.JSONNode SimpleJSON.JSONClass::Remove(SimpleJSON.JSONNode)
extern void JSONClass_Remove_m87288080284E5E6D25E27CD27848F1548A55C508 (void);
// 0x000005E8 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONClass::get_Childs()
extern void JSONClass_get_Childs_m9CCCF5791C458910932A682DF0F62F6F903B4F50 (void);
// 0x000005E9 System.Collections.IEnumerator SimpleJSON.JSONClass::GetEnumerator()
extern void JSONClass_GetEnumerator_mD397398C72B8A02F9D264B04C1FA11EFE3C0CF3A (void);
// 0x000005EA System.String SimpleJSON.JSONClass::ToString()
extern void JSONClass_ToString_mC05EC1CDA411BD5CCD42E7A5D64C28E122310D66 (void);
// 0x000005EB System.String SimpleJSON.JSONClass::ToString(System.String)
extern void JSONClass_ToString_mB04CDE6B692750B230DB29719FDC8AFC438BE2CA (void);
// 0x000005EC System.Void SimpleJSON.JSONClass::Serialize(System.IO.BinaryWriter)
extern void JSONClass_Serialize_m96D53FC580DE031FDE82FDC6AE1CC0D0B510C321 (void);
// 0x000005ED System.Void SimpleJSON.JSONClass::.ctor()
extern void JSONClass__ctor_mF10F4D8793CFDCC66146835E35FE21CDD64D9F6C (void);
// 0x000005EE System.Void SimpleJSON.JSONClass/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mBB42DF8E5C129D67D2491C3BBE5E7A470C1AB1DA (void);
// 0x000005EF System.Boolean SimpleJSON.JSONClass/<>c__DisplayClass12_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass12_0_U3CRemoveU3Eb__0_mE7239193384B5B8AA901FB2F2CFE85EE0CD19CBB (void);
// 0x000005F0 System.Void SimpleJSON.JSONClass/<get_Childs>d__14::.ctor(System.Int32)
extern void U3Cget_ChildsU3Ed__14__ctor_mEE1C01E6EFDA9B4635A56660AB0D9DC7750EDF05 (void);
// 0x000005F1 System.Void SimpleJSON.JSONClass/<get_Childs>d__14::System.IDisposable.Dispose()
extern void U3Cget_ChildsU3Ed__14_System_IDisposable_Dispose_mFFB990A9BF4C4F78F537B8691277BD8826348E19 (void);
// 0x000005F2 System.Boolean SimpleJSON.JSONClass/<get_Childs>d__14::MoveNext()
extern void U3Cget_ChildsU3Ed__14_MoveNext_m5E739C9FFF695DE7F830A21461F073C4FA5A4C5B (void);
// 0x000005F3 System.Void SimpleJSON.JSONClass/<get_Childs>d__14::<>m__Finally1()
extern void U3Cget_ChildsU3Ed__14_U3CU3Em__Finally1_m037FA09FF10B7EDDD6C2292ED217B611E84F6F5E (void);
// 0x000005F4 SimpleJSON.JSONNode SimpleJSON.JSONClass/<get_Childs>d__14::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mDDA801467B2864ABB763B6B4BA9489BF107AB19E (void);
// 0x000005F5 System.Void SimpleJSON.JSONClass/<get_Childs>d__14::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_Reset_m739FFFB50ED6101ABF28724E67A4D6F071FE05C9 (void);
// 0x000005F6 System.Object SimpleJSON.JSONClass/<get_Childs>d__14::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_get_Current_m6FD0B87A669091FD7BB63A979D83D25D74E0D184 (void);
// 0x000005F7 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONClass/<get_Childs>d__14::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m551589F37908CD3DAA752FFBBC9D1D891D584B86 (void);
// 0x000005F8 System.Collections.IEnumerator SimpleJSON.JSONClass/<get_Childs>d__14::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_mF36855B379BF93F36912B6C343FE284FAF727C82 (void);
// 0x000005F9 System.Void SimpleJSON.JSONClass/<GetEnumerator>d__15::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__15__ctor_mE0CCB9ECD70B08B0A19C7830D6FEF1543E520CB2 (void);
// 0x000005FA System.Void SimpleJSON.JSONClass/<GetEnumerator>d__15::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__15_System_IDisposable_Dispose_mFF622866B648141D10BB5F74E2EFBA282160137B (void);
// 0x000005FB System.Boolean SimpleJSON.JSONClass/<GetEnumerator>d__15::MoveNext()
extern void U3CGetEnumeratorU3Ed__15_MoveNext_mAEC7795B6B36542CA98FE18D92023D6570D48AD2 (void);
// 0x000005FC System.Void SimpleJSON.JSONClass/<GetEnumerator>d__15::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__15_U3CU3Em__Finally1_mDBE6650843355BCA2E73313BDF63D6595BE605F2 (void);
// 0x000005FD System.Object SimpleJSON.JSONClass/<GetEnumerator>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE70F34764406D091477ED9A573997292C7D369EE (void);
// 0x000005FE System.Void SimpleJSON.JSONClass/<GetEnumerator>d__15::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_Reset_m1CA435191A682FEA49B5346AA45C1ABCC1382106 (void);
// 0x000005FF System.Object SimpleJSON.JSONClass/<GetEnumerator>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_get_Current_m85A5819512967EE2DE4F97D246A0FE378C2A548B (void);
// 0x00000600 System.String SimpleJSON.JSONData::get_Value()
extern void JSONData_get_Value_mAC8BB77AAE3FD3BFD05E4E29A9D99D8FDB649792 (void);
// 0x00000601 System.Void SimpleJSON.JSONData::set_Value(System.String)
extern void JSONData_set_Value_m172D1A924C9966EEBDFDFF95469EEB8B6C1BF9E0 (void);
// 0x00000602 System.Void SimpleJSON.JSONData::.ctor(System.String)
extern void JSONData__ctor_mE61EABC6FDB97F0C999D3257665849919906F382 (void);
// 0x00000603 System.Void SimpleJSON.JSONData::.ctor(System.Single)
extern void JSONData__ctor_m915D92C57E1F9C91FBD62B77E31DB3BF048B889E (void);
// 0x00000604 System.Void SimpleJSON.JSONData::.ctor(System.Double)
extern void JSONData__ctor_m9977313E9F1EFC6A6E96CE43506377B977581A15 (void);
// 0x00000605 System.Void SimpleJSON.JSONData::.ctor(System.Boolean)
extern void JSONData__ctor_mE49E656122CABAF2CCA85252136E96F05BEAB45F (void);
// 0x00000606 System.Void SimpleJSON.JSONData::.ctor(System.Int32)
extern void JSONData__ctor_m008B0A7698BD04861CC161F4817A150F22A74966 (void);
// 0x00000607 System.String SimpleJSON.JSONData::ToString()
extern void JSONData_ToString_mC94A08B0C73C360F61052DC128D96D83FC0E29EC (void);
// 0x00000608 System.String SimpleJSON.JSONData::ToString(System.String)
extern void JSONData_ToString_m0DB9AAC62F1FE9D6CF1405B7FD56A569E984C7B6 (void);
// 0x00000609 System.Void SimpleJSON.JSONData::Serialize(System.IO.BinaryWriter)
extern void JSONData_Serialize_mD22C6A789FBB58221C9A971132827F79D8899F1A (void);
// 0x0000060A System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE (void);
// 0x0000060B System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A (void);
// 0x0000060C System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_m9C40ED5996DFD3D796BFAFBC25EEAB76F29F871F (void);
// 0x0000060D SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446 (void);
// 0x0000060E System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E (void);
// 0x0000060F SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753 (void);
// 0x00000610 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF (void);
// 0x00000611 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B (void);
// 0x00000612 System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37 (void);
// 0x00000613 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8 (void);
// 0x00000614 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA (void);
// 0x00000615 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9 (void);
// 0x00000616 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF (void);
// 0x00000617 System.String SimpleJSON.JSONLazyCreator::ToString()
extern void JSONLazyCreator_ToString_mAB3B9D4B94FB78CC15E75A69F65D80FA6DA8B080 (void);
// 0x00000618 System.String SimpleJSON.JSONLazyCreator::ToString(System.String)
extern void JSONLazyCreator_ToString_m1432B2846BF58B6DA6ABD2F63C766375AC3D0A95 (void);
// 0x00000619 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012 (void);
// 0x0000061A System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587 (void);
// 0x0000061B System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4 (void);
// 0x0000061C System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956 (void);
// 0x0000061D System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545 (void);
// 0x0000061E System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2 (void);
// 0x0000061F System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00 (void);
// 0x00000620 System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094 (void);
// 0x00000621 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6 (void);
// 0x00000622 SimpleJSON.JSONClass SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_mFD6886C4ED273AA71AB620C63AFB22DF449646DD (void);
// 0x00000623 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421 (void);
// 0x00000624 System.Void MRTK.Tutorials.GettingStarted.DirectionalIndicatorController::OnBecameInvisible()
extern void DirectionalIndicatorController_OnBecameInvisible_m0E4A338FC4C70E296D1B8286AA2FD9E833D947C2 (void);
// 0x00000625 System.Void MRTK.Tutorials.GettingStarted.DirectionalIndicatorController::.ctor()
extern void DirectionalIndicatorController__ctor_mD5EA1948C4529FAFD1E496942ADAFBEC7E4A8018 (void);
// 0x00000626 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::set_IsPunEnabled(System.Boolean)
extern void ExplodeViewController_set_IsPunEnabled_mF24170F7F9A608F7414FFF2ABF39EE706BC434F5 (void);
// 0x00000627 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::Start()
extern void ExplodeViewController_Start_mE07825796104C61B74ED0AD2D5A5CC38E0BAAE6D (void);
// 0x00000628 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::Update()
extern void ExplodeViewController_Update_mC1FADA194292F3EBAE4E56B1AE64EB407E87F2F8 (void);
// 0x00000629 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::ToggleExplodedView()
extern void ExplodeViewController_ToggleExplodedView_m74189DE87897AB5232FA72B72753903F85952972 (void);
// 0x0000062A System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::Toggle()
extern void ExplodeViewController_Toggle_m321B6176A1D390111076B1919CC2E44D6DB7E5E2 (void);
// 0x0000062B System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::add_OnToggleExplodedView(MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate)
extern void ExplodeViewController_add_OnToggleExplodedView_mA25D0A8E94652AC021B35E5C2591EFB4A65EFAE0 (void);
// 0x0000062C System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::remove_OnToggleExplodedView(MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate)
extern void ExplodeViewController_remove_OnToggleExplodedView_mF561824B96EAA8FD7770BEA6265349C3A65B8ECA (void);
// 0x0000062D System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::.ctor()
extern void ExplodeViewController__ctor_mE72637A87DA07F869CC7C9FD74591AE0C8F60176 (void);
// 0x0000062E System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::.ctor(System.Object,System.IntPtr)
extern void ExplodeViewControllerDelegate__ctor_m6DFCEE099C96781241186B1537B313641CC3A8AB (void);
// 0x0000062F System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::Invoke()
extern void ExplodeViewControllerDelegate_Invoke_mEC016D4928ADCA613702B28C1E3378F7EC29B58C (void);
// 0x00000630 System.IAsyncResult MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ExplodeViewControllerDelegate_BeginInvoke_m458C786F225088793798F9A79882A606822BD0B9 (void);
// 0x00000631 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::EndInvoke(System.IAsyncResult)
extern void ExplodeViewControllerDelegate_EndInvoke_m1190D2F43012408A716B9FC790DEB913A85D9476 (void);
// 0x00000632 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::set_IsPunEnabled(System.Boolean)
extern void PartAssemblyController_set_IsPunEnabled_m87E32D7952387591F37EC578D7850CCF1CCE4D55 (void);
// 0x00000633 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::Start()
extern void PartAssemblyController_Start_m76EEAA396C555EFAC8E6EA60B32B1E7380570CE7 (void);
// 0x00000634 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::SetPlacement()
extern void PartAssemblyController_SetPlacement_mB6BDE1A16F74DF3B4950483F7C58E3201B0A0851 (void);
// 0x00000635 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::Set()
extern void PartAssemblyController_Set_m77CC9BC1F649F4F08C89FD060E2C74C4F45C91FD (void);
// 0x00000636 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::ResetPlacement()
extern void PartAssemblyController_ResetPlacement_m7FA91500780C316EBF560CECD7078E559460ACD0 (void);
// 0x00000637 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::Reset()
extern void PartAssemblyController_Reset_m129940529723E9498D4E5035FB917610DA5C8981 (void);
// 0x00000638 System.Collections.IEnumerator MRTK.Tutorials.GettingStarted.PartAssemblyController::CheckPlacement()
extern void PartAssemblyController_CheckPlacement_m058EC3FBCB6313D9C85FF551A4831601805F5A4D (void);
// 0x00000639 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::add_OnResetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_add_OnResetPlacement_m1B5F77146056299BFF14210549A8A71139406319 (void);
// 0x0000063A System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::remove_OnResetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_remove_OnResetPlacement_mDDD37DC93CE99FCE605DFCB9CBC37413364E0D03 (void);
// 0x0000063B System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::add_OnSetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_add_OnSetPlacement_m5B9ED392DAD16C5B23E31DDEF213CFC470C8D1B6 (void);
// 0x0000063C System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::remove_OnSetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_remove_OnSetPlacement_mA87246A60289C2E7B03E07D5248B7FFE9E533E3B (void);
// 0x0000063D System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::.ctor()
extern void PartAssemblyController__ctor_m43D020771B20D09433D17616C90F0D19569F9D9E (void);
// 0x0000063E System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::.ctor(System.Object,System.IntPtr)
extern void PartAssemblyControllerDelegate__ctor_m1B455F877BBF4D8B34BAA0778ABFA1B0C8FB3C87 (void);
// 0x0000063F System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::Invoke()
extern void PartAssemblyControllerDelegate_Invoke_m35EC047435BEF3AC833FE7F6FDED12C1D2FAF2F9 (void);
// 0x00000640 System.IAsyncResult MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void PartAssemblyControllerDelegate_BeginInvoke_mE2B0F86102E0D5E6BCD5ADDF6FE25CC9FC8A06D2 (void);
// 0x00000641 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::EndInvoke(System.IAsyncResult)
extern void PartAssemblyControllerDelegate_EndInvoke_m3BC4B8B7B165CE27C98FCB26D91E4E7004F5DA92 (void);
// 0x00000642 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::.ctor(System.Int32)
extern void U3CCheckPlacementU3Ed__25__ctor_m1AE9DAAD6457E2BA3C8AF018FC38B348AEEAFD35 (void);
// 0x00000643 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.IDisposable.Dispose()
extern void U3CCheckPlacementU3Ed__25_System_IDisposable_Dispose_m5F0CAD7B8EEB902CEC60BA6283E5D2176899ECD0 (void);
// 0x00000644 System.Boolean MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::MoveNext()
extern void U3CCheckPlacementU3Ed__25_MoveNext_mDF7F144AFC4007613EFED9BAC836382AE491AD0A (void);
// 0x00000645 System.Object MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckPlacementU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2770AE4D2AC80980A1982783D11A2A481D1F5AD (void);
// 0x00000646 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.Collections.IEnumerator.Reset()
extern void U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_Reset_m3B06BF13F7C5A5FD0544E0A77EF07BF84BEB6237 (void);
// 0x00000647 System.Object MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_get_Current_mF23A2B08C5CAD169D0663319DCDF005B7BF30371 (void);
// 0x00000648 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::set_IsPunEnabled(System.Boolean)
extern void PlacementHintsController_set_IsPunEnabled_m3F5768D83A150962864A1DF4E4A041A75E006B00 (void);
// 0x00000649 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::Start()
extern void PlacementHintsController_Start_mA284074B6E1D6E5C2EFEE25ED73B92718FD44DA5 (void);
// 0x0000064A System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::TogglePlacementHints()
extern void PlacementHintsController_TogglePlacementHints_mC6E69154176B5D87ECC700CC21DDF3AE3B79F0A9 (void);
// 0x0000064B System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::Toggle()
extern void PlacementHintsController_Toggle_mED247D0E903D8499BE48BC9C0DF2EC3B94F3494B (void);
// 0x0000064C System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::add_OnTogglePlacementHints(MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate)
extern void PlacementHintsController_add_OnTogglePlacementHints_m74DE84CC6DD60CCD720BFB6797EFA9C145A040D8 (void);
// 0x0000064D System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::remove_OnTogglePlacementHints(MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate)
extern void PlacementHintsController_remove_OnTogglePlacementHints_m4DD9666380C389D70F6B9B9DA474147403E890E9 (void);
// 0x0000064E System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::.ctor()
extern void PlacementHintsController__ctor_m63F5AE600226FB6BEACE406120A542B3EB796634 (void);
// 0x0000064F System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::.ctor(System.Object,System.IntPtr)
extern void PlacementHintsControllerDelegate__ctor_mC6033B0A83C28817C35AC67DB8440C7B75CA605B (void);
// 0x00000650 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::Invoke()
extern void PlacementHintsControllerDelegate_Invoke_mD8415531C38108E1B7B41AB3C5BE9ED938DE5BAA (void);
// 0x00000651 System.IAsyncResult MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void PlacementHintsControllerDelegate_BeginInvoke_m729BAD5B777CAC07017B397546C8485D026C1672 (void);
// 0x00000652 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::EndInvoke(System.IAsyncResult)
extern void PlacementHintsControllerDelegate_EndInvoke_m5245DC13168B9F203E7495622C99EA9C6EC60327 (void);
// 0x00000653 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::Update()
extern void AppDispatcher_Update_m6EB441148A11804FEE08FF2E9B7D0D97C23B0409 (void);
// 0x00000654 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::Enqueue(System.Collections.IEnumerator)
extern void AppDispatcher_Enqueue_m3CBD02504B9964AE4BBED84ADDD4DBA64555F77A (void);
// 0x00000655 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::Enqueue(System.Action)
extern void AppDispatcher_Enqueue_mCCC7B27F1F2D58D6C1E3FD4EC258CC67E1739BE3 (void);
// 0x00000656 System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::ActionWrapper(System.Action)
extern void AppDispatcher_ActionWrapper_m4F07C992CCAA2F5E85EFCA658F6954A49EF9D392 (void);
// 0x00000657 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::Exists()
extern void AppDispatcher_Exists_mFCAB00D8CE87E7A155674A5D7607D3A663667982 (void);
// 0x00000658 MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::Instance()
extern void AppDispatcher_Instance_m2605059D86F44BA363758BB58264D5228C37C94F (void);
// 0x00000659 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::Awake()
extern void AppDispatcher_Awake_m33258451B9D16610A07D299ED217C0E85DCD2450 (void);
// 0x0000065A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::OnDestroy()
extern void AppDispatcher_OnDestroy_m8D2A3B638D86CD0CE547AFFA18D62179F843626B (void);
// 0x0000065B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::.ctor()
extern void AppDispatcher__ctor_m9C230BDD0C6DA61407D42CCE1B9D830D2E8426AB (void);
// 0x0000065C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher::.cctor()
extern void AppDispatcher__cctor_mFCF1773E629920DF4D61207261E197735BB7389F (void);
// 0x0000065D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mA06B35493C03997F256101C531B66D289EF0C58F (void);
// 0x0000065E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<>c__DisplayClass2_0::<Enqueue>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CEnqueueU3Eb__0_mB306F90CBC4E2283AA78B4F3E5ABB0E177642F26 (void);
// 0x0000065F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<ActionWrapper>d__4::.ctor(System.Int32)
extern void U3CActionWrapperU3Ed__4__ctor_mB7FCD240C73B2B7CADC904435C5BDFCC63B00DC7 (void);
// 0x00000660 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<ActionWrapper>d__4::System.IDisposable.Dispose()
extern void U3CActionWrapperU3Ed__4_System_IDisposable_Dispose_m4F1D84162CE4C09B1D6834FC877034CD091DC9A9 (void);
// 0x00000661 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<ActionWrapper>d__4::MoveNext()
extern void U3CActionWrapperU3Ed__4_MoveNext_m8987804010E6C23DC0101B6DA8BFFD38C19F2B0D (void);
// 0x00000662 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<ActionWrapper>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActionWrapperU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4768E2C8ADE26C7330AB4AD83E442682D821F0E4 (void);
// 0x00000663 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<ActionWrapper>d__4::System.Collections.IEnumerator.Reset()
extern void U3CActionWrapperU3Ed__4_System_Collections_IEnumerator_Reset_m2689F9D4A85AF7E1CD260C36BCF1360C1F8A50EE (void);
// 0x00000664 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.AppDispatcher/<ActionWrapper>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CActionWrapperU3Ed__4_System_Collections_IEnumerator_get_Current_m50B12E0432836056134AA66E29CD38FF1C1685BF (void);
// 0x00000665 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter::Start()
extern void DebugBlobWriter_Start_m5C3BDEAE444EB5FD0862CC8C5C3FC082222A4AE2 (void);
// 0x00000666 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter::OnDestroy()
extern void DebugBlobWriter_OnDestroy_m3EC9CEE83C2ECD524C1DB136EB5B2AC544D18E20 (void);
// 0x00000667 System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter::CheckLogsToWriteCoroutine()
extern void DebugBlobWriter_CheckLogsToWriteCoroutine_mECF6232D266CE0B602E693A7549F84D006DB200B (void);
// 0x00000668 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter::WriteMessages(System.String)
extern void DebugBlobWriter_WriteMessages_mC02C6FF3EEC31F562D34F44201F0DD38B4FD58FB (void);
// 0x00000669 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter::HandleOnlogMessageReceived(System.String,System.String,UnityEngine.LogType)
extern void DebugBlobWriter_HandleOnlogMessageReceived_m7DE683EC355E247810F1EC59246596C9133F64C8 (void);
// 0x0000066A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter::.ctor()
extern void DebugBlobWriter__ctor_m06F2D9F8EF3278102366F8D9614CAA834289F1CC (void);
// 0x0000066B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_m451334A42B884B78FAE2BBDCA8FB79FAD4BD20DF (void);
// 0x0000066C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<Start>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__9_SetStateMachine_m68B53E75DE39D39B25594881F84D029E91ACE7A6 (void);
// 0x0000066D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<CheckLogsToWriteCoroutine>d__11::.ctor(System.Int32)
extern void U3CCheckLogsToWriteCoroutineU3Ed__11__ctor_m95868D81C11F87AABEDE672BD231469773CC50DE (void);
// 0x0000066E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<CheckLogsToWriteCoroutine>d__11::System.IDisposable.Dispose()
extern void U3CCheckLogsToWriteCoroutineU3Ed__11_System_IDisposable_Dispose_m9C6BA71941DC0E7D798E4B277749367A67AD9125 (void);
// 0x0000066F System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<CheckLogsToWriteCoroutine>d__11::MoveNext()
extern void U3CCheckLogsToWriteCoroutineU3Ed__11_MoveNext_m3DF048596AC130C0993D24181037A6F67103E133 (void);
// 0x00000670 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<CheckLogsToWriteCoroutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckLogsToWriteCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C30FF6D988EB7857B8D0A650D20DD10DC755E9C (void);
// 0x00000671 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<CheckLogsToWriteCoroutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CCheckLogsToWriteCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_mA6811064D23D668FBBE6DE870C8A47C0888EB96C (void);
// 0x00000672 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<CheckLogsToWriteCoroutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CCheckLogsToWriteCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_mBD72CF86AAC5FBFDC2EF67F68DD096B6A2714876 (void);
// 0x00000673 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<WriteMessages>d__12::MoveNext()
extern void U3CWriteMessagesU3Ed__12_MoveNext_m1B7731FD9248E3A64D59366816A9963C272128DF (void);
// 0x00000674 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugBlobWriter/<WriteMessages>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWriteMessagesU3Ed__12_SetStateMachine_m10ED05D0745B0BC452EC0BE04732A4E33BF8326F (void);
// 0x00000675 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugWindow::Start()
extern void DebugWindow_Start_mDE099B975E648B514D55DB1FEC5922533792A62C (void);
// 0x00000676 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugWindow::OnDestroy()
extern void DebugWindow_OnDestroy_m662F00C4DFFCB120484FD26F4CB0749FB4091024 (void);
// 0x00000677 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugWindow::HandleLog(System.String,System.String,UnityEngine.LogType)
extern void DebugWindow_HandleLog_mDE33752AD625873DC003D563C214FF78E53CA1D3 (void);
// 0x00000678 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.DebugWindow::.ctor()
extern void DebugWindow__ctor_mDF2D0A3D9E3997170F50E0FC60A592BA72B31DE8 (void);
// 0x00000679 UnityEngine.Sprite MRTK.Tutorials.AzureCloudServices.Scripts.Utilities.Utilities::CreateSprite(UnityEngine.Texture2D)
extern void Utilities_CreateSprite_mCF463CD38D7B210C1EA2CABBAA780DE897792EC7 (void);
// 0x0000067A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorArrowGuide::OnBecameInvisible()
extern void AnchorArrowGuide_OnBecameInvisible_mDE4C319B2F7667FFBB9FA5E6C4D77A6A4840E671 (void);
// 0x0000067B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorArrowGuide::SetTargetObject(UnityEngine.Transform)
extern void AnchorArrowGuide_SetTargetObject_m740A733E4CA54111AECE2C3AB27D46C2666C76D2 (void);
// 0x0000067C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorArrowGuide::.ctor()
extern void AnchorArrowGuide__ctor_mF31FF56153D90A165427D3F988FAC15DE6BA4073 (void);
// 0x0000067D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController::Start()
extern void AnchorCreationController_Start_mC9BF4F147BC3D9AB360123F29881D05759135822 (void);
// 0x0000067E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController::StartProgressIndicatorSession()
extern void AnchorCreationController_StartProgressIndicatorSession_m02296F0E95CA2166283AD647202B337BA4366E8D (void);
// 0x0000067F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController::.ctor()
extern void AnchorCreationController__ctor_m91D6321A36EF2B41F0FF28CE4919FC7FDFBF6CF5 (void);
// 0x00000680 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController::<Start>b__5_0(System.Object,System.EventArgs)
extern void AnchorCreationController_U3CStartU3Eb__5_0_mD6D058011EE332CAB234D89F8EFFF2BCA04A31FF (void);
// 0x00000681 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController::<Start>b__5_1(System.Object,System.String)
extern void AnchorCreationController_U3CStartU3Eb__5_1_mF34BECAAEABCDC10007C049A8CB6F3ED9E1D1047 (void);
// 0x00000682 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController/<StartProgressIndicatorSession>d__6::MoveNext()
extern void U3CStartProgressIndicatorSessionU3Ed__6_MoveNext_m5C3F80324352E494DC30D01C0BEC24A613F04C67 (void);
// 0x00000683 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorCreationController/<StartProgressIndicatorSession>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartProgressIndicatorSessionU3Ed__6_SetStateMachine_m78BAD933A123E38DCD4EDF9744CCDFD87D9ACB06 (void);
// 0x00000684 MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition::get_TrackedObject()
extern void AnchorPosition_get_TrackedObject_m24B438355A66245B1D8197C1100E655245D17F73 (void);
// 0x00000685 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition::Init(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void AnchorPosition_Init_mD9BCDBBCBAF2A46DBB321CADAA2A80DB15C4BC28 (void);
// 0x00000686 System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition::DelayedInitCoroutine()
extern void AnchorPosition_DelayedInitCoroutine_mF8F9A2B59B484CC8CA78EF8C71F0CC06C546B2B2 (void);
// 0x00000687 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition::.ctor()
extern void AnchorPosition__ctor_m759F414830E1EC1E3248749EE788B4A099EFF8CA (void);
// 0x00000688 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition/<DelayedInitCoroutine>d__7::.ctor(System.Int32)
extern void U3CDelayedInitCoroutineU3Ed__7__ctor_mA1FE2BB72C3531187721EDE8E70D898B348B433C (void);
// 0x00000689 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition/<DelayedInitCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CDelayedInitCoroutineU3Ed__7_System_IDisposable_Dispose_m59715A98082003E1609C01F2968C7753B48A527F (void);
// 0x0000068A System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition/<DelayedInitCoroutine>d__7::MoveNext()
extern void U3CDelayedInitCoroutineU3Ed__7_MoveNext_mFD73261168B4B39115B649D5A1132B0ECBAEA31B (void);
// 0x0000068B System.Object MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition/<DelayedInitCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedInitCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A2D0C523A7367E1271143FF534A3A4B895DBCB3 (void);
// 0x0000068C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition/<DelayedInitCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CDelayedInitCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m337B3E0A57C88CF66E48C6F72345C88A1A32C317 (void);
// 0x0000068D System.Object MRTK.Tutorials.AzureCloudServices.Scripts.UX.AnchorPosition/<DelayedInitCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedInitCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mA641DCEAEEBDF60716251380D04C4F7854070474 (void);
// 0x0000068E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.InteractableToggleRadialViewStateUpdater::Awake()
extern void InteractableToggleRadialViewStateUpdater_Awake_m0824B626D36D377AF455004269A10550C62E5402 (void);
// 0x0000068F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.InteractableToggleRadialViewStateUpdater::OnEnable()
extern void InteractableToggleRadialViewStateUpdater_OnEnable_m23A4C83ADA71A7F752E0675D985155547BAC3C3A (void);
// 0x00000690 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.UX.InteractableToggleRadialViewStateUpdater::.ctor()
extern void InteractableToggleRadialViewStateUpdater__ctor_mA3EDC1B2EC4E8CB86813E392AB8D807E52458BA2 (void);
// 0x00000691 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::add_OnCreateAnchorSucceeded(System.EventHandler`1<System.String>)
extern void AnchorManager_add_OnCreateAnchorSucceeded_mEDCCF4D5E3E9D70869AADE2CB3F86172D7445325 (void);
// 0x00000692 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::remove_OnCreateAnchorSucceeded(System.EventHandler`1<System.String>)
extern void AnchorManager_remove_OnCreateAnchorSucceeded_mF8BB49ACAB795E4F48ED321B09E79BFD42F65618 (void);
// 0x00000693 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::add_OnCreateAnchorFailed(System.EventHandler)
extern void AnchorManager_add_OnCreateAnchorFailed_mB5AE1ACE26A3662017D539C636923DAA847D586E (void);
// 0x00000694 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::remove_OnCreateAnchorFailed(System.EventHandler)
extern void AnchorManager_remove_OnCreateAnchorFailed_mC562E27DB28CD5DC4579AEA6BCE918A256AB04F9 (void);
// 0x00000695 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::add_OnFindAnchorSucceeded(System.EventHandler)
extern void AnchorManager_add_OnFindAnchorSucceeded_mDDDA48475F0061CA5FB72343EEA920485989785A (void);
// 0x00000696 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::remove_OnFindAnchorSucceeded(System.EventHandler)
extern void AnchorManager_remove_OnFindAnchorSucceeded_mC01468A1E13B33DC3185BE0B2CAC068514BFCDD3 (void);
// 0x00000697 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::add_OnPlaceAnchorCanceled(System.EventHandler)
extern void AnchorManager_add_OnPlaceAnchorCanceled_m5B3667536C9A7B603D72CBEE018B449E79F1CC02 (void);
// 0x00000698 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::remove_OnPlaceAnchorCanceled(System.EventHandler)
extern void AnchorManager_remove_OnPlaceAnchorCanceled_m4439B883FF868EEE7A33C867463E0A5B8B6199EA (void);
// 0x00000699 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::Start()
extern void AnchorManager_Start_mD7694DF37253C3F1F0E4D0B7C7D32ABD42F072D8 (void);
// 0x0000069A System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::CheckIsAnchorActiveForTrackedObject(System.String)
extern void AnchorManager_CheckIsAnchorActiveForTrackedObject_m00ADE86A6DC4FFDD519A9AF3CA8B4F495A8DF25A (void);
// 0x0000069B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::GuideToAnchor(System.String)
extern void AnchorManager_GuideToAnchor_m88963F8C80BA51DA18CAEEB0931972C920A007B0 (void);
// 0x0000069C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::StartPlacingAnchor(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void AnchorManager_StartPlacingAnchor_m7C3DE1C794074B2C68FA63BD8E5FBFB626A9D6BC (void);
// 0x0000069D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::CreateAnchor(UnityEngine.Transform)
extern void AnchorManager_CreateAnchor_m091A9B78381E45970CC7D8E215D494B4ADBE40AE (void);
// 0x0000069E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::FindAnchor(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void AnchorManager_FindAnchor_m8028B9243EDC01A06E346717BC75234A557DC946 (void);
// 0x0000069F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::CreateAsaAnchorEditor(UnityEngine.Transform)
extern void AnchorManager_CreateAsaAnchorEditor_mFADFD0B9F63E856C9F0EC18DF71B858C445FB7D7 (void);
// 0x000006A0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::CreateAsaAnchor(UnityEngine.Transform)
extern void AnchorManager_CreateAsaAnchor_m9155A8192A1CB025F38F4362F5290100ED772A89 (void);
// 0x000006A1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::FindAsaAnchor()
extern void AnchorManager_FindAsaAnchor_m62D25C4038B5F8583FCA018CACEEC60FBF616441 (void);
// 0x000006A2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::FindAsaAnchorEditor()
extern void AnchorManager_FindAsaAnchorEditor_mD6A9411075AE362CF19227F095A5C6B60FABBB16 (void);
// 0x000006A3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::StopAzureSession()
extern void AnchorManager_StopAzureSession_m4F2363B780C097CEFC758F6F7911FEC4BA6C7CF6 (void);
// 0x000006A4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::HandleAnchorLocated(System.Object,Microsoft.Azure.SpatialAnchors.AnchorLocatedEventArgs)
extern void AnchorManager_HandleAnchorLocated_m422CB3B2AC259E66D344B2C89F0AF51C1BD34905 (void);
// 0x000006A5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::HandleOnAnchorPlaced(System.Object,UnityEngine.Transform)
extern void AnchorManager_HandleOnAnchorPlaced_mFF6A5045F37C6E7BE6036AF1B1AC6FF7CAB61720 (void);
// 0x000006A6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::HandleOnAnchorPlacementCanceled(System.Object,System.EventArgs)
extern void AnchorManager_HandleOnAnchorPlacementCanceled_mF5A616C9E50182E3C74035268EE10BE8D03DCADD (void);
// 0x000006A7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::.ctor()
extern void AnchorManager__ctor_mFBC62411AD152C71A391F5180675CDF4B4D4F5DC (void);
// 0x000006A8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::<FindAsaAnchorEditor>b__32_0()
extern void AnchorManager_U3CFindAsaAnchorEditorU3Eb__32_0_mFF0BC6A0A093A8BC7DF482157EE95530A0DEA53D (void);
// 0x000006A9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager::<HandleAnchorLocated>b__34_0()
extern void AnchorManager_U3CHandleAnchorLocatedU3Eb__34_0_mF742397B00B8EBB2FC98DFE31757EDFA183D72BB (void);
// 0x000006AA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m431B16D278BC0DFCD58984DC93BCCFC728932239 (void);
// 0x000006AB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m188598A0F3F7C8877BD4518AB6EE6368C95D7EFC (void);
// 0x000006AC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c::<Start>b__23_0(System.Object,Microsoft.Azure.SpatialAnchors.SessionUpdatedEventArgs)
extern void U3CU3Ec_U3CStartU3Eb__23_0_m98A4233CA4B0B2CBA4B60163E6DB7CF2A823F970 (void);
// 0x000006AD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c::<Start>b__23_1(System.Object,Microsoft.Azure.SpatialAnchors.OnLogDebugEventArgs)
extern void U3CU3Ec_U3CStartU3Eb__23_1_m653EF0BFFADA6994A82C6E81F80436E4E680F7DF (void);
// 0x000006AE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m5359BAE5D521CDD17D978EF0F4145EC0AF1736A9 (void);
// 0x000006AF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass29_0::<CreateAsaAnchorEditor>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CCreateAsaAnchorEditorU3Eb__0_m47611DC915B8096BA736921257E2022B438A4CFF (void);
// 0x000006B0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<CreateAsaAnchorEditor>d__29::MoveNext()
extern void U3CCreateAsaAnchorEditorU3Ed__29_MoveNext_mF72D7A571AF2F5E9CB2719F72CCED585ED23CAF9 (void);
// 0x000006B1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<CreateAsaAnchorEditor>d__29::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateAsaAnchorEditorU3Ed__29_SetStateMachine_m61AFA54E5F838432398C8D4AC8E44F34FF5075F0 (void);
// 0x000006B2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m7839B54008BF25EFC6FA40A16E34C38A3D0EA394 (void);
// 0x000006B3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass30_0::<CreateAsaAnchor>b__1()
extern void U3CU3Ec__DisplayClass30_0_U3CCreateAsaAnchorU3Eb__1_m600D9EE08EB6E88645687265E7474CD852C19954 (void);
// 0x000006B4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass30_0::<CreateAsaAnchor>b__2()
extern void U3CU3Ec__DisplayClass30_0_U3CCreateAsaAnchorU3Eb__2_mA7A3AF644A134A2E0D97D59B106DBE9E1762FE48 (void);
// 0x000006B5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass30_1::.ctor()
extern void U3CU3Ec__DisplayClass30_1__ctor_mED1F47357F43E54195BD71294D2987ECAA31C834 (void);
// 0x000006B6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<>c__DisplayClass30_1::<CreateAsaAnchor>b__0()
extern void U3CU3Ec__DisplayClass30_1_U3CCreateAsaAnchorU3Eb__0_mE26C6480FD61A4DA41B52D2577134E735CF90AD1 (void);
// 0x000006B7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<CreateAsaAnchor>d__30::MoveNext()
extern void U3CCreateAsaAnchorU3Ed__30_MoveNext_m01F48D0EE30F81576103027AFB86F2454088B3C3 (void);
// 0x000006B8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<CreateAsaAnchor>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateAsaAnchorU3Ed__30_SetStateMachine_m159D78088F46C450CE0E21FFDBF6C0ABA8A7EB5B (void);
// 0x000006B9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<FindAsaAnchor>d__31::MoveNext()
extern void U3CFindAsaAnchorU3Ed__31_MoveNext_m7471F513BDE1BB5388A834CE009D3063AE8DC942 (void);
// 0x000006BA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<FindAsaAnchor>d__31::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFindAsaAnchorU3Ed__31_SetStateMachine_m2DE169F9B79FCD78627B470D2C4682A2C469476A (void);
// 0x000006BB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<FindAsaAnchorEditor>d__32::MoveNext()
extern void U3CFindAsaAnchorEditorU3Ed__32_MoveNext_m706A98BDD79F2E0C87675081DB12AF0D446C5B80 (void);
// 0x000006BC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<FindAsaAnchorEditor>d__32::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFindAsaAnchorEditorU3Ed__32_SetStateMachine_mF067B93045A8B49D35BD254868D8DB7AD322AB1A (void);
// 0x000006BD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<StopAzureSession>d__33::MoveNext()
extern void U3CStopAzureSessionU3Ed__33_MoveNext_m63FC6BEFC62458445D2C673734A04937AE211A53 (void);
// 0x000006BE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager/<StopAzureSession>d__33::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStopAzureSessionU3Ed__33_SetStateMachine_m512A308EC6C9E9CB8A84D245069CBBE1FC1AE533 (void);
// 0x000006BF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::add_OnConversationStarted(System.EventHandler`1<System.String>)
extern void ChatBotManager_add_OnConversationStarted_m7B0D3310DB1FB48D212BA56A8FAE8C33966DEC44 (void);
// 0x000006C0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::remove_OnConversationStarted(System.EventHandler`1<System.String>)
extern void ChatBotManager_remove_OnConversationStarted_mF4CCA05D98B5A60FEF84C537DE4621557BCAB436 (void);
// 0x000006C1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::add_OnMessageSent(System.EventHandler`1<System.String>)
extern void ChatBotManager_add_OnMessageSent_mB37CD86E4ECE8194B050B6E9CD980671E0884C9D (void);
// 0x000006C2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::remove_OnMessageSent(System.EventHandler`1<System.String>)
extern void ChatBotManager_remove_OnMessageSent_mA0D73F735FA2A9797E0C75440127008E3451C62D (void);
// 0x000006C3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::add_OnMessagesReceived(System.EventHandler`1<System.Collections.Generic.IList`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity>>)
extern void ChatBotManager_add_OnMessagesReceived_mCDA220CDE6675719B94E89E95CA7E586E5AF70FF (void);
// 0x000006C4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::remove_OnMessagesReceived(System.EventHandler`1<System.Collections.Generic.IList`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity>>)
extern void ChatBotManager_remove_OnMessagesReceived_m4DE9BE0CB7D85E49AB3F0BE2BD5404D1CA3F8FCB (void);
// 0x000006C5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::Awake()
extern void ChatBotManager_Awake_mE8703AC5534D1A0576690BFAC9D26F1A62795CC4 (void);
// 0x000006C6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::StartConversation()
extern void ChatBotManager_StartConversation_m456ECCBA67CB172299CF84DD1A630A536CC9360F (void);
// 0x000006C7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::ReceiveMessages(System.String)
extern void ChatBotManager_ReceiveMessages_m624C5C57EF297A70D5D74B658A1E37D7CE529586 (void);
// 0x000006C8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::SentMessage(System.String,System.String,System.String)
extern void ChatBotManager_SentMessage_m98584DF6519272AE3AB3462C4DC8ABA395D83E15 (void);
// 0x000006C9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::HandleBotResponse(System.Object,MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs)
extern void ChatBotManager_HandleBotResponse_mEEE9C0703CD1A1363D0D247C1F3807514BA7B230 (void);
// 0x000006CA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ChatBotManager::.ctor()
extern void ChatBotManager__ctor_mB234C30783FFAA9215C5576432688D980962FDE9 (void);
// 0x000006CB System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::get_IsReady()
extern void DataManager_get_IsReady_mE96F544BFF278A16693DCA07134C1D20D5938AFC (void);
// 0x000006CC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::set_IsReady(System.Boolean)
extern void DataManager_set_IsReady_m008AEAAA5105A52BB110766D519DE1EAA904CC31 (void);
// 0x000006CD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::Awake()
extern void DataManager_Awake_m17FF6C4D2BA3B35E05784F89FDA17A764B6DD8B4 (void);
// 0x000006CE System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::GetOrCreateProject()
extern void DataManager_GetOrCreateProject_mD41C02BECEEE074BB2930A93BB89F22669B57A4C (void);
// 0x000006CF System.Threading.Tasks.Task`1<System.Boolean> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::UpdateProject(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project)
extern void DataManager_UpdateProject_m34A62BBF7137D6EC94D7D0A95A6D000DE36E0921 (void);
// 0x000006D0 System.Threading.Tasks.Task`1<System.Boolean> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::UploadOrUpdate(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void DataManager_UploadOrUpdate_m7C17D2E7B2ECC0FF48C8F57E24440770CCDAF408 (void);
// 0x000006D1 System.Threading.Tasks.Task`1<System.Collections.Generic.List`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject>> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::GetAllTrackedObjects()
extern void DataManager_GetAllTrackedObjects_mF9B03702BCD842690311560B3E6CA79871BCEC13 (void);
// 0x000006D2 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::FindTrackedObjectById(System.String)
extern void DataManager_FindTrackedObjectById_m9EE1DAB74335B1BC211C06024031D59AF3B26A84 (void);
// 0x000006D3 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::FindTrackedObjectByName(System.String)
extern void DataManager_FindTrackedObjectByName_mA2CF50F7FBB278EA0F11876581AFCDBB4F6793B5 (void);
// 0x000006D4 System.Threading.Tasks.Task`1<System.Boolean> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::DeleteTrackedObject(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void DataManager_DeleteTrackedObject_m9970CF7CE1AFC179809AA40F279C5C1786C0B6B1 (void);
// 0x000006D5 System.Threading.Tasks.Task`1<System.String> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::UploadBlob(System.Byte[],System.String)
extern void DataManager_UploadBlob_m6AF38340678594A978E8527293EB799BEF5FEB33 (void);
// 0x000006D6 System.Threading.Tasks.Task`1<System.Byte[]> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::DownloadBlob(System.String)
extern void DataManager_DownloadBlob_mF80F866B66A4FCFD669ABA804DD3D1D9B91962A5 (void);
// 0x000006D7 System.Threading.Tasks.Task`1<System.Boolean> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::DeleteBlob(System.String)
extern void DataManager_DeleteBlob_m72000A175EB253F13BB3DBC747F3ECA1A76D565F (void);
// 0x000006D8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager::.ctor()
extern void DataManager__ctor_mFFD4AE09B5E90423F34D7ED76C9D554A781CE361 (void);
// 0x000006D9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<Awake>d__20::MoveNext()
extern void U3CAwakeU3Ed__20_MoveNext_m3AD421FA6A7A4884310255D9AA87D2BF7A4B285B (void);
// 0x000006DA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<Awake>d__20::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAwakeU3Ed__20_SetStateMachine_mD715611BF40330195B4B6D43446F58DBC8D53981 (void);
// 0x000006DB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<GetOrCreateProject>d__21::MoveNext()
extern void U3CGetOrCreateProjectU3Ed__21_MoveNext_m8A4493BBC01000EDB711FE161F6AA7AA01BE3453 (void);
// 0x000006DC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<GetOrCreateProject>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetOrCreateProjectU3Ed__21_SetStateMachine_m9B12E853B601A0591C6A8837C92F5BD739D595B4 (void);
// 0x000006DD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<UpdateProject>d__22::MoveNext()
extern void U3CUpdateProjectU3Ed__22_MoveNext_m5D02DF33E77BEB1166F8E4795C509AAA6A73A13C (void);
// 0x000006DE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<UpdateProject>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUpdateProjectU3Ed__22_SetStateMachine_m4829BCBCBAD690F5C79D4AB6E91E52CFEDEB1094 (void);
// 0x000006DF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<UploadOrUpdate>d__23::MoveNext()
extern void U3CUploadOrUpdateU3Ed__23_MoveNext_m97EAEAE6F3ED132508622ACB8FA7E36C76416657 (void);
// 0x000006E0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<UploadOrUpdate>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUploadOrUpdateU3Ed__23_SetStateMachine_m44AACB808A3BC1A9D9B3AD96590EFE499D57DD07 (void);
// 0x000006E1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<GetAllTrackedObjects>d__24::MoveNext()
extern void U3CGetAllTrackedObjectsU3Ed__24_MoveNext_m29F6EF2E0BE57F0A1CDF2CD592E11A82B8BD8396 (void);
// 0x000006E2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<GetAllTrackedObjects>d__24::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetAllTrackedObjectsU3Ed__24_SetStateMachine_m3632968E435819290A63D8F402C8DCDBBF1F4094 (void);
// 0x000006E3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<FindTrackedObjectById>d__25::MoveNext()
extern void U3CFindTrackedObjectByIdU3Ed__25_MoveNext_m123A428C131B1E6C966CF3102730AA332B6DD26E (void);
// 0x000006E4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<FindTrackedObjectById>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFindTrackedObjectByIdU3Ed__25_SetStateMachine_m33AA8DFF1B28BEEDBA247995E19C7D2CA0A7F45A (void);
// 0x000006E5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<FindTrackedObjectByName>d__26::MoveNext()
extern void U3CFindTrackedObjectByNameU3Ed__26_MoveNext_m7C087B0BFA047015B30C1BAF9ECE6DF88124B63C (void);
// 0x000006E6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<FindTrackedObjectByName>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFindTrackedObjectByNameU3Ed__26_SetStateMachine_m785410F74E274E94EFC441418EAF3DCEC565839E (void);
// 0x000006E7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<DeleteTrackedObject>d__27::MoveNext()
extern void U3CDeleteTrackedObjectU3Ed__27_MoveNext_m056A71511FD99FA70CD926B62B523EB1B5078AB0 (void);
// 0x000006E8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<DeleteTrackedObject>d__27::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDeleteTrackedObjectU3Ed__27_SetStateMachine_mBDB7E59EF871FB402C7A575D0EA212DDAB3C6A48 (void);
// 0x000006E9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<UploadBlob>d__28::MoveNext()
extern void U3CUploadBlobU3Ed__28_MoveNext_m67369901C66B76F01F0E7F14DA8E3F41CDA25CED (void);
// 0x000006EA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<UploadBlob>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUploadBlobU3Ed__28_SetStateMachine_mD30FB69399F3DE0ECDF4B04386ECF2ACF49FEBAA (void);
// 0x000006EB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<DownloadBlob>d__29::MoveNext()
extern void U3CDownloadBlobU3Ed__29_MoveNext_mD33AC5DF8B290B4404804FD711CA1F15F51B43AE (void);
// 0x000006EC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<DownloadBlob>d__29::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDownloadBlobU3Ed__29_SetStateMachine_m6931AFC6D0CE2EA2A83EF31881469DCC10B787FD (void);
// 0x000006ED System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<DeleteBlob>d__30::MoveNext()
extern void U3CDeleteBlobU3Ed__30_MoveNext_mFD9445A5EE67292CF8E920D243907AC9706F2CB9 (void);
// 0x000006EE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager/<DeleteBlob>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDeleteBlobU3Ed__30_SetStateMachine_mCA39473460418F90AEEABA0C98F68CA59F2BA6C4 (void);
// 0x000006EF System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::CreateTag(System.String)
extern void ObjectDetectionManager_CreateTag_m6F3C5F68073D56C78BCB59701163B57D3A02CBD8 (void);
// 0x000006F0 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagesCreatedResult> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::UploadTrainingImage(System.Byte[],System.String)
extern void ObjectDetectionManager_UploadTrainingImage_mD4ABA5444DE28E7067005D66C49766399B88E183 (void);
// 0x000006F1 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::TrainProject()
extern void ObjectDetectionManager_TrainProject_m92FE29B8DE15C11173C1A7071BAD6F500B1022E7 (void);
// 0x000006F2 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::GetTrainingStatus(System.String)
extern void ObjectDetectionManager_GetTrainingStatus_mF9AD7CCD3044CC7F70B3AF910268230FFF1E3532 (void);
// 0x000006F3 System.Threading.Tasks.Task`1<System.Boolean> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::PublishTrainingIteration(System.String,System.String)
extern void ObjectDetectionManager_PublishTrainingIteration_m968FB407BFC6564DDA4106F63C24D5EB38F698F8 (void);
// 0x000006F4 System.Threading.Tasks.Task`1<System.Boolean> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::DeleteTrainingIteration(System.String)
extern void ObjectDetectionManager_DeleteTrainingIteration_mE57BC004B250A4D95268AC04220A7249CC9269F9 (void);
// 0x000006F5 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::DetectImage(System.Byte[],System.String)
extern void ObjectDetectionManager_DetectImage_m4528CB10FD3416FED8AF795DD7967F857746D32C (void);
// 0x000006F6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager::.ctor()
extern void ObjectDetectionManager__ctor_mB48BEDB4F6335BF495DC215F4DC4E6ACB3435CB6 (void);
// 0x000006F7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<CreateTag>d__6::MoveNext()
extern void U3CCreateTagU3Ed__6_MoveNext_m262D3BFCC6F7DE95508858D0AE759E8C36679691 (void);
// 0x000006F8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<CreateTag>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateTagU3Ed__6_SetStateMachine_mE13F1D769729E4EE7399FF3C3AC5FBFC3407D1D7 (void);
// 0x000006F9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<UploadTrainingImage>d__7::MoveNext()
extern void U3CUploadTrainingImageU3Ed__7_MoveNext_m64280BED4257534201728C7A53DB60BF099E664C (void);
// 0x000006FA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<UploadTrainingImage>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUploadTrainingImageU3Ed__7_SetStateMachine_mE12CB7D9ECCADB497F0DF92A83E5279147AA0EB3 (void);
// 0x000006FB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<TrainProject>d__8::MoveNext()
extern void U3CTrainProjectU3Ed__8_MoveNext_m4309543749D82E359B04A4A87B685B1E684E304C (void);
// 0x000006FC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<TrainProject>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CTrainProjectU3Ed__8_SetStateMachine_m8127814BE515070D4B1695ACD6269A26AD7DE841 (void);
// 0x000006FD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<GetTrainingStatus>d__9::MoveNext()
extern void U3CGetTrainingStatusU3Ed__9_MoveNext_m5231D81B76D768CCE0069A4E6D63081F4268C5AC (void);
// 0x000006FE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<GetTrainingStatus>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetTrainingStatusU3Ed__9_SetStateMachine_m68EB6F0F3AB7E0F2A09B5FCC644AA1E468BDEA1D (void);
// 0x000006FF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<PublishTrainingIteration>d__10::MoveNext()
extern void U3CPublishTrainingIterationU3Ed__10_MoveNext_m97403A57DA6CC5895BB5D11BAC7FF96AA801601C (void);
// 0x00000700 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<PublishTrainingIteration>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPublishTrainingIterationU3Ed__10_SetStateMachine_m809D97A970A63210B1B4891447D9C9D8D98372F2 (void);
// 0x00000701 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<DeleteTrainingIteration>d__11::MoveNext()
extern void U3CDeleteTrainingIterationU3Ed__11_MoveNext_m64F345A69A88C6E46ADD9282B515F0B3E46F5FD4 (void);
// 0x00000702 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<DeleteTrainingIteration>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDeleteTrainingIterationU3Ed__11_SetStateMachine_m73D10666246CC785EF9287162F7D96AA177FE7DC (void);
// 0x00000703 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<DetectImage>d__12::MoveNext()
extern void U3CDetectImageU3Ed__12_MoveNext_mF88473934D84B02005B7AE864694836F5CEDC84F (void);
// 0x00000704 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager/<DetectImage>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDetectImageU3Ed__12_SetStateMachine_mEE11B0C103C209B3D7505A47206DFF6FC5C2E015 (void);
// 0x00000705 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::set_IsCameraActive(System.Boolean)
extern void SceneController_set_IsCameraActive_m70A081F1C8BC03CD1536FF21CF00C52B54E0A2CD (void);
// 0x00000706 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::get_IsCameraActive()
extern void SceneController_get_IsCameraActive_mC06009624527BF7073B28444CB61F15CF9E05085 (void);
// 0x00000707 MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::get_CurrentProject()
extern void SceneController_get_CurrentProject_mD30DB395AFD0776DADAC1BBFD1E0249C5920EE3D (void);
// 0x00000708 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::set_CurrentProject(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project)
extern void SceneController_set_CurrentProject_m36ECB2D8C1FC90880CD74095DEFCB4F7F90BA4B0 (void);
// 0x00000709 MRTK.Tutorials.AzureCloudServices.Scripts.Managers.DataManager MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::get_DataManager()
extern void SceneController_get_DataManager_m9A0DB668314AED236646FFD37E3C2417B5F38D42 (void);
// 0x0000070A MRTK.Tutorials.AzureCloudServices.Scripts.Managers.ObjectDetectionManager MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::get_ObjectDetectionManager()
extern void SceneController_get_ObjectDetectionManager_m2E79566E8AD20FA4DA97456A21CA654B5FAAAC0C (void);
// 0x0000070B MRTK.Tutorials.AzureCloudServices.Scripts.Managers.AnchorManager MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::get_AnchorManager()
extern void SceneController_get_AnchorManager_m92ED5C44A96922E87FD4BF2128507A286DBA48AF (void);
// 0x0000070C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::Start()
extern void SceneController_Start_mD7462702546DFB9CF2D0BF96E3A003A16338B92A (void);
// 0x0000070D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::Init()
extern void SceneController_Init_m8F8C8AFF1CBAB7A457E0DA9EAD1502B1F4238B2B (void);
// 0x0000070E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::StartCamera()
extern void SceneController_StartCamera_mAD71F0DC604DED6869A614E58808E9F08BAB2ACF (void);
// 0x0000070F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::StopCamera()
extern void SceneController_StopCamera_m6105C85CBF019E865F291AAA20176FA4DC3439BA (void);
// 0x00000710 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::StartPhotoMode()
extern void SceneController_StartPhotoMode_mBABC7E63AE4DBB5AEBAD0105B81A27CA13B9BA50 (void);
// 0x00000711 System.Threading.Tasks.Task`1<System.Byte[]> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::TakePhoto()
extern void SceneController_TakePhoto_mFC59C88BFAE0FF19275265698564F9C060FC92E1 (void);
// 0x00000712 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::TakePhotoWithThumbnail()
extern void SceneController_TakePhotoWithThumbnail_m3DD1226E7C65B9312B43700EAEFD0EFBECC494A6 (void);
// 0x00000713 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::OpenConnectionMenu()
extern void SceneController_OpenConnectionMenu_m67BCE3A485AB7D25D1BEDDC59900BF34B62314A3 (void);
// 0x00000714 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::.ctor()
extern void SceneController__ctor_m0788EABA785925A129BF621F2AB4DEE16EE9D732 (void);
// 0x00000715 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::<StartCamera>b__23_0(UnityEngine.Windows.WebCam.PhotoCapture)
extern void SceneController_U3CStartCameraU3Eb__23_0_mA8ECBEE532C173CE47A6FB699348734941E45721 (void);
// 0x00000716 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::<StopCamera>b__24_0(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult)
extern void SceneController_U3CStopCameraU3Eb__24_0_m03A389918961530F8D9A478BA58C25C4E3A55471 (void);
// 0x00000717 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::<StartPhotoMode>b__25_1(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult)
extern void SceneController_U3CStartPhotoModeU3Eb__25_1_m02706633973B4A92AD113B08C4A04E54029CD081 (void);
// 0x00000718 System.Threading.Tasks.Task`1<System.Byte[]> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::<TakePhoto>b__26_0()
extern void SceneController_U3CTakePhotoU3Eb__26_0_m18B197611F353CCAD60CB214959BD934237D657B (void);
// 0x00000719 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail> MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController::<TakePhotoWithThumbnail>b__27_0()
extern void SceneController_U3CTakePhotoWithThumbnailU3Eb__27_0_m47ACDA6315A0FE214D6602B1974B67CB61D19EFA (void);
// 0x0000071A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<Init>d__22::MoveNext()
extern void U3CInitU3Ed__22_MoveNext_m2153D52A515E4952E299C2AFAA57AAAAF472DEDE (void);
// 0x0000071B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<Init>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInitU3Ed__22_SetStateMachine_mA7572AF7FDC75F69E8146D527BFCDCDAC583E03F (void);
// 0x0000071C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c::.cctor()
extern void U3CU3Ec__cctor_m83A83018D520BEC76DF1EC33932552C77CCCA30E (void);
// 0x0000071D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c::.ctor()
extern void U3CU3Ec__ctor_mE9EC0CB77BED37307EF19FDA2E554C402F4C0837 (void);
// 0x0000071E System.Int32 MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c::<StartPhotoMode>b__25_0(UnityEngine.Resolution)
extern void U3CU3Ec_U3CStartPhotoModeU3Eb__25_0_m7C5BFE2399E0E8EC8012C9DE5C03145AA85B06EC (void);
// 0x0000071F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m5434BB95A62FF791CB31573153BB38CB8A91AFD5 (void);
// 0x00000720 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c__DisplayClass26_0::<TakePhoto>b__1()
extern void U3CU3Ec__DisplayClass26_0_U3CTakePhotoU3Eb__1_m364843730F8BD96AA0AB60359886411221167A27 (void);
// 0x00000721 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c__DisplayClass26_0::<TakePhoto>b__2(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult,UnityEngine.Windows.WebCam.PhotoCaptureFrame)
extern void U3CU3Ec__DisplayClass26_0_U3CTakePhotoU3Eb__2_mF9E0ED144F5AAC0BED50DCC8E0BAFA0A8887992A (void);
// 0x00000722 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m21D4A71F08DE6D9BACD9BCF9C9471AD2C5A10D84 (void);
// 0x00000723 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c__DisplayClass27_0::<TakePhotoWithThumbnail>b__1()
extern void U3CU3Ec__DisplayClass27_0_U3CTakePhotoWithThumbnailU3Eb__1_mB82D28E74E9758D9B2BB489A561FBF26EEE41D32 (void);
// 0x00000724 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Managers.SceneController/<>c__DisplayClass27_0::<TakePhotoWithThumbnail>b__2(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult,UnityEngine.Windows.WebCam.PhotoCaptureFrame)
extern void U3CU3Ec__DisplayClass27_0_U3CTakePhotoWithThumbnailU3Eb__2_m562554E6F633123D6DC33D18A1EAD31E2DCFCE6A (void);
// 0x00000725 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::get_SourceUrl()
extern void ImageElement_get_SourceUrl_m082DAED5E0D4B3EC6A59DBFDB0F5A6EC0B81D6B5 (void);
// 0x00000726 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::set_SourceUrl(System.String)
extern void ImageElement_set_SourceUrl_m0C027154BC650B6C754C234E3369E435E22D11BB (void);
// 0x00000727 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::get_Status()
extern void ImageElement_get_Status_m62C2325304AF8547D8770C17565FD2BC58CB1C3C (void);
// 0x00000728 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::set_Status(System.String)
extern void ImageElement_set_Status_m7651564090F8FB0F0CE921C4A2EFC27BFB2FEDD3 (void);
// 0x00000729 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::get_Image()
extern void ImageElement_get_Image_m159EFA377B97D9856132DDFC0855A76A1F46EB47 (void);
// 0x0000072A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::set_Image(MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo)
extern void ImageElement_set_Image_m09B0EE1D36961826F6306A9F2BAB8A442EEB689D (void);
// 0x0000072B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement::.ctor()
extern void ImageElement__ctor_m6B68CD872E2E371AF217B9766DEC5BFD2ADA4BD6 (void);
// 0x0000072C System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_Id()
extern void ImageInfo_get_Id_mE8139FCF7144BD8C6267D05D437B77A2A1A82C38 (void);
// 0x0000072D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_Id(System.String)
extern void ImageInfo_set_Id_m2DD4072BAA646243FE7E591D5FEE1411BA537B82 (void);
// 0x0000072E System.DateTimeOffset MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_Created()
extern void ImageInfo_get_Created_mB5C366603769FE94C503D228ABA5D9ECFCFD2C10 (void);
// 0x0000072F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_Created(System.DateTimeOffset)
extern void ImageInfo_set_Created_m071237FC0B2FB95B40839367932D995B1BEB81EA (void);
// 0x00000730 System.Int32 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_Width()
extern void ImageInfo_get_Width_m2D9159F98EEC0293CFF445C4A98EEEAC80245364 (void);
// 0x00000731 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_Width(System.Int32)
extern void ImageInfo_set_Width_mF5D4F4F69682DCB7B654EB67E5FE1EB4F931E78F (void);
// 0x00000732 System.Int32 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_Height()
extern void ImageInfo_get_Height_m907CC3DE9360BC143083A0FCA81EE98C23608581 (void);
// 0x00000733 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_Height(System.Int32)
extern void ImageInfo_set_Height_mFCCACEE5A5D0BBD408C8870FF252D90A978B5AD7 (void);
// 0x00000734 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_ResizedImageUri()
extern void ImageInfo_get_ResizedImageUri_m6F59C7B30FC6E46E6F3A7EC09ADB7F9F30984207 (void);
// 0x00000735 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_ResizedImageUri(System.String)
extern void ImageInfo_set_ResizedImageUri_m712E6C400E412E1B3792A084E24C24E921CB29C7 (void);
// 0x00000736 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_OriginalImageUri()
extern void ImageInfo_get_OriginalImageUri_m32FFDC23C729C638CF63542ED72E53D0B924F344 (void);
// 0x00000737 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_OriginalImageUri(System.String)
extern void ImageInfo_set_OriginalImageUri_m26D97EF5F9603905E911B0294D834DE522BDE6C7 (void);
// 0x00000738 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_ThumbnailUri()
extern void ImageInfo_get_ThumbnailUri_mA9CD48E0C60A24CA0490BC961641B1B49A264196 (void);
// 0x00000739 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_ThumbnailUri(System.String)
extern void ImageInfo_set_ThumbnailUri_mB8FF97D7816E84F12DBB1D58C8E23543AD128917 (void);
// 0x0000073A MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag[] MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::get_Tags()
extern void ImageInfo_get_Tags_mCC32B681EF7DB649E561890E01C4641BBF76DBD3 (void);
// 0x0000073B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::set_Tags(MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag[])
extern void ImageInfo_set_Tags_m7ED37CB2454210B8C8F66A1AEA7D3A824D4277C6 (void);
// 0x0000073C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageInfo::.ctor()
extern void ImageInfo__ctor_mBEA17D16F17302222B5B89C684C8E76C4617F4A2 (void);
// 0x0000073D System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::get_Id()
extern void ImagePredictionResult_get_Id_mCF0130A073E2F792BE7F6109A20DBF33A3027663 (void);
// 0x0000073E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::set_Id(System.String)
extern void ImagePredictionResult_set_Id_m2D5187FAB3B6318AF065AA668B1DF1A8BAE3B472 (void);
// 0x0000073F System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::get_Project()
extern void ImagePredictionResult_get_Project_mCACB9B9884F6FAA453F6DB5702CBE208D8980387 (void);
// 0x00000740 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::set_Project(System.String)
extern void ImagePredictionResult_set_Project_mAC849D758FDEACB95E541BC7E141D1AFE66226E4 (void);
// 0x00000741 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::get_Iteration()
extern void ImagePredictionResult_get_Iteration_mC09C25CAB1CA1E4D123B2F413F87CF5616F5C5FE (void);
// 0x00000742 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::set_Iteration(System.String)
extern void ImagePredictionResult_set_Iteration_m5D4D098FEA00A5FA1FD2809B02A06424A0F4A32A (void);
// 0x00000743 System.DateTimeOffset MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::get_Created()
extern void ImagePredictionResult_get_Created_m0FD2C1F80180CCC090CAF68C275C2C3BC2DFC232 (void);
// 0x00000744 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::set_Created(System.DateTimeOffset)
extern void ImagePredictionResult_set_Created_m3905D391F539F8D3918A5482E0ED3306C31F8DA9 (void);
// 0x00000745 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction[] MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::get_Predictions()
extern void ImagePredictionResult_get_Predictions_m134157D88681D3E38252827B59A19B497C5A279F (void);
// 0x00000746 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::set_Predictions(MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction[])
extern void ImagePredictionResult_set_Predictions_m65CE5660328BBA905F38C4E70F0CDB6B78A0A2D3 (void);
// 0x00000747 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagePredictionResult::.ctor()
extern void ImagePredictionResult__ctor_m7017714579DFF253DE83E222242A3063CD3EE9F9 (void);
// 0x00000748 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::get_Id()
extern void ImageQuicktestResult_get_Id_mD16398F97E831CCB47718B5908C6F1310B5E7CD7 (void);
// 0x00000749 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::set_Id(System.String)
extern void ImageQuicktestResult_set_Id_mD08000B5AA8A0919667E2895CAADE5F9C0DDCD9C (void);
// 0x0000074A System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::get_Project()
extern void ImageQuicktestResult_get_Project_m6E016A6EDFEEA8677F5689C3B2C23D5C90B52AEA (void);
// 0x0000074B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::set_Project(System.String)
extern void ImageQuicktestResult_set_Project_mCD881BBEE9894970ABE73253573DC338FA2DED21 (void);
// 0x0000074C System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::get_Iteration()
extern void ImageQuicktestResult_get_Iteration_m9E9DE64B9F3E1E875F6327E2DBB377ADD7A31411 (void);
// 0x0000074D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::set_Iteration(System.String)
extern void ImageQuicktestResult_set_Iteration_m41180178AA0076801B5C078CBF7A8F9DDCE0B580 (void);
// 0x0000074E System.DateTimeOffset MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::get_Created()
extern void ImageQuicktestResult_get_Created_mBD4F7CF8E8417919D632AAFFC9B9F5F16D41468E (void);
// 0x0000074F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::set_Created(System.DateTimeOffset)
extern void ImageQuicktestResult_set_Created_m618F469E5C7DD2BB1F4C6027E4DA6F071CD41AC1 (void);
// 0x00000750 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction[] MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::get_Predictions()
extern void ImageQuicktestResult_get_Predictions_m99D50CA63FDEBA3FB0581EE7D442E321B6F1BCC3 (void);
// 0x00000751 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::set_Predictions(MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction[])
extern void ImageQuicktestResult_set_Predictions_m71D966064A075427A6F99CAB1188BA4A32C0A904 (void);
// 0x00000752 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageQuicktestResult::.ctor()
extern void ImageQuicktestResult__ctor_m67FF6DDE22D66C868B5C89083D759844F980033C (void);
// 0x00000753 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagesCreatedResult::get_IsBatchSuccessful()
extern void ImagesCreatedResult_get_IsBatchSuccessful_m80C1B6CAA89619E51CEAF2BFE5AA3183395345ED (void);
// 0x00000754 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagesCreatedResult::set_IsBatchSuccessful(System.Boolean)
extern void ImagesCreatedResult_set_IsBatchSuccessful_m3C382CCF49A93FB79CC8958B776CEF2C86EC2229 (void);
// 0x00000755 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement[] MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagesCreatedResult::get_Images()
extern void ImagesCreatedResult_get_Images_m04ADAC5D1622FDD042752475FCF9F296B01678CF (void);
// 0x00000756 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagesCreatedResult::set_Images(MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImageElement[])
extern void ImagesCreatedResult_set_Images_m2CD9774672C2AF3FEE68F5FF394039C86645191B (void);
// 0x00000757 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.ImagesCreatedResult::.ctor()
extern void ImagesCreatedResult__ctor_mAF0323FE2FE16517FABC3720A94861E2D8B53AF6 (void);
// 0x00000758 System.Double MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::get_Probability()
extern void Prediction_get_Probability_m5BB08975ADEB1D5B0FFB8787D07748C11F9D687D (void);
// 0x00000759 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::set_Probability(System.Double)
extern void Prediction_set_Probability_m7314E43E34AB9B3AD4C1040CDC6DDFD198FFF0BC (void);
// 0x0000075A System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::get_TagId()
extern void Prediction_get_TagId_mB53863D0AD4AFD22022CFD900D1B119FB1E75DCB (void);
// 0x0000075B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::set_TagId(System.String)
extern void Prediction_set_TagId_mF27B6AE66479D77B6F2A314290E6230921B0FE36 (void);
// 0x0000075C System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::get_TagName()
extern void Prediction_get_TagName_m13FCC6C003EAA6CF19F286353C2F9182C1D81530 (void);
// 0x0000075D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::set_TagName(System.String)
extern void Prediction_set_TagName_mABF665C6C59750054B62FE397E3393EBA165D506 (void);
// 0x0000075E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction::.ctor()
extern void Prediction__ctor_mC04150136E4689CD538E1B25B618336C541CAD24 (void);
// 0x0000075F System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::get_TagId()
extern void Tag_get_TagId_m1BA333E512C0E02B9AC519740D5F9ECA57D9084D (void);
// 0x00000760 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::set_TagId(System.String)
extern void Tag_set_TagId_m525E40A78991748347D4E82726FF2A01E828AF35 (void);
// 0x00000761 System.DateTimeOffset MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::get_Created()
extern void Tag_get_Created_mFDCE2CB9BB90F8DE78A776625C9537DB29595123 (void);
// 0x00000762 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::set_Created(System.DateTimeOffset)
extern void Tag_set_Created_mE3A9C2C822E489E8375291E6721746158030FC2F (void);
// 0x00000763 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::get_TagName()
extern void Tag_get_TagName_mDC779F8323CA8901067A3A3F0DDE326B53E6E247 (void);
// 0x00000764 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::set_TagName(System.String)
extern void Tag_set_TagName_mC33DC637FF663EAB161FE76F090620A6B7B8931F (void);
// 0x00000765 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Tag::.ctor()
extern void Tag__ctor_m037B210B75877C9E1189A77F59A0268F13364582 (void);
// 0x00000766 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::get_Id()
extern void TagCreationResult_get_Id_m7728DB49DC439368D40D3BEEFC06C836760F7609 (void);
// 0x00000767 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::set_Id(System.String)
extern void TagCreationResult_set_Id_m66CAFA1D7B616232C7A7D73D4C13F7203530257A (void);
// 0x00000768 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::get_Name()
extern void TagCreationResult_get_Name_mECDD70E6B680F139045848C67E707FC3247508BD (void);
// 0x00000769 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::set_Name(System.String)
extern void TagCreationResult_set_Name_m5AD7EED90E83BB56BDB901050F349E2F070C00C4 (void);
// 0x0000076A System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::get_Description()
extern void TagCreationResult_get_Description_mFF88C55A3ED1B73E36DEA1199DB7C6EFDDEDC6F0 (void);
// 0x0000076B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::set_Description(System.String)
extern void TagCreationResult_set_Description_m3091895732863BB65EE8C17E3A3A5968197680C6 (void);
// 0x0000076C System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::get_Type()
extern void TagCreationResult_get_Type_m55D0AB5FE34386381769471079BAE5AA3E3012A1 (void);
// 0x0000076D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::set_Type(System.String)
extern void TagCreationResult_set_Type_m320878BD445C02036EA771D4A36AD60FD9AD5A2F (void);
// 0x0000076E System.Int32 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::get_ImageCount()
extern void TagCreationResult_get_ImageCount_m5B09BEB8C57426C655FC0F09E62BF5AC948E6A7F (void);
// 0x0000076F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::set_ImageCount(System.Int32)
extern void TagCreationResult_set_ImageCount_m18EAEF5F1597CA6068A7C89DFCB546B5DEBD3C77 (void);
// 0x00000770 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TagCreationResult::.ctor()
extern void TagCreationResult__ctor_mAB363C509D50E8BE86BED663195F4BBDC1AAA7AA (void);
// 0x00000771 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_Id()
extern void TrainProjectResult_get_Id_mEC31C19E198F812DF4CF5C8D1DBDA916111D28A9 (void);
// 0x00000772 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_Id(System.String)
extern void TrainProjectResult_set_Id_mD480931D8C50E8A81FFA0DF32063E60F163E36BB (void);
// 0x00000773 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_Name()
extern void TrainProjectResult_get_Name_mA20970955D6C0CD7DBE2CCC2FE41198C99DBA5BB (void);
// 0x00000774 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_Name(System.String)
extern void TrainProjectResult_set_Name_m9AFF968E5D21A9A15F8FD3FDE14DC84C40CD096C (void);
// 0x00000775 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_Status()
extern void TrainProjectResult_get_Status_mD7B6341A053B7048984BEC60512C736A656906DB (void);
// 0x00000776 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_Status(System.String)
extern void TrainProjectResult_set_Status_m12C45AB7831D12B3A391ABDC75C9B0CE0416389F (void);
// 0x00000777 System.DateTimeOffset MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_Created()
extern void TrainProjectResult_get_Created_m0581CF0349935EE33B56E670BF39B509D98CBF5C (void);
// 0x00000778 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_Created(System.DateTimeOffset)
extern void TrainProjectResult_set_Created_m854B5194D7ACFAA4123A5CDB7830A2DB94820365 (void);
// 0x00000779 System.DateTimeOffset MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_LastModified()
extern void TrainProjectResult_get_LastModified_mA2F8866668D6BEA36AC0072D67792D354228E8A1 (void);
// 0x0000077A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_LastModified(System.DateTimeOffset)
extern void TrainProjectResult_set_LastModified_m2DCC6C8D06643B4637DED36602F54138D18FBE53 (void);
// 0x0000077B System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_ProjectId()
extern void TrainProjectResult_get_ProjectId_m854E81CACFC7D03F5BD39111038A9CEA1E50611A (void);
// 0x0000077C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_ProjectId(System.String)
extern void TrainProjectResult_set_ProjectId_m6A8C4A93A984AFA428C4B82CC35E128BDB1A9073 (void);
// 0x0000077D System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_Exportable()
extern void TrainProjectResult_get_Exportable_m35455E76F9EAAAFFA354B57520EA1973D9E9C213 (void);
// 0x0000077E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_Exportable(System.Boolean)
extern void TrainProjectResult_set_Exportable_m314D691E8CBB2471D1FABCCA9A90361828A4B33C (void);
// 0x0000077F System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_DomainId()
extern void TrainProjectResult_get_DomainId_m092888C33FE60C3C36F819467FFECBBF48893B37 (void);
// 0x00000780 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_DomainId(System.Object)
extern void TrainProjectResult_set_DomainId_m2FA4010B9A6E767B843140F54981B48D6B3AA81D (void);
// 0x00000781 System.String[] MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_ExportableTo()
extern void TrainProjectResult_get_ExportableTo_m7B7205A1773F5BB27CABD997E0D2127623F84AC9 (void);
// 0x00000782 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_ExportableTo(System.String[])
extern void TrainProjectResult_set_ExportableTo_m668049DBDF2BDBCE9D4CF2A5FBCE77ACD5303BB9 (void);
// 0x00000783 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_TrainingType()
extern void TrainProjectResult_get_TrainingType_m7E2B548CB721033CF0399BB6BE1C74B7F9CF423B (void);
// 0x00000784 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_TrainingType(System.String)
extern void TrainProjectResult_set_TrainingType_m3BFF31574EFD2A3D5A35037085E7BDF3008011E9 (void);
// 0x00000785 System.Int64 MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_ReservedBudgetInHours()
extern void TrainProjectResult_get_ReservedBudgetInHours_m1EA58DD09F4F6AFD60C7951E439553492EE27863 (void);
// 0x00000786 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_ReservedBudgetInHours(System.Int64)
extern void TrainProjectResult_set_ReservedBudgetInHours_m66F1B910D567683D05251D27954442122C3456B4 (void);
// 0x00000787 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::get_PublishName()
extern void TrainProjectResult_get_PublishName_m6583F01AB52C1FD7EB0A9331AD126139B3694AE6 (void);
// 0x00000788 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::set_PublishName(System.String)
extern void TrainProjectResult_set_PublishName_m7087C52FA8D59DC46C03A5E425DF7886717842FF (void);
// 0x00000789 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::IsCompleted()
extern void TrainProjectResult_IsCompleted_m44215592218455FA00D65FD038266B64F42665AE (void);
// 0x0000078A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.TrainProjectResult::.ctor()
extern void TrainProjectResult__ctor_m50B9D4CEAA732921C06EAE449FA6DED950A46D6F (void);
// 0x0000078B System.Byte[] MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail::get_ImageData()
extern void ImageThumbnail_get_ImageData_m4B55FA41EB95517D48444EED6B4EE9622B9CF771 (void);
// 0x0000078C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail::set_ImageData(System.Byte[])
extern void ImageThumbnail_set_ImageData_m240C6D230A7C4E024A1EBA33F036113E09DECFA6 (void);
// 0x0000078D UnityEngine.Texture2D MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail::get_Texture()
extern void ImageThumbnail_get_Texture_m4732AC295A1EAB74AC93C9095D6A76C68E638C99 (void);
// 0x0000078E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail::set_Texture(UnityEngine.Texture2D)
extern void ImageThumbnail_set_Texture_m95F64DDAE5A155EDA9AC9B14B97ECD0AEEE3CB33 (void);
// 0x0000078F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.ImageThumbnail::.ctor()
extern void ImageThumbnail__ctor_m3AD22B39FF20D5E889324A877B694762DF0DBFAD (void);
// 0x00000790 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::get_Name()
extern void Project_get_Name_m97C629861FDB10FA7E29D1E9845DEC59C3DB29AA (void);
// 0x00000791 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::set_Name(System.String)
extern void Project_set_Name_m3B264D415A2D6C0A6EE91C629B375B6319486C67 (void);
// 0x00000792 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::get_CustomVisionIterationId()
extern void Project_get_CustomVisionIterationId_mAD2E89BA608ED91A43934E1AD025CF747E7E0038 (void);
// 0x00000793 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::set_CustomVisionIterationId(System.String)
extern void Project_set_CustomVisionIterationId_m92ACED54431F73D8A69A273CAC469C59F49CF293 (void);
// 0x00000794 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::get_CustomVisionPublishedModelName()
extern void Project_get_CustomVisionPublishedModelName_m457772F979D8ED01A4BE4310842D6355C954C59B (void);
// 0x00000795 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::set_CustomVisionPublishedModelName(System.String)
extern void Project_set_CustomVisionPublishedModelName_m13BFF3A812CCA91FA8BB6BAAED9277CFBD2B5683 (void);
// 0x00000796 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.Project::.ctor()
extern void Project__ctor_mD0F17267A698CB49C03B7D911049559A7B55FB4D (void);
// 0x00000797 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_Name()
extern void TrackedObject_get_Name_m4965538CB6B148003B454160B204A2548D8861F0 (void);
// 0x00000798 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_Name(System.String)
extern void TrackedObject_set_Name_mFC8E35E1517C271ED07C81CD53C5DF8244282562 (void);
// 0x00000799 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_Description()
extern void TrackedObject_get_Description_m1BFF890F6AF85BBCCF9067F981C72055DCA79954 (void);
// 0x0000079A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_Description(System.String)
extern void TrackedObject_set_Description_m67E695C84ED0241305F90A10201F19DE47F4E526 (void);
// 0x0000079B System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_ThumbnailBlobName()
extern void TrackedObject_get_ThumbnailBlobName_mB2422C3CC14BAE80611486E540D8775C4D771978 (void);
// 0x0000079C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_ThumbnailBlobName(System.String)
extern void TrackedObject_set_ThumbnailBlobName_mFC2F02EC0DF8310E9E156078E1D89C3B8D5A9A57 (void);
// 0x0000079D System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_SpatialAnchorId()
extern void TrackedObject_get_SpatialAnchorId_mC41FC75E9714B9811435C161A090E161266F3689 (void);
// 0x0000079E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_SpatialAnchorId(System.String)
extern void TrackedObject_set_SpatialAnchorId_m50C9F394E159DFA26AA2525F9462EEBFCE5D1B7F (void);
// 0x0000079F System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_CustomVisionTagId()
extern void TrackedObject_get_CustomVisionTagId_m0ADF58B30E9487B0C495D85BFC7BB3EA46795B85 (void);
// 0x000007A0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_CustomVisionTagId(System.String)
extern void TrackedObject_set_CustomVisionTagId_m4FBC2DD05CDCA663C3EDCC80B5DF3FF8513BAD7C (void);
// 0x000007A1 System.String MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_CustomVisionTagName()
extern void TrackedObject_get_CustomVisionTagName_m91A2EEF9AF25FC1CB1DFA6C4A958AFD2ED69F1E6 (void);
// 0x000007A2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_CustomVisionTagName(System.String)
extern void TrackedObject_set_CustomVisionTagName_mD4BC523A5B5A06B85B582783DB4A23BFC7D9859D (void);
// 0x000007A3 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::get_HasBeenTrained()
extern void TrackedObject_get_HasBeenTrained_m233A89C233EBAEA9EBAAFC49F753945FEB7A7005 (void);
// 0x000007A4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::set_HasBeenTrained(System.Boolean)
extern void TrackedObject_set_HasBeenTrained_m0041B1536E26AF8B7121DA928CF0663AE1A89B03 (void);
// 0x000007A5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::.ctor()
extern void TrackedObject__ctor_m17AE0CBC07FF927857BEED2A8C921F9BEF29F09A (void);
// 0x000007A6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject::.ctor(System.String)
extern void TrackedObject__ctor_m0B36448DCA656CBFC14EB27DA5A55AC0934CA8C6 (void);
// 0x000007A7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::add_OnIndicatorPlaced(System.EventHandler`1<UnityEngine.Transform>)
extern void AnchorPlacementController_add_OnIndicatorPlaced_m0661A7C2C3586AE30EE86B695AF2CCF00FD85D92 (void);
// 0x000007A8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::remove_OnIndicatorPlaced(System.EventHandler`1<UnityEngine.Transform>)
extern void AnchorPlacementController_remove_OnIndicatorPlaced_mFAE0DA24C9EF3B341CDAB457A09DD242126368F7 (void);
// 0x000007A9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::Awake()
extern void AnchorPlacementController_Awake_m892A92630C1675C9379BA51738FF206735FCE869 (void);
// 0x000007AA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::Start()
extern void AnchorPlacementController_Start_mC9CBAD81DDBD3C353EF494EF6FDFF55C3D34FE72 (void);
// 0x000007AB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::StartIndicator()
extern void AnchorPlacementController_StartIndicator_mB406B39E8C3F0F79850B01D1F961E9708AF89173 (void);
// 0x000007AC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::HandleOnSubmitButtonClick()
extern void AnchorPlacementController_HandleOnSubmitButtonClick_m8A5CAA6450E0BC9284C5E2E3CE101B21B8872451 (void);
// 0x000007AD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::HandleOnCancelButtonClick()
extern void AnchorPlacementController_HandleOnCancelButtonClick_mAABEFB37A1D673565CD8A265633E83888DDB9A36 (void);
// 0x000007AE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::OnPlacingStopped()
extern void AnchorPlacementController_OnPlacingStopped_m9BB4996FA27D296C395C398237F883C1482D3997 (void);
// 0x000007AF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.AnchorPlacementController::.ctor()
extern void AnchorPlacementController__ctor_mED763E19DA32F158AD66E535E7A1C7AFF19F871D (void);
// 0x000007B0 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::get_IsListening()
extern void ChatBotController_get_IsListening_m11E67614F91775E978A73230B4F5E17DEF615D8C (void);
// 0x000007B1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::set_IsListening(System.Boolean)
extern void ChatBotController_set_IsListening_mEDE71388945AE9D12137C55C1B44F2CC18EC6320 (void);
// 0x000007B2 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::get_IsSpeaking()
extern void ChatBotController_get_IsSpeaking_m755B8CF88888457A9C932CA773DB45007F7E43B5 (void);
// 0x000007B3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::set_IsSpeaking(System.Boolean)
extern void ChatBotController_set_IsSpeaking_m4A150418C84E06C9AAAA5ED54DEF7AD74FCCCF72 (void);
// 0x000007B4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::Awake()
extern void ChatBotController_Awake_m09E5D390F235D5E0C75E199532F747C852DAC0D4 (void);
// 0x000007B5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::OnEnable()
extern void ChatBotController_OnEnable_mD01EC7B71B96498EC7D3F4F6CC6BAB2C2472C03A (void);
// 0x000007B6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::OnDisable()
extern void ChatBotController_OnDisable_mA3B7C3AABDAE0B3DC8E37D92E215D0DDBA8A6B81 (void);
// 0x000007B7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::StartConversation()
extern void ChatBotController_StartConversation_m6B63EBE6E7C0A7FA0D6F8DBF94844137A3F4108E (void);
// 0x000007B8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::StopConversation()
extern void ChatBotController_StopConversation_mA636BD1EAFC3359B559DA377F7D2025C5C25953C (void);
// 0x000007B9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::StartListening()
extern void ChatBotController_StartListening_m23CFAF262EF727ED2B3CBF66EC95043E65C4C81F (void);
// 0x000007BA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::HandleOnConversationStarted(System.Object,System.String)
extern void ChatBotController_HandleOnConversationStarted_m1DF06F2F390604B4B639D1E3FF5C794CA50C50BD (void);
// 0x000007BB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::HandleOnMessagesReceived(System.Object,System.Collections.Generic.IList`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity>)
extern void ChatBotController_HandleOnMessagesReceived_mD3C93693E37C01B593EA5BDEC8512FA4DA087D4B (void);
// 0x000007BC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::HandleOnMessageSent(System.Object,System.String)
extern void ChatBotController_HandleOnMessageSent_m240CBC6AC024D72273967F6266D7CBB19D686CC2 (void);
// 0x000007BD System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::HandleReceivedMessagesCoroutine(System.Collections.Generic.IList`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity>)
extern void ChatBotController_HandleReceivedMessagesCoroutine_m981AF07233DFCA13DEC8251D63E3AEA1E59C9F9B (void);
// 0x000007BE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::OnDictationComplete(System.String)
extern void ChatBotController_OnDictationComplete_m526AD9682668947E88D6C3EDD019AC555752D74E (void);
// 0x000007BF System.String MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::SanitizeDictation(System.String)
extern void ChatBotController_SanitizeDictation_mB3B47CB637D4ABE1915A495D5537F8B75F7BF552 (void);
// 0x000007C0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::.ctor()
extern void ChatBotController__ctor_m054CE4F6F35D0F529D88436FF9F99E878D8A49BE (void);
// 0x000007C1 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController::<HandleReceivedMessagesCoroutine>b__28_0(MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity)
extern void ChatBotController_U3CHandleReceivedMessagesCoroutineU3Eb__28_0_m6137411422FD57C9CE26BC168D9E06E4FAA0C2D0 (void);
// 0x000007C2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController/<HandleReceivedMessagesCoroutine>d__28::.ctor(System.Int32)
extern void U3CHandleReceivedMessagesCoroutineU3Ed__28__ctor_m306C1B63B5764C89EAD5519340CD287035638C0B (void);
// 0x000007C3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController/<HandleReceivedMessagesCoroutine>d__28::System.IDisposable.Dispose()
extern void U3CHandleReceivedMessagesCoroutineU3Ed__28_System_IDisposable_Dispose_m0699B61B6060ACEFBED0FD5FD4AAF0DE1127055E (void);
// 0x000007C4 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController/<HandleReceivedMessagesCoroutine>d__28::MoveNext()
extern void U3CHandleReceivedMessagesCoroutineU3Ed__28_MoveNext_m191F10D67EC369745D6D24CCDF23F42EE2487376 (void);
// 0x000007C5 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController/<HandleReceivedMessagesCoroutine>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHandleReceivedMessagesCoroutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32D6E1B4458ED190BA8CE82FBC34A0838248D8B0 (void);
// 0x000007C6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController/<HandleReceivedMessagesCoroutine>d__28::System.Collections.IEnumerator.Reset()
extern void U3CHandleReceivedMessagesCoroutineU3Ed__28_System_Collections_IEnumerator_Reset_m3C391763B8084B3569295CBFEB88BF906044B320 (void);
// 0x000007C7 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ChatBotController/<HandleReceivedMessagesCoroutine>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CHandleReceivedMessagesCoroutineU3Ed__28_System_Collections_IEnumerator_get_Current_m7B56C9CF821250D0E92A5454C74DCCAF1ED9B9DB (void);
// 0x000007C8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.CloseDialogController::SetOriginMenu(UnityEngine.GameObject)
extern void CloseDialogController_SetOriginMenu_mD724DFF91CB4A099770EFAD41E6BC8E22850903A (void);
// 0x000007C9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.CloseDialogController::SetTargetMenu(UnityEngine.GameObject)
extern void CloseDialogController_SetTargetMenu_m294748E64A6AD2ED317BEA22F151275860395752 (void);
// 0x000007CA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.CloseDialogController::HandleOkButtonClick()
extern void CloseDialogController_HandleOkButtonClick_mB7AA008BC267CCE1D80EE38B1FA1EE927E04CC27 (void);
// 0x000007CB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.CloseDialogController::HandleCancelButtonClick()
extern void CloseDialogController_HandleCancelButtonClick_m2EF963CEE9EDA3C930F6F799A0EE2E0C542074B4 (void);
// 0x000007CC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.CloseDialogController::.ctor()
extern void CloseDialogController__ctor_mBFB8D12EB8330EF7882275A804CE6EF5B766158C (void);
// 0x000007CD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::Awake()
extern void ComputerVisionController_Awake_mF68FB901A9BCE846390979C2B94940C775498114 (void);
// 0x000007CE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::Start()
extern void ComputerVisionController_Start_m4FB668D6F490415E41F6C1DF81CBDC83FAAB27EA (void);
// 0x000007CF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::Init(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void ComputerVisionController_Init_mBF5F38DAA84D7521EB67997DBEF687A5E7B29EA7 (void);
// 0x000007D0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::StartPhotoCapture()
extern void ComputerVisionController_StartPhotoCapture_m633186895CC82BCCC366959249581A2CC26481BB (void);
// 0x000007D1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::DeleteCurrentPhoto()
extern void ComputerVisionController_DeleteCurrentPhoto_m6491ADB422C0DA11F5FDDD45813CB029681A8410 (void);
// 0x000007D2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::StartModelTraining()
extern void ComputerVisionController_StartModelTraining_mD817EF954C86E192954450C8A2C0EBD7F1CB2C1F (void);
// 0x000007D3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::HandleOnPointerClick()
extern void ComputerVisionController_HandleOnPointerClick_m4CDA92AACB4305FA232FCAB8332AD60F466D1664 (void);
// 0x000007D4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::CapturePhoto()
extern void ComputerVisionController_CapturePhoto_mE991C61AFF0002FB246C4C923C501128E0888AC5 (void);
// 0x000007D5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::SetButtonsInteractiveState(System.Boolean)
extern void ComputerVisionController_SetButtonsInteractiveState_mEBD76868CC4FA01E3A9F1CC6A8489A30C1D3C49E (void);
// 0x000007D6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController::.ctor()
extern void ComputerVisionController__ctor_mEFEB3B0FD6EB5F1495AFB8B92DEAB879D89A0058 (void);
// 0x000007D7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController/<Init>d__17::MoveNext()
extern void U3CInitU3Ed__17_MoveNext_m564665D37149E8291EF61F9BA0F73EEAC3DA8A4D (void);
// 0x000007D8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController/<Init>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInitU3Ed__17_SetStateMachine_mA96BE34976F7466CC337181A09E10D6D5D4D1169 (void);
// 0x000007D9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController/<StartModelTraining>d__20::MoveNext()
extern void U3CStartModelTrainingU3Ed__20_MoveNext_m772784304C4A99A08730E52A3755C523056EE597 (void);
// 0x000007DA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController/<StartModelTraining>d__20::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartModelTrainingU3Ed__20_SetStateMachine_m674DC58B684DFDB1EA92C7EA1EED44812DA5E566 (void);
// 0x000007DB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController/<CapturePhoto>d__22::MoveNext()
extern void U3CCapturePhotoU3Ed__22_MoveNext_mC0E8D3541C56083719C9C1CD127428D9881E65E3 (void);
// 0x000007DC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ComputerVisionController/<CapturePhoto>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCapturePhotoU3Ed__22_SetStateMachine_m1ECC5810664E60F0C103D14F18CA1E8506218098 (void);
// 0x000007DD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::Awake()
extern void ObjectCardViewController_Awake_m66D77B236620F2DFB83FE2CDA26B4D816023F32C (void);
// 0x000007DE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::OnDisable()
extern void ObjectCardViewController_OnDisable_mA7BE81F4CDFC487B33AFE9D8E04BA73E747B943D (void);
// 0x000007DF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::Init(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void ObjectCardViewController_Init_mE7F008F5EC7596FFEB1C5BEF9C91ECB31AC32479 (void);
// 0x000007E0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::StartComputerVisionDetection()
extern void ObjectCardViewController_StartComputerVisionDetection_mE34C8708BED6D9B76BD69488AE06743754766A50 (void);
// 0x000007E1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::StartFindLocation()
extern void ObjectCardViewController_StartFindLocation_m8653309E4A46FCED26A308C74DC9AF320AEF15B8 (void);
// 0x000007E2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::HandleOnAnchorFound(System.Object,System.EventArgs)
extern void ObjectCardViewController_HandleOnAnchorFound_m4C093E111AE46D09C12474115E38E2304DDECE53 (void);
// 0x000007E3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::CloseCard()
extern void ObjectCardViewController_CloseCard_mBFC9B59C2AB8AE7DA92E80D0D3EB1F0BFC94941C (void);
// 0x000007E4 System.Threading.Tasks.Task MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::SearchWithComputerVision()
extern void ObjectCardViewController_SearchWithComputerVision_mC4138AD7BA13D5B644B5EAE48D0620733E8CDB49 (void);
// 0x000007E5 System.Threading.Tasks.Task`1<UnityEngine.Sprite> MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::LoadThumbnailImage()
extern void ObjectCardViewController_LoadThumbnailImage_mE37EEDDEF89FFDA2E310B0808AAD3BE938C38A01 (void);
// 0x000007E6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::SetButtonsInteractiveState(System.Boolean)
extern void ObjectCardViewController_SetButtonsInteractiveState_m0EF125BF4A65EB2F17B3679FAD17E1D38EEAF2B7 (void);
// 0x000007E7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::.ctor()
extern void ObjectCardViewController__ctor_m62B3D1A47C5500BDAA66151DEAC9523E10C73163 (void);
// 0x000007E8 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController::<SearchWithComputerVision>b__17_0(MRTK.Tutorials.AzureCloudServices.Scripts.Dtos.Prediction)
extern void ObjectCardViewController_U3CSearchWithComputerVisionU3Eb__17_0_m7E074DC5736CA8A50A5C6686482B97210FB782E1 (void);
// 0x000007E9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<Init>d__12::MoveNext()
extern void U3CInitU3Ed__12_MoveNext_m6F9068BC7CD0CFCDF4E0E8DD04EE293E48279E42 (void);
// 0x000007EA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<Init>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInitU3Ed__12_SetStateMachine_mC439122519743780B8E5205E111895D665E99134 (void);
// 0x000007EB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<StartComputerVisionDetection>d__13::MoveNext()
extern void U3CStartComputerVisionDetectionU3Ed__13_MoveNext_mAC4E0D875BCAE51C94D4AF3B11D3F65A40752159 (void);
// 0x000007EC System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<StartComputerVisionDetection>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartComputerVisionDetectionU3Ed__13_SetStateMachine_m45AB4FE13AEA39E60F55344E1A78876232C9BEB4 (void);
// 0x000007ED System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<SearchWithComputerVision>d__17::MoveNext()
extern void U3CSearchWithComputerVisionU3Ed__17_MoveNext_mA3ADFB2E4556288E51AA1F6023BBDEAACD3BE314 (void);
// 0x000007EE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<SearchWithComputerVision>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSearchWithComputerVisionU3Ed__17_SetStateMachine_m1C63E2CF4E05B0FCD6148BAE033C8BADFAE0F8FB (void);
// 0x000007EF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<LoadThumbnailImage>d__18::MoveNext()
extern void U3CLoadThumbnailImageU3Ed__18_MoveNext_m3953192F759B21F12B04147EF688553834BC1F70 (void);
// 0x000007F0 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectCardViewController/<LoadThumbnailImage>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadThumbnailImageU3Ed__18_SetStateMachine_m50A978F8F755E653AD24942B4554D91471C74AFE (void);
// 0x000007F1 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::Awake()
extern void ObjectEditController_Awake_m298B004C23699C85A9D7730BEB4D0CCB7CBF038F (void);
// 0x000007F2 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::Start()
extern void ObjectEditController_Start_mBA687156C02490967633024D597F976FD0B0BEA0 (void);
// 0x000007F3 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::Init(MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject)
extern void ObjectEditController_Init_mE097D91AAA5BDB649B9D9B363A0287D284D907E3 (void);
// 0x000007F4 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::TakeThumbnailPhoto()
extern void ObjectEditController_TakeThumbnailPhoto_mE4E802ECF9175FFD021E126061A28551FCEC5FD8 (void);
// 0x000007F5 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::DeleteThumbnailPhoto()
extern void ObjectEditController_DeleteThumbnailPhoto_m1CEDDB50339812797D0DC032959D6497F6777028 (void);
// 0x000007F6 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::SaveChanges()
extern void ObjectEditController_SaveChanges_mBB2214B96339B2A960861C4B64E2F52722056A9D (void);
// 0x000007F7 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::OpenComputerVisionFlow()
extern void ObjectEditController_OpenComputerVisionFlow_mBCAF3690DD33CA2D6CE64BEE6C0B640B7656D3C9 (void);
// 0x000007F8 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::OpenSpatialAnchorsFlow()
extern void ObjectEditController_OpenSpatialAnchorsFlow_m42F4153A4FBE413DB0BE87D2D732879C365AD628 (void);
// 0x000007F9 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::HandleOnPointerClick()
extern void ObjectEditController_HandleOnPointerClick_m5F58765FF4FBC912CB18177778D8508E552BF14A (void);
// 0x000007FA System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::CapturePhoto()
extern void ObjectEditController_CapturePhoto_m38A96A2B0B0B57E5865A19D6BB2D13971AD49868 (void);
// 0x000007FB System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::HandleOnCreateAnchorSucceeded(System.Object,System.String)
extern void ObjectEditController_HandleOnCreateAnchorSucceeded_m219D3D6EE75A06988A9C69A293E592CAF1782F30 (void);
// 0x000007FC System.Threading.Tasks.Task`1<UnityEngine.Sprite> MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::LoadThumbnailImage()
extern void ObjectEditController_LoadThumbnailImage_mEB6977AF1F33037AFDDAD6A4DFC0AE19ED246E99 (void);
// 0x000007FD System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::SetButtonsInteractiveState(System.Boolean)
extern void ObjectEditController_SetButtonsInteractiveState_mC46E56F30362B8489896E6FCD82E0001CF7E7645 (void);
// 0x000007FE System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController::.ctor()
extern void ObjectEditController__ctor_m30C876CE447EEB59FEA72CBB55D52738B7E80281 (void);
// 0x000007FF System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<Init>d__14::MoveNext()
extern void U3CInitU3Ed__14_MoveNext_mA3CA7720B916403A3643F8F75D3A4B3F8082B7D8 (void);
// 0x00000800 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<Init>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInitU3Ed__14_SetStateMachine_m5A247ABD81740A7CC69B11E2D6AD92BFC5FF19B0 (void);
// 0x00000801 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<DeleteThumbnailPhoto>d__16::MoveNext()
extern void U3CDeleteThumbnailPhotoU3Ed__16_MoveNext_m9212698B7C0C3D9BDB1F9C89D127AAFACDE90D94 (void);
// 0x00000802 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<DeleteThumbnailPhoto>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDeleteThumbnailPhotoU3Ed__16_SetStateMachine_m6A3BAC8934E715428EC93195021E769541317001 (void);
// 0x00000803 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<SaveChanges>d__17::MoveNext()
extern void U3CSaveChangesU3Ed__17_MoveNext_m88607E1A32FE1BAC3235A646B5BEC613469CD2CB (void);
// 0x00000804 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<SaveChanges>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveChangesU3Ed__17_SetStateMachine_m31775CB6BE2905AD4477A57AEFBDFFD78E290898 (void);
// 0x00000805 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<CapturePhoto>d__21::MoveNext()
extern void U3CCapturePhotoU3Ed__21_MoveNext_mDC92048B8293E18F846704A5E498F2BF72D3702D (void);
// 0x00000806 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<CapturePhoto>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCapturePhotoU3Ed__21_SetStateMachine_m6BB7B5B993A8FC0CFDE187F9E6EA24C9E723CC2D (void);
// 0x00000807 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<HandleOnCreateAnchorSucceeded>d__22::MoveNext()
extern void U3CHandleOnCreateAnchorSucceededU3Ed__22_MoveNext_m082A6F4DB234CFECB29A2DF4AB5A5ED6E5A61653 (void);
// 0x00000808 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<HandleOnCreateAnchorSucceeded>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleOnCreateAnchorSucceededU3Ed__22_SetStateMachine_mEDCA3ECFB52D5402CF8E6C6A37156C5A6DB4B2CF (void);
// 0x00000809 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<LoadThumbnailImage>d__23::MoveNext()
extern void U3CLoadThumbnailImageU3Ed__23_MoveNext_mA08007D052DA2FAEE9723B90566013DF24942F49 (void);
// 0x0000080A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEditController/<LoadThumbnailImage>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadThumbnailImageU3Ed__23_SetStateMachine_m5314C7BBD0BBFC800434CC75B188A4D7B9A67DEC (void);
// 0x0000080B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::Awake()
extern void ObjectEntryController_Awake_mC2D2ED6B926790B4778DE3EDA2F65E9729420590 (void);
// 0x0000080C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::OnEnable()
extern void ObjectEntryController_OnEnable_m610CC669D67FD9EEBB8A6B888D7A080C1902E7C8 (void);
// 0x0000080D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::SetSearchMode(System.Boolean)
extern void ObjectEntryController_SetSearchMode_mDC3863941D52FCCFA97374A257F048BF242E5139 (void);
// 0x0000080E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::SubmitQuery()
extern void ObjectEntryController_SubmitQuery_m75870E932C00E52F9ABE4F1FE1DAC386021DDC8D (void);
// 0x0000080F System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject> MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::FindObject(System.String)
extern void ObjectEntryController_FindObject_m5F4DD4E61C004801269FF8D3ADC8AC0F8E3A58DC (void);
// 0x00000810 System.Threading.Tasks.Task`1<MRTK.Tutorials.AzureCloudServices.Scripts.Domain.TrackedObject> MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::CreateObject(System.String)
extern void ObjectEntryController_CreateObject_mB31790598205AEC6FC8DBD08A5283220B7C2E8EE (void);
// 0x00000811 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::SetButtonsInteractiveState(System.Boolean)
extern void ObjectEntryController_SetButtonsInteractiveState_mCA60C579EAC6252961E6AD970518F77EA790A9BD (void);
// 0x00000812 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController::.ctor()
extern void ObjectEntryController__ctor_m31E9E61C5322DF69D4DB41FB1AB6D26E48BD244B (void);
// 0x00000813 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController/<SubmitQuery>d__13::MoveNext()
extern void U3CSubmitQueryU3Ed__13_MoveNext_m17543901304155A13FB73367239EEDA73124483C (void);
// 0x00000814 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController/<SubmitQuery>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSubmitQueryU3Ed__13_SetStateMachine_m76B07DB9F0E42E42BB396F819009392B81915CC3 (void);
// 0x00000815 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController/<FindObject>d__14::MoveNext()
extern void U3CFindObjectU3Ed__14_MoveNext_m35CAC51C1D4884C68E48ABD6BF25B2A874894DC0 (void);
// 0x00000816 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController/<FindObject>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFindObjectU3Ed__14_SetStateMachine_mAA8D0EB3D92A7093C8035BB269D14873A8091A95 (void);
// 0x00000817 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController/<CreateObject>d__15::MoveNext()
extern void U3CCreateObjectU3Ed__15_MoveNext_m9524FB8BDA7E0237E633CCA346D5420935B36E53 (void);
// 0x00000818 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.Controller.ObjectEntryController/<CreateObject>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateObjectU3Ed__15_SetStateMachine_m0A0891CE2BEAF1749257440380F54E0B66EA18E1 (void);
// 0x00000819 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::add_BotResponse(System.EventHandler`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs>)
extern void BotDirectLineManager_add_BotResponse_mDDA6FFBB0E0CECE42EAAE3218901D867EAA93F81 (void);
// 0x0000081A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::remove_BotResponse(System.EventHandler`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs>)
extern void BotDirectLineManager_remove_BotResponse_mD5093BCE6AAC37B0EDCB6D21FC276EDA082F4BF1 (void);
// 0x0000081B MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::get_Instance()
extern void BotDirectLineManager_get_Instance_mFA2C230ED0CD2019307207BC4577536C498D6346 (void);
// 0x0000081C System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::get_IsInitialized()
extern void BotDirectLineManager_get_IsInitialized_m21F9D92E5F4CF960A0D20AE28FB2FCE6A52BDF12 (void);
// 0x0000081D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::set_IsInitialized(System.Boolean)
extern void BotDirectLineManager_set_IsInitialized_m094CAADC334DA8FC8C0DB97082DD88671771756B (void);
// 0x0000081E System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::get_SecretKey()
extern void BotDirectLineManager_get_SecretKey_m3354C9DD6EC3BACB58220DFE3AB5A81A8AFD63E9 (void);
// 0x0000081F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::set_SecretKey(System.String)
extern void BotDirectLineManager_set_SecretKey_m374FC0F46381B3E39DB4A686EBD6D740D535BC2B (void);
// 0x00000820 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::.ctor()
extern void BotDirectLineManager__ctor_m0A681A6502F21DCF84764FB1B1593E3610430AB5 (void);
// 0x00000821 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::Initialize(System.String)
extern void BotDirectLineManager_Initialize_m074F49C99224FADF360A0B28393EF49CCE1DE3DF (void);
// 0x00000822 System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::StartConversationCoroutine()
extern void BotDirectLineManager_StartConversationCoroutine_m60C80ED82FE7ED25744CF96E627DEAB0F4BD4EB8 (void);
// 0x00000823 System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::SendMessageCoroutine(System.String,System.String,System.String,System.String)
extern void BotDirectLineManager_SendMessageCoroutine_m65784C01CB7C9B31B0C50CB68CE9831E913AD800 (void);
// 0x00000824 System.Collections.IEnumerator MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::GetMessagesCoroutine(System.String,System.String)
extern void BotDirectLineManager_GetMessagesCoroutine_mB80599D9AB9C230303DC51D3D51F6CC49C41340D (void);
// 0x00000825 UnityEngine.Networking.UnityWebRequest MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::CreateWebRequest(MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/WebRequestMethods,System.String,System.String)
extern void BotDirectLineManager_CreateWebRequest_m082656659DB953557780F5A1E57B8C046A88A264 (void);
// 0x00000826 MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::CreateBotResponseEventArgs(System.String)
extern void BotDirectLineManager_CreateBotResponseEventArgs_m345A7C7C639BB01FB77D886F8CC99F2DC5D18038 (void);
// 0x00000827 System.Byte[] MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager::Utf8StringToByteArray(System.String)
extern void BotDirectLineManager_Utf8StringToByteArray_m424EA912B7E142B48B55FAEC033DD68EA100A48C (void);
// 0x00000828 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<StartConversationCoroutine>d__21::.ctor(System.Int32)
extern void U3CStartConversationCoroutineU3Ed__21__ctor_mE66E0C6F842B5CF724F30E6B960AC6A5F693D97D (void);
// 0x00000829 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<StartConversationCoroutine>d__21::System.IDisposable.Dispose()
extern void U3CStartConversationCoroutineU3Ed__21_System_IDisposable_Dispose_m675C7B2C01F5E8F87C432AED7CB665BBC9EF4C7C (void);
// 0x0000082A System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<StartConversationCoroutine>d__21::MoveNext()
extern void U3CStartConversationCoroutineU3Ed__21_MoveNext_m048DB2E3EEA7999574F09DB596197FD1D32B48C1 (void);
// 0x0000082B System.Object MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<StartConversationCoroutine>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartConversationCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE1662B13B1FF7BE87AE824B5040529CE3113241 (void);
// 0x0000082C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<StartConversationCoroutine>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartConversationCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5941ADB6D7ADE577519B65ED6CFC2E4192B1550E (void);
// 0x0000082D System.Object MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<StartConversationCoroutine>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartConversationCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_mB010DDA6C03AFAC7A1887055B3314DF5A5318099 (void);
// 0x0000082E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<SendMessageCoroutine>d__22::.ctor(System.Int32)
extern void U3CSendMessageCoroutineU3Ed__22__ctor_m9A21274C214EC839F0F68DAA24AD99B4C91EE14C (void);
// 0x0000082F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<SendMessageCoroutine>d__22::System.IDisposable.Dispose()
extern void U3CSendMessageCoroutineU3Ed__22_System_IDisposable_Dispose_m4AE86D26B440B860E5BF572953958616B98A3D16 (void);
// 0x00000830 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<SendMessageCoroutine>d__22::MoveNext()
extern void U3CSendMessageCoroutineU3Ed__22_MoveNext_m1C4A3167BA715627E0957F5BD4B449A4722E335C (void);
// 0x00000831 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<SendMessageCoroutine>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendMessageCoroutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9406F22EA5D27BFDF9890904560F9F0C1616B01F (void);
// 0x00000832 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<SendMessageCoroutine>d__22::System.Collections.IEnumerator.Reset()
extern void U3CSendMessageCoroutineU3Ed__22_System_Collections_IEnumerator_Reset_mDE1A55EA026398E8402C73A1CD94B58461B0FE96 (void);
// 0x00000833 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<SendMessageCoroutine>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CSendMessageCoroutineU3Ed__22_System_Collections_IEnumerator_get_Current_m7AAA5907061EA77CA4FB803E609F79AC08DABB54 (void);
// 0x00000834 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<GetMessagesCoroutine>d__23::.ctor(System.Int32)
extern void U3CGetMessagesCoroutineU3Ed__23__ctor_m1BB4B4DF8BB78CB56DCCC6F9F8769CE724DC115B (void);
// 0x00000835 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<GetMessagesCoroutine>d__23::System.IDisposable.Dispose()
extern void U3CGetMessagesCoroutineU3Ed__23_System_IDisposable_Dispose_m66B4019B9DE325F4957A891D50B04480727E1A5A (void);
// 0x00000836 System.Boolean MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<GetMessagesCoroutine>d__23::MoveNext()
extern void U3CGetMessagesCoroutineU3Ed__23_MoveNext_mFBE25A228E96C354E74E9D2B2503D6CC7F7D86C4 (void);
// 0x00000837 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<GetMessagesCoroutine>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetMessagesCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD9D12A649FE899CC90D8A5C80D2718B534202BB (void);
// 0x00000838 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<GetMessagesCoroutine>d__23::System.Collections.IEnumerator.Reset()
extern void U3CGetMessagesCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_mFC1E7D11970BF765C12708FFE30AFC90B7F4DB30 (void);
// 0x00000839 System.Object MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotDirectLineManager/<GetMessagesCoroutine>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CGetMessagesCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m6C18544D68791BC7DCF202678691CC8DDBCFE97E (void);
// 0x0000083A System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotJsonProtocol::.ctor()
extern void BotJsonProtocol__ctor_mDF9EF054F224E5D2A42C7FFD8402C0C350D22495 (void);
// 0x0000083B MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.EventTypes MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_EventType()
extern void BotResponseEventArgs_get_EventType_m25493DDDE06870680C4043C384C74F7C2DFF8C17 (void);
// 0x0000083C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_EventType(MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.EventTypes)
extern void BotResponseEventArgs_set_EventType_m54129A8D8E98D40DD91E71937DB455DFE64C2BFD (void);
// 0x0000083D System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_SentMessageId()
extern void BotResponseEventArgs_get_SentMessageId_mB9CD0FBD853525FED543B807ADE39037A703AAC4 (void);
// 0x0000083E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_SentMessageId(System.String)
extern void BotResponseEventArgs_set_SentMessageId_m8536596B0AFB7EE528B5191320A2AA89FF5FFE8D (void);
// 0x0000083F System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_ConversationId()
extern void BotResponseEventArgs_get_ConversationId_m03DE6D59E357C84B7CBF73C536A7514CA3B42DD2 (void);
// 0x00000840 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_ConversationId(System.String)
extern void BotResponseEventArgs_set_ConversationId_m624262BED6F9CFA857A8F153D7611CDE87A783B7 (void);
// 0x00000841 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_Code()
extern void BotResponseEventArgs_get_Code_m8ABF27E9D91C8A8B3469A72AC36D121BE825E232 (void);
// 0x00000842 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_Code(System.String)
extern void BotResponseEventArgs_set_Code_mFF1A1B39F5681EE4820B5BA5CAB5D399B97D67E7 (void);
// 0x00000843 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_Message()
extern void BotResponseEventArgs_get_Message_m9C13A7826D5DF73E5E100AC2BF13621E2D665C0F (void);
// 0x00000844 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_Message(System.String)
extern void BotResponseEventArgs_set_Message_mBFB9F4E9BFC8E540A2E30A3E3E2809AF7C521744 (void);
// 0x00000845 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_Watermark()
extern void BotResponseEventArgs_get_Watermark_mCDCA680D26CE0AD026601320E164EA1F1158E577 (void);
// 0x00000846 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_Watermark(System.String)
extern void BotResponseEventArgs_set_Watermark_m658BF4F21ECE1AD1ECEF3312FB15435A3DAEA7AE (void);
// 0x00000847 System.Collections.Generic.IList`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity> MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::get_Messages()
extern void BotResponseEventArgs_get_Messages_m58D435B5FAF026C4E6DFACB76F13DEDB6DB5B88D (void);
// 0x00000848 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::set_Messages(System.Collections.Generic.IList`1<MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity>)
extern void BotResponseEventArgs_set_Messages_m45EC587BDF9732C1D35DAE205DEA5BFA06396F40 (void);
// 0x00000849 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::.ctor()
extern void BotResponseEventArgs__ctor_mAFCDE1AF1E74A3639CE0AD8D85989DEC429F752C (void);
// 0x0000084A System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.BotResponseEventArgs::ToString()
extern void BotResponseEventArgs_ToString_m342BCA1AB5672D37F5751A80B9F650BC8A5F987F (void);
// 0x0000084B System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::get_ConversationId()
extern void ConversationState_get_ConversationId_m9A39155CFA11EB421BAF1BEFCE85FA868FD3412D (void);
// 0x0000084C System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::set_ConversationId(System.String)
extern void ConversationState_set_ConversationId_m58F5C72CAC797CB6A985206793E968D4B87BAF8B (void);
// 0x0000084D System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::get_PreviouslySentMessageId()
extern void ConversationState_get_PreviouslySentMessageId_m65E412478DDD843EB7653E776316BA1398A4A45E (void);
// 0x0000084E System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::set_PreviouslySentMessageId(System.String)
extern void ConversationState_set_PreviouslySentMessageId_mB3C70E6E577BD8CBA2A4018E9EDD973761ED5317 (void);
// 0x0000084F System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::get_Watermark()
extern void ConversationState_get_Watermark_m47DFD940F2CC93ECFB0B158A8858232790B71179 (void);
// 0x00000850 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::set_Watermark(System.String)
extern void ConversationState_set_Watermark_m555A200EDCEEA6D0D22ED63772B5C9E8CF2D62A1 (void);
// 0x00000851 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.ConversationState::.ctor()
extern void ConversationState__ctor_mE9C448D88C9642BB5E3E536C7516B5270AF3E352 (void);
// 0x00000852 System.DateTime MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_Timestamp()
extern void MessageActivity_get_Timestamp_mFC8B9107050E8BD148141904B656912B18EBB294 (void);
// 0x00000853 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_Timestamp(System.DateTime)
extern void MessageActivity_set_Timestamp_m87A506C0189D11A856425758770690873C05E167 (void);
// 0x00000854 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_Id()
extern void MessageActivity_get_Id_m9219957FB6DBAC7BF1FD55AA9C63C7073A05708F (void);
// 0x00000855 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_Id(System.String)
extern void MessageActivity_set_Id_m13B8968A045EA657E464D501FD3520D0718D455C (void);
// 0x00000856 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_ChannelId()
extern void MessageActivity_get_ChannelId_m619D057773EA6ADEA0B0A2D4843FFCC8E074A50C (void);
// 0x00000857 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_ChannelId(System.String)
extern void MessageActivity_set_ChannelId_m75913F27A0C269DD1F0F6C86C08B4743F30BD2AD (void);
// 0x00000858 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_Locale()
extern void MessageActivity_get_Locale_m85A33FF9F8F87E615CB58B32AC65E08F8C1D18EA (void);
// 0x00000859 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_Locale(System.String)
extern void MessageActivity_set_Locale_mFDF2ADE1CDAFBF334B606E0FE6570D91DCF71A1D (void);
// 0x0000085A System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_FromId()
extern void MessageActivity_get_FromId_m7A903C2D8AB2D1F84F459B81CB23E4AEFD5D9472 (void);
// 0x0000085B System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_FromId(System.String)
extern void MessageActivity_set_FromId_m567954C11699ED6D9EF1B36A67240EBB994C4F27 (void);
// 0x0000085C System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_FromName()
extern void MessageActivity_get_FromName_m184D9FF1A04CB442A1AB99B6CE0535C3A3FC2023 (void);
// 0x0000085D System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_FromName(System.String)
extern void MessageActivity_set_FromName_m41973CA50E2A4391CB7AA290AD343B2EA7DDD47E (void);
// 0x0000085E System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_ConversationId()
extern void MessageActivity_get_ConversationId_m41334B4052434042965922710185289E85D994B4 (void);
// 0x0000085F System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_ConversationId(System.String)
extern void MessageActivity_set_ConversationId_mDC61A9F119B76AEB7662993413056D032E934B15 (void);
// 0x00000860 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_Text()
extern void MessageActivity_get_Text_m4F4EA7C5EE3F39A7257F4B06DAE383DF01950B9B (void);
// 0x00000861 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_Text(System.String)
extern void MessageActivity_set_Text_m4BD941BDD4213FC804663813ED80828A9DD5D255 (void);
// 0x00000862 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::get_ReplyToId()
extern void MessageActivity_get_ReplyToId_m2BFED1A4CC86CAF82A5223834133509A01D55345 (void);
// 0x00000863 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::set_ReplyToId(System.String)
extern void MessageActivity_set_ReplyToId_m40DF289F5F58A877413575BC1033C5107761B15F (void);
// 0x00000864 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::.ctor()
extern void MessageActivity__ctor_m73CD13E7FCD3CA5C34A1D9FFFB47EFB8E2738863 (void);
// 0x00000865 System.Void MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void MessageActivity__ctor_m85B00C7BE26AD4CA7491DFEB6A922D38CB2F2EE5 (void);
// 0x00000866 MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::FromJson(SimpleJSON.JSONNode)
extern void MessageActivity_FromJson_m837F2BBA751807C91282BB03D728F998AA2ED8F8 (void);
// 0x00000867 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::ToJsonString()
extern void MessageActivity_ToJsonString_mFE2347F3B921AE03F039F1C38FDBA3CDDCF67ADD (void);
// 0x00000868 System.String MRTK.Tutorials.AzureCloudServices.Scripts.BotDirectLine.MessageActivity::ToString()
extern void MessageActivity_ToString_m832BB1F35CBA339D9D486AC311BB3150757A714D (void);
static Il2CppMethodPointer s_methodPointers[2152] = 
{
	HolographicRemoteConnect_Connect_m0E82B1FF6F081D4C9F7EE426A8E51844928DDB57,
	HolographicRemoteConnect_OnGUI_m12662133938EFE0D369C2A1447A4FC7EF577FDCB,
	HolographicRemoteConnect__ctor_mB947007991C533B059F3CF272D4258357D3C743D,
	ToggleButton_ToggleClipping_mE7203AADB2A4CF3154E82054854E46BCD045DFF5,
	ToggleButton__ctor_m7051B7DCF4943D16DFD85C6F6C518B9C8A230957,
	ToggleSpatialMap_ToggleSpatialMaps_mACF9F6644BB819CE7FA4FB9D2CC938F024A9D4C4,
	ToggleSpatialMap_get_IsObserverRunning_m1F6EE04DA7C52A3432E6CAD588C7F843B414E4FE,
	ToggleSpatialMap__ctor_m14E2950855F302A84CE45C185C2A3F119341FF39,
	ViewButtonControl_Start_m1A5D56852C6C4B3F8D764FF2FE325349421C3D74,
	ViewButtonControl_NextModel_m97F19332A0B5EE3D419B9F6A950E3631334BB469,
	ViewButtonControl_PreviousModel_m31396945DBF6B181921DBEC0A6EF2F3E9E8DF796,
	ViewButtonControl__ctor_mADD68095E9D500AF804D34D38B39D8E95543A6DA,
	AnnotationManager_Start_m749C00A8299BB2B177ECA91A4CA23656B1D70401,
	AnnotationManager_Update_m71BEB92C391788841A97642E4BAD1829ABB255C0,
	AnnotationManager_addObject_m7D581E1AC7CE5B7F62251F77AE726698E74704A7,
	AnnotationManager_undo_m7C088695AD7CE2509B5B14D8484CFDA80012529C,
	AnnotationManager_redo_m0642B2A3A4615F8640C5B678B48EFC50AA6EB62D,
	AnnotationManager_clear_m601B99D5364071127D0E0EC12B62B088F8679F58,
	AnnotationManager__ctor_m38A19A7AD9A7D25152FA607261741ABA0171DC23,
	EnterAnnotation_Start_m520E5E498DD580937EE9A0080B0A5FD723AA70EE,
	EnterAnnotation_Update_m56F9DC5E2075E54C49ED4F5636A698C1C68F2F87,
	EnterAnnotation_send_m4339E7F4D4D4E23970F996877F65BB3DD5E8C855,
	EnterAnnotation_delete_mD0CE9185B5C7ED4DB10D6E9ED202DA105F70710A,
	EnterAnnotation__ctor_mA350D08990EA3F23F131E8CE4015925BCDB9A006,
	Info_Start_m8F84B2622094B7EB30853C075A94AC4FC34CA561,
	Info_Update_mE90E589BC0A55D94B1D8F0BE1DB3C122799FB8BD,
	Info__ctor_m2837F74E1391252CA7D29C3BECD03D7F097BFC84,
	LampScript_Start_m4D798F0F5C1AE071D5BF76BF6E7B6CB9100E5A3F,
	LampScript_Awake_m23E31B6FF97E19213B872BEE21A778C574AF0F45,
	LampScript_Update_m719DF24899E86771D3B3FF2509E08B1DEA111789,
	LampScript_OnPointerDown_mD452AFB94094716B0FB8AFDB76FBEF34F036FBA1,
	LampScript_OnPointerDragged_mC9BB7FBA2515E96696AEE18CB28CE9993E5E2FB0,
	LampScript_OnPointerUp_mB0A44926916559829E8523DFCCCDD44264E9BAF4,
	LampScript_OnPointerClicked_mB210DA889C50C59CAC75C75BCB7955D092BEBC0D,
	LampScript__ctor_m8211CD082E2F2149F7AFF06213E69B320EFB798C,
	OneDriveFilePicker_get_ClientID_m67A0ABAE34E87A17802B390454BFEC9D9ECA73EA,
	OneDriveFilePicker_set_ClientID_mE4297097A3235352E7059E6ADA70F886DF59AC0E,
	OneDriveFilePicker_get_ClientSecret_m7CBCCE155886291E1CEA0D4955543636F77DFA3E,
	OneDriveFilePicker_set_ClientSecret_mA5362556AC0329C6F9CC4C2904E50E7F8F5ABDD3,
	OneDriveFilePicker_Start_m4A1B7A79E25B571B8AE49B99FEAA3465C9F97503,
	OneDriveFilePicker_Update_m43F63720C9965013AAF9C5EC71054D03CC09074D,
	OneDriveFilePicker__ctor_mECECA642ABE9FCBE30478385224EB69D8D0E90BB,
	Organize3DModelWindow_Awake_m29F0EA566B920914AF94135BAF8C8688E4F97E36,
	Organize3DModelWindow_Update_mB5717B76100567BB5399DC861D0FC56A3E52B0A8,
	Organize3DModelWindow_ReorganizeWindow_mF66C510D87B556160250432FA00E7C349837BE90,
	Organize3DModelWindow__ctor_mEFC9A19FFABA57E1848E0DF35D3B796CD0BEEADD,
	Pen_GetFirstPointer_m38D7BEAA88A679B62ADBEF5607A8D60723A16E6D,
	Pen_GetPointersGrabPoint_m1E6CE5428163BD131BFB8FEC85536B006BB5DFBA,
	Pen_GetHandPositionArray_m26A2FEB2B0D182F7D34EF47EAD17ABF7D16056DD,
	Pen_IsNearDrawing_mB72D7988D9604907FD1A27464D59EB6CAB54932E,
	Pen_OnSliderUpdatedRed_m8AB790688DB4E7197FEA51E023E3028E1AE28EDC,
	Pen_OnSliderUpdatedGreen_m544D0EEF7373D02EC5894EC78555EADE7B84317A,
	Pen_OnSliderUpdateBlue_m76BA607E567F68D26C77B58CC01DA280C38EDF70,
	Pen_Start_mF06F77F9C9D8058B5D9CC0E261F86EF230403B07,
	Pen_Awake_m6E1AC355EBE45ED706B6589F661BD6EB690C8065,
	Pen_Update_mB2FD1E9B4484A8BAFFE8D2A61FAABE8C45CC3884,
	Pen_OnDisable_m73B1826C68A6B91F72632B5D3E950C0F9C8546E3,
	Pen_OnEnable_m39A5047DB41A3925CA354B540ED19CE620EE409D,
	Pen_OnInputDown_m0145D6532AC810106F79483FCC3F2EBF6555E836,
	Pen_OnInputUp_m3591BDDEA6EBB35BD4E1783028CFC6B80724781C,
	Pen__ctor_mBF2019946D0DEF1A0C0A1289EEB295C8B2E68E92,
	PointerData__ctor_m1C270542C6E8CA3F5536722DAC86B2EECC3B2AD0,
	PointerData_get_IsNearPointer_mCEC5F5FC9A255DFAFA14CA5A4B5ED7251FCEE37F,
	PointerData_get_GrabPoint_m6C4BF165B3C043AEB832099DE2033B8204473B61,
	ResetStone_Awake_mB5691D559DE49F2A4F1BE1A8F11602DD4A1049D7,
	ResetStone_Reset_m41924A86A15431A5EA3170398232332BDFA7295A,
	ResetStone__ctor_mCD5906B12EE6CFF8CC6F242D9640CF837D7E99BD,
	ReverseNormals_Start_m52BF288A38C6B3796AFC5D501210C7C062E581C4,
	ReverseNormals__ctor_mB5B3AD5724E884C1D0DD0FC8EF08B54715D9E308,
	ReverseTriangles_Start_m6170CB3FC8D2A201B001FD935185EB4FF53B22D0,
	ReverseTriangles__ctor_mDE7E3B9D413151BF6B0DF61AE83C35C4B9B1CB09,
	SaveAnnotation_Start_m2EFD9530DBEAD74A6D475D690D53DF39599A868C,
	SaveAnnotation_Update_m97596240EDF64F1ED15CF8272B16F2C957DD0A4D,
	SaveAnnotation_save_m809A2D47C1C91D209D1AFEB18ABEC2FE4EE9F759,
	SaveAnnotation_load_m2D989A1A8776024D36DEC68A0FE3E32FBC55E66B,
	SaveAnnotation_writeToBinary_mC5F3B1A657A04939345A358D3CF74CF2C90BDBE7,
	SaveAnnotation_readFromBinary_m7B966C9512BFCD6A6C12857BBFDD81366F593E7B,
	SaveAnnotation_changeAccessMode_m32015E5062322C69DDD6A557BD70D1E36F342052,
	SaveAnnotation_getAccessMode_m16B456FCBF7E23370494248A6E1EC77E11DE15FA,
	SaveAnnotation__ctor_mDD846578093B824E2495ECF371397E2172AE926D,
	SavableObject__ctor_mF621B4053DB71CB5BA86720BADE136B0AEDD4D79,
	SavableObject_TryReturnVertices_mD5ADCF3B52594A5689C5FF67FEE1B535C798D7E6,
	SavableObject_TryReturnText_mE12A84DF51A9251DFBA56AA6D7A7AF7AA3F17545,
	SavableObject_ReturnPosition_m63FB8AFB5B4815EA85AFC1322BF6FE2D2AFAA509,
	SavableObject_ReturnLocalScale_m2C48189FFE50C9D7895261178E8D215AFC81F670,
	SavableObject_ReturnRotation_mD7A3303077F46DBF5ECE07EFFEE5E34DE2FABC2D,
	Vector3Ser_get_x_m6F1973AA868052BC30D1D6683E58694D41EBD422,
	Vector3Ser_set_x_mEB455F2E8464EB93AA74D7AA4A46A4D5624B9774,
	Vector3Ser_get_y_m4D7B84975201BCA452B7E1E2BE88FEAADACADEA3,
	Vector3Ser_set_y_mD7E6AF4B7E3996879CDDE9F26C9B62B3A06E6685,
	Vector3Ser_get_Z_m2AEED0B4E5E71055FD6859D604CEB64A73ECBFDA,
	Vector3Ser_set_Z_m2833E66EE3859E429B260E8854F2E0C027B1F196,
	Vector3Ser__ctor_mD21F0FF3C4F00FDBE9CCC635FB4E5FD26B4C7EE6,
	Vector3Ser__ctor_m94B96C75D8958CC7383AD3EC832E8796C508184A,
	SelectingArea_Awake_mC9135DB37D9D0909E5C5763EF28AFC920047A44D,
	SelectingArea_Start_mBF942E7ED5E1EADED98B9B0A9DED0687193EBA24,
	SelectingArea_Update_m64988F7F12EEDBA5B87B7CC5CE799192730D7CC7,
	SelectingArea_OnInputDown_mDCE512801A313E8B63D2E1ECFF7A80BA7D8B1175,
	SelectingArea_OnInputUp_mD49F9ED49A237440F76E10EFEDE923AD26287A85,
	SelectingArea_RectangleSelection_mCEC2B39856EB98301C263041426CF015DF07CF36,
	SelectingArea_RectangleProjection_m1F5BD603EF4547AEC24A6113C45B979251C374B1,
	SelectingArea_UpdateQuadVertices_m02A7581E815635FDC029C040533A4BB95862FC36,
	SelectingArea_FlipQuadNormals_m69859F329B91EAEA4C3DA03DCF4B675CE3D3C149,
	SelectingArea_HideMeshObjList_mD930A9D269208EA9579C4C218D1499A0D39B35A9,
	SelectingArea_DestroyMeshObjList_mA7B89B6DAEBEA92420AE9AF5313F3B4A53FCBBBA,
	SelectingArea_HideQuadList_mF113801CCAA6EDFFEAA4B98AF14DD49845280AB2,
	SelectingArea_SetOption_m44713811A1570A7DF164C58D4E0FE263EEFD8CAA,
	SelectingArea_OnDisable_m269205A0ABCC2674ADE32C892A7357F494CF15E9,
	SelectingArea_OnEnable_mEBF13F742648492A1BE449E4884878B06443BDBD,
	SelectingArea_OnSourceDetected_mAF5C3740DA8FF3116FF6F96438E00CB97F40405E,
	SelectingArea_OnSourceLost_m4F50969E80F6B874AF55D15AFF1DA1498671B683,
	SelectingArea__ctor_mBE5C0F7E48F5672012D77D48F0D437C9CE69DBC8,
	Test_OnPointerClicked_mF212E2D8C44BC4BBB3356ECC564F45FFCA5959A3,
	Test_OnPointerDown_m8A79B9EA26E0CD31A2A4C5CC5C7F8E47ACB52BB2,
	Test_OnPointerDragged_mED001DD316C6C2A0A8702D422E802AFD4912AD75,
	Test_OnPointerUp_m82519CE3B9AA909CEDD4094C9C2C8D9256A64D4E,
	Test_Start_m4D6FA0B24EBFD6471F4596A93EC95EC1EB5355D8,
	Test_Update_m85FFE4CC559D42AAFA74D4DEAE19415052BB70D0,
	Test__ctor_m1C2B5E11A339FD79C0C458FB86723E5F2B74545C,
	UploadNewModel_Start_m609E149586BB178ADDE88F7E912A7FB4EDBC245D,
	UploadNewModel_Update_m4B6BDF4888EC73970771632DA2B8FBECB87CD654,
	UploadNewModel__ctor_m42A2740E1E9772D70040A9F33DFAC60371BDFE29,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	TextToSpeechManager_BytesToFloat_m00198F96D71C22BD29F8572E4FBFACFCF381C585,
	TextToSpeechManager_BytesToInt_m635AD055DBD2569EF8698DAB3B13BB67EAEE0F7F,
	TextToSpeechManager_ToClip_m70FDE0CE26C25CD3D7692B2B1E6EAFEED2CDCC79,
	TextToSpeechManager_ToUnityAudio_mEAF3F4F39A74BD95F7600721944E6A4BC7A428CC,
	TextToSpeechManager_LogSpeech_m36EBC522D8011A6B2092A605170BC2E01D686F2C,
	TextToSpeechManager_PlaySpeech_m54C2EC6DFB4B80FF67BFA594419367DFE4F1F9B1,
	TextToSpeechManager_Start_m112AFD8569A086B85BE0358B1CD03282B1CEC7E2,
	TextToSpeechManager_SpeakSsml_m30BDFD8475DC679C2CD2DBF69A76C932ECAA1971,
	TextToSpeechManager_SpeakText_mEA9C9E2437A4BCC11C90A0F157C0535FDBFB9878,
	TextToSpeechManager_get_AudioSource_m85FF350D00BBA44EA643B29502B2F3F0A0C0301E,
	TextToSpeechManager_set_AudioSource_m1708481C623D2387572B4FED2C17422FA8ACA37E,
	TextToSpeechManager_get_Voice_mEC5E487B2E6216A7760077CF7E4F7908AD309C27,
	TextToSpeechManager_set_Voice_m2C0F68DDE982C6B88240491F53EE00138F71D8E9,
	TextToSpeechManager__ctor_mA610D3650F7A25852D7AF89C3C12616A13618CD3,
	U3CU3Ec__DisplayClass9_0__ctor_m18BBE49444A95B46397D4E57BCDAE33346323459,
	U3CU3Ec__DisplayClass9_0_U3CPlaySpeechU3Eb__0_m7AD83D3004206869B0B6CE54154797FD57AF24CE,
	U3CU3CPlaySpeechU3Eb__0U3Ed_MoveNext_m604395E2D0545ABA13308F844EF82D1E3359B46E,
	U3CU3CPlaySpeechU3Eb__0U3Ed_SetStateMachine_mFD91BAA9F1F1A2294AA3CCF16050C660FC13F7F0,
	U3CU3Ec__DisplayClass9_1__ctor_m5D8327F509E5E02F836E1563D1C19ED98FB9C474,
	U3CU3Ec__DisplayClass9_1_U3CPlaySpeechU3Eb__1_m2A87E639A3140FD6D4BEC8AFB8C4B3EB5F74FED3,
	U3CU3Ec__DisplayClass9_2__ctor_mB55DC1141A110A010701A5BA87B3E965C26B0F91,
	U3CU3Ec__DisplayClass9_2_U3CPlaySpeechU3Eb__2_m0871A2DCFB641E9BE77F2D22FC262DA7D4895BFB,
	U3CU3Ec__DisplayClass11_0__ctor_mF01AA4E90CB37150BD05E1A7FFC12D40E0E1C1A7,
	U3CU3Ec__DisplayClass11_0_U3CSpeakSsmlU3Eb__0_mD2F84B12192A5644CC5E98EA87E7B2B8AE6115B3,
	U3CU3Ec__DisplayClass12_0__ctor_mAC0A19C259154E1C1FDB8BC0E8FF9FF3DD966FE1,
	U3CU3Ec__DisplayClass12_0_U3CSpeakTextU3Eb__0_mF73BE698BBE002423F86876103ACCDF207A82E56,
	BaseDwellSample_get_DwellHandler_mE7E88483FEEF32A0EC8E2A14677CC3BAD02878FE,
	BaseDwellSample_set_DwellHandler_mA7ED0780EA864E6E8E437DD6A109D80D333A0923,
	BaseDwellSample_get_IsDwelling_mF8BA92D92DCEEA358F1D504417350886800D3BC5,
	BaseDwellSample_set_IsDwelling_mB339F111599B576E0C353619B6F03DC1CAA33139,
	BaseDwellSample_Awake_m92902301B904E0A84BC2DDE8111358B1B8E6FF83,
	BaseDwellSample_DwellStarted_m4BC95AE386B073E97646AD8D75D9299800ED3515,
	BaseDwellSample_DwellIntended_mE225ABD345ABEB4EC4879283516B35C6AC5E381A,
	BaseDwellSample_DwellCanceled_mB4EE479879225BCAD03DFC16A41ADB42F31A395D,
	BaseDwellSample_DwellCompleted_m0278883E0090F2ADCC9D2B648609C6A9DAF04E68,
	BaseDwellSample_ButtonExecute_m9DF3D058CCA03D2955F3EEB9CC797A73CFF52802,
	BaseDwellSample__ctor_mF07F35663FCDF204D2A4D270BF679694B015B4F0,
	InstantDwellSample_Update_m020095FE6938C0C5E0F61BC122B19040FB9E3BD4,
	InstantDwellSample_DwellCompleted_mD57B332DD31239D1ABEE52DBBAEDFC0584046DC2,
	InstantDwellSample_ButtonExecute_mA7083DD44C7E00376F73CB8D285B1244B4D6D032,
	InstantDwellSample__ctor_mF63A380F57BADD53FB5D6AD43F7CD6CA74265C27,
	ListItemDwell_Awake_m68AC969200A596B60F891C22FA9D4FD2E2677F4A,
	ListItemDwell_Update_m717C89E13D412F11580BDE4AB02CC1AF1601FA5F,
	ListItemDwell_DwellCompleted_mF27529880A038665A5F68D5E0A9C48D4CC6DC33B,
	ListItemDwell_ButtonExecute_mE27FB4CD8CDC2DA550C17054B648E696DBBB839E,
	ListItemDwell__ctor_mEF18F64091788C951EAAADADB90B8091F0BD6854,
	ToggleDwellSample_Update_m30A06B48D82AD4F220CDF15B7D1BC397AA712C9E,
	ToggleDwellSample_DwellIntended_m9A839EB5BE2707ABC9C9F070E134DD79187E75FD,
	ToggleDwellSample_DwellStarted_m32D126A396C497A7330E814F4004F7A769ED46DA,
	ToggleDwellSample_DwellCanceled_m277D291308981DBED6AB4C39127AEDAE29742CB4,
	ToggleDwellSample_DwellCompleted_m57F979B2EFF69A91918344ABA3709A4A0BFBFCA4,
	ToggleDwellSample_ButtonExecute_m7F9003C0CE7B5E3E3A66DF8B8AA219CBACFBE628,
	ToggleDwellSample__ctor_m7ED34BCBD44C2D0DC9DDBB65956554AC89C58331,
	GestureTester_OnEnable_m0140D4F607D27D42F445714D070CC7EADA275E24,
	GestureTester_OnGestureStarted_m769D9B5F7D3FA69B08CD2396FE4E8EB9C4CE9041,
	GestureTester_OnGestureUpdated_m2CF619D7BD37384BC7765A1AFC43FF355613A234,
	GestureTester_OnGestureUpdated_mEA2E33387D62C4D1B919C2C4D9C6AD1163B5CB70,
	GestureTester_OnGestureCompleted_m857D1FEB9276A76A9D1A872BEABB8B0337D79194,
	GestureTester_OnGestureCompleted_m18D1D6159094D9C15E3EE5EBAC03B0214051B2E8,
	GestureTester_OnGestureCanceled_mEC678CBBBC1E312636C51D78A83EA8E293680C3F,
	GestureTester_SetIndicator_mC10E281F42604E9478C157E2E1D6A10045789FDA,
	GestureTester_SetIndicator_mCE66488AB17DA7410AD184940079B68B013EDD63,
	GestureTester_ShowRails_m503612C0788C264B5250059F23F4F267F6662C78,
	GestureTester_HideRails_mEF63845796950C93EF3A59BB7E9D9EB6B82192B9,
	GestureTester__ctor_mCA9059AE04A957A48F5E848BB37BC4FB1A5AD782,
	GrabTouchExample_Awake_m193BA2448772C129BA391D7666A5373CA98EAD17,
	GrabTouchExample_OnInputDown_m07A209D9218A662FEFDE282E5CCCCC92D78DDADA,
	GrabTouchExample_OnInputUp_m628ACEE16B43D23EBDD71F97D768E936B0FF2B7E,
	GrabTouchExample_OnInputPressed_m45CCFEA21313A542F9FD03150F3C83521A3CB242,
	GrabTouchExample_OnPositionInputChanged_m548EBF13250031CF2857C36BB10AD640954CFE03,
	GrabTouchExample_OnTouchCompleted_m4B01CAC3AAA8B2BD773161831871894E71DBB926,
	GrabTouchExample_OnTouchStarted_m36E8794CB581EDCB902B0FDE20671D70F42A221F,
	GrabTouchExample_OnTouchUpdated_m94E2D7C86FF55ACB515079E5958902AAD67BB04B,
	GrabTouchExample__ctor_m6FFF0E748673903DFA2072F856F569ECDC1BBF6A,
	LeapMotionOrientationDisplay__ctor_mC9E9FDEE213D10636277EF57AAA73C69CA0A9DA1,
	RotateWithPan_OnEnable_mFA342F4128FB0B7B1FBB2AF91ACAD08BF1B34AA1,
	RotateWithPan_OnDisable_m606981F1E173DE557743B647BAFCA2F3E2404600,
	RotateWithPan_OnPanEnded_m293831F1B00757AFB383BD53E6D94BD08A42FB6E,
	RotateWithPan_OnPanning_m1E07E3B6FB54BA55684D6ED5E5D9A2CBC431C0A3,
	RotateWithPan_OnPanStarted_mEC903265CA88C185DB163CEBF2EFD0A7BA224FE9,
	RotateWithPan__ctor_mA18E5BAD40CDA091610D697BE9FD1C8636A68EDE,
	WidgetElasticDemo_Start_m3AA84DAFCBDA72AF1EEB77B0069CB15753D196C5,
	WidgetElasticDemo_Update_m243D8FCC1B9E729D737357722C43C7AAE7CEB34E,
	WidgetElasticDemo_ToggleInflate_mCE86AF4A69EEBEA32A437BA152C589D4EFE6EE8B,
	WidgetElasticDemo_DeflateCoroutine_m84135534DD3C09B645B81B0EB6115FDD02C00429,
	WidgetElasticDemo_InflateCoroutine_mE2F383E15019773883AF45EF7DD49AA449F4875B,
	WidgetElasticDemo__ctor_m8B8A9189D8C89D4FB77AF954F2FAE39FC1BBE1AB,
	WidgetElasticDemo__cctor_m15492C9A3999ECA43B8235AC74CC6990646903DE,
	U3CDeflateCoroutineU3Ed__17__ctor_mEA2390B9684B886A451971C951655213D65AC906,
	U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_mFCA12467EBE6761B70E1E53B3C4A840AF292F943,
	U3CDeflateCoroutineU3Ed__17_MoveNext_m08BE00565057C7244AF5B0F58AE8F00CBB46D80B,
	U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C6CC0497CBBFA4475663EABF571C620908EE095,
	U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m95CFD6D6D284C2903E685D5BFF75638564042683,
	U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_mAD2FAE910FD557443AFE0E15488E50AF6CA1A7C5,
	U3CInflateCoroutineU3Ed__18__ctor_m12D0CEBD462022A3EC020D476F2F86769928A257,
	U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_mFC8253327295702721A20B6545F95CF3BB337974,
	U3CInflateCoroutineU3Ed__18_MoveNext_mB98E20FF4A5938A765AD1CCB0E7E4A37E685C3DF,
	U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E4B6452DAA034D58A0F8BE4B5AB52D8191B084D,
	U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_m2D65C6C7F8392C468C35F5054DF01AF0F916C3F1,
	U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_mBD973DB790EE2644898B112F81BEB648D33F3EC9,
	DialogExampleController_get_DialogPrefabLarge_mDB86B8120B06D2EB8A7F4F48C14AAABE00D807D4,
	DialogExampleController_set_DialogPrefabLarge_mB896606829DAC0F184B850C2676DA2ACC7ACEF2E,
	DialogExampleController_get_DialogPrefabMedium_m63245472CCF878280A7C9B4BC7F59512E227711B,
	DialogExampleController_set_DialogPrefabMedium_m7D86E5FCE17FE4B50B3BC9C521784FFACB8FF5BC,
	DialogExampleController_get_DialogPrefabSmall_mD7AC4C762F1C70ACABE36E07BBEF87F03D0F51B1,
	DialogExampleController_set_DialogPrefabSmall_m66E13D24F52C672ADAEDFFA8FC355CFF5DD268FE,
	DialogExampleController_OpenConfirmationDialogLarge_m32B34312C88F143A9C0601D520F2A9884E661E24,
	DialogExampleController_OpenChoiceDialogLarge_m199DF164ABF6B47C96EB6883FA97961208D9308B,
	DialogExampleController_OpenConfirmationDialogMedium_m3FDEDB6E1CFAB8D85FB08D07E2467A5ED08A9DD8,
	DialogExampleController_OpenChoiceDialogMedium_mB24EE11A661367692654F1C799DCFD507A3E2253,
	DialogExampleController_OpenConfirmationDialogSmall_m7F576C35E0CF36914C7863728E3F2C10E062077E,
	DialogExampleController_OpenChoiceDialogSmall_m2B360094918BC60C0DB3498CE354E172F6D0E5AE,
	DialogExampleController_OnClosedDialogEvent_m0CDAC35415EEB0E0B16E12CB070FC02AD90E1E67,
	DialogExampleController__ctor_m02DEAEF5905B7D243CFEE40C6D25C654831734AC,
	BoundaryVisualizationDemo_Awake_mADF2C333750B0965A7C0E530803818305B3C27AB,
	BoundaryVisualizationDemo_Start_mF2856E7037862C0E0074967C845A7CA5800B1C03,
	BoundaryVisualizationDemo_Update_mF2C8117468A4E28EE51EC3777EA0FE0004018783,
	BoundaryVisualizationDemo_OnEnable_m6C14109111EBFCD295E5A123FE40A8E8C34ABC26,
	BoundaryVisualizationDemo_OnDisable_m102AF5706F009FFF389AB0E0839265E6CF03424F,
	BoundaryVisualizationDemo_OnBoundaryVisualizationChanged_m09A05D153DE8DF4957C740438360566A9791DBB9,
	BoundaryVisualizationDemo_AddMarkers_mC6C2F70AC796A12C938B38212C97BE9EDA496453,
	BoundaryVisualizationDemo__ctor_m60F00512A5599ACBA2666128B81FA3F1AD10092F,
	DebugTextOutput_SetTextWithTimestamp_mDF010EB22994E08515BD0D7168C5FD8A2A79D6C0,
	DebugTextOutput__ctor_mBA36C4F1E296266F1F198C5A87A3F2EA9AE9E201,
	DemoTouchButton_Awake_m1AF45B8901FCF6F00A529AEFCAE5506AE7555E02,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m84EAE1A02F16D7B72C51B5CD236826E8839104BC,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mC005178DA61C7FE367B2962D91B018E38C0DFB08,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m18B30B55011CD005213C0317A3180051D2D6A26C,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m1A0CD1D270C21807A0F2910702FB9D7D18B1BB39,
	DemoTouchButton__ctor_m8C7E042730F36A15DCA22444DC001BC632B961CF,
	HandInteractionTouch_Start_mEA26CFA5F9C0670533A9D81F9599ACAC2E884FBF,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchCompleted_m3D6DF00714DBC2BA327BD969D6E2AA2655AAD43A,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchStarted_m5CBDAF9E02F3D13A8138610576D56581B02624B0,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_m2566E12443FEB048F8BD99C6B8FE9366834D5FCB,
	HandInteractionTouch__ctor_m95795EEB965D94E5EE65E4F18F8D1ADE174F6E10,
	HandInteractionTouchRotate_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_m5635032533A00F90E61210644F3CFBEA14F4BA9C,
	HandInteractionTouchRotate__ctor_m04F2A1112ABDEFB18CC1D8CB81232B06D7AD8622,
	LaunchUri_Launch_mC659D1BDD2E134A84AA1791D4D44C87A2A901962,
	LaunchUri__ctor_m32487E2888A148FCAD613C50117270B1293E3431,
	LeapCoreAssetsDetector_Start_mC7AE99208F21C227403657D0886ACA11D32C2732,
	LeapCoreAssetsDetector__ctor_m4AEEC0EBF67469344098FF87A216AC35B67DCEC5,
	SolverTrackedTargetType_ChangeTrackedTargetTypeHead_m2B0CD7B51F5FA879B77DCDD61DEB5D9C63E7A037,
	SolverTrackedTargetType_ChangeTrackedTargetTypeHandJoint_m4B00304A18536F7663D2657FC0D960D1100754B5,
	SolverTrackedTargetType__ctor_m8CC435EED303E982EE5530B33180A75965B19D15,
	TetheredPlacement_Start_m6CACD480BBE24C0EB87A6E89715F20893DD5AAE8,
	TetheredPlacement_LateUpdate_m2B4BE1BD4E712C73D57EED7018B72DB1CC99B124,
	TetheredPlacement_LockSpawnPoint_mA32ABA5BBD3753BB351C15CF983B486EC5518236,
	TetheredPlacement__ctor_mDD6A080D7A8DF6DC83BD6C7C89CF27CF289CFE09,
	ToggleBoundingBox_Awake_m20C0BB56F14913BCB69B995A4A2D6E8508802224,
	ToggleBoundingBox_ToggleBoundingBoxActiveState_m4CA2880B8D2C11ED0EFDD0C9F38ACF6D70B35977,
	ToggleBoundingBox__ctor_mF49E02C24C50FB0472BBBD80B9A6E247BC036D11,
	DisablePointersExample_Start_m3E31DF9B8F38D17AF1AEDEC7B1E05AD3C198DDE9,
	DisablePointersExample_ResetExample_m9E6E34DCE84AC4EFD9A17098B529A5DCCC1AC529,
	DisablePointersExample_Update_m13BB16503BC11195A9CF914C7CE6FFAFC58BA763,
	NULL,
	DisablePointersExample__ctor_m7E6F097FC701D4F2E642999EEEF1988A8DDB88C7,
	Rotator_Rotate_mCAACB33C1CA545A2E38FCB30EB19EC90678C8CB4,
	Rotator__ctor_m4F79EFA6E5945343F8F4DA70EEBAF97E267FBE03,
	InputDataExample_Update_m0F1B0CDFAB7DAF0C26D2A1726BEC5D4857CE1ECD,
	InputDataExample_Start_m5EAC869FDF9D300F62E609A3FA495879F17FF3BC,
	InputDataExample__ctor_m883220AABF5DABB410458C4A1A4DB6279C82FB14,
	InputDataExampleGizmo_SetIsDataAvailable_m095B653F634FB98B0D54A2B09F36F010C42BD730,
	InputDataExampleGizmo_Update_m66CEFC1AE337DD44EB4E64D2255FCA3EF4ED206D,
	InputDataExampleGizmo__ctor_m073E47C354EB42BCABFCC24E2310A21118A685A1,
	SpawnOnPointerEvent_Spawn_mEE71AF33FC68D78B16CC5214545522B34B69694E,
	SpawnOnPointerEvent__ctor_mC540FBBA46ADB0EC808F31F01AAA48AE3E3C8491,
	PrimaryPointerHandlerExample_OnEnable_m6FDDA02E110E76093918206D568068FB5BE436F8,
	PrimaryPointerHandlerExample_OnPrimaryPointerChanged_m379D9C08E09FA6DE5B20CF5FBB4FAE6BECA5985B,
	PrimaryPointerHandlerExample_OnDisable_m90A94E8D78E3DBC461BBDC45D6438E3366D811C2,
	PrimaryPointerHandlerExample__ctor_m97D53B12F721E0EAFA962463B8A1AE9A64E7160F,
	ScrollableListPopulator_get_ScrollView_m042238B5B001927FDD8C67EB26015C9E641665A7,
	ScrollableListPopulator_set_ScrollView_m7FA0A4D292E3A1AF78A9D3B4A76C7AF2791F2675,
	ScrollableListPopulator_get_DynamicItem_mD96CFFBB028D28F379C71B65514B96E259B431BB,
	ScrollableListPopulator_set_DynamicItem_m1C76D9E63F971C530F4DF29081D801F553C8BFA1,
	ScrollableListPopulator_get_NumItems_m73437AA1C4B5CC09418930DB87CD6A5D92C94B17,
	ScrollableListPopulator_set_NumItems_m33015E2D6684E547C9C90ACC22F582EAE86CA93E,
	ScrollableListPopulator_get_LazyLoad_m055FEF55C1ADAFC1E12A0AF4EAA3CAEA321B9003,
	ScrollableListPopulator_set_LazyLoad_m9463C287A6C9BAF30FD266F116D9CE7FFD17F9BA,
	ScrollableListPopulator_get_ItemsPerFrame_mDBB98A7F2205F3E4B2DB72CFE053F23335FD94FE,
	ScrollableListPopulator_set_ItemsPerFrame_mE58DDF07ECB10546F4D464FE256EDEC7CA5D0BAA,
	ScrollableListPopulator_get_Loader_m8D4D089B552F12567D862F7851561143F2B3963C,
	ScrollableListPopulator_set_Loader_mD782F54965CEDEA3414FF58DFFAE272977E8CFCE,
	ScrollableListPopulator_OnEnable_mA5F8A1BAE41B1060D05E53498E872315A88F2131,
	ScrollableListPopulator_MakeScrollingList_mAD71C9D7BF987ED7B6D689E54B577243BC63F532,
	ScrollableListPopulator_UpdateListOverTime_mF520005989CE5A4FB04B9A18072DB1A7A32680F2,
	ScrollableListPopulator_MakeItem_mDB70A887590A8252C685F2230B8F7B721F781E49,
	ScrollableListPopulator__ctor_m504433E97D003B122568DF23225038D109ECCD84,
	U3CUpdateListOverTimeU3Ed__33__ctor_mD6DC4198EA19B70B22CFB394251F3584E4638F88,
	U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_m96767201EB44DE2678760CEC09A37615EDF9C67D,
	U3CUpdateListOverTimeU3Ed__33_MoveNext_m9F34E80A591109FCBC9F2BBCF24AB9CD6040FB05,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50FE87AB5366443CF64C7D0D0170CCD6F07D8695,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_mCB10BA6CFE80B992125294E8B2970BE7214CD81C,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_mE153FE28CE0FD6B843D064D1D40662D94E46A417,
	ScrollablePagination_get_ScrollView_m37EBC4CA0A2B6B1306B474F6AA7FE6310480FF07,
	ScrollablePagination_set_ScrollView_m7BA82F6CE9207BA4F003FA07F246D60A4E62CEF3,
	ScrollablePagination_ScrollByTier_m265EDEA535DFAA8FC0FF4F864586E0F6C7D538C2,
	ScrollablePagination__ctor_m368543C1E56F5ACF4334F4CB2A5E4D0600A34475,
	HideTapToPlaceLabel_Start_mE89B6110A5F19CD9E9C64C9860F2E300E57AAE38,
	HideTapToPlaceLabel_AddTapToPlaceListeners_m2A5A470FA7A269DF07BAB1CEB3D6EC0EF59446C9,
	HideTapToPlaceLabel__ctor_m8B8EA7B412E3EA9C8D9E297306273DA51E20AE7A,
	HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_mF9C234F125FD4A2CBF763DFFE1B15B0E90023749,
	HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m02F105131E5262BB8991FE6D7F077ABD81F89528,
	SolverExampleManager_get_TrackedType_m4DABEE0D8106E2502E426C0367F15A628B5803B5,
	SolverExampleManager_set_TrackedType_mD8D843F03F2C7064F77F8C7220A84FFFD30336DF,
	SolverExampleManager_Awake_m6087680ED6CCA8589EED17BB1DF368424AB28702,
	SolverExampleManager_SetTrackedHead_m82AAA89811D9E07D26FE7244466E4A26D7800F60,
	SolverExampleManager_SetTrackedController_m9687D772AFE232B76A9A5E37E71AD9C5510C128B,
	SolverExampleManager_SetTrackedHands_m363BDD043A03F58F728CD522EFFC3E39F7B58DEA,
	SolverExampleManager_SetTrackedCustom_mC7023779CF83B9858CF3C41A2D853695B449D53F,
	SolverExampleManager_SetRadialView_m9964B0FEA35B75F55900E06EA434238280B4E406,
	SolverExampleManager_SetOrbital_m4DD257DA9CE338F170691B77C9D7FD60F339A4B3,
	SolverExampleManager_SetSurfaceMagnetism_m225A0CA340934D421A6830A0BF89E2155AAA199D,
	NULL,
	SolverExampleManager_RefreshSolverHandler_m7593ADE5877D611339AB4BF944BF6DB5B78E8D99,
	SolverExampleManager_DestroySolver_m982039E6B7C5EF861AB2B140ABD46BEFB73106DE,
	SolverExampleManager__ctor_mF5C0C58F5C1D6876E4A4F9BDBFA78CE295E29BBD,
	ClearSpatialObservations_ToggleObservers_m9FC49F6F59044E6C31118F6A477DECC3EE60C7C8,
	ClearSpatialObservations__ctor_m0D7B5227EEC0F97CDD8E1F4F02F7A457123734B5,
	ShowPlaneFindingInstructions_Start_m5F47C24A1CEAFACC9A1B9F5978771509FADA294D,
	ShowPlaneFindingInstructions__ctor_m9D9CAFC3AC7EC35325562355292941860F81BC8B,
	BoundingBoxExampleTest_OnEnable_m9295F0AD2432A82F8F1789D1255C5F6A67EE9F1C,
	BoundingBoxExampleTest_OnDisable_m02195DEA798C73BFE83917C7A3FD39444763071B,
	BoundingBoxExampleTest_Start_mF3A4F299620D7F8FCE98348C4C80609331E71587,
	BoundingBoxExampleTest_SetStatus_mD8CA641DA51CFC1BE8F4D7210B4D909E43B437D0,
	BoundingBoxExampleTest_Sequence_m2815993AD477DD94A3F3022481E7071C91A7BFAC,
	BoundingBoxExampleTest_DebugDrawObjectBounds_m63B2C326983FD65B4326BB3DB39C4CA871D75321,
	BoundingBoxExampleTest_WaitForSpeechCommand_mCCD711FC1BABDFF3BAE9DEC26BC51B2995A3B110,
	BoundingBoxExampleTest_OnSpeechKeywordRecognized_m7F093F58ECF69047FE0783DFE1AADF29037D96A0,
	BoundingBoxExampleTest__ctor_m0EF5184A995D83B82467C2EE19B858808659C502,
	BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_m5401FB3A85CC9416A3A0F53641AE1710458D7138,
	BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_mD5787D43CEEAC5B444A4014F7A594ECFCF841A4A,
	U3CSequenceU3Ed__12__ctor_mA663B54F4980B4C13C28ECB85AA899FC07F85317,
	U3CSequenceU3Ed__12_System_IDisposable_Dispose_mFC94D94B07EF3F2C1247D365E0D84769024ED144,
	U3CSequenceU3Ed__12_MoveNext_m68C6D02B10634D65142180FD6B8F3D47F414193E,
	U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B985A513FD5EFE77C262DC64681E5E2C4B0AC4B,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m5FBFDB842A09B18EA25BB9D3FC0E50B76D987347,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mB9BB258F4FE7467F169CB5DA57A9705EB24076E8,
	U3CWaitForSpeechCommandU3Ed__14__ctor_m19EBB56BB6F7A447FAD350D93775EEE2212125D8,
	U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_mC2F9F1E1C5F34227ED0B6D32BFF5D071A398D7AC,
	U3CWaitForSpeechCommandU3Ed__14_MoveNext_m45BDD1E7A696EB2A0767B31AA28D9C171F6847D9,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28F4EB16558BB5E01DAFF2FDAE29BEFCC976CD83,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m1482C0A7977C2AD6641EE11C52B6096C7F3C2D0E,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m38FCB3DA2EFBA5A05689FD0DDA9FEC89210F01BB,
	BoundsControlRuntimeExample_OnEnable_m32043787E11B8ABCBBEFF1E9404451B69D7C93FD,
	BoundsControlRuntimeExample_OnDisable_m07B769690C8482614E64D0FCF8225F7DE2767D3C,
	BoundsControlRuntimeExample_Start_m7E51A67593A7B5E72815A22E551473B1234E9B34,
	BoundsControlRuntimeExample_SetStatus_m3DC9EF2E593D96E794BE9786018B6EDAA45C20D0,
	BoundsControlRuntimeExample_Sequence_m5D3B4D8884CAA38FDD9CB04C6AAE923ECDC57F55,
	BoundsControlRuntimeExample_DebugDrawObjectBounds_m39B257E66BFCA6B53BBCA143E00A44C671C9B390,
	BoundsControlRuntimeExample_WaitForSpeechCommand_m92D8C8953D2F8B338F8F4AC8F835C23CD91382BC,
	BoundsControlRuntimeExample_OnSpeechKeywordRecognized_m951D513FE1437B071A9C868CE0BF181101208BAF,
	BoundsControlRuntimeExample__ctor_m6CB3D841CFC8900DA38BAC8E41DA9178F907E4F8,
	BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_mD25E64FA74BA6AA310D821E423EF22DF0AA85BC7,
	BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_m742CEECC7C111C331DE17B3599E20CF5540B2A58,
	U3CSequenceU3Ed__12__ctor_m55B247941B692847AA4CCC0ED16980FC2D03B94C,
	U3CSequenceU3Ed__12_System_IDisposable_Dispose_mD723CAC7F92786F4DAF791EB47907BF11835D2B9,
	U3CSequenceU3Ed__12_MoveNext_mFA3FBE98FF0A6A0C9FDB3CBD283DFA58F749409C,
	U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m660BA921571B846752FE727B7A59A91889CDB038,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m6E69902A58F67293E421398F6D3DD7983F2B50A9,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_m44C927A6B3A701C153035A23FB7A42B60814A648,
	U3CWaitForSpeechCommandU3Ed__14__ctor_m7EC3A4ABDBCACE6D9B0E5916C6D6A6F9FE2DA7A1,
	U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_m30FFC6585ADBF2325EAD45119CF8D3010F6A6DBE,
	U3CWaitForSpeechCommandU3Ed__14_MoveNext_m084B1D7816E8485A5382284DC422D017CC1A49E8,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3332944F5E4DB1D4F67ABC6396679665BF2074AA,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m23D9D167B9C254D9E0EDE56BD05605CB1A1EFC8E,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m2749F8A7420AC313C2106276C2E3B1EE028354A0,
	GridObjectLayoutControl_NextLayout_m23BB04DC59B670FB446E11A42213A88C4B12694B,
	GridObjectLayoutControl_PreviousLayout_m24F8056EEF08DA248D846AA1AF83C9D5C7D8F73F,
	GridObjectLayoutControl_RunTest_m4D7483AE0ADD69566A77D63C5E84B7AB5D38ABEE,
	GridObjectLayoutControl_Start_mF244041C4BC701356A043DE288B5293FE4236A14,
	GridObjectLayoutControl_UpdateUI_m63B641F14890EE88A96E81C708F94388C513533E,
	GridObjectLayoutControl_TestAnchors_mD647505E82D2241C5F965A41A1B445A013D7D3B3,
	GridObjectLayoutControl_PrintGrid_m4F6EE3C292819EB66185ACE0210CCFCF4F7EAAA2,
	GridObjectLayoutControl__ctor_mF0E73D9A1FB611F7954EABD5C7E31FC53A6F313E,
	U3CTestAnchorsU3Ed__7__ctor_m3B3F2D3E9732092D37D550372E6A197CD5B82428,
	U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m34BE0906298167ED5412589AF1475A38E94545F8,
	U3CTestAnchorsU3Ed__7_MoveNext_m59D0733A17890C0A4FAFC8BB78529243D345A531,
	U3CTestAnchorsU3Ed__7_U3CU3Em__Finally1_m573F2C47858941BBB4E65BFC96F53E47800B953B,
	U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23425D4DA6043B0BACFB553ECE773D481B7F6F03,
	U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_m122EFF9A2DDD3862957246285AD5706F44F99D63,
	U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_mD4E8A7309758EF3897AC9EB48789686F179BA575,
	ChangeManipulation_Start_mD6003C257B8C32C1F10208C2D2C3524550F388AE,
	ChangeManipulation_Update_mB6CD2E3249852087DD12E5C6F3DEBE2450958F44,
	ChangeManipulation_TryStopManipulation_m321451715A9445BA4F35C5E111C52C5C61FD0089,
	ChangeManipulation__ctor_mBB2442734228CE7BF6A47B503F66D5F7856292E5,
	ReturnToBounds_get_FrontBounds_m7A47E013948C72481150EEF1AEB6222585121045,
	ReturnToBounds_set_FrontBounds_m5CD7964F5726000F6B3CB5143A0B3C97692C3234,
	ReturnToBounds_get_BackBounds_m3CC5E50F406B99E2C3F5D847CCDA293044E0D59C,
	ReturnToBounds_set_BackBounds_m0C11D4EE3EEEE05D52EC5012E263CB1782A6A3CF,
	ReturnToBounds_get_LeftBounds_mF9E268BBA59DA87DCDB531DDF62DB4916D358F2B,
	ReturnToBounds_set_LeftBounds_m428D0FD67CC5433434830EAE5F354C5450B45C9A,
	ReturnToBounds_get_RightBounds_m68B51E70BAB055CC8337EC329E7E311704629370,
	ReturnToBounds_set_RightBounds_m05152BFD550D63A25BE2AB4F619EFF96883801B6,
	ReturnToBounds_get_BottomBounds_m835748A162A673F3D99801C4394F3E529F9C8A81,
	ReturnToBounds_set_BottomBounds_m6CF789953842D6403EAB1F4D678AAB487294144F,
	ReturnToBounds_get_TopBounds_mD66F106483D03BA32D6F66C92B5C729E8CB98381,
	ReturnToBounds_set_TopBounds_mF88FD05B07BE1C5B2B8EBC68B0460A6F675190BB,
	ReturnToBounds_Start_m75EC0D3A42A356FAA123868E4D2E44FC384D7215,
	ReturnToBounds_Update_m08B29ED9C4F8C510F5C100503CBD0947C0A5B4F8,
	ReturnToBounds__ctor_m6296DD3E33E6BA87D261D981EFFBAF0D3AB24ED4,
	ProgressIndicatorDemo_OnClickBar_mE249485554831AF9642197827B5597C69F09CB06,
	ProgressIndicatorDemo_OnClickRotating_m9EC74D04852B93C4991F3891E4E2032012BF8043,
	ProgressIndicatorDemo_OnClickOrbs_m2CC5B22649A43CB5BB3A684ED0E1A260C95D4B17,
	ProgressIndicatorDemo_HandleButtonClick_mB8DADCD9DA6F66028B8C789D2D22014B4D02556E,
	ProgressIndicatorDemo_OnEnable_mEC7E4C409CD5992D87A70E61FF8B0D5F4EEA9864,
	ProgressIndicatorDemo_Update_m4AB1D7597BD053CFD0B7525FC6DC1A6B8E4B9251,
	ProgressIndicatorDemo_OpenProgressIndicator_mEA02AB6401CCE2F4B69BB6E25ADCD75E0F67ADB2,
	ProgressIndicatorDemo__ctor_m1BB04733ABF3D2B03BFB47B2208505F73FDCAAD7,
	U3CHandleButtonClickU3Ed__14_MoveNext_mF720161ECDEF41103FA127C6530AD8437E8EA6A6,
	U3CHandleButtonClickU3Ed__14_SetStateMachine_m5B82CD99AA2F16295DDEC5460785119016DA29FD,
	U3COpenProgressIndicatorU3Ed__17_MoveNext_m1BD23385324B99F28EA5A32496561FA695B5F61A,
	U3COpenProgressIndicatorU3Ed__17_SetStateMachine_mA9B346FEC4EADA24B0E0784703212DFFD1215ACF,
	SliderLunarLander_OnSliderUpdated_m5EF016138B6DC527F44B05F56FBF57A06BE4B5BF,
	SliderLunarLander__ctor_m16C9648E3A00E0CD8CEDCA1DB4119FB7C6F48026,
	MixedRealityCapabilityDemo_Start_m4C359D1DEEB760A211B692325FAC59EA77879A8F,
	MixedRealityCapabilityDemo__ctor_m8F7A48E8A9FC779C6899C89A8198ADF3F5A15E7B,
	ReadingModeSceneBehavior_Update_m2565D68981EF0DA269C19A09A9885EF1B907559A,
	ReadingModeSceneBehavior_EnableReadingMode_mC34A0D03AA61CB6DB7A43831361FA3BE247CD051,
	ReadingModeSceneBehavior_DisableReadingMode_m02077800F845467CEE761AAD7457361268D9FA2F,
	ReadingModeSceneBehavior__ctor_m384062F15608B93A080381545A638FFE62ED6C0E,
	ColorTap_Awake_m5B0748BC43CB2EB1A25F9B91961C2A3411A17DFE,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m75AAB5DB726346536886AA399F0F41770808F465,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m9343BF91A62EAB87D67344D703054B93247E26CD,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m6577294DB1F606E9C531688D70DFEBD18D2B34FD,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m39879CF0E9D84DC65C5DBBB913B6849F487F17F3,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m2907579CFD6051BA5FE627A8500B8BBE11D5CAA4,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m7724C137D555DA8BDE49890ED09645FBCEFE7EA8,
	ColorTap__ctor_m3B0B18CE2B657CAA5E91505CAE68AAA339F725AE,
	FollowEyeGazeGazeProvider_Update_mB689FFA9AA489E4A7F3A53B977562DE423BA31BB,
	FollowEyeGazeGazeProvider__ctor_m702A0A5EF6D725D4AA103018B5FF96A03369E6CC,
	PanZoomBase_get_EyeSaccadeProvider_m811BB49108CA3819FB0F6FFF76148B3C33EEC455,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PanZoomBase_Start_m9E1A94CE9A9DE0DC66689391455028074550284A,
	PanZoomBase_get_CustomColliderSizeOnLookAt_mE6219C39F17861C551D4431C3DA809807613ABB6,
	PanZoomBase_AutoPan_mF4225157DFC50748761B21C33894C055ADE2B32A,
	PanZoomBase_PanHorizontally_mC28F0AD9A7708DE29AC7AF306AAA9594C549FC4E,
	PanZoomBase_PanVertically_m4997AC2B8EC987057C66FAA7E33F980A598203D4,
	PanZoomBase_EnableHandZoom_mE8AC9DF3B7B0A5576377B3ABF926D10CC88016FE,
	PanZoomBase_DisableHandZoom_mF676B88558C48ED63E369211266850E8DC52B2E2,
	PanZoomBase_ZoomStart_m60CC810675822C9F3175636C87C84C8F26FF44D1,
	PanZoomBase_ZoomInStart_m59DB8482763052590B41D1C71063FDE5CE469857,
	PanZoomBase_ZoomOutStart_m8E0E25B9B8296E8C703CD6E775D30834F2522314,
	PanZoomBase_ZoomStop_m62575FEDCC8B7F8C1FC5ACB5714F46702ACAC3B8,
	PanZoomBase_NavigationStart_mFE247FC7AFA1E31C03BD9D823F65FBD4063175EE,
	PanZoomBase_NavigationStop_mC82C14FDAFF691F5F55754AE1FC716094EA023C4,
	PanZoomBase_NavigationUpdate_mDC9595D123232E6A6CD134FFCDEDC467F6908CE5,
	PanZoomBase_Update_m4DEC4FB6B13010D0042171BA9CAC61326FC762B5,
	PanZoomBase_SetSkimProofUpdateSpeed_m87E3C4DAF595EE1768A8A7575A8CFDC7D075EDEB,
	PanZoomBase_ResetNormFixator_m536160EB4A351BD28CC7B1813A39F495A4C27EFC,
	PanZoomBase_IncrementNormFixator_m5C54FAE13E9A82DD0D0B84B667CFEBC00F257C52,
	PanZoomBase_ResetScroll_OnSaccade_m2B772378EC95AE787C31ECB9606B6A53D70A3975,
	PanZoomBase_LateUpdate_m4F49BCB26913084A61B0A3616C3EF198268AD800,
	PanZoomBase_AutomaticGazePanning_mE5E8E8DE77A0EB31349F1988D07114CD9B63260B,
	PanZoomBase_get_MyCollider_mD6AA9AD640EA709EE208933FAE25421FFD043979,
	PanZoomBase_set_MyCollider_mFD875A8827CECB4B8E8B96C20BB85E468A554CB8,
	PanZoomBase_PanUpDown_mFBA766EAC54C966F185642F32923E73B1EE1DF18,
	PanZoomBase_PanLeftRight_m216BDB263C0735562E809E9C8EDDEB2179656DD8,
	PanZoomBase_UpdateZoom_mA77F02A03DAA6B6017F5F5A5373F3864AC6F70E2,
	PanZoomBase_LimitScaling_mA79A4B2BEB4FF523475A80C865468E08AFCF2ED3,
	PanZoomBase_ZoomIn_Timed_mAC6F4E7A38D4E0D08131ACF0023C61412080A341,
	PanZoomBase_ZoomOut_Timed_m98575816334CC9F7BC55B964326A9BC965CC22E8,
	PanZoomBase_ZoomAndStop_m3BBADDBDF41664DC7702B155D7F4F2C1F7417A21,
	NULL,
	PanZoomBase_StartFocusing_m357A96792947A2C46EEC402C81EFAAED58016A9B,
	PanZoomBase_StopFocusing_m2D85E400C402C6E750AA3B5D7F07E8721A753A78,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m8A735A80147B96EBB033BE84963D2AD7514A4F5C,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m683370431A263429C1E1CFE19F9C7A0D005494EB,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m9FA5D7496D5BA0B7DFD0E9074BD990DDD6747BA1,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m5F5417379D572CD2DD0F9D7829FEB52BD100B960,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m6BBAAE5223655643AD7FCC4AF75CF1612F4C134C,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m6DA4F77E4CD2345C1976F5A949C058F13EE52AE8,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m3A5CECC1DC75E173CDAF359D4A4965DEBAB7C26C,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_mF19020B6E1661B1EDCCA800A083101D2C107E63D,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_mAE7541398B00185D9D197E51EDB0D0BAE38D4D35,
	PanZoomBase__ctor_m44859B817973C6500079E081A36AB5B59B2ED690,
	U3CZoomAndStopU3Ed__78__ctor_m30C53A8A39CC3FBAE8A279C57EC3FDC2C55706B7,
	U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_mBAEE47B4402D4A15A9019E8BE21DF0E709776BBE,
	U3CZoomAndStopU3Ed__78_MoveNext_mABBEFDB098EC2BC20D7F0A2C666A9E54C58CF892,
	U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C5C426516CF1CC7EF5EF725543F34C316B3137F,
	U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_m0C9556653FCC60711B49BCB31FCB46DAE13556DD,
	U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_mBD9E41C773B5305A57FE6361E5A74B5AFFAF3137,
	PanZoomBaseRectTransf_get_IsValid_m33B1D98FB5A97CA8EDB7E85444BBD530A3395DC1,
	PanZoomBaseRectTransf_Initialize_m2E0F2FF44E985D4D27786CD42BB995C72E313A83,
	PanZoomBaseRectTransf_ComputePanSpeed_m88D9336E7A56D2C3651E559771E7D8F6F68EF964,
	PanZoomBaseRectTransf_ZoomDir_m6B2CAA4BC7E141464367FDA33A0AF625233F0CFE,
	PanZoomBaseRectTransf_ZoomIn_mF634DFB0F1F818BB3C0EA4CC322B10C84D91E77A,
	PanZoomBaseRectTransf_ZoomOut_mCE0AC547E432E2302CD89C36293CB6157F21CA8B,
	PanZoomBaseRectTransf_UpdatePanZoom_m6CDE014A4584C41AA97410460CACE1B786139EFC,
	PanZoomBaseRectTransf_LimitPanning_m8626D4A0DB5642C15DFD4BEA2A4A7BF38A045842,
	PanZoomBaseRectTransf_ZoomInOut_RectTransform_mF5CE843B79AA6E60FCB1C2BA2EDB2057AFA8514B,
	PanZoomBaseRectTransf_UpdateCursorPosInHitBox_m1495886A2942E11C4A69A502613510BAD9DF7661,
	PanZoomBaseRectTransf__ctor_m5791DAA60859F5378DFFFF789E5A6C383D67F3B3,
	PanZoomBaseTexture_get_TextureShaderProperty_m72B81821F9738602E98B87BA525103FDFC5D629A,
	PanZoomBaseTexture_set_TextureShaderProperty_m7415E1D1C6BC45D5F29876E22346F31C70EA5F15,
	PanZoomBaseTexture_get_IsValid_mDB1F9C9DFE43502BAFA02EB4A8F6238EA10FDC61,
	PanZoomBaseTexture_Initialize_m76DEB5F9F75B20748CD0585038E0AD02C0DCAB4F,
	PanZoomBaseTexture_Initialize_m044FFA8F9C6FB6334E7AACB3CFF1410E45103989,
	PanZoomBaseTexture_ComputePanSpeed_mB591FEDAD310D0EB7F16B2384A613D096013C8CE,
	PanZoomBaseTexture_UpdatePanZoom_mAB9D05E4C43D9DF69D2CA654BF38A6625720B777,
	PanZoomBaseTexture_ZoomDir_m2748177E06D502EA36BA162B23144217D720BE1A,
	PanZoomBaseTexture_ZoomIn_mF5AED4DAF9BB5E2AA1597339893567BCB88CE52A,
	PanZoomBaseTexture_ZoomOut_mF30A71FC3E55C0EAA0E267F69896D454545B8CC1,
	PanZoomBaseTexture_ZoomInOut_m903899DA63F9408FF245CB268C1B32AF26C934EA,
	PanZoomBaseTexture_UpdateCursorPosInHitBox_m2E96ABA642771F86E30182D9A56781BC1AFC36A3,
	PanZoomBaseTexture__ctor_mB335A71BD80791CDD2D580D3FFE2D47E958333FB,
	PanZoomRectTransf_Start_m9C9F5CB03E2EC4CCA21AC7CC7FF1EC1B1E5EB348,
	PanZoomRectTransf_Update_mA3707EBA4508008C84CACD8DCAF353FD70F621BB,
	PanZoomRectTransf__ctor_m1E3DADE797DC6C487D2899868ABF6C80D9F0DA5F,
	PanZoomTexture_Start_m38B9EA010636DAC2FD2D03F84921848C51C778B0,
	PanZoomTexture_Update_m51BFB8F50E32FA32CA0748AB670E1623C19AD0A6,
	PanZoomTexture__ctor_m4A80E1EC5003A94A339DC3D9DD7EE6EAA972CAF6,
	ScrollRectTransf_Start_mB3FEAD54E13A4583692759A19C373AF30646B292,
	ScrollRectTransf_UpdatePivot_m1E27424744FA1F3AFD878F5B72574BC00A28951E,
	ScrollRectTransf_Update_m06F0A161CCDFD80537096DAAE1D646B81E73F1F1,
	ScrollRectTransf__ctor_mD10FCDB4DB284D45DC16882D579B30F02C20CFB5,
	ScrollTexture_Start_m5B690489FCAD1D61DCC57AE7976F921593EA3FC8,
	ScrollTexture_Update_m4C24E9A0029B0B6E4D4F536A37DF071613B5B1FE,
	ScrollTexture__ctor_mE2FDA788487523755B18303B864B706E8D763982,
	TargetMoveToCamera_Start_m941551F3BE41FB020F73E171F21193BAC90492B1,
	TargetMoveToCamera_Update_mDD41B84708CD03E20CF1933B9BD9EEA975C7EE6E,
	TargetMoveToCamera_OnEyeFocusStop_m7DE5069C043EFBEBA5F28838019EBCABAB3256D3,
	TargetMoveToCamera_OnSelect_m160D3D80A06B651B06C6CF55722EE165324DCE4C,
	TargetMoveToCamera_TransitionToUser_m3D3A2F7C149CAB8A5BBD48548DF0D32622D79534,
	TargetMoveToCamera_ReturnHome_m993554E6685A17671618F47DA3A0DD6582537385,
	TargetMoveToCamera_TransitionToCamera_m864B7147454F17CFDF626C6CA7F9DF6483095C0F,
	TargetMoveToCamera__ctor_m04D8CE0BD97A138AAAE45883147D359C0394EA5C,
	GrabReleaseDetector_Awake_m5C16858474773B506742617DD012141E79BB5209,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mF8D3E2F68F5D5B4BAFE287FFF371FA34E2F37591,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mE2BB94F79D6328FA0053BC2173ABC82EF521602A,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB11638EB582D5D5E3AE4EBAECD12F2BA4D9E8D67,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m4C9F19BB3B5F61B5E5D377A020958CB60AA5EF2E,
	GrabReleaseDetector__ctor_m6AA5BA7C598B58ECD098DD13A8895C0C75D5DCC7,
	MoveObjByEyeGaze_get_EyeTrackingProvider_mA8DBE1CD875F04E83D11DBAEAD0619706F0E9B26,
	MoveObjByEyeGaze_get_ConstrX_mD0A87209424A466F68A765EB622DF7A3284D5F2B,
	MoveObjByEyeGaze_get_ConstrY_mECE865983ABA30D469541DAB01F5E456468B4844,
	MoveObjByEyeGaze_get_ConstrZ_mCF15E8138318027BE7C2F5EC820DD125B495F1A4,
	MoveObjByEyeGaze_Start_m0AF6953E3915D39941DF3B6DD1F41F1EF116BA37,
	MoveObjByEyeGaze_Update_mDE77D9A6EBB6F00F104C949B0EDDDFAD07AB9F76,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m13BFBECF659FFA9C3EAC18225003A925AC582E81,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_mCCBA974FF15D8329AA1AFA12D330DA1542195D4B,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m8FB158C1817A4CA659C9E1BAA630A2EB50100A42,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m85728852918C79AF823C8FA3C27B7493DC31B2A6,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m71AC0138D46C10488FA9AFD05A11626E49244C16,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mBD17757DABDB930BB1AF49CDD67AC8C205625B9F,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m725AF569AADECB6F1D8528C04FC47125DB003C4C,
	MoveObjByEyeGaze_HandDrag_Start_m00794FAEDB4F0760C1A2E52AA1EE17102E962AFA,
	MoveObjByEyeGaze_HandDrag_Stop_m3FDBE6C977EAB91577034E8D3090834EE7669E26,
	MoveObjByEyeGaze_IsLookingAwayFromTarget_m2EE6761557E0B665C94D46912A835AC6797E11A8,
	MoveObjByEyeGaze_IsLookingAwayFromPreview_m40607FA8770A5597BD45EE0D5D2FB4353917FAD7,
	MoveObjByEyeGaze_IsDestinationPlausible_m4F10BABC537B5440871B8AB858BDE56FD34B1F2D,
	MoveObjByEyeGaze_GetValidPlacemLocation_m3090052CDA5C67A17DCB861DFB314FD2F8067DC8,
	MoveObjByEyeGaze_ActivatePreview_mA89566A6C348FC868D20FCAA3937230A3AD9A947,
	MoveObjByEyeGaze_DeactivatePreview_m6DA3A23F2DDD010487FD7DE2AD33634BECD7C3F6,
	MoveObjByEyeGaze_DragAndDrop_Start_mC1BDB8D6A6470B6869FDAF597B4A2A69BEC59228,
	MoveObjByEyeGaze_DragAndDrop_Finish_m4D7FAF9AACD1065019620CBFD7F28568739F8FDC,
	MoveObjByEyeGaze_RelativeMoveUpdate_m1CE0A59A37F580D56041DB6400E6601B5BDEB491,
	MoveObjByEyeGaze_Angle_InitialGazeToCurrGazeDir_mD2B898C78C5DFFAC5F818F39266624B56703423E,
	MoveObjByEyeGaze_Angle_ToCurrHitTarget_m48A42A21185624CF74BB5B7D96FAE0D0C1743DBD,
	MoveObjByEyeGaze_HeadIsInMotion_m9A1B8381BDF396403C2AB2B82E6C48BAEE2964FB,
	MoveObjByEyeGaze_MoveTargetBy_m2BC0BECA50693B6BA0AE5DAA959BDEDC8158B1ED,
	MoveObjByEyeGaze_UpdateSliderTextOutput_mBDD1AB6AB3C368F0E15867BD02F0D7638AFA14A2,
	MoveObjByEyeGaze_ConstrainMovement_m8D13C5B3045F23CF0F6FF87EA809FFC0EE073CF4,
	MoveObjByEyeGaze_OnDrop_SnapToClosestDecimal_m255DD9EA82DA6856163D140513C804838A5FFBAC,
	MoveObjByEyeGaze_MoveTargetTo_mBDBABE716B44243CBF76D61016EFB0BEDBB23DB0,
	MoveObjByEyeGaze_ShouldObjBeWarped_mC565CF4B3EB836AE7068A41E01F49227E2A8603B,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m9C319765FB5E879EC16AAE33BC0BB3DF1F9CA34A,
	MoveObjByEyeGaze__ctor_mE90ED6991F03A2DF3400DEBADB45F5CBF3573F5F,
	MoveObjByEyeGaze__cctor_m39C2BE838E485A6CCBB57FCE21B5E7B974C83A47,
	SnapTo__ctor_m28F482C53D1C3BF487E8CC24295477B656FB1B58,
	TransportToRespawnLocation_OnTriggerEnter_mAB4066F48FB8BE6AD812407A97D9A7F1C235D0A8,
	TransportToRespawnLocation__ctor_mB11A32C5C32FBFF544F8C5E973DB64A10EF209A8,
	TriggerZonePlaceObjsWithin_Start_m82741DCB01D9758DA02F743446DD06B1381AA137,
	TriggerZonePlaceObjsWithin_OnTriggerEnter_m00DE1A1057B04E3F308AEEA04BB19B8E22AF2DF4,
	TriggerZonePlaceObjsWithin_OnTriggerExit_m9D73E6A37966B80907C6372CF02ED65BCA73B274,
	TriggerZonePlaceObjsWithin_CheckForCompletion_mF362892B6AF50C461D36D5913995E9A3D1BE5165,
	TriggerZonePlaceObjsWithin_TargetDropped_m1E654BA879B9359ABBB4D48A641F28A31BFB470B,
	TriggerZonePlaceObjsWithin_AreWeThereYet_m672574735FF19F1AA08E0FB6DE7A884766E215F8,
	TriggerZonePlaceObjsWithin__ctor_mA1E439FA78080A4B4173257A47114957ECBCB3FE,
	HitBehaviorDestroyOnSelect_Start_m48487AAC00C5461B68D16F5D1DC33763F3A49026,
	HitBehaviorDestroyOnSelect_SetUpAudio_m2E2C109048C9CDE854B53591CC3659406EB49887,
	HitBehaviorDestroyOnSelect_PlayAudioOnHit_mDBD5D04684169F9C9008C88E1C4AAF2BD3D32E77,
	HitBehaviorDestroyOnSelect_PlayAnimationOnHit_m7A76B49083DBF3C240D9E8EFD2FE2D0127E18C2F,
	HitBehaviorDestroyOnSelect_TargetSelected_m84D6E172E557643F9E927C08C9E35DC1CB88B473,
	HitBehaviorDestroyOnSelect_HandleTargetGridIterator_mE4FBB3310B9D8B0949CC11B78D0EFAC1141309E6,
	HitBehaviorDestroyOnSelect__ctor_mC583F57875912AE72E1AD608DFDD72D187DE63A0,
	RotateWithConstSpeedDir_RotateTarget_m9808D6B937D1E3D1C8172F7EDFB8A1F14BADB5F4,
	RotateWithConstSpeedDir__ctor_mF88607AA9D1A217C680F212360BE902DAFA2AE34,
	TargetGroupCreatorRadial_Start_m831613E6B5C6F5A063AD16D1746AD4095EFE8DD2,
	TargetGroupCreatorRadial_Update_m440B0DBC7C39FB38F6969A73C4BE695AA55602BA,
	TargetGroupCreatorRadial_HideTemplates_mE5F73C348AD52A91413B1A72BF767DDBECF00893,
	TargetGroupCreatorRadial_CreateNewTargets_RadialLayout_m2AB6A8EA5BBE6BFBEC3C81A0D2C15F9447CAD4FD,
	TargetGroupCreatorRadial_get_InstantiatedObjects_mFE13FF0C02937405C0385E28B269500F644B50C0,
	TargetGroupCreatorRadial_GetRandomTemplate_m0546AE27D46912D6D5C3CC9CEC55F12FC0084984,
	TargetGroupCreatorRadial_InstantiateRadialLayoutedTarget_m8F2AC71FE3DC54D5DAA8EB0EAD18ADC51E31A2D0,
	TargetGroupCreatorRadial_KeepConstantVisAngleTargetSize_m79688D8259703160D79A8270288B4A80D4C4E10F,
	TargetGroupCreatorRadial_KeepFacingTheCamera_m337F48BC581563B43B4C65BC2796D1BA844CA374,
	TargetGroupCreatorRadial__ctor_m44939C31AD440025D5C852D1A460B7815E9D7B4C,
	TargetGroupIterator_get_HighlightColor_m2DEE097D7D4A652ECDCB9E7B2B79EA228CA02528,
	TargetGroupIterator_add_OnAllTargetsSelected_m6C0E9568229293FD6865AD8A9A664EBBAF4B8297,
	TargetGroupIterator_remove_OnAllTargetsSelected_m0058693A19333EAEC032003269F6F94335FD2F0C,
	TargetGroupIterator_add_OnTargetSelected_m5E6C941090BB244AAA18AA580E9A70957860C5D6,
	TargetGroupIterator_remove_OnTargetSelected_mDB22A4E5586BAF0909DEBA3D7D40A43FF822FC4C,
	TargetGroupIterator_Start_m17A656D6BEE047C14129DAF9A657DB9961AB24ED,
	TargetGroupIterator_Update_m4E12751AF44610E8165268695065489677F0105F,
	TargetGroupIterator_ResetAmountOfTries_m3AED4FD554CFD322237C012A5C84D946554EC5BE,
	TargetGroupIterator_ResetIterator_m6C1F315D4955A371E2E63A131454F4777AD16097,
	TargetGroupIterator_ProceedToNextTarget_mEB2B71CC30D84070933D99615F6EE94C4636A327,
	TargetGroupIterator_get_CurrentTarget_m11CC2AE3EF318E3E889EE1E5482D9539FC93FD1A,
	TargetGroupIterator_get_PreviousTarget_m36CBD481ACB1DB69CFCD4620994B1F76160F50F0,
	TargetGroupIterator_get_CurrentTargetIsValid_mFBBF420EBF95662187F671E51FE2EA79246C2571,
	TargetGroupIterator_SaveOriginalColors_m46119C0C8851B04FCBAB56682C07E3F8AC1B13FF,
	TargetGroupIterator_HighlightTarget_m6F02D4547C8C4458A14DAD0A3397918BD6B4409D,
	TargetGroupIterator_HighlightTarget_mD8EB8C3D2D854B6867FD611F8F2449B9C6882E3A,
	TargetGroupIterator_ShowVisualMarkerForCurrTarget_m1DDB110FF0E39C3763404403E8EE78A836EC0119,
	TargetGroupIterator_HideVisualMarkerForCurrTarget_mFC89BA7B36CB12929DA2AC4CD47C6EC2CB078E85,
	TargetGroupIterator_UnhighlightTarget_m7157393A06F3650FB072141E84BDBF1CC6B6559E,
	TargetGroupIterator_ShowHighlights_m3EC739F509C5BDB6F467FBEE993EA35458F560F1,
	TargetGroupIterator_HideHighlights_m5308D071A4D64FDB57D4DFF626A442CCCC1CC68C,
	TargetGroupIterator_Fire_OnAllTargetsSelected_m90C6833C574F27FF3CE79379C5915C2B4D37F7A6,
	TargetGroupIterator_Fire_OnTargetSelected_mBE3AC9A807BE7FFED530CA72571137D15494EA58,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mADECE26B73ABFA292D0374C9B1765058C75EA14F,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mB5372602D56F3983B471A4B3515A74DE67C83E16,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m7B2CEB7A6F30E03E08180D8733BC24F391AC5336,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m9F92C02C2C9DD924A24CEC1236A8BDFB359147F6,
	TargetGroupIterator__ctor_m614A02C7BDA69CED830D7D9764CB1CC8287130FF,
	TargetGroupEventHandler__ctor_m997C89086AD15A00E869AB6E434D653AF34D993E,
	TargetGroupEventHandler_Invoke_mBEB04FD68BFAE54AA1D98E5277F2B9EB9DF0096A,
	TargetGroupEventHandler_BeginInvoke_m46C2DAF130C84AB4AA790519A6CAA0E64051AFE1,
	TargetGroupEventHandler_EndInvoke_m7164986017E581F23F4E12B26827CED82D85D441,
	ToggleGameObject_ShowIt_mA31F1D58FFEC19F10F822CDB729FE86289106F49,
	ToggleGameObject_HideIt_mD60E92D7F00FCA4D1FB249BE161A082FE532C477,
	ToggleGameObject_ShowIt_m232C17535A6619725980E6432B560F98D5398303,
	ToggleGameObject__ctor_mB5EC9D7E66468ED63D2AC3905482D496340D072D,
	DrawOnTexture_get_EyeTarget_mA70E643C1E86AA742B828A1F17671FA9FD295DFB,
	DrawOnTexture_Start_m327596B8F4A0E2AAC2053C3E901AB84FD9B0B723,
	DrawOnTexture_OnLookAt_m540C4800E116A89CABCFDBBB35DA58433141C55A,
	DrawOnTexture_DrawAtThisHitPos_m047A7C6A53B8D4ECEE1DEDCC8F3DA70212912A1C,
	DrawOnTexture_DrawAt_m5F0F192DB617A2E7FB522CAE89E3F657827491E5,
	DrawOnTexture_DrawAt2_m8B027C59B579518ABA1E3FF3527570DCBE08178F,
	DrawOnTexture_DrawAt_mACF2F8ADBF4397769AE19F1349A1ABCAE5E27D09,
	DrawOnTexture_ComputeHeatmapAt_m6C6D602F8B92BD20208D4B693DF4B22300DB86F0,
	DrawOnTexture_ComputeHeatmapColorAt_m383E4A8E46C915FE697F45980D05DA752A22324D,
	DrawOnTexture_get_MyRenderer_m78CD25F04403DACC07F3F486F7F308E729A110C7,
	DrawOnTexture_get_MyDrawTexture_m9DE426485BF5D05377FE28682895668DBC96438B,
	DrawOnTexture_GetCursorPosInTexture_mFF3065FFFA16BE80B82D3A79F3920ED73E9785A4,
	DrawOnTexture__ctor_mF8891432993C7EF93E82C8A6257542466CBA73A9,
	U3CDrawAtU3Ed__19__ctor_mABD515A84774524FDB73B1E4F727A8D187845FE7,
	U3CDrawAtU3Ed__19_System_IDisposable_Dispose_mD855D631D537C8C972022C07A4E6B86F20694767,
	U3CDrawAtU3Ed__19_MoveNext_m4DF6A2F7745D70FFF3ADBE96578F3012CCF60ACC,
	U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60FBFD67550BBF502D2B84D09BF924A6F53A72B3,
	U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m519DAF7D703E82BD40D6C88FD166E9C92FF15E54,
	U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_mC139E877DB3EE6F74DB6CA4BA39C6255D8D43451,
	U3CComputeHeatmapAtU3Ed__20__ctor_m53B7C7F16531AC26E5DEAFA91C43B854C9B32579,
	U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m16413974FA759817A38994519D529BFBC814F259,
	U3CComputeHeatmapAtU3Ed__20_MoveNext_mE1FE991D3205CB8680BCB0E361355BE082D9E8B2,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FC8124B6EBADC8E4051496092E545DD8444561B,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_m40447F82651458D01A92A2E255E914D430A93DD1,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_m5756DA265E1D39955B13A9266C169C371F832D8F,
	OnSelectVisualizerInputController_Awake_m82957B06EAF6AFAACEB6F8E37EDFFCB9C8C4F9EB,
	OnSelectVisualizerInputController_OnTargetSelected_m0AA0FBCF573A8A7FEF253392A2090399361408B4,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mD3DFF9EE88EF810D9AFDF1078E642D64A9AF2A54,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m21554712FD0EA7E6403AB8687E260FDE66F5389F,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB4E80D6D0A9DBF4C386568CE24A880CC23E555A8,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mCEEA16E41BDF811BABCAC1035A30D4A14B31F4A2,
	OnSelectVisualizerInputController__ctor_m6FE60D81C1E8051770AB91546E5ACEFBF3FDCE34,
	ParticleHeatmap_Start_m05347379F9A7FA3C65876A9D3EFCC6BB527127FB,
	ParticleHeatmap_SetParticle_m559A29E8081760B3E56D5CF75EF3167AED1EC353,
	ParticleHeatmap_GetPositionOfParticle_m14896739BD05E1DABD3BAF93AF7862E2E0D37D54,
	ParticleHeatmap_DetermineNormalizedIntensity_m54771F152B1FFA5AA1DEB19D255B28A19049E3E2,
	ParticleHeatmap_UpdateColorForAllParticles_mAE24C104FF6FABFA70C731D2245D18E7C3469B3D,
	ParticleHeatmap_saturate_m0D63575121242EFAF3ED8383642568A5942EB146,
	ParticleHeatmap_DisplayParticles_mDA6F6C2342CC9D48B7B51D8C7915221313EFE828,
	ParticleHeatmap_ShowHeatmap_m46CAA1254ACB043F9DCC54D3B926585693AA3D0D,
	ParticleHeatmap_HideHeatmap_m2E469EA0C52AF24C0D6A74EB4BEBD2413DC318CA,
	ParticleHeatmap__ctor_m15C54986BE73F77665A1DE903C50BE4969D8794B,
	ParticleHeatmapParticleData__ctor_m6CB919346735511FC3D5429615FB65C10B1E2A8C,
	AudioFeedbackPlayer_get_Instance_m912630AC6CAD66FD421E7A52FDCB1CF948D3C2D9,
	AudioFeedbackPlayer_set_Instance_m48FD2B8B89EFF6DD59A9C457F3B06AE8920ABE6A,
	AudioFeedbackPlayer_Start_mE32ABA1C89FD78754589D045C6136903EE27CA42,
	AudioFeedbackPlayer_SetupAudioSource_m6FED5C5810DC8568F04C63A43A3210375074B42B,
	AudioFeedbackPlayer_PlaySound_m0F80FA8C3D86D06B605514F298C4C19D83C60FFC,
	AudioFeedbackPlayer__ctor_mBE6DE1EA39695BCE2978149DDC785A49DEDD73BB,
	FollowEyeGaze_Update_m1CA67FC6955B8BA18760175CCBBD1BC5160A47BB,
	FollowEyeGaze__ctor_mDC6451236CF451885296C0097AF6ADBB2A3EE783,
	SpeechVisualFeedback_get_MyTextMesh_m7809A802D36E4B4051C4D500C85207818961DB2A,
	SpeechVisualFeedback_UpdateTextMesh_m8A492269EEF70E7D0FED953F4D4518D7F6F1C66B,
	SpeechVisualFeedback_ShowVisualFeedback_mE9575158229425BD3C15A304C7F2FF609A31CA1B,
	SpeechVisualFeedback_Update_m1B57AF94099A602F2190E739438FECF17F4ABC33,
	SpeechVisualFeedback_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m1AF24F9AFBD6141EA377FD55A35B7AC8BDEF8944,
	SpeechVisualFeedback__ctor_mC968FA1A631A51E110C4B2BFD2A72C83EA2AD164,
	BlendOut_Start_m92CF6BB3F1E50CE30E0AACD8200A87A5FAD2DC42,
	BlendOut_InitialSetup_m341E859D5277A1CA5462A59B63554D91399CB2AD,
	BlendOut_Engage_m7F24EFE9BC7AB7BDD871CEF103DD1D9F9A734876,
	BlendOut_Disengage_mE1A6575BD4D343D8B469783C37EA89F374D00E1A,
	BlendOut_DwellSucceeded_m11AFA8D81D0F3B2CF1B2328114F61BE8EE9B1501,
	BlendOut_Update_m30ED869C44C58A92929404F54FB3FBE1453794BE,
	BlendOut_SlowlyBlendOut_m87AFE227ECAC3D57480F21E5D708B8AEF4DD3513,
	BlendOut_ChangeTransparency_mAFF086F037D07D718B0A820D4DF8118300AD790B,
	BlendOut_ChangeTransparency_m359E3957A3492831C4D1CFD0C2BC81F04E4C83EB,
	BlendOut_Materials_BlendOut_m8F3BD3FC54BD74C7FACAC5DB6DE5170C5D6DBE6E,
	BlendOut__ctor_m66AC9A8DC165FADA57242E3A19D8A5938417283D,
	ChangeSize_Start_m8FE6A1A724C46AEA65727FE899C5166F395FF788,
	ChangeSize_Engage_m40C66E8A2C8F410BCEEBE9CDE9AB07D4737214FE,
	ChangeSize_Disengage_m4000A6294C3B0278DE8846829525F32A85532FDB,
	ChangeSize_InitialSetup_m654A4784D159CE6D364E921E49DC9D7943DC39FE,
	ChangeSize_Update_m2F8853498A0F35A83B9699597C1D661CC979669B,
	ChangeSize_OnLookAt_IncreaseTargetSize_m563F8A6B0E0EEDC5DC35D8C7AEFE6B010C374CC1,
	ChangeSize_OnLookAway_ReturnToOriginalTargetSize_m2BB092B93CC1386F1864FEEEB30CD88F85814150,
	ChangeSize_AreWeThereYet_mD19A19E9D31445E02AE02B9CC91B4255D35F6AAF,
	ChangeSize__ctor_m24FA050091DBD07218344EE79A049EFC146C1492,
	FaceUser_Start_m745571D26DDBBF85366F4E4A997CF41F2B0EA5B2,
	FaceUser_InitialSetup_mB8E86391DFB496D0FD176D1D182028F31F7A74F4,
	FaceUser_Update_mFD6A52D6A65CE218B2224506D44C6A438AF3280C,
	FaceUser_TurnToUser_m1D9591AF2DF395937766D9193BB47F35D39E23A1,
	FaceUser_ReturnToOriginalRotation_m9DB7C5783B52550F1F93261336C6AC387B27A13D,
	FaceUser_Engage_mC609B46E080445B5CA409968ED931065A9CD13C8,
	FaceUser_Disengage_mD76051E077D7D0E3D6DB46C41AC711CBCCD17A8A,
	FaceUser__ctor_m0ACF4D51A5A138C8322B1C867D6EAD5DB9115927,
	KeepFacingCamera_Awake_m68EDDD927545FB3316A5A0CF6C8249F3F021B713,
	KeepFacingCamera_Start_m32A6B37DE55EDDEB03E22062A4B739DEBBDF6605,
	KeepFacingCamera_Update_m1D90A01AC83754B6CE5B31F85EB57A9601043C94,
	KeepFacingCamera__ctor_m6C7432F67BB02A7EEF9D8D541B577EAAB1B4DEAA,
	LoadAdditiveScene_LoadScene_m4C2D7DEE921E17B9FD6C243FDE12ADB8AA34A3F8,
	LoadAdditiveScene_LoadScene_m5E35643C373870B340B243FA3D4A6ECA9CA24146,
	LoadAdditiveScene_LoadNewScene_m41B80A9D1B71E2BC35FC564D347578C5D79CD0B1,
	LoadAdditiveScene__ctor_mA807C178CB329689965D38022A55023EC8020BBF,
	LoadAdditiveScene__cctor_mCEA48A9111093FC445F0C0BECC58D0924E781B42,
	U3CLoadNewSceneU3Ed__6__ctor_m1331A95D8499FADDB82F2FB52E9AE3FDD5006176,
	U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_mCE27DCE29F01564C1FC0C47B33E186218792D0B8,
	U3CLoadNewSceneU3Ed__6_MoveNext_m85E3DA53F286661F66C6AEB06260A43105CBC82B,
	U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m282DC92EF12B29B3CB54A878B4B0962B4299E57B,
	U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_m5893EC4779342E70D038F6799047B868C1477831,
	U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m81939C50D638BDAC85C6AADAE36D762E5A80F4C1,
	MoveWithCamera_Update_mF5E2FF364275E6F437E0132BD2D920CC46B42896,
	MoveWithCamera__ctor_mC86F01FF3D2CD80E6573D2A00932FB3B2F98C980,
	OnLoadStartScene_Start_mA65E479C82FFC13659F92122E8EABC789347163F,
	OnLoadStartScene_LoadOnDevice_m9EE1C8972C73480CB013D787E7A0FBF65CAEC004,
	OnLoadStartScene_LoadNewScene_m69315DF288B92BD307BC86F1F1CB6B2FE58675C3,
	OnLoadStartScene__ctor_mA09AB0E4DDFA73414D6F3DBF232FF59E55963D26,
	OnLookAtRotateByEyeGaze_OnEyeFocusStay_mA2D0FEE0C229342825D2FA6D5D57E75619AF1EF1,
	OnLookAtRotateByEyeGaze_RotateHitTarget_m635E6052BA2FE5C416C8FA6F18CC1F73E5B62EE0,
	OnLookAtRotateByEyeGaze_ClampAngleInDegree_mB0B27DE6617523C3867E77C728B26413EBC29742,
	OnLookAtRotateByEyeGaze__ctor_m54F302C5AB29F2E03479C35BEE7E25D96E72488A,
	DwellSelection_Start_m26131C5585A6D086A1789BC898FE0FAA0BE31306,
	DwellSelection_EtTarget_OnTargetSelected_m8920976D57D2E531DCAD538FAAA85E1CB5E0655A,
	DwellSelection_EnableDwell_mFFADB227DEF696C5676CE33FAC44CCFCC01A8ABE,
	DwellSelection_DisableDwell_mDB0594528DF6DF35D1FCD85EFEA14BB0C57BBA08,
	DwellSelection_get_UseDwell_mCFAE17F0BD28FCABD3E3540FC5076FFA7AF99A23,
	DwellSelection_OnEyeFocusStart_m86466E84D200B34BE3CF4EA88F216152AB9D3B23,
	DwellSelection_OnEyeFocusStay_mE1F92A433993D8AE4A00FF81252C20FC781F2FE1,
	DwellSelection_OnEyeFocusStop_m9C4B6F635BCCAA5893CB822F1109EED149D961ED,
	DwellSelection_StartDwellFeedback_m39C04C7A743F8D75A6B3AD9443EA0681CE29E732,
	DwellSelection_ResetDwellFeedback_m2B9461B09996D61AD7BE6F4EDF0C06AD063709AC,
	DwellSelection_get_SpeedSizeChangePerSecond_m1DDDEBA6D680AEB9888D7E8FA5F9730DABAD03BE,
	DwellSelection_Update_mEC9951AC65D973629CCE691DDC366E99B5A09CDF,
	DwellSelection_ClampVector3_m6AE9B31FEF019D17D347A735A55FE656ADDA97CA,
	DwellSelection_UpdateTransparency_m109B9047A0BAD7FF2D2B2A73AF2F36C6EB003E30,
	DwellSelection_LerpTransparency_mD84EDBED6528B6BC1E07F5E253EDF238995F0D13,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mA77998B3632503CCE1D5E910E57CC7446942D25B,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m6B284F3161920A7949248683FF61206DD3482F81,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m297EE8AAFB38D8A3CC061A7115F818F63EF38A72,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mFC64E1A1C837F0D5CC4EB7A108018A9BE892DC04,
	DwellSelection_get_TextureShaderProperty_mF6D0878E318EE601DBB12AE7289EDDCDC7644477,
	DwellSelection_set_TextureShaderProperty_m8ABDC9C9BF8A2B0E4C46013494E3D06245EFCD2E,
	DwellSelection__ctor_m00D9CBAD7C19D27B475424B3C87ECF0C5C9EF158,
	DwellSelection__cctor_mA736173BF55B05663938C21F429617AD3FF0F5BE,
	TargetEventArgs_get_HitTarget_m2A5AE1ACE5D805D810A47F1C50FF564DF2DF44F0,
	TargetEventArgs_set_HitTarget_mD19716DFCABC5AE632B8EA4992BCC31832DEC023,
	TargetEventArgs__ctor_m139BCF25211B31F194CF5BEB82B4A9F2E743AB69,
	ChangeRenderMode_ChangeRenderModes_mFDC2BA9202CCECEC488C7FF4145C2829DE0DECF1,
	DoNotRender_Start_m5FDAFABF8A2B7012D56CA1AEDF1E069FEB39BA66,
	DoNotRender__ctor_m59717DCA45151107257DA1C0BD6630CB8AFEB0D7,
	EyeCalibrationChecker_Update_m79C82B57C73B837B39319DB3AE3707ABDC65E805,
	EyeCalibrationChecker__ctor_mA349C3C484C1921EE4E333D2DC0D5ACCFCC4040F,
	KeepThisAlive_get_Instance_mFE6D6461311FC9AA2644F7FB8E9F9B78382274CD,
	KeepThisAlive_set_Instance_m7E6EE81F37E81A5B03B89ED652060479A762A5EB,
	KeepThisAlive_Awake_m32D4F5CD19AA286255F77D998112ECC295038FDD,
	KeepThisAlive_Start_mAA40CC746F4DAEE258ED3AD10F4725F95EBC05A2,
	KeepThisAlive__ctor_m7AE743E0FC87ED0EC8136743410B0E8D726721D2,
	StatusText_get_Instance_mA4DAF9B004FDE3BCD2D57ADD050177514247218F,
	StatusText_Awake_m9F66EB7110483420B375A31126502F3255124B79,
	StatusText_Start_mD14E96259C7D7006A86055C331F56DA8C4173FE0,
	StatusText_Log_m22A9134C95F17165C1020AEF6A62F37DDD4C1873,
	StatusText__ctor_mEE38C8F214A04919137FBDB5919C35367ED47CFA,
	EyeTrackingDemoUtils_GetValidFilename_m1769969410FB8F47232D227A80EECC173D6D0C2E,
	EyeTrackingDemoUtils_GetFullName_mB06CD362F096B1E8612A26D9896514DBB725BC9B,
	EyeTrackingDemoUtils_GetFullName_m6011F94619750174835C6731606608D64BDBDEC2,
	EyeTrackingDemoUtils_Normalize_m7102A77A1A6F2050B1C4CF4837156BA3DD92BDFC,
	NULL,
	EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m1576FDB9B2194F38340A2365F6725CD4400A22BB,
	EyeTrackingDemoUtils_VisAngleInDegreesToMeters_mB4B9167DC5777679DF86C4845FBFA4E2E7B02188,
	EyeTrackingDemoUtils_LoadNewScene_mD937808E2CA495D5F29C629129C2C6DE9263E28A,
	EyeTrackingDemoUtils_LoadNewScene_mE6E7B24DACB3B93997AABE189E3589D7B4170F6A,
	EyeTrackingDemoUtils_GameObject_ChangeColor_mBAD6F4E89DE1D680B3F98E4CD4C4679B7C96233C,
	EyeTrackingDemoUtils_GameObject_ChangeTransparency_m68980832260767045305D6F6146BAEC882E2B311,
	EyeTrackingDemoUtils_GameObject_ChangeTransparency_m184502037227044035227EE9BB674B3E988A1F3F,
	EyeTrackingDemoUtils_Renderers_ChangeTransparency_m65E324A122674266F7BD8C5751F65C75E400EF8F,
	U3CLoadNewSceneU3Ed__8__ctor_m3A2F1CBA96F1179B92E3A52E1EE44A2B193FF1FA,
	U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_m58345FC11B56BAD0D15DB69FD494D264F254761B,
	U3CLoadNewSceneU3Ed__8_MoveNext_mD2AE7E7882DB08130868AE0361D333F82D66F86A,
	U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5817BBEAE74A29AEE51F5AEC7E7376B119D031D2,
	U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_m781F3CC9710C2418B6244439728EA2E856C8F44E,
	U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_m8C387795FF25C2D3F4383BC593F25C59A3F8A9C9,
	OnLookAtShowHoverFeedback_Start_m059CD02F7D337D91E93EA98EA7159895E33149B2,
	OnLookAtShowHoverFeedback_Update_mE004FD57BC941EC83BC5FC6B449824944FAA05BC,
	OnLookAtShowHoverFeedback_OnDestroy_m2C3FA5CC8D81FC5A98A86FBA45C408A21595923B,
	OnLookAtShowHoverFeedback_NormalizedInterest_Dwell_mF67325411E93CB4B80609505D0BAADC1AF7D1475,
	OnLookAtShowHoverFeedback_NormalizedDisinterest_LookAway_mCDB9AFB247A9B64E16A8D37BA734A17EFE5AC273,
	OnLookAtShowHoverFeedback_DestroyLocalFeedback_mCBEEB10FC81A23D56E5887260C6C95C7582C375F,
	OnLookAtShowHoverFeedback_ShowFeedback_m518894BE3D5E8CFABE4914BA3EE15A6CBBF6A930,
	OnLookAtShowHoverFeedback_ShowFeedback_Overlay_mCF240FED25D2FF7DBD82E8581641502580B649F0,
	OnLookAtShowHoverFeedback_ShowFeedback_Highlight_mFDE4E6960674DD10936AFBFFE174BAF6E04FF0B7,
	OnLookAtShowHoverFeedback_TransitionAdjustedInterest_m1A09B57AB20D30A67E2CB04EDCB627E12201A014,
	OnLookAtShowHoverFeedback_get_LookAwayTimeInMs_mC97D3065EF61567B9DE4D2F95DE54ADABFE98C56,
	OnLookAtShowHoverFeedback_get_DwellTimeInMs_m04854C77D70088945F4297294AE4F10A70F4F6D2,
	OnLookAtShowHoverFeedback_OnLookAtStop_mC1B7AB41559D84E40BC8E39B4442A5B28BDAB8CA,
	OnLookAtShowHoverFeedback_OnLookAtStart_m00A48B6842673BFCF201BCA1BA0E6FCEC9ED8F0C,
	OnLookAtShowHoverFeedback_SaveOriginalColor_m3D9AED9FBF4DBA54885F4B39F9DA2B3FE3178193,
	OnLookAtShowHoverFeedback_GetColorsByProperty_m454147565927415A1C6CFEF157FB85D518AAD53B,
	OnLookAtShowHoverFeedback_BlendColors_mA7110F451ACD334FA6B36D8A52E352A1F2BFB067,
	OnLookAtShowHoverFeedback_divBlendColor_mCAD946E68F7D4570F18AF6F5598A79DD4D3D1E94,
	OnLookAtShowHoverFeedback__ctor_mA02E007E932886BA028CCADFF7DE0999EB6DBC35,
	AsyncHelpers_RunSync_m749F9636EFAD43F9F8F564D79351D869A4A72D04,
	NULL,
	ExclusiveSynchronizationContext_get_InnerException_mF62B2FC120AAA96FDFAB07DCA41B1FDCF2930D44,
	ExclusiveSynchronizationContext_set_InnerException_mBDAC674E7236860DE7EE3A2B36A9CFFCD21E5680,
	ExclusiveSynchronizationContext_Send_mECC1E7E76598E502E9F3BFCFE01A7343189C3E1D,
	ExclusiveSynchronizationContext_Post_m626611A2FD0A186362E88D01FCB2F44650B079DA,
	ExclusiveSynchronizationContext_EndMessageLoop_mDBA1461B9A1A176909AB4849C1D5F2AEE21F3F8A,
	ExclusiveSynchronizationContext_BeginMessageLoop_m3F18EC869AB291241D7F81C1486909145A170563,
	ExclusiveSynchronizationContext_CreateCopy_mAB38AC92A4462D1EC4102F7ABD78C7D22B5C03A1,
	ExclusiveSynchronizationContext__ctor_m55B3E14A95501A29CB0BCF55B3CD4E0920EAE217,
	ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m9BE0BB978BCBC49997EAF9109511A0A0EC52C213,
	U3CU3Ec__DisplayClass0_0__ctor_mAD5C24DE56C36DB5CEF2C89CAE901EA304E40153,
	U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mF614B85D7EC692BA75E582FF3216638A145288E5,
	U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m4D4B2A2F50874C4C173492BC496BD868518AD19E,
	U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m0B908A49592D587220B0D4F5EDF8DC82DCCC2431,
	NULL,
	NULL,
	NULL,
	NULL,
	BasicInputLogger_SetUserName_m0F18AE9CAB8E242D62BE322EBC8304D045E650C8,
	BasicInputLogger_SetSessionDescr_m85B3A0308F86431BBDC10609410D007D2A14764C,
	BasicInputLogger_get_LogDirectory_mEC6DB4DC9AFC7F80A48E46018DB9421D6603F651,
	NULL,
	NULL,
	BasicInputLogger_CreateNewLogFile_mEE3B1C07FA9A4B6F75F9F3C1A7754A2373DC62DD,
	BasicInputLogger_CheckIfInitialized_mEC57E59C70E58D9C116C234657CD1724CA187DC7,
	BasicInputLogger_ResetLog_mA7BC0B6794FEFFB09DB51DBFC80FD308E7600E9B,
	BasicInputLogger_get_FormattedTimeStamp_m644D10667B8CB4AC4A38BA9CAF9233FB3720C19A,
	BasicInputLogger_AddLeadingZeroToSingleDigitIntegers_mE8EF6E463EA00E4831D81FAF72950A3491751F04,
	BasicInputLogger_Append_m3BB3B3F37660A66253AB486ECF859FFF71BBA9D0,
	BasicInputLogger_LoadLogs_m263523BC5198887EB7ABE226A4F4CD1330B241DC,
	BasicInputLogger_SaveLogs_m4C965A36341E67DE7F0D1B1F143C6B386FFC18E3,
	BasicInputLogger_get_Filename_m8564B5352FFA1F2BD9A741A34CEE88EC21BE5BF6,
	BasicInputLogger_get_FilenameWithTimestamp_m4BDC98552F2129CCCEAA73513F81864A03D1B80E,
	BasicInputLogger_get_FilenameNoTimestamp_m49EDFC8CBE04395ACB778668B53C0552E3D4775F,
	BasicInputLogger_OnDestroy_mB7B71D726184C6E314CE53C7F4CACA4A674AB707,
	BasicInputLogger__ctor_m13449FB20D73D923A5035E07077BCD4DBCD64566,
	U3CCreateNewLogFileU3Ed__14_MoveNext_m51610C8380D963ECD36549D7396D660051DC203D,
	U3CCreateNewLogFileU3Ed__14_SetStateMachine_mE9AF39239F16491AA8140EA2885F104078421DE0,
	U3CLoadLogsU3Ed__21_MoveNext_mBFBDA1A9170A471CB4AB057033A5665AF89420E1,
	U3CLoadLogsU3Ed__21_SetStateMachine_m52CD7696C92235AB638D02D5D50AD1F4E2FD8CE0,
	U3CSaveLogsU3Ed__22_MoveNext_m2DBD29F6521D3A72E1288518B9ACE7E6BFCB9D3C,
	U3CSaveLogsU3Ed__22_SetStateMachine_m08AA69B7E317EF2D112C6AD190A55B92EC99840D,
	CustomInputLogger_CustomAppend_m034F789A2E14846879796B9D27CF62BAB8C5C49D,
	CustomInputLogger_CreateNewLog_m47BF3C4F791E26F603003A079B76B6DEB0EA3385,
	CustomInputLogger_StartLogging_mEAEF562633A9EBE87A561E52D91FD13861C582AA,
	CustomInputLogger_StopLoggingAndSave_m5132B60BBE50403150C4FE95C00C3747AA950241,
	CustomInputLogger_CancelLogging_mA8701DBBABCFEC2EC042B6EECE331332BCC7E654,
	CustomInputLogger__ctor_mF55E765FD4C24BA1114E38DBEF1ED070FAF6FC2F,
	InputPointerVisualizer_Start_m89871394B7B69221AAA2D03C23680E21E1C58C2F,
	InputPointerVisualizer_ResetVisualizations_m6847E8783C70355193DF17FFA8C91A770910864E,
	InputPointerVisualizer_InitPointClouds_m973CB9C4AC370704992BC1CB7FD87D224E736D88,
	InputPointerVisualizer_InitVisArrayObj_m6F50ED1ADEF2BDAD3959AC69EE330B92F0B0A9BF,
	InputPointerVisualizer_ResetVis_m0C48AACCD9DA5F64DBBDE76E8ED62E60D158FA70,
	InputPointerVisualizer_ResetVis_m9665A3FE91EA6F6F40FD6A25F148B970BDEB8EB4,
	InputPointerVisualizer_ResetPointCloudVis_mFBC9BA65DC1E64D253D3D7D6205E54A7882609E8,
	InputPointerVisualizer_SetActive_DataVis_m0C93E38A1CAEBD3107C1C1EF9DC9E137EC507B86,
	InputPointerVisualizer_SetActive_DataVis_mD35978CCB6F81F65B815FDD5D878B953A3C71BAE,
	InputPointerVisualizer_SetActive_DataVis_m5BEE4C203346C18456E8BBC88348B8364399B8F7,
	InputPointerVisualizer_SetActive_PointCloudVis_m18DC4A1818F41A0E13EFE40E30E58D4C10D238DC,
	InputPointerVisualizer_UpdateDataVisPos_m5A994AAA9DDAFE8E82505188929A0C19AFFE9ECF,
	InputPointerVisualizer_UpdateVis_PointCloud_m4FE5899CB908CC587AC1316855A4C0FC22342C30,
	InputPointerVisualizer_UpdateConnectorLines_m3861799033ED9C1F184E88B179A54B44DB2485DD,
	InputPointerVisualizer_GetPrevLineIndex_m7DB648F08CBA65FDCEADB33CD4FD3E883CD2F59B,
	InputPointerVisualizer_UpdateDataVis_m8411A7A7997785E0E7C895A21D77D6D25AC597BA,
	InputPointerVisualizer_PerformHitTest_m2A09568D6836438E8BCFD1F783BCA97C98A3B1AF,
	InputPointerVisualizer_Update_m91DA053457D0F5E0137A59CCE4CD9F73C0315E2B,
	InputPointerVisualizer_IsDwelling_m32FED22023C69C94BB66DA356F8C1597694BFFE6,
	InputPointerVisualizer_get_AmountOfSamples_mB43F7C198CF1FA1EA69C889E1DB9263766EA2F12,
	InputPointerVisualizer_set_AmountOfSamples_m087B7ACAD2EB276230D5771B8D82402D80C5A0D4,
	InputPointerVisualizer_ToggleAppState_m22D601F3A252368492D61DA8E7C3FC51DF4799A1,
	InputPointerVisualizer_PauseApp_mDDEC107A59C29B0B2159CF3024BB95DB1D6A3E16,
	InputPointerVisualizer_UnpauseApp_m5AFBB94CCBAB9A39367E3BF516AC06D4CC70FD07,
	InputPointerVisualizer_SetAppState_m30B822699BA7E422DDC27CE328B73125FCEDE1F3,
	InputPointerVisualizer__ctor_m2BCE260DCEF580704F2C8480DB6775667239E23C,
	LogStructure_GetHeaderColumns_m0047E48DBE6BB223DE37365C81DF1F732ACAC695,
	LogStructure_GetData_m4B4479666DE3093EB2D2F5FAFFA46E99A77E3FD1,
	LogStructure__ctor_m0182A0D086BD5CCFAF13309374DAFF68F4C79DFA,
	LogStructureEyeGaze_get_EyeTrackingProvider_m94D080F7DF046AFD77E75061F8FEA8D902373D41,
	LogStructureEyeGaze_GetHeaderColumns_mC038607A40573425E7DA6BC2D27199BDF6B724D0,
	LogStructureEyeGaze_GetData_m67095F0D44011A0E990603891CF4D7737B3FE70A,
	LogStructureEyeGaze__ctor_m89EAC1BCCBE3C3A8B288FBD6D4F83938D75B9F83,
	UserInputPlayback_Start_m54AB6E8D8253EC21DB6E8524B04A0BBC91BEF7D4,
	UserInputPlayback_ResetCurrentStream_mBD6BAE2CF723C8C32AABD93EF19BE24D7DA2B52D,
	UserInputPlayback_UWP_Load_m2B55367B83F51DF8C569F21506D151C0E80973C0,
	UserInputPlayback_UWP_LoadNewFile_m7796B983149F6B8E3AEBFFC316F60F8395AA65F7,
	UserInputPlayback_UWP_FileExists_mB5CAD0835C681AC363FE9A4B5330CDC6121B8BAB,
	UserInputPlayback_UWP_ReadData_mB31485383089D9766D0B24E497A9B9F37E3E5E35,
	UserInputPlayback_LoadNewFile_m01CB44DDF2B697A82D2EE077DE93C28FFF5F850E,
	UserInputPlayback_TryParseStringToVector3_m666C74C9F35DA534F7E5451BF6DA5B3110CB4C5F,
	UserInputPlayback_ParseStringToFloat_m0050255DE52420E8DE57E2C035EA0C1B691EA9F9,
	UserInputPlayback_Load_m46C9F6D4927F66AD61CB6A0F549F379FE0ED8746,
	UserInputPlayback_LoadInEditor_mF677C262C5F4C51ACEC7C6591E3BDADBF11A2C9C,
	UserInputPlayback_LoadInUWP_m7A8AA7608180B430BE699238462FC3452C81AEEE,
	UserInputPlayback_get_FileName_m8D74BE21E7A22F5B0E1FD238B85BA9B173209F1B,
	UserInputPlayback_set_IsPlaying_m972CCD61E1228F96DEC35B7A32AD4D82F247B439,
	UserInputPlayback_get_IsPlaying_mCCC5F1149ECAFACBEB06A7264006EC59DD4824BA,
	UserInputPlayback_Play_mC59A6B44FFE6CC1F14594D2850AE8CEC9498368B,
	UserInputPlayback_Pause_m0B6D744ACAC9ABA0E6A1E99735B65809D7BC37EC,
	UserInputPlayback_Clear_m5ACE3831BC2D633217DF9B15F40E0F23A07E2ADE,
	UserInputPlayback_SpeedUp_mCC7E61525B0C8E111BE6CB59FD928E54E65BC032,
	UserInputPlayback_SlowDown_m494CD1A1B515238F34835B8D708ECB1BE357E372,
	UserInputPlayback_ShowAllAndFreeze_mFDB6D7F4BADF206989A90E065884A8F59C4314CB,
	UserInputPlayback_ShowAllAndFreeze_m5DC6645984B47D78E3F82E812ADE8AF779C3B845,
	UserInputPlayback_ShowHeatmap_mD9B93CC89FDDA7275FCF3AF3DF0FE24EDA6680B8,
	UserInputPlayback_LoadingStatus_Hide_m757A84635080D2B7A78B6B4174F9E561DD4BE108,
	UserInputPlayback_LoadingStatus_Show_mBBC3FC8557AF8603331E13D732274344377BB043,
	UserInputPlayback_UpdateLoadingStatus_m78A0F2B88C23D5545FE0BFDC8416542F523EBF13,
	UserInputPlayback_Log_m8139F7014EC270F6AB273857867644964D8DE42D,
	UserInputPlayback_PopulateHeatmap_m637AA234900CF056E60B7043CF62C0EA2611101A,
	UserInputPlayback_UpdateStatus_mCDE75363C04014A6F1C1AE6600B5C9011AD381B1,
	UserInputPlayback_AddToCounter_mB9134E39F625B862A75A763532D0931B59F08C04,
	UserInputPlayback_GetEyeRay_m8B68E9E5EBE2F05410292AAF1B6623B36A6013AD,
	UserInputPlayback_GetRay_mFBEF18D743487F92C604E39B22234F78B4CB9C9A,
	UserInputPlayback_get_DataIsLoaded_mE578EA092A33EA8E79DA30578A2E1F189BDFAA07,
	UserInputPlayback_UpdateTimestampForNextReplay_m406E3B154B4AEF5ED5633C84E220D12119BB5161,
	UserInputPlayback_UpdateEyeGazeSignal_mE7C51CDF1A73150A61F1BFBC8FC05001C7B0EA56,
	UserInputPlayback_UpdateHeadGazeSignal_m9B3F6951772CF3D598BBF27C0438813CCF952AFD,
	UserInputPlayback_UpdateTargetingSignal_m9BB1C4774454102B34D25E4FA65C44C4A8675A8E,
	UserInputPlayback_Update_m19FB263ECBBA12D19B548FF3A994B445C35A9048,
	UserInputPlayback_PlayNext_mBAD27CCB948DE35AABF9E38BD6DEF1081A00A9C4,
	UserInputPlayback__ctor_m378AB66FB7D5934D1533F4A8DBB325B2DA3BCA3D,
	UserInputPlayback__cctor_m3C7EBF9D548CEB4CEAFF08D9A24B128BBFEE317F,
	UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_m582F19E7B0B2E5B68B1203A7189A0EF2C6D10117,
	U3CUWP_LoadU3Ed__12_MoveNext_mF50633445CA445C15EB96BCBFEE6B0D0EAE5750B,
	U3CUWP_LoadU3Ed__12_SetStateMachine_m591DA891C7D71B6BE2A2BEF8B43C3085CA166AD0,
	U3CUWP_LoadNewFileU3Ed__13_MoveNext_mB09A0EF138A0B6388ACD97A9551D04E6A72180F2,
	U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m0662CBA1196DF3F7A847D11CC714071670225D45,
	U3CUWP_FileExistsU3Ed__14_MoveNext_m45168326C1D3F26DA0853F2BB2770F5A2175DA0C,
	U3CUWP_FileExistsU3Ed__14_SetStateMachine_m0E7C98FD2BBBB9620A93E4779B89C2437C450B25,
	U3CUWP_ReadDataU3Ed__15_MoveNext_mC1061B1A4D9E4ECF16413DCB85C41EC0C215C381,
	U3CUWP_ReadDataU3Ed__15_SetStateMachine_m254C3E9028A12B79C84A1945BE7B7D5A04D7B183,
	U3CLoadInUWPU3Ed__21_MoveNext_m43043EEB6C1601ECF81ABB1FD6933A79D82F134B,
	U3CLoadInUWPU3Ed__21_SetStateMachine_m7F5529D96FD69AE974D6B8E3417740D88B06C266,
	U3CPopulateHeatmapU3Ed__42__ctor_mD7F558F041990420DF91A544D2EC0E4A12569DF7,
	U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m968703DCCF8F30F6E5AB40CA817BD294700AE49C,
	U3CPopulateHeatmapU3Ed__42_MoveNext_m5A43B1C0A8D1E446A94BB3B299D35C562E9580B2,
	U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEBA75C228FBBCD232FA4C0E03AE58697C7893E34,
	U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m7222AD3D3A78DFF63D494B95D7A11907B60AE9F4,
	U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_m3D1E82CFFE98A8F15C08C42151C37B3270E0FB93,
	U3CUpdateStatusU3Ed__43__ctor_mE161EBBFEF609B0F65CD0CE15945C1D1EC6FC4AF,
	U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_m52E11C9DDCEC6F3629A428A7ADB9CAE81F1470BB,
	U3CUpdateStatusU3Ed__43_MoveNext_m5B4572E9557830322F0748570F651C40F42400FB,
	U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8074B2EB191D1B18FBBB93DD55185C12E4317327,
	U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_m0AD9A16C4686B50EC28E28CFFF3BF5F2D0D3E5A0,
	U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_mAAE632C13CA80355BC2F8C7B0F9E61C892EFFE1D,
	U3CAddToCounterU3Ed__44__ctor_mC4B3BCCCF949C0BAF5F66E437C38954E177E500E,
	U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mD3303648584768F0BE84E38A10E6D2358B72D511,
	U3CAddToCounterU3Ed__44_MoveNext_m453A54E1D482866BDDF7602492A710C1F2D31256,
	U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EC981668509CD31D32AB48230F40F16415C6C34,
	U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_mF63C30885BB8BF2D12A452E0152B216737529A33,
	U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_mE233AE8D42A08801E4AC5CCD7721E66B1C2BCC1C,
	UserInputRecorder_get_Instance_m2AD896F101FD5D1165B98E57394696664DD53833,
	UserInputRecorder_GetHeader_m2B8F52FC9EDDE9B4DF786A0D4BC2461A355F8AD0,
	UserInputRecorder_GetData_Part1_m8F9987DF7BD3BB6F4F60C50A886C2A4456251C26,
	UserInputRecorder_MergeObjArrays_mBDA409BEF5E657E4AFB2478AB03F8F80902859FC,
	UserInputRecorder_GetFileName_m24FCA6F95DE59856189C807AAB833B3A29CABFE1,
	UserInputRecorder_LimitStringLength_mDEF0CC2CF9570097CDB352C5C4817ED8EC0374D3,
	UserInputRecorder_GetStringFormat_m1A3616A837821EECFE95837D2A7EE963CD35A516,
	UserInputRecorder_UpdateLog_m019FA69FB87AA0CB3EF0E911720FC538A1227571,
	UserInputRecorder_CustomAppend_m063047DFEEBB9318F93AE1C92B8FE0AB39F90C80,
	UserInputRecorder_UpdateLog_m2187053D96641A9B676B2B2BFE675494EB6338F9,
	UserInputRecorder_Update_mDCA18C5D8F130482E4EF8E9FE594C9AEF3FCF19E,
	UserInputRecorder_OnDestroy_m6C6D8A14BE0BA04AAF807C6B86FE4CE38CE812E4,
	UserInputRecorder__ctor_m523C9771D6DD426B36AE3502B802F067D412D707,
	UserInputRecorderFeedback_PlayAudio_m855CCDDA5C75FA7234431D3B41DD944CB88826D6,
	UserInputRecorderFeedback_UpdateStatusText_m6D5FFFF743C7FD9A0DB40F1E803DC4A3CDE90D3D,
	UserInputRecorderFeedback_ResetStatusText_mC6A8C2050421A7BA49164F7BFE12B2497CEA39D8,
	UserInputRecorderFeedback_Update_m7B60199FF60C5FAD16C61F02359A9628323D76EE,
	UserInputRecorderFeedback_StartRecording_m29FE7A7D706928AA6283B26EC8951ACE28FCD6C1,
	UserInputRecorderFeedback_StopRecording_m52FF35ADB862C4B98960D9D3B95034E415ACEC7D,
	UserInputRecorderFeedback_LoadData_m8C4ACCC0F62BAD4CD38112AE376BB8B582F97036,
	UserInputRecorderFeedback_StartReplay_m55E941EED25826F76FF0145E94CDBD8D95356876,
	UserInputRecorderFeedback_PauseReplay_m4665C9A3DF1BD59CFEF1C26A9552E71CD4022541,
	UserInputRecorderFeedback__ctor_m7784EA7A5D02EE62EDD8F542CE2E7B1394DED1E0,
	UserInputRecorderUIController_Start_mAAE1DAF1092EF41454A58BB933FE4CABD32F041D,
	UserInputRecorderUIController_StartRecording_m7DBB3F9EF8D6964695E027F4AA22811DD5A7A668,
	UserInputRecorderUIController_StopRecording_mB4B352D3D43090FF21A0D84F0CEF74224243D546,
	UserInputRecorderUIController_RecordingUI_Reset_mBC9DD0DDA89A8E0E413ABE854391D9ADA802C1E3,
	UserInputRecorderUIController_LoadData_m379CC243B823D3A636AEC87FD377DE38FC7E3F8E,
	UserInputRecorderUIController_ReplayUI_SetActive_m21E6B70283000B375DF81F4DD2DC18C594C80496,
	UserInputRecorderUIController_StartReplay_m3132F5241D80529B61E6CE1505AD21BDEC40A590,
	UserInputRecorderUIController_PauseReplay_m8D7242E8CB657CD67D23BBF5EEE61FAD8F274C5C,
	UserInputRecorderUIController_ResetPlayback_mCD89B5E8CEFF741885F7E7590DEF482C53D263A5,
	UserInputRecorderUIController__ctor_m915DA99929AE13AAC143C1DA673BC4F236164B1B,
	TapToPlace_get_AutoStart_m6374D48DC107466BB0FAAB1480ECB1236365E1BF,
	TapToPlace_set_AutoStart_m489E97811C6AAA422BB49DB22E2BC28F5D77F9B9,
	TapToPlace_get_DefaultPlacementDistance_m760304B5AD2203B6BDB8AE81C1AD8104C824B68B,
	TapToPlace_set_DefaultPlacementDistance_m85427D0C199F34B01527D736F154EDE174ECBD29,
	TapToPlace_get_MaxRaycastDistance_m56A592BB8CB3961CFCB8EF4020D9ED1498428A72,
	TapToPlace_set_MaxRaycastDistance_m1DEB527C7ED530D08C18F246FE45EF3D75250570,
	TapToPlace_get_IsBeingPlaced_m9DB86AC090F7A1F2CB11EE1EC10C4A80EA797614,
	TapToPlace_set_IsBeingPlaced_m7991270BCF77F77018715BF41AA87339E250F25F,
	TapToPlace_get_SurfaceNormalOffset_m058966A0B6194ACF7C0CDA0C347D98E35AEC32D5,
	TapToPlace_set_SurfaceNormalOffset_m3040E9A012EE9E635E3E13AA3DE7566AAB9B5DDC,
	TapToPlace_get_KeepOrientationVertical_m8A6B6FE6208E10FA41E732E6EECB843E46ED1B46,
	TapToPlace_set_KeepOrientationVertical_m25AC11588192589566C071B9865D5C55521EA6D0,
	TapToPlace_get_RotateAccordingToSurface_m610A8CC1409498021D3B58FF579391BE847A768D,
	TapToPlace_set_RotateAccordingToSurface_m00231C60960FAE5D7CEAD8C40156ED9A7FB9D841,
	TapToPlace_get_MagneticSurfaces_m24C05D9D7C221A38B7C133F05685F3507BEB5539,
	TapToPlace_set_MagneticSurfaces_m03443E0FBD8716769975C733816F93AA1E5F717B,
	TapToPlace_get_DebugEnabled_m3370364D2A1AA51040B12394F879707588FB56E2,
	TapToPlace_set_DebugEnabled_m5243049BFB7D75138F08D79C03CEC953B8F1B387,
	TapToPlace_get_OnPlacingStarted_mE442436C9D044BEAE8A3905C665AF1B5A4A69A27,
	TapToPlace_set_OnPlacingStarted_mD19936891BC2EE2D83FEAB438C7D37385C1DA639,
	TapToPlace_get_OnPlacingStopped_m9B20DA76E41E89A92582347949AB3EDBEF6F81CF,
	TapToPlace_set_OnPlacingStopped_m9175D2A804141BE4610C8F995D4780B576433188,
	TapToPlace_get_GameObjectLayer_m0C8950ACEA68495F9E599696726F3451A677E666,
	TapToPlace_set_GameObjectLayer_mCBD515CFBBD695DB2C4606CBF73AB655A376BAAC,
	TapToPlace_get_IsColliderPresent_mC141EFA6674C2A135EE71BCA649003DE7217F6D8,
	TapToPlace_Start_mAD0CE835E0DA29F65D7CB175CD8D6F96C947DCD4,
	TapToPlace_OnDisable_mD0731FDCE887BAF862D0E44A24ADD5C3E76F2BEC,
	TapToPlace_StartPlacement_m0639FFE90B9BD36EEE7222E0311FE1EA0371861A,
	TapToPlace_StopPlacement_mC3E17C2C0F11F3979549F5A4975177A2CFAAE8BB,
	TapToPlace_SolverUpdate_mF4388FF02A60E041E48F041B75193C284475924F,
	TapToPlace_PerformRaycast_mFE63B48B6C14EAA721A71F8E5016930B75361112,
	TapToPlace_SetPosition_m1832DD81E1B1C03D489A66B3F180A932C446F1EC,
	TapToPlace_SetRotation_mDC415B841C82DC8E65105EAF7741F15B4D99EF3F,
	TapToPlace_OnPointerDown_mA8A5020D73A119D587B3CE17FF4BD27622397849,
	TapToPlace_OnPointerDragged_m57F0EF0B94CF71A0A476DA2B0E9CB9A87BEEF4BC,
	TapToPlace_OnPointerUp_m5260088AE86E65B710ACA659768879CD4B2DE46E,
	TapToPlace_OnPointerClicked_m1C985F0522359DD0155434C92DC94735678CAF04,
	TapToPlace__ctor_mA0591E8902EA48A093E28ACD5C8CDA4322FD6B6A,
	DemoSceneUnderstandingController_Start_m81FDB4ECD9DEADE2E0FC9897B69E7CC2B448612C,
	DemoSceneUnderstandingController_OnEnable_m14DC558B9F0BE230E0073833B1FCC838268AC695,
	DemoSceneUnderstandingController_OnDisable_m8BE11CF03CC02E7558C89CE70D243BD7F13FB7EB,
	DemoSceneUnderstandingController_OnDestroy_m83101B507334B0F9A6C55039D8295E4230E897B4,
	DemoSceneUnderstandingController_OnObservationAdded_mFB2D1F9863A3452BBD366DA97E96FA2C6BE7A376,
	DemoSceneUnderstandingController_OnObservationUpdated_m80D1C5B6342BCE4E18D5D1D5A7FA3948BDD2D773,
	DemoSceneUnderstandingController_OnObservationRemoved_mD1253B1190BEB10F3C5145E37C03119494888B47,
	DemoSceneUnderstandingController_GetSceneObjectsOfType_m3630FDF67DBB49AA6E5413E41797E2BEA418FEA7,
	DemoSceneUnderstandingController_UpdateScene_m7E7A36484F24DDB8B5C2BFC3D9509C2A0E6A38E8,
	DemoSceneUnderstandingController_SaveScene_m4C006E2A5AEDC23A35389220B26D4BEDA95E0171,
	DemoSceneUnderstandingController_ClearScene_mC54D363F795FF530800BA1FCC9567D8EFA7E5F39,
	DemoSceneUnderstandingController_ToggleAutoUpdate_m0F3331D3ED4D877783FBF814ABE6E708E01FAB9F,
	DemoSceneUnderstandingController_ToggleOcclusionMask_mC667E7FBF9697AB95CA2A6026CBA48EC985F3E05,
	DemoSceneUnderstandingController_ToggleGeneratePlanes_m2150D76D96C4D7F7CE63DC1445647D96ED74EE41,
	DemoSceneUnderstandingController_ToggleGenerateMeshes_m4FAAA3BBD333AF28936E2DB67F2DC6826201B6A7,
	DemoSceneUnderstandingController_ToggleFloors_mE5352EF8BD112A46B8B30F1F70481DDD9F9533D9,
	DemoSceneUnderstandingController_ToggleWalls_m61FD7A872F156D121F0E10B53C48C06AA409CCE8,
	DemoSceneUnderstandingController_ToggleCeilings_mA9EDD05F95228284EEDC3AB3DD5BAFA7AE334EF0,
	DemoSceneUnderstandingController_TogglePlatforms_mDC7989260ED46FE37A03D33D966A0C4E0050382D,
	DemoSceneUnderstandingController_ToggleInferRegions_m2BA28C4F24F87653E2C18FFE0F93F55A0D214F7B,
	DemoSceneUnderstandingController_ToggleWorld_m3A285FA70CCDA72B1FE62D77A0009D365FE36749,
	DemoSceneUnderstandingController_ToggleBackground_m1518A97736F9E28F43D9E0214EC0CF7E157597B5,
	DemoSceneUnderstandingController_ToggleCompletelyInferred_m2EAD63DCCA93B6C27D2AAF4F84DDCA7484C6E22D,
	DemoSceneUnderstandingController_InitToggleButtonState_m9D71A4D589CB4355266C744108757E6ADAAD5E00,
	DemoSceneUnderstandingController_ColorForSurfaceType_m80B7E4172D409DBB702726C8CD00800D72720EA1,
	DemoSceneUnderstandingController_ClearAndUpdateObserver_m917617CA6286CDD85F6DA7BF7F6302D8904ADAD6,
	DemoSceneUnderstandingController_ToggleObservedSurfaceType_mAF3CB636F2F664BC777F0B00FC36DF35255D4CCA,
	DemoSceneUnderstandingController__ctor_m7545F95E952FA57B2471CEAF16B2FD73FE6356C4,
	JoystickSliders_Start_m8969386B0FDCD0C0DCC536AD5949623F29D5E642,
	JoystickSliders_CalculateValues_m4AFB14141CDF0BDA48F2EF76343FBDB4A4F6CE64,
	JoystickSliders_UpdateSliderValues_m3138DC151B9D426A294DB62C5C09DD1A57A02A63,
	JoystickSliders__ctor_m8A84CA57812CB0EF3AC3906C51EF4C1AAFD82F86,
	HideOnDevice_Start_m542FED820F24D18D743A5484A82AF9D550586DEC,
	HideOnDevice__ctor_mFE24A0B9BA58C70A3CA5102222781A5FCA2F7169,
	MixedRealityKeyboard_get_Visible_mD6D5981F52A35FDEEE326E55F11DAA1DF70F0661,
	MixedRealityKeyboard_get_Text_mA9138CAF80EE85CC1D1FC71303896CAD356C5B28,
	MixedRealityKeyboard_set_Text_m568E4AF62B73D6CD049B702337B8D025AEA6AC69,
	MixedRealityKeyboard_get_CaretIndex_m00F252F2BB6AE73648A82EBC90A16D41FA7002CF,
	MixedRealityKeyboard_set_CaretIndex_mD68B44C9DAA9C7E5EDC293934393016EA7A710C8,
	MixedRealityKeyboard_get_OnShowKeyboard_mC560D247AEB95AB8EBCA90F7C04987D736C162AF,
	MixedRealityKeyboard_set_OnShowKeyboard_m97EC0A3FA11E1E2DA42D11427406D61AF6B07F5A,
	MixedRealityKeyboard_get_OnCommitText_m9706EC7A8CE10D3BA5C7E7A9F643976D191973CB,
	MixedRealityKeyboard_set_OnCommitText_m54FDECFC19733A7EC87B59EBBA689687712154D6,
	MixedRealityKeyboard_get_OnHideKeyboard_mFD4F7F5504734655D2A7A1F32101C4E3AA9AC923,
	MixedRealityKeyboard_set_OnHideKeyboard_m45A28986C9B7145C788A55631710224FDDEDD88B,
	MixedRealityKeyboard_get_State_mD009D2CA7777921952ED86F627F7A749ADE23A63,
	MixedRealityKeyboard_set_State_mA543947C28F00EC13277B59034A9E785928FC109,
	MixedRealityKeyboard_Start_m88FD2A265450BF27E747EEE54E2A8FE0141ECE20,
	MixedRealityKeyboard_Update_m41BF0D668900C471CA2DCE24FF2AE856BAA3E4CD,
	MixedRealityKeyboard_ShowKeyboard_m0B6057E5146F139F388668BF36985C30BB6FF23B,
	MixedRealityKeyboard_HideKeyboard_m520EC46393E05E1F7944A4E3EA772E5085F218E0,
	MixedRealityKeyboard_ClearKeyboardText_mBB6A2AADFB8E4EAF94EB1346DE2F98CBC7A94140,
	MixedRealityKeyboard_UpdateText_m99E10E97C2229FC8C6EE1CABED5DAD3D6D141D5B,
	MixedRealityKeyboard_IsPreviewCaretAtEnd_m3C97CEBFAD0776FA3E9B917135C277D0988553B6,
	MixedRealityKeyboard_MovePreviewCaretToEnd_mF3DF68995332558504103EE1748D9772BFAFD6A0,
	MixedRealityKeyboard_OnKeyboardHiding_m6566D649C21604B8D0E4198A9714F9E14E090EDD,
	MixedRealityKeyboard_OnKeyboardShowing_m22EBFA6BE5AC53D0BA67B13FB11DE1849707433A,
	MixedRealityKeyboard__ctor_m69B4A1D0E77CEDE81D5ED44AD7085D93D33E3DA9,
	MixedRealityKeyboard_U3CStartU3Eb__30_0_m02624AAD7D376D7E696607910BEF2426546988C2,
	MixedRealityKeyboard_U3CStartU3Eb__30_1_m14A785D4B43E4DFB192D5C1BC7E32A95FE45C58D,
	MixedRealityKeyboard_U3CStartU3Eb__30_2_m09BFFD7D8210B409C5DA6CD1294549C48B908681,
	MixedRealityKeyboard_U3CShowKeyboardU3Eb__32_0_m5F497EB9FFD36998D8FB21F8F2C111441F772DE8,
	MixedRealityKeyboard_U3CHideKeyboardU3Eb__33_0_m70561532B63A742608CD100C6B5CAA9C399A81F8,
	KeyboardTest_OnPointerDown_m53BF9AE3E12D88F545CA0EE40BC901D42333538E,
	KeyboardTest_UpdateText_m3B7597ED54941BC90F04A2415030C27AABE54C38,
	KeyboardTest_DisableKeyboard_mE36CE6548B2BE770D02E35525E38C5EB286E4188,
	KeyboardTest__ctor_m0BC979C0C7D1A61D003EEA44E4361BEC9F684106,
	JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4,
	JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807,
	JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7,
	JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D,
	JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07,
	JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1,
	JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA,
	JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269,
	JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2,
	JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47,
	JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053,
	JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA,
	JSONNode_get_Childs_m62FABDFCC2A9EE8029721B2B42B55DBF17FDA3A4,
	JSONNode_get_DeepChilds_mD9ACDC378594A79ED2F10B1816BF27077004B25B,
	JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98,
	JSONNode_ToString_m7FCA0DFCAB4289D983E8FB21216993980AF4CCB7,
	JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA,
	JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75,
	JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB,
	JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1,
	JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656,
	JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5,
	JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D,
	JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F,
	JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829,
	JSONNode_get_AsObject_m965C37E386EEF32FF6EE5C59DE5743F11D58B9E8,
	JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2,
	JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668,
	JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F,
	JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE,
	JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF,
	JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834,
	JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68,
	JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076,
	JSONNode_Serialize_m4E6645099B140A079610B4F644E4A9E978778CAE,
	JSONNode_SaveToStream_m5F95DFA988259F086D8C657151B8FCB5EBB4AEF7,
	JSONNode_SaveToCompressedStream_m939EB2E9267698F54AD64849D96E4ADFCCF5A00A,
	JSONNode_SaveToCompressedFile_m66A1234959DDCEE44D9000255F86A8FD32EC87BC,
	JSONNode_SaveToCompressedBase64_mF1BB008CE575D288A986676FD23806E62C98D539,
	JSONNode_SaveToFile_m7B15985908548F36FEB6654CFBDA7D5E1ABB1839,
	JSONNode_SaveToBase64_m55A648ED569B7E22511D0E71DA5A1348E1E908DC,
	JSONNode_Deserialize_m38C70CC7D80AEDBD4ADF9618BBC161224C3F45E1,
	JSONNode_LoadFromCompressedFile_m50BB9E1D951C7444EFE1FE570894E5A92BA35275,
	JSONNode_LoadFromCompressedStream_m6D59AB50712DF46BE7D37DB5EDB8EDD7C0B6D04D,
	JSONNode_LoadFromCompressedBase64_mBC74C1DE9C65EF47BA738413E2140F1370A9F433,
	JSONNode_LoadFromStream_m490074C19DC1E5CFA73842C6839BED6DA62F12BE,
	JSONNode_LoadFromFile_m7FC0A7565712C07EF7B7954586381E50A193B9B9,
	JSONNode_LoadFromBase64_mF1CFF8D5D2484CC31D32A39A8F88C95E46AD8644,
	JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E,
	U3Cget_ChildsU3Ed__17__ctor_mD2F92AF443A79C6319D6675D9B8EB54C3F69AF70,
	U3Cget_ChildsU3Ed__17_System_IDisposable_Dispose_m1843C95CC9C3F00F24A341C006AF9481FB408A93,
	U3Cget_ChildsU3Ed__17_MoveNext_m62DB18AFF29AF270D32CB504C0677A61E0E56369,
	U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m446EABB222302FCA20A65E6FC3EDE4CA67F00309,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_Reset_m20DEC2764F0917653F7DBE54394410D0CCF75538,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerator_get_Current_mE68F3F2E789E20E3919A83F45CADD50C4BC63D72,
	U3Cget_ChildsU3Ed__17_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF6FF6762A1746B8536A34B80AB04F7FB4574226F,
	U3Cget_ChildsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mEBCE9E490A3EF4D9FD87A5E4724B9A0666DC818B,
	U3Cget_DeepChildsU3Ed__19__ctor_mBEE3D8BB361728473C66F7871C19838BDE8E6EE2,
	U3Cget_DeepChildsU3Ed__19_System_IDisposable_Dispose_mC6F8AFB4A4716135BE8A9252CE2B620E7F3CA0DD,
	U3Cget_DeepChildsU3Ed__19_MoveNext_mE1419F074852413368A8E1D3DD8C68A76F7F6814,
	U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally1_m3C13EB580FB1D6EB0A781B9785EAD11BFFDCD4C0,
	U3Cget_DeepChildsU3Ed__19_U3CU3Em__Finally2_m913DD85500791510F835734C52BF1FCE6E70D365,
	U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6055AD945E93ADFCFD74E87F8B1B348038A3B65A,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_Reset_mE4AA0EB22642B477A594C4B2814DCCBD19470621,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerator_get_Current_mD8FB3E7E3C35922C4DCA32785C2F6CB2432BFBB8,
	U3Cget_DeepChildsU3Ed__19_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m03E96284C0131C15D5289DE63DE3BBE2652AF42A,
	U3Cget_DeepChildsU3Ed__19_System_Collections_IEnumerable_GetEnumerator_m5789A070C8140458EBC69890EF2E314B803EA170,
	JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC,
	JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF,
	JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B,
	JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE,
	JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B,
	JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685,
	JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9,
	JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E,
	JSONArray_get_Childs_mAC9E9800F9F99F137D2032D7B9B00A2E8E4FB323,
	JSONArray_GetEnumerator_m6C84A7480387B68D91988514017653738819E7B2,
	JSONArray_ToString_mA4E44724197E6EBDED42F5648AEBEE6DAF95DA2E,
	JSONArray_ToString_m96E4DCAF1E5B0C88D6B73CC58488B47405D2F9C5,
	JSONArray_Serialize_m742EF25F05EB47F6DF6910A21044C6DE62FFDD03,
	JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512,
	U3Cget_ChildsU3Ed__13__ctor_m21A583D968F53AE41E2FA06917ABAC9D9699473E,
	U3Cget_ChildsU3Ed__13_System_IDisposable_Dispose_m5A1086E94D62110F2271E60FB85840059984A7DA,
	U3Cget_ChildsU3Ed__13_MoveNext_mF0AD3C64CE7A11C3EFEA0D40EBD8F1A3095AA4B6,
	U3Cget_ChildsU3Ed__13_U3CU3Em__Finally1_m143EA669B23A1936BC3B1E1701A20A58B09B860F,
	U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD4786DBC563CD88FDF86558BD112197AD43674FC,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_Reset_m8A61E7D4389FF479518A7AA2379FB8D8514B9EA2,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerator_get_Current_m82EF5BD3F486BBF4F56C7D6AB3268AB3F3783DFC,
	U3Cget_ChildsU3Ed__13_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m9AC9E6F2235ACB8E42AA6B2928B5BDE3B999B3AB,
	U3Cget_ChildsU3Ed__13_System_Collections_IEnumerable_GetEnumerator_mB16BA5F771503FAA010363CC7F565CDA477FC056,
	U3CGetEnumeratorU3Ed__14__ctor_m9443943452B220F583808D225CB05DBC67AE4EAB,
	U3CGetEnumeratorU3Ed__14_System_IDisposable_Dispose_m4BD1094EA16FCAFCAE7FBA46A90E495346239B2E,
	U3CGetEnumeratorU3Ed__14_MoveNext_mB002FEFA4F25E85F7ABBAACE45DE15123B43E647,
	U3CGetEnumeratorU3Ed__14_U3CU3Em__Finally1_m7A8C62B8E3949D722715DCDF5B35CE140219DA89,
	U3CGetEnumeratorU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1824EC362F1755F5DF4066F6A9E481F50253905,
	U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_Reset_m96F051F06E4D2972A326277E2A0B8902F0C4DDB9,
	U3CGetEnumeratorU3Ed__14_System_Collections_IEnumerator_get_Current_mEE886034A8F9C19F494478CA1E9159ABEB428F4D,
	JSONClass_get_Item_mEBC27C74AB8C3DEEB9E243952B0F1EA2C25528D7,
	JSONClass_set_Item_m7D109D31520EF4D4CB27B22778883393CDCBA33F,
	JSONClass_get_Item_mDF11AC8ACFE3A539B55000FC2A6671ACEB5B15C5,
	JSONClass_set_Item_mDFCA44AC3C44FDE1E776D4A295431C72022208C3,
	JSONClass_get_Count_m4B5121347CACB0517E4E461729AC144764FBCE06,
	JSONClass_Add_m5C0424C984499D48E81F00373DDBAA5CC295155B,
	JSONClass_Remove_m34FAC69B637A22128801233A3A8F466A0B944D40,
	JSONClass_Remove_mABE15123ABACCEFC0B331A16E8DC8B0B079E2B73,
	JSONClass_Remove_m87288080284E5E6D25E27CD27848F1548A55C508,
	JSONClass_get_Childs_m9CCCF5791C458910932A682DF0F62F6F903B4F50,
	JSONClass_GetEnumerator_mD397398C72B8A02F9D264B04C1FA11EFE3C0CF3A,
	JSONClass_ToString_mC05EC1CDA411BD5CCD42E7A5D64C28E122310D66,
	JSONClass_ToString_mB04CDE6B692750B230DB29719FDC8AFC438BE2CA,
	JSONClass_Serialize_m96D53FC580DE031FDE82FDC6AE1CC0D0B510C321,
	JSONClass__ctor_mF10F4D8793CFDCC66146835E35FE21CDD64D9F6C,
	U3CU3Ec__DisplayClass12_0__ctor_mBB42DF8E5C129D67D2491C3BBE5E7A470C1AB1DA,
	U3CU3Ec__DisplayClass12_0_U3CRemoveU3Eb__0_mE7239193384B5B8AA901FB2F2CFE85EE0CD19CBB,
	U3Cget_ChildsU3Ed__14__ctor_mEE1C01E6EFDA9B4635A56660AB0D9DC7750EDF05,
	U3Cget_ChildsU3Ed__14_System_IDisposable_Dispose_mFFB990A9BF4C4F78F537B8691277BD8826348E19,
	U3Cget_ChildsU3Ed__14_MoveNext_m5E739C9FFF695DE7F830A21461F073C4FA5A4C5B,
	U3Cget_ChildsU3Ed__14_U3CU3Em__Finally1_m037FA09FF10B7EDDD6C2292ED217B611E84F6F5E,
	U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mDDA801467B2864ABB763B6B4BA9489BF107AB19E,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_Reset_m739FFFB50ED6101ABF28724E67A4D6F071FE05C9,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerator_get_Current_m6FD0B87A669091FD7BB63A979D83D25D74E0D184,
	U3Cget_ChildsU3Ed__14_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m551589F37908CD3DAA752FFBBC9D1D891D584B86,
	U3Cget_ChildsU3Ed__14_System_Collections_IEnumerable_GetEnumerator_mF36855B379BF93F36912B6C343FE284FAF727C82,
	U3CGetEnumeratorU3Ed__15__ctor_mE0CCB9ECD70B08B0A19C7830D6FEF1543E520CB2,
	U3CGetEnumeratorU3Ed__15_System_IDisposable_Dispose_mFF622866B648141D10BB5F74E2EFBA282160137B,
	U3CGetEnumeratorU3Ed__15_MoveNext_mAEC7795B6B36542CA98FE18D92023D6570D48AD2,
	U3CGetEnumeratorU3Ed__15_U3CU3Em__Finally1_mDBE6650843355BCA2E73313BDF63D6595BE605F2,
	U3CGetEnumeratorU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE70F34764406D091477ED9A573997292C7D369EE,
	U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_Reset_m1CA435191A682FEA49B5346AA45C1ABCC1382106,
	U3CGetEnumeratorU3Ed__15_System_Collections_IEnumerator_get_Current_m85A5819512967EE2DE4F97D246A0FE378C2A548B,
	JSONData_get_Value_mAC8BB77AAE3FD3BFD05E4E29A9D99D8FDB649792,
	JSONData_set_Value_m172D1A924C9966EEBDFDFF95469EEB8B6C1BF9E0,
	JSONData__ctor_mE61EABC6FDB97F0C999D3257665849919906F382,
	JSONData__ctor_m915D92C57E1F9C91FBD62B77E31DB3BF048B889E,
	JSONData__ctor_m9977313E9F1EFC6A6E96CE43506377B977581A15,
	JSONData__ctor_mE49E656122CABAF2CCA85252136E96F05BEAB45F,
	JSONData__ctor_m008B0A7698BD04861CC161F4817A150F22A74966,
	JSONData_ToString_mC94A08B0C73C360F61052DC128D96D83FC0E29EC,
	JSONData_ToString_m0DB9AAC62F1FE9D6CF1405B7FD56A569E984C7B6,
	JSONData_Serialize_mD22C6A789FBB58221C9A971132827F79D8899F1A,
	JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE,
	JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A,
	JSONLazyCreator_Set_m9C40ED5996DFD3D796BFAFBC25EEAB76F29F871F,
	JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446,
	JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E,
	JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753,
	JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF,
	JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B,
	JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37,
	JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8,
	JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA,
	JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9,
	JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF,
	JSONLazyCreator_ToString_mAB3B9D4B94FB78CC15E75A69F65D80FA6DA8B080,
	JSONLazyCreator_ToString_m1432B2846BF58B6DA6ABD2F63C766375AC3D0A95,
	JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012,
	JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587,
	JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4,
	JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956,
	JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545,
	JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2,
	JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00,
	JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094,
	JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6,
	JSONLazyCreator_get_AsObject_mFD6886C4ED273AA71AB620C63AFB22DF449646DD,
	JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421,
	DirectionalIndicatorController_OnBecameInvisible_m0E4A338FC4C70E296D1B8286AA2FD9E833D947C2,
	DirectionalIndicatorController__ctor_mD5EA1948C4529FAFD1E496942ADAFBEC7E4A8018,
	ExplodeViewController_set_IsPunEnabled_mF24170F7F9A608F7414FFF2ABF39EE706BC434F5,
	ExplodeViewController_Start_mE07825796104C61B74ED0AD2D5A5CC38E0BAAE6D,
	ExplodeViewController_Update_mC1FADA194292F3EBAE4E56B1AE64EB407E87F2F8,
	ExplodeViewController_ToggleExplodedView_m74189DE87897AB5232FA72B72753903F85952972,
	ExplodeViewController_Toggle_m321B6176A1D390111076B1919CC2E44D6DB7E5E2,
	ExplodeViewController_add_OnToggleExplodedView_mA25D0A8E94652AC021B35E5C2591EFB4A65EFAE0,
	ExplodeViewController_remove_OnToggleExplodedView_mF561824B96EAA8FD7770BEA6265349C3A65B8ECA,
	ExplodeViewController__ctor_mE72637A87DA07F869CC7C9FD74591AE0C8F60176,
	ExplodeViewControllerDelegate__ctor_m6DFCEE099C96781241186B1537B313641CC3A8AB,
	ExplodeViewControllerDelegate_Invoke_mEC016D4928ADCA613702B28C1E3378F7EC29B58C,
	ExplodeViewControllerDelegate_BeginInvoke_m458C786F225088793798F9A79882A606822BD0B9,
	ExplodeViewControllerDelegate_EndInvoke_m1190D2F43012408A716B9FC790DEB913A85D9476,
	PartAssemblyController_set_IsPunEnabled_m87E32D7952387591F37EC578D7850CCF1CCE4D55,
	PartAssemblyController_Start_m76EEAA396C555EFAC8E6EA60B32B1E7380570CE7,
	PartAssemblyController_SetPlacement_mB6BDE1A16F74DF3B4950483F7C58E3201B0A0851,
	PartAssemblyController_Set_m77CC9BC1F649F4F08C89FD060E2C74C4F45C91FD,
	PartAssemblyController_ResetPlacement_m7FA91500780C316EBF560CECD7078E559460ACD0,
	PartAssemblyController_Reset_m129940529723E9498D4E5035FB917610DA5C8981,
	PartAssemblyController_CheckPlacement_m058EC3FBCB6313D9C85FF551A4831601805F5A4D,
	PartAssemblyController_add_OnResetPlacement_m1B5F77146056299BFF14210549A8A71139406319,
	PartAssemblyController_remove_OnResetPlacement_mDDD37DC93CE99FCE605DFCB9CBC37413364E0D03,
	PartAssemblyController_add_OnSetPlacement_m5B9ED392DAD16C5B23E31DDEF213CFC470C8D1B6,
	PartAssemblyController_remove_OnSetPlacement_mA87246A60289C2E7B03E07D5248B7FFE9E533E3B,
	PartAssemblyController__ctor_m43D020771B20D09433D17616C90F0D19569F9D9E,
	PartAssemblyControllerDelegate__ctor_m1B455F877BBF4D8B34BAA0778ABFA1B0C8FB3C87,
	PartAssemblyControllerDelegate_Invoke_m35EC047435BEF3AC833FE7F6FDED12C1D2FAF2F9,
	PartAssemblyControllerDelegate_BeginInvoke_mE2B0F86102E0D5E6BCD5ADDF6FE25CC9FC8A06D2,
	PartAssemblyControllerDelegate_EndInvoke_m3BC4B8B7B165CE27C98FCB26D91E4E7004F5DA92,
	U3CCheckPlacementU3Ed__25__ctor_m1AE9DAAD6457E2BA3C8AF018FC38B348AEEAFD35,
	U3CCheckPlacementU3Ed__25_System_IDisposable_Dispose_m5F0CAD7B8EEB902CEC60BA6283E5D2176899ECD0,
	U3CCheckPlacementU3Ed__25_MoveNext_mDF7F144AFC4007613EFED9BAC836382AE491AD0A,
	U3CCheckPlacementU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2770AE4D2AC80980A1982783D11A2A481D1F5AD,
	U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_Reset_m3B06BF13F7C5A5FD0544E0A77EF07BF84BEB6237,
	U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_get_Current_mF23A2B08C5CAD169D0663319DCDF005B7BF30371,
	PlacementHintsController_set_IsPunEnabled_m3F5768D83A150962864A1DF4E4A041A75E006B00,
	PlacementHintsController_Start_mA284074B6E1D6E5C2EFEE25ED73B92718FD44DA5,
	PlacementHintsController_TogglePlacementHints_mC6E69154176B5D87ECC700CC21DDF3AE3B79F0A9,
	PlacementHintsController_Toggle_mED247D0E903D8499BE48BC9C0DF2EC3B94F3494B,
	PlacementHintsController_add_OnTogglePlacementHints_m74DE84CC6DD60CCD720BFB6797EFA9C145A040D8,
	PlacementHintsController_remove_OnTogglePlacementHints_m4DD9666380C389D70F6B9B9DA474147403E890E9,
	PlacementHintsController__ctor_m63F5AE600226FB6BEACE406120A542B3EB796634,
	PlacementHintsControllerDelegate__ctor_mC6033B0A83C28817C35AC67DB8440C7B75CA605B,
	PlacementHintsControllerDelegate_Invoke_mD8415531C38108E1B7B41AB3C5BE9ED938DE5BAA,
	PlacementHintsControllerDelegate_BeginInvoke_m729BAD5B777CAC07017B397546C8485D026C1672,
	PlacementHintsControllerDelegate_EndInvoke_m5245DC13168B9F203E7495622C99EA9C6EC60327,
	AppDispatcher_Update_m6EB441148A11804FEE08FF2E9B7D0D97C23B0409,
	AppDispatcher_Enqueue_m3CBD02504B9964AE4BBED84ADDD4DBA64555F77A,
	AppDispatcher_Enqueue_mCCC7B27F1F2D58D6C1E3FD4EC258CC67E1739BE3,
	AppDispatcher_ActionWrapper_m4F07C992CCAA2F5E85EFCA658F6954A49EF9D392,
	AppDispatcher_Exists_mFCAB00D8CE87E7A155674A5D7607D3A663667982,
	AppDispatcher_Instance_m2605059D86F44BA363758BB58264D5228C37C94F,
	AppDispatcher_Awake_m33258451B9D16610A07D299ED217C0E85DCD2450,
	AppDispatcher_OnDestroy_m8D2A3B638D86CD0CE547AFFA18D62179F843626B,
	AppDispatcher__ctor_m9C230BDD0C6DA61407D42CCE1B9D830D2E8426AB,
	AppDispatcher__cctor_mFCF1773E629920DF4D61207261E197735BB7389F,
	U3CU3Ec__DisplayClass2_0__ctor_mA06B35493C03997F256101C531B66D289EF0C58F,
	U3CU3Ec__DisplayClass2_0_U3CEnqueueU3Eb__0_mB306F90CBC4E2283AA78B4F3E5ABB0E177642F26,
	U3CActionWrapperU3Ed__4__ctor_mB7FCD240C73B2B7CADC904435C5BDFCC63B00DC7,
	U3CActionWrapperU3Ed__4_System_IDisposable_Dispose_m4F1D84162CE4C09B1D6834FC877034CD091DC9A9,
	U3CActionWrapperU3Ed__4_MoveNext_m8987804010E6C23DC0101B6DA8BFFD38C19F2B0D,
	U3CActionWrapperU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4768E2C8ADE26C7330AB4AD83E442682D821F0E4,
	U3CActionWrapperU3Ed__4_System_Collections_IEnumerator_Reset_m2689F9D4A85AF7E1CD260C36BCF1360C1F8A50EE,
	U3CActionWrapperU3Ed__4_System_Collections_IEnumerator_get_Current_m50B12E0432836056134AA66E29CD38FF1C1685BF,
	DebugBlobWriter_Start_m5C3BDEAE444EB5FD0862CC8C5C3FC082222A4AE2,
	DebugBlobWriter_OnDestroy_m3EC9CEE83C2ECD524C1DB136EB5B2AC544D18E20,
	DebugBlobWriter_CheckLogsToWriteCoroutine_mECF6232D266CE0B602E693A7549F84D006DB200B,
	DebugBlobWriter_WriteMessages_mC02C6FF3EEC31F562D34F44201F0DD38B4FD58FB,
	DebugBlobWriter_HandleOnlogMessageReceived_m7DE683EC355E247810F1EC59246596C9133F64C8,
	DebugBlobWriter__ctor_m06F2D9F8EF3278102366F8D9614CAA834289F1CC,
	U3CStartU3Ed__9_MoveNext_m451334A42B884B78FAE2BBDCA8FB79FAD4BD20DF,
	U3CStartU3Ed__9_SetStateMachine_m68B53E75DE39D39B25594881F84D029E91ACE7A6,
	U3CCheckLogsToWriteCoroutineU3Ed__11__ctor_m95868D81C11F87AABEDE672BD231469773CC50DE,
	U3CCheckLogsToWriteCoroutineU3Ed__11_System_IDisposable_Dispose_m9C6BA71941DC0E7D798E4B277749367A67AD9125,
	U3CCheckLogsToWriteCoroutineU3Ed__11_MoveNext_m3DF048596AC130C0993D24181037A6F67103E133,
	U3CCheckLogsToWriteCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C30FF6D988EB7857B8D0A650D20DD10DC755E9C,
	U3CCheckLogsToWriteCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_mA6811064D23D668FBBE6DE870C8A47C0888EB96C,
	U3CCheckLogsToWriteCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_mBD72CF86AAC5FBFDC2EF67F68DD096B6A2714876,
	U3CWriteMessagesU3Ed__12_MoveNext_m1B7731FD9248E3A64D59366816A9963C272128DF,
	U3CWriteMessagesU3Ed__12_SetStateMachine_m10ED05D0745B0BC452EC0BE04732A4E33BF8326F,
	DebugWindow_Start_mDE099B975E648B514D55DB1FEC5922533792A62C,
	DebugWindow_OnDestroy_m662F00C4DFFCB120484FD26F4CB0749FB4091024,
	DebugWindow_HandleLog_mDE33752AD625873DC003D563C214FF78E53CA1D3,
	DebugWindow__ctor_mDF2D0A3D9E3997170F50E0FC60A592BA72B31DE8,
	Utilities_CreateSprite_mCF463CD38D7B210C1EA2CABBAA780DE897792EC7,
	AnchorArrowGuide_OnBecameInvisible_mDE4C319B2F7667FFBB9FA5E6C4D77A6A4840E671,
	AnchorArrowGuide_SetTargetObject_m740A733E4CA54111AECE2C3AB27D46C2666C76D2,
	AnchorArrowGuide__ctor_mF31FF56153D90A165427D3F988FAC15DE6BA4073,
	AnchorCreationController_Start_mC9BF4F147BC3D9AB360123F29881D05759135822,
	AnchorCreationController_StartProgressIndicatorSession_m02296F0E95CA2166283AD647202B337BA4366E8D,
	AnchorCreationController__ctor_m91D6321A36EF2B41F0FF28CE4919FC7FDFBF6CF5,
	AnchorCreationController_U3CStartU3Eb__5_0_mD6D058011EE332CAB234D89F8EFFF2BCA04A31FF,
	AnchorCreationController_U3CStartU3Eb__5_1_mF34BECAAEABCDC10007C049A8CB6F3ED9E1D1047,
	U3CStartProgressIndicatorSessionU3Ed__6_MoveNext_m5C3F80324352E494DC30D01C0BEC24A613F04C67,
	U3CStartProgressIndicatorSessionU3Ed__6_SetStateMachine_m78BAD933A123E38DCD4EDF9744CCDFD87D9ACB06,
	AnchorPosition_get_TrackedObject_m24B438355A66245B1D8197C1100E655245D17F73,
	AnchorPosition_Init_mD9BCDBBCBAF2A46DBB321CADAA2A80DB15C4BC28,
	AnchorPosition_DelayedInitCoroutine_mF8F9A2B59B484CC8CA78EF8C71F0CC06C546B2B2,
	AnchorPosition__ctor_m759F414830E1EC1E3248749EE788B4A099EFF8CA,
	U3CDelayedInitCoroutineU3Ed__7__ctor_mA1FE2BB72C3531187721EDE8E70D898B348B433C,
	U3CDelayedInitCoroutineU3Ed__7_System_IDisposable_Dispose_m59715A98082003E1609C01F2968C7753B48A527F,
	U3CDelayedInitCoroutineU3Ed__7_MoveNext_mFD73261168B4B39115B649D5A1132B0ECBAEA31B,
	U3CDelayedInitCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A2D0C523A7367E1271143FF534A3A4B895DBCB3,
	U3CDelayedInitCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m337B3E0A57C88CF66E48C6F72345C88A1A32C317,
	U3CDelayedInitCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mA641DCEAEEBDF60716251380D04C4F7854070474,
	InteractableToggleRadialViewStateUpdater_Awake_m0824B626D36D377AF455004269A10550C62E5402,
	InteractableToggleRadialViewStateUpdater_OnEnable_m23A4C83ADA71A7F752E0675D985155547BAC3C3A,
	InteractableToggleRadialViewStateUpdater__ctor_mA3EDC1B2EC4E8CB86813E392AB8D807E52458BA2,
	AnchorManager_add_OnCreateAnchorSucceeded_mEDCCF4D5E3E9D70869AADE2CB3F86172D7445325,
	AnchorManager_remove_OnCreateAnchorSucceeded_mF8BB49ACAB795E4F48ED321B09E79BFD42F65618,
	AnchorManager_add_OnCreateAnchorFailed_mB5AE1ACE26A3662017D539C636923DAA847D586E,
	AnchorManager_remove_OnCreateAnchorFailed_mC562E27DB28CD5DC4579AEA6BCE918A256AB04F9,
	AnchorManager_add_OnFindAnchorSucceeded_mDDDA48475F0061CA5FB72343EEA920485989785A,
	AnchorManager_remove_OnFindAnchorSucceeded_mC01468A1E13B33DC3185BE0B2CAC068514BFCDD3,
	AnchorManager_add_OnPlaceAnchorCanceled_m5B3667536C9A7B603D72CBEE018B449E79F1CC02,
	AnchorManager_remove_OnPlaceAnchorCanceled_m4439B883FF868EEE7A33C867463E0A5B8B6199EA,
	AnchorManager_Start_mD7694DF37253C3F1F0E4D0B7C7D32ABD42F072D8,
	AnchorManager_CheckIsAnchorActiveForTrackedObject_m00ADE86A6DC4FFDD519A9AF3CA8B4F495A8DF25A,
	AnchorManager_GuideToAnchor_m88963F8C80BA51DA18CAEEB0931972C920A007B0,
	AnchorManager_StartPlacingAnchor_m7C3DE1C794074B2C68FA63BD8E5FBFB626A9D6BC,
	AnchorManager_CreateAnchor_m091A9B78381E45970CC7D8E215D494B4ADBE40AE,
	AnchorManager_FindAnchor_m8028B9243EDC01A06E346717BC75234A557DC946,
	AnchorManager_CreateAsaAnchorEditor_mFADFD0B9F63E856C9F0EC18DF71B858C445FB7D7,
	AnchorManager_CreateAsaAnchor_m9155A8192A1CB025F38F4362F5290100ED772A89,
	AnchorManager_FindAsaAnchor_m62D25C4038B5F8583FCA018CACEEC60FBF616441,
	AnchorManager_FindAsaAnchorEditor_mD6A9411075AE362CF19227F095A5C6B60FABBB16,
	AnchorManager_StopAzureSession_m4F2363B780C097CEFC758F6F7911FEC4BA6C7CF6,
	AnchorManager_HandleAnchorLocated_m422CB3B2AC259E66D344B2C89F0AF51C1BD34905,
	AnchorManager_HandleOnAnchorPlaced_mFF6A5045F37C6E7BE6036AF1B1AC6FF7CAB61720,
	AnchorManager_HandleOnAnchorPlacementCanceled_mF5A616C9E50182E3C74035268EE10BE8D03DCADD,
	AnchorManager__ctor_mFBC62411AD152C71A391F5180675CDF4B4D4F5DC,
	AnchorManager_U3CFindAsaAnchorEditorU3Eb__32_0_mFF0BC6A0A093A8BC7DF482157EE95530A0DEA53D,
	AnchorManager_U3CHandleAnchorLocatedU3Eb__34_0_mF742397B00B8EBB2FC98DFE31757EDFA183D72BB,
	U3CU3Ec__cctor_m431B16D278BC0DFCD58984DC93BCCFC728932239,
	U3CU3Ec__ctor_m188598A0F3F7C8877BD4518AB6EE6368C95D7EFC,
	U3CU3Ec_U3CStartU3Eb__23_0_m98A4233CA4B0B2CBA4B60163E6DB7CF2A823F970,
	U3CU3Ec_U3CStartU3Eb__23_1_m653EF0BFFADA6994A82C6E81F80436E4E680F7DF,
	U3CU3Ec__DisplayClass29_0__ctor_m5359BAE5D521CDD17D978EF0F4145EC0AF1736A9,
	U3CU3Ec__DisplayClass29_0_U3CCreateAsaAnchorEditorU3Eb__0_m47611DC915B8096BA736921257E2022B438A4CFF,
	U3CCreateAsaAnchorEditorU3Ed__29_MoveNext_mF72D7A571AF2F5E9CB2719F72CCED585ED23CAF9,
	U3CCreateAsaAnchorEditorU3Ed__29_SetStateMachine_m61AFA54E5F838432398C8D4AC8E44F34FF5075F0,
	U3CU3Ec__DisplayClass30_0__ctor_m7839B54008BF25EFC6FA40A16E34C38A3D0EA394,
	U3CU3Ec__DisplayClass30_0_U3CCreateAsaAnchorU3Eb__1_m600D9EE08EB6E88645687265E7474CD852C19954,
	U3CU3Ec__DisplayClass30_0_U3CCreateAsaAnchorU3Eb__2_mA7A3AF644A134A2E0D97D59B106DBE9E1762FE48,
	U3CU3Ec__DisplayClass30_1__ctor_mED1F47357F43E54195BD71294D2987ECAA31C834,
	U3CU3Ec__DisplayClass30_1_U3CCreateAsaAnchorU3Eb__0_mE26C6480FD61A4DA41B52D2577134E735CF90AD1,
	U3CCreateAsaAnchorU3Ed__30_MoveNext_m01F48D0EE30F81576103027AFB86F2454088B3C3,
	U3CCreateAsaAnchorU3Ed__30_SetStateMachine_m159D78088F46C450CE0E21FFDBF6C0ABA8A7EB5B,
	U3CFindAsaAnchorU3Ed__31_MoveNext_m7471F513BDE1BB5388A834CE009D3063AE8DC942,
	U3CFindAsaAnchorU3Ed__31_SetStateMachine_m2DE169F9B79FCD78627B470D2C4682A2C469476A,
	U3CFindAsaAnchorEditorU3Ed__32_MoveNext_m706A98BDD79F2E0C87675081DB12AF0D446C5B80,
	U3CFindAsaAnchorEditorU3Ed__32_SetStateMachine_mF067B93045A8B49D35BD254868D8DB7AD322AB1A,
	U3CStopAzureSessionU3Ed__33_MoveNext_m63FC6BEFC62458445D2C673734A04937AE211A53,
	U3CStopAzureSessionU3Ed__33_SetStateMachine_m512A308EC6C9E9CB8A84D245069CBBE1FC1AE533,
	ChatBotManager_add_OnConversationStarted_m7B0D3310DB1FB48D212BA56A8FAE8C33966DEC44,
	ChatBotManager_remove_OnConversationStarted_mF4CCA05D98B5A60FEF84C537DE4621557BCAB436,
	ChatBotManager_add_OnMessageSent_mB37CD86E4ECE8194B050B6E9CD980671E0884C9D,
	ChatBotManager_remove_OnMessageSent_mA0D73F735FA2A9797E0C75440127008E3451C62D,
	ChatBotManager_add_OnMessagesReceived_mCDA220CDE6675719B94E89E95CA7E586E5AF70FF,
	ChatBotManager_remove_OnMessagesReceived_m4DE9BE0CB7D85E49AB3F0BE2BD5404D1CA3F8FCB,
	ChatBotManager_Awake_mE8703AC5534D1A0576690BFAC9D26F1A62795CC4,
	ChatBotManager_StartConversation_m456ECCBA67CB172299CF84DD1A630A536CC9360F,
	ChatBotManager_ReceiveMessages_m624C5C57EF297A70D5D74B658A1E37D7CE529586,
	ChatBotManager_SentMessage_m98584DF6519272AE3AB3462C4DC8ABA395D83E15,
	ChatBotManager_HandleBotResponse_mEEE9C0703CD1A1363D0D247C1F3807514BA7B230,
	ChatBotManager__ctor_mB234C30783FFAA9215C5576432688D980962FDE9,
	DataManager_get_IsReady_mE96F544BFF278A16693DCA07134C1D20D5938AFC,
	DataManager_set_IsReady_m008AEAAA5105A52BB110766D519DE1EAA904CC31,
	DataManager_Awake_m17FF6C4D2BA3B35E05784F89FDA17A764B6DD8B4,
	DataManager_GetOrCreateProject_mD41C02BECEEE074BB2930A93BB89F22669B57A4C,
	DataManager_UpdateProject_m34A62BBF7137D6EC94D7D0A95A6D000DE36E0921,
	DataManager_UploadOrUpdate_m7C17D2E7B2ECC0FF48C8F57E24440770CCDAF408,
	DataManager_GetAllTrackedObjects_mF9B03702BCD842690311560B3E6CA79871BCEC13,
	DataManager_FindTrackedObjectById_m9EE1DAB74335B1BC211C06024031D59AF3B26A84,
	DataManager_FindTrackedObjectByName_mA2CF50F7FBB278EA0F11876581AFCDBB4F6793B5,
	DataManager_DeleteTrackedObject_m9970CF7CE1AFC179809AA40F279C5C1786C0B6B1,
	DataManager_UploadBlob_m6AF38340678594A978E8527293EB799BEF5FEB33,
	DataManager_DownloadBlob_mF80F866B66A4FCFD669ABA804DD3D1D9B91962A5,
	DataManager_DeleteBlob_m72000A175EB253F13BB3DBC747F3ECA1A76D565F,
	DataManager__ctor_mFFD4AE09B5E90423F34D7ED76C9D554A781CE361,
	U3CAwakeU3Ed__20_MoveNext_m3AD421FA6A7A4884310255D9AA87D2BF7A4B285B,
	U3CAwakeU3Ed__20_SetStateMachine_mD715611BF40330195B4B6D43446F58DBC8D53981,
	U3CGetOrCreateProjectU3Ed__21_MoveNext_m8A4493BBC01000EDB711FE161F6AA7AA01BE3453,
	U3CGetOrCreateProjectU3Ed__21_SetStateMachine_m9B12E853B601A0591C6A8837C92F5BD739D595B4,
	U3CUpdateProjectU3Ed__22_MoveNext_m5D02DF33E77BEB1166F8E4795C509AAA6A73A13C,
	U3CUpdateProjectU3Ed__22_SetStateMachine_m4829BCBCBAD690F5C79D4AB6E91E52CFEDEB1094,
	U3CUploadOrUpdateU3Ed__23_MoveNext_m97EAEAE6F3ED132508622ACB8FA7E36C76416657,
	U3CUploadOrUpdateU3Ed__23_SetStateMachine_m44AACB808A3BC1A9D9B3AD96590EFE499D57DD07,
	U3CGetAllTrackedObjectsU3Ed__24_MoveNext_m29F6EF2E0BE57F0A1CDF2CD592E11A82B8BD8396,
	U3CGetAllTrackedObjectsU3Ed__24_SetStateMachine_m3632968E435819290A63D8F402C8DCDBBF1F4094,
	U3CFindTrackedObjectByIdU3Ed__25_MoveNext_m123A428C131B1E6C966CF3102730AA332B6DD26E,
	U3CFindTrackedObjectByIdU3Ed__25_SetStateMachine_m33AA8DFF1B28BEEDBA247995E19C7D2CA0A7F45A,
	U3CFindTrackedObjectByNameU3Ed__26_MoveNext_m7C087B0BFA047015B30C1BAF9ECE6DF88124B63C,
	U3CFindTrackedObjectByNameU3Ed__26_SetStateMachine_m785410F74E274E94EFC441418EAF3DCEC565839E,
	U3CDeleteTrackedObjectU3Ed__27_MoveNext_m056A71511FD99FA70CD926B62B523EB1B5078AB0,
	U3CDeleteTrackedObjectU3Ed__27_SetStateMachine_mBDB7E59EF871FB402C7A575D0EA212DDAB3C6A48,
	U3CUploadBlobU3Ed__28_MoveNext_m67369901C66B76F01F0E7F14DA8E3F41CDA25CED,
	U3CUploadBlobU3Ed__28_SetStateMachine_mD30FB69399F3DE0ECDF4B04386ECF2ACF49FEBAA,
	U3CDownloadBlobU3Ed__29_MoveNext_mD33AC5DF8B290B4404804FD711CA1F15F51B43AE,
	U3CDownloadBlobU3Ed__29_SetStateMachine_m6931AFC6D0CE2EA2A83EF31881469DCC10B787FD,
	U3CDeleteBlobU3Ed__30_MoveNext_mFD9445A5EE67292CF8E920D243907AC9706F2CB9,
	U3CDeleteBlobU3Ed__30_SetStateMachine_mCA39473460418F90AEEABA0C98F68CA59F2BA6C4,
	ObjectDetectionManager_CreateTag_m6F3C5F68073D56C78BCB59701163B57D3A02CBD8,
	ObjectDetectionManager_UploadTrainingImage_mD4ABA5444DE28E7067005D66C49766399B88E183,
	ObjectDetectionManager_TrainProject_m92FE29B8DE15C11173C1A7071BAD6F500B1022E7,
	ObjectDetectionManager_GetTrainingStatus_mF9AD7CCD3044CC7F70B3AF910268230FFF1E3532,
	ObjectDetectionManager_PublishTrainingIteration_m968FB407BFC6564DDA4106F63C24D5EB38F698F8,
	ObjectDetectionManager_DeleteTrainingIteration_mE57BC004B250A4D95268AC04220A7249CC9269F9,
	ObjectDetectionManager_DetectImage_m4528CB10FD3416FED8AF795DD7967F857746D32C,
	ObjectDetectionManager__ctor_mB48BEDB4F6335BF495DC215F4DC4E6ACB3435CB6,
	U3CCreateTagU3Ed__6_MoveNext_m262D3BFCC6F7DE95508858D0AE759E8C36679691,
	U3CCreateTagU3Ed__6_SetStateMachine_mE13F1D769729E4EE7399FF3C3AC5FBFC3407D1D7,
	U3CUploadTrainingImageU3Ed__7_MoveNext_m64280BED4257534201728C7A53DB60BF099E664C,
	U3CUploadTrainingImageU3Ed__7_SetStateMachine_mE12CB7D9ECCADB497F0DF92A83E5279147AA0EB3,
	U3CTrainProjectU3Ed__8_MoveNext_m4309543749D82E359B04A4A87B685B1E684E304C,
	U3CTrainProjectU3Ed__8_SetStateMachine_m8127814BE515070D4B1695ACD6269A26AD7DE841,
	U3CGetTrainingStatusU3Ed__9_MoveNext_m5231D81B76D768CCE0069A4E6D63081F4268C5AC,
	U3CGetTrainingStatusU3Ed__9_SetStateMachine_m68EB6F0F3AB7E0F2A09B5FCC644AA1E468BDEA1D,
	U3CPublishTrainingIterationU3Ed__10_MoveNext_m97403A57DA6CC5895BB5D11BAC7FF96AA801601C,
	U3CPublishTrainingIterationU3Ed__10_SetStateMachine_m809D97A970A63210B1B4891447D9C9D8D98372F2,
	U3CDeleteTrainingIterationU3Ed__11_MoveNext_m64F345A69A88C6E46ADD9282B515F0B3E46F5FD4,
	U3CDeleteTrainingIterationU3Ed__11_SetStateMachine_m73D10666246CC785EF9287162F7D96AA177FE7DC,
	U3CDetectImageU3Ed__12_MoveNext_mF88473934D84B02005B7AE864694836F5CEDC84F,
	U3CDetectImageU3Ed__12_SetStateMachine_mEE11B0C103C209B3D7505A47206DFF6FC5C2E015,
	SceneController_set_IsCameraActive_m70A081F1C8BC03CD1536FF21CF00C52B54E0A2CD,
	SceneController_get_IsCameraActive_mC06009624527BF7073B28444CB61F15CF9E05085,
	SceneController_get_CurrentProject_mD30DB395AFD0776DADAC1BBFD1E0249C5920EE3D,
	SceneController_set_CurrentProject_m36ECB2D8C1FC90880CD74095DEFCB4F7F90BA4B0,
	SceneController_get_DataManager_m9A0DB668314AED236646FFD37E3C2417B5F38D42,
	SceneController_get_ObjectDetectionManager_m2E79566E8AD20FA4DA97456A21CA654B5FAAAC0C,
	SceneController_get_AnchorManager_m92ED5C44A96922E87FD4BF2128507A286DBA48AF,
	SceneController_Start_mD7462702546DFB9CF2D0BF96E3A003A16338B92A,
	SceneController_Init_m8F8C8AFF1CBAB7A457E0DA9EAD1502B1F4238B2B,
	SceneController_StartCamera_mAD71F0DC604DED6869A614E58808E9F08BAB2ACF,
	SceneController_StopCamera_m6105C85CBF019E865F291AAA20176FA4DC3439BA,
	SceneController_StartPhotoMode_mBABC7E63AE4DBB5AEBAD0105B81A27CA13B9BA50,
	SceneController_TakePhoto_mFC59C88BFAE0FF19275265698564F9C060FC92E1,
	SceneController_TakePhotoWithThumbnail_m3DD1226E7C65B9312B43700EAEFD0EFBECC494A6,
	SceneController_OpenConnectionMenu_m67BCE3A485AB7D25D1BEDDC59900BF34B62314A3,
	SceneController__ctor_m0788EABA785925A129BF621F2AB4DEE16EE9D732,
	SceneController_U3CStartCameraU3Eb__23_0_mA8ECBEE532C173CE47A6FB699348734941E45721,
	SceneController_U3CStopCameraU3Eb__24_0_m03A389918961530F8D9A478BA58C25C4E3A55471,
	SceneController_U3CStartPhotoModeU3Eb__25_1_m02706633973B4A92AD113B08C4A04E54029CD081,
	SceneController_U3CTakePhotoU3Eb__26_0_m18B197611F353CCAD60CB214959BD934237D657B,
	SceneController_U3CTakePhotoWithThumbnailU3Eb__27_0_m47ACDA6315A0FE214D6602B1974B67CB61D19EFA,
	U3CInitU3Ed__22_MoveNext_m2153D52A515E4952E299C2AFAA57AAAAF472DEDE,
	U3CInitU3Ed__22_SetStateMachine_mA7572AF7FDC75F69E8146D527BFCDCDAC583E03F,
	U3CU3Ec__cctor_m83A83018D520BEC76DF1EC33932552C77CCCA30E,
	U3CU3Ec__ctor_mE9EC0CB77BED37307EF19FDA2E554C402F4C0837,
	U3CU3Ec_U3CStartPhotoModeU3Eb__25_0_m7C5BFE2399E0E8EC8012C9DE5C03145AA85B06EC,
	U3CU3Ec__DisplayClass26_0__ctor_m5434BB95A62FF791CB31573153BB38CB8A91AFD5,
	U3CU3Ec__DisplayClass26_0_U3CTakePhotoU3Eb__1_m364843730F8BD96AA0AB60359886411221167A27,
	U3CU3Ec__DisplayClass26_0_U3CTakePhotoU3Eb__2_mF9E0ED144F5AAC0BED50DCC8E0BAFA0A8887992A,
	U3CU3Ec__DisplayClass27_0__ctor_m21D4A71F08DE6D9BACD9BCF9C9471AD2C5A10D84,
	U3CU3Ec__DisplayClass27_0_U3CTakePhotoWithThumbnailU3Eb__1_mB82D28E74E9758D9B2BB489A561FBF26EEE41D32,
	U3CU3Ec__DisplayClass27_0_U3CTakePhotoWithThumbnailU3Eb__2_m562554E6F633123D6DC33D18A1EAD31E2DCFCE6A,
	ImageElement_get_SourceUrl_m082DAED5E0D4B3EC6A59DBFDB0F5A6EC0B81D6B5,
	ImageElement_set_SourceUrl_m0C027154BC650B6C754C234E3369E435E22D11BB,
	ImageElement_get_Status_m62C2325304AF8547D8770C17565FD2BC58CB1C3C,
	ImageElement_set_Status_m7651564090F8FB0F0CE921C4A2EFC27BFB2FEDD3,
	ImageElement_get_Image_m159EFA377B97D9856132DDFC0855A76A1F46EB47,
	ImageElement_set_Image_m09B0EE1D36961826F6306A9F2BAB8A442EEB689D,
	ImageElement__ctor_m6B68CD872E2E371AF217B9766DEC5BFD2ADA4BD6,
	ImageInfo_get_Id_mE8139FCF7144BD8C6267D05D437B77A2A1A82C38,
	ImageInfo_set_Id_m2DD4072BAA646243FE7E591D5FEE1411BA537B82,
	ImageInfo_get_Created_mB5C366603769FE94C503D228ABA5D9ECFCFD2C10,
	ImageInfo_set_Created_m071237FC0B2FB95B40839367932D995B1BEB81EA,
	ImageInfo_get_Width_m2D9159F98EEC0293CFF445C4A98EEEAC80245364,
	ImageInfo_set_Width_mF5D4F4F69682DCB7B654EB67E5FE1EB4F931E78F,
	ImageInfo_get_Height_m907CC3DE9360BC143083A0FCA81EE98C23608581,
	ImageInfo_set_Height_mFCCACEE5A5D0BBD408C8870FF252D90A978B5AD7,
	ImageInfo_get_ResizedImageUri_m6F59C7B30FC6E46E6F3A7EC09ADB7F9F30984207,
	ImageInfo_set_ResizedImageUri_m712E6C400E412E1B3792A084E24C24E921CB29C7,
	ImageInfo_get_OriginalImageUri_m32FFDC23C729C638CF63542ED72E53D0B924F344,
	ImageInfo_set_OriginalImageUri_m26D97EF5F9603905E911B0294D834DE522BDE6C7,
	ImageInfo_get_ThumbnailUri_mA9CD48E0C60A24CA0490BC961641B1B49A264196,
	ImageInfo_set_ThumbnailUri_mB8FF97D7816E84F12DBB1D58C8E23543AD128917,
	ImageInfo_get_Tags_mCC32B681EF7DB649E561890E01C4641BBF76DBD3,
	ImageInfo_set_Tags_m7ED37CB2454210B8C8F66A1AEA7D3A824D4277C6,
	ImageInfo__ctor_mBEA17D16F17302222B5B89C684C8E76C4617F4A2,
	ImagePredictionResult_get_Id_mCF0130A073E2F792BE7F6109A20DBF33A3027663,
	ImagePredictionResult_set_Id_m2D5187FAB3B6318AF065AA668B1DF1A8BAE3B472,
	ImagePredictionResult_get_Project_mCACB9B9884F6FAA453F6DB5702CBE208D8980387,
	ImagePredictionResult_set_Project_mAC849D758FDEACB95E541BC7E141D1AFE66226E4,
	ImagePredictionResult_get_Iteration_mC09C25CAB1CA1E4D123B2F413F87CF5616F5C5FE,
	ImagePredictionResult_set_Iteration_m5D4D098FEA00A5FA1FD2809B02A06424A0F4A32A,
	ImagePredictionResult_get_Created_m0FD2C1F80180CCC090CAF68C275C2C3BC2DFC232,
	ImagePredictionResult_set_Created_m3905D391F539F8D3918A5482E0ED3306C31F8DA9,
	ImagePredictionResult_get_Predictions_m134157D88681D3E38252827B59A19B497C5A279F,
	ImagePredictionResult_set_Predictions_m65CE5660328BBA905F38C4E70F0CDB6B78A0A2D3,
	ImagePredictionResult__ctor_m7017714579DFF253DE83E222242A3063CD3EE9F9,
	ImageQuicktestResult_get_Id_mD16398F97E831CCB47718B5908C6F1310B5E7CD7,
	ImageQuicktestResult_set_Id_mD08000B5AA8A0919667E2895CAADE5F9C0DDCD9C,
	ImageQuicktestResult_get_Project_m6E016A6EDFEEA8677F5689C3B2C23D5C90B52AEA,
	ImageQuicktestResult_set_Project_mCD881BBEE9894970ABE73253573DC338FA2DED21,
	ImageQuicktestResult_get_Iteration_m9E9DE64B9F3E1E875F6327E2DBB377ADD7A31411,
	ImageQuicktestResult_set_Iteration_m41180178AA0076801B5C078CBF7A8F9DDCE0B580,
	ImageQuicktestResult_get_Created_mBD4F7CF8E8417919D632AAFFC9B9F5F16D41468E,
	ImageQuicktestResult_set_Created_m618F469E5C7DD2BB1F4C6027E4DA6F071CD41AC1,
	ImageQuicktestResult_get_Predictions_m99D50CA63FDEBA3FB0581EE7D442E321B6F1BCC3,
	ImageQuicktestResult_set_Predictions_m71D966064A075427A6F99CAB1188BA4A32C0A904,
	ImageQuicktestResult__ctor_m67FF6DDE22D66C868B5C89083D759844F980033C,
	ImagesCreatedResult_get_IsBatchSuccessful_m80C1B6CAA89619E51CEAF2BFE5AA3183395345ED,
	ImagesCreatedResult_set_IsBatchSuccessful_m3C382CCF49A93FB79CC8958B776CEF2C86EC2229,
	ImagesCreatedResult_get_Images_m04ADAC5D1622FDD042752475FCF9F296B01678CF,
	ImagesCreatedResult_set_Images_m2CD9774672C2AF3FEE68F5FF394039C86645191B,
	ImagesCreatedResult__ctor_mAF0323FE2FE16517FABC3720A94861E2D8B53AF6,
	Prediction_get_Probability_m5BB08975ADEB1D5B0FFB8787D07748C11F9D687D,
	Prediction_set_Probability_m7314E43E34AB9B3AD4C1040CDC6DDFD198FFF0BC,
	Prediction_get_TagId_mB53863D0AD4AFD22022CFD900D1B119FB1E75DCB,
	Prediction_set_TagId_mF27B6AE66479D77B6F2A314290E6230921B0FE36,
	Prediction_get_TagName_m13FCC6C003EAA6CF19F286353C2F9182C1D81530,
	Prediction_set_TagName_mABF665C6C59750054B62FE397E3393EBA165D506,
	Prediction__ctor_mC04150136E4689CD538E1B25B618336C541CAD24,
	Tag_get_TagId_m1BA333E512C0E02B9AC519740D5F9ECA57D9084D,
	Tag_set_TagId_m525E40A78991748347D4E82726FF2A01E828AF35,
	Tag_get_Created_mFDCE2CB9BB90F8DE78A776625C9537DB29595123,
	Tag_set_Created_mE3A9C2C822E489E8375291E6721746158030FC2F,
	Tag_get_TagName_mDC779F8323CA8901067A3A3F0DDE326B53E6E247,
	Tag_set_TagName_mC33DC637FF663EAB161FE76F090620A6B7B8931F,
	Tag__ctor_m037B210B75877C9E1189A77F59A0268F13364582,
	TagCreationResult_get_Id_m7728DB49DC439368D40D3BEEFC06C836760F7609,
	TagCreationResult_set_Id_m66CAFA1D7B616232C7A7D73D4C13F7203530257A,
	TagCreationResult_get_Name_mECDD70E6B680F139045848C67E707FC3247508BD,
	TagCreationResult_set_Name_m5AD7EED90E83BB56BDB901050F349E2F070C00C4,
	TagCreationResult_get_Description_mFF88C55A3ED1B73E36DEA1199DB7C6EFDDEDC6F0,
	TagCreationResult_set_Description_m3091895732863BB65EE8C17E3A3A5968197680C6,
	TagCreationResult_get_Type_m55D0AB5FE34386381769471079BAE5AA3E3012A1,
	TagCreationResult_set_Type_m320878BD445C02036EA771D4A36AD60FD9AD5A2F,
	TagCreationResult_get_ImageCount_m5B09BEB8C57426C655FC0F09E62BF5AC948E6A7F,
	TagCreationResult_set_ImageCount_m18EAEF5F1597CA6068A7C89DFCB546B5DEBD3C77,
	TagCreationResult__ctor_mAB363C509D50E8BE86BED663195F4BBDC1AAA7AA,
	TrainProjectResult_get_Id_mEC31C19E198F812DF4CF5C8D1DBDA916111D28A9,
	TrainProjectResult_set_Id_mD480931D8C50E8A81FFA0DF32063E60F163E36BB,
	TrainProjectResult_get_Name_mA20970955D6C0CD7DBE2CCC2FE41198C99DBA5BB,
	TrainProjectResult_set_Name_m9AFF968E5D21A9A15F8FD3FDE14DC84C40CD096C,
	TrainProjectResult_get_Status_mD7B6341A053B7048984BEC60512C736A656906DB,
	TrainProjectResult_set_Status_m12C45AB7831D12B3A391ABDC75C9B0CE0416389F,
	TrainProjectResult_get_Created_m0581CF0349935EE33B56E670BF39B509D98CBF5C,
	TrainProjectResult_set_Created_m854B5194D7ACFAA4123A5CDB7830A2DB94820365,
	TrainProjectResult_get_LastModified_mA2F8866668D6BEA36AC0072D67792D354228E8A1,
	TrainProjectResult_set_LastModified_m2DCC6C8D06643B4637DED36602F54138D18FBE53,
	TrainProjectResult_get_ProjectId_m854E81CACFC7D03F5BD39111038A9CEA1E50611A,
	TrainProjectResult_set_ProjectId_m6A8C4A93A984AFA428C4B82CC35E128BDB1A9073,
	TrainProjectResult_get_Exportable_m35455E76F9EAAAFFA354B57520EA1973D9E9C213,
	TrainProjectResult_set_Exportable_m314D691E8CBB2471D1FABCCA9A90361828A4B33C,
	TrainProjectResult_get_DomainId_m092888C33FE60C3C36F819467FFECBBF48893B37,
	TrainProjectResult_set_DomainId_m2FA4010B9A6E767B843140F54981B48D6B3AA81D,
	TrainProjectResult_get_ExportableTo_m7B7205A1773F5BB27CABD997E0D2127623F84AC9,
	TrainProjectResult_set_ExportableTo_m668049DBDF2BDBCE9D4CF2A5FBCE77ACD5303BB9,
	TrainProjectResult_get_TrainingType_m7E2B548CB721033CF0399BB6BE1C74B7F9CF423B,
	TrainProjectResult_set_TrainingType_m3BFF31574EFD2A3D5A35037085E7BDF3008011E9,
	TrainProjectResult_get_ReservedBudgetInHours_m1EA58DD09F4F6AFD60C7951E439553492EE27863,
	TrainProjectResult_set_ReservedBudgetInHours_m66F1B910D567683D05251D27954442122C3456B4,
	TrainProjectResult_get_PublishName_m6583F01AB52C1FD7EB0A9331AD126139B3694AE6,
	TrainProjectResult_set_PublishName_m7087C52FA8D59DC46C03A5E425DF7886717842FF,
	TrainProjectResult_IsCompleted_m44215592218455FA00D65FD038266B64F42665AE,
	TrainProjectResult__ctor_m50B9D4CEAA732921C06EAE449FA6DED950A46D6F,
	ImageThumbnail_get_ImageData_m4B55FA41EB95517D48444EED6B4EE9622B9CF771,
	ImageThumbnail_set_ImageData_m240C6D230A7C4E024A1EBA33F036113E09DECFA6,
	ImageThumbnail_get_Texture_m4732AC295A1EAB74AC93C9095D6A76C68E638C99,
	ImageThumbnail_set_Texture_m95F64DDAE5A155EDA9AC9B14B97ECD0AEEE3CB33,
	ImageThumbnail__ctor_m3AD22B39FF20D5E889324A877B694762DF0DBFAD,
	Project_get_Name_m97C629861FDB10FA7E29D1E9845DEC59C3DB29AA,
	Project_set_Name_m3B264D415A2D6C0A6EE91C629B375B6319486C67,
	Project_get_CustomVisionIterationId_mAD2E89BA608ED91A43934E1AD025CF747E7E0038,
	Project_set_CustomVisionIterationId_m92ACED54431F73D8A69A273CAC469C59F49CF293,
	Project_get_CustomVisionPublishedModelName_m457772F979D8ED01A4BE4310842D6355C954C59B,
	Project_set_CustomVisionPublishedModelName_m13BFF3A812CCA91FA8BB6BAAED9277CFBD2B5683,
	Project__ctor_mD0F17267A698CB49C03B7D911049559A7B55FB4D,
	TrackedObject_get_Name_m4965538CB6B148003B454160B204A2548D8861F0,
	TrackedObject_set_Name_mFC8E35E1517C271ED07C81CD53C5DF8244282562,
	TrackedObject_get_Description_m1BFF890F6AF85BBCCF9067F981C72055DCA79954,
	TrackedObject_set_Description_m67E695C84ED0241305F90A10201F19DE47F4E526,
	TrackedObject_get_ThumbnailBlobName_mB2422C3CC14BAE80611486E540D8775C4D771978,
	TrackedObject_set_ThumbnailBlobName_mFC2F02EC0DF8310E9E156078E1D89C3B8D5A9A57,
	TrackedObject_get_SpatialAnchorId_mC41FC75E9714B9811435C161A090E161266F3689,
	TrackedObject_set_SpatialAnchorId_m50C9F394E159DFA26AA2525F9462EEBFCE5D1B7F,
	TrackedObject_get_CustomVisionTagId_m0ADF58B30E9487B0C495D85BFC7BB3EA46795B85,
	TrackedObject_set_CustomVisionTagId_m4FBC2DD05CDCA663C3EDCC80B5DF3FF8513BAD7C,
	TrackedObject_get_CustomVisionTagName_m91A2EEF9AF25FC1CB1DFA6C4A958AFD2ED69F1E6,
	TrackedObject_set_CustomVisionTagName_mD4BC523A5B5A06B85B582783DB4A23BFC7D9859D,
	TrackedObject_get_HasBeenTrained_m233A89C233EBAEA9EBAAFC49F753945FEB7A7005,
	TrackedObject_set_HasBeenTrained_m0041B1536E26AF8B7121DA928CF0663AE1A89B03,
	TrackedObject__ctor_m17AE0CBC07FF927857BEED2A8C921F9BEF29F09A,
	TrackedObject__ctor_m0B36448DCA656CBFC14EB27DA5A55AC0934CA8C6,
	AnchorPlacementController_add_OnIndicatorPlaced_m0661A7C2C3586AE30EE86B695AF2CCF00FD85D92,
	AnchorPlacementController_remove_OnIndicatorPlaced_mFAE0DA24C9EF3B341CDAB457A09DD242126368F7,
	AnchorPlacementController_Awake_m892A92630C1675C9379BA51738FF206735FCE869,
	AnchorPlacementController_Start_mC9CBAD81DDBD3C353EF494EF6FDFF55C3D34FE72,
	AnchorPlacementController_StartIndicator_mB406B39E8C3F0F79850B01D1F961E9708AF89173,
	AnchorPlacementController_HandleOnSubmitButtonClick_m8A5CAA6450E0BC9284C5E2E3CE101B21B8872451,
	AnchorPlacementController_HandleOnCancelButtonClick_mAABEFB37A1D673565CD8A265633E83888DDB9A36,
	AnchorPlacementController_OnPlacingStopped_m9BB4996FA27D296C395C398237F883C1482D3997,
	AnchorPlacementController__ctor_mED763E19DA32F158AD66E535E7A1C7AFF19F871D,
	ChatBotController_get_IsListening_m11E67614F91775E978A73230B4F5E17DEF615D8C,
	ChatBotController_set_IsListening_mEDE71388945AE9D12137C55C1B44F2CC18EC6320,
	ChatBotController_get_IsSpeaking_m755B8CF88888457A9C932CA773DB45007F7E43B5,
	ChatBotController_set_IsSpeaking_m4A150418C84E06C9AAAA5ED54DEF7AD74FCCCF72,
	ChatBotController_Awake_m09E5D390F235D5E0C75E199532F747C852DAC0D4,
	ChatBotController_OnEnable_mD01EC7B71B96498EC7D3F4F6CC6BAB2C2472C03A,
	ChatBotController_OnDisable_mA3B7C3AABDAE0B3DC8E37D92E215D0DDBA8A6B81,
	ChatBotController_StartConversation_m6B63EBE6E7C0A7FA0D6F8DBF94844137A3F4108E,
	ChatBotController_StopConversation_mA636BD1EAFC3359B559DA377F7D2025C5C25953C,
	ChatBotController_StartListening_m23CFAF262EF727ED2B3CBF66EC95043E65C4C81F,
	ChatBotController_HandleOnConversationStarted_m1DF06F2F390604B4B639D1E3FF5C794CA50C50BD,
	ChatBotController_HandleOnMessagesReceived_mD3C93693E37C01B593EA5BDEC8512FA4DA087D4B,
	ChatBotController_HandleOnMessageSent_m240CBC6AC024D72273967F6266D7CBB19D686CC2,
	ChatBotController_HandleReceivedMessagesCoroutine_m981AF07233DFCA13DEC8251D63E3AEA1E59C9F9B,
	ChatBotController_OnDictationComplete_m526AD9682668947E88D6C3EDD019AC555752D74E,
	ChatBotController_SanitizeDictation_mB3B47CB637D4ABE1915A495D5537F8B75F7BF552,
	ChatBotController__ctor_m054CE4F6F35D0F529D88436FF9F99E878D8A49BE,
	ChatBotController_U3CHandleReceivedMessagesCoroutineU3Eb__28_0_m6137411422FD57C9CE26BC168D9E06E4FAA0C2D0,
	U3CHandleReceivedMessagesCoroutineU3Ed__28__ctor_m306C1B63B5764C89EAD5519340CD287035638C0B,
	U3CHandleReceivedMessagesCoroutineU3Ed__28_System_IDisposable_Dispose_m0699B61B6060ACEFBED0FD5FD4AAF0DE1127055E,
	U3CHandleReceivedMessagesCoroutineU3Ed__28_MoveNext_m191F10D67EC369745D6D24CCDF23F42EE2487376,
	U3CHandleReceivedMessagesCoroutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32D6E1B4458ED190BA8CE82FBC34A0838248D8B0,
	U3CHandleReceivedMessagesCoroutineU3Ed__28_System_Collections_IEnumerator_Reset_m3C391763B8084B3569295CBFEB88BF906044B320,
	U3CHandleReceivedMessagesCoroutineU3Ed__28_System_Collections_IEnumerator_get_Current_m7B56C9CF821250D0E92A5454C74DCCAF1ED9B9DB,
	CloseDialogController_SetOriginMenu_mD724DFF91CB4A099770EFAD41E6BC8E22850903A,
	CloseDialogController_SetTargetMenu_m294748E64A6AD2ED317BEA22F151275860395752,
	CloseDialogController_HandleOkButtonClick_mB7AA008BC267CCE1D80EE38B1FA1EE927E04CC27,
	CloseDialogController_HandleCancelButtonClick_m2EF963CEE9EDA3C930F6F799A0EE2E0C542074B4,
	CloseDialogController__ctor_mBFB8D12EB8330EF7882275A804CE6EF5B766158C,
	ComputerVisionController_Awake_mF68FB901A9BCE846390979C2B94940C775498114,
	ComputerVisionController_Start_m4FB668D6F490415E41F6C1DF81CBDC83FAAB27EA,
	ComputerVisionController_Init_mBF5F38DAA84D7521EB67997DBEF687A5E7B29EA7,
	ComputerVisionController_StartPhotoCapture_m633186895CC82BCCC366959249581A2CC26481BB,
	ComputerVisionController_DeleteCurrentPhoto_m6491ADB422C0DA11F5FDDD45813CB029681A8410,
	ComputerVisionController_StartModelTraining_mD817EF954C86E192954450C8A2C0EBD7F1CB2C1F,
	ComputerVisionController_HandleOnPointerClick_m4CDA92AACB4305FA232FCAB8332AD60F466D1664,
	ComputerVisionController_CapturePhoto_mE991C61AFF0002FB246C4C923C501128E0888AC5,
	ComputerVisionController_SetButtonsInteractiveState_mEBD76868CC4FA01E3A9F1CC6A8489A30C1D3C49E,
	ComputerVisionController__ctor_mEFEB3B0FD6EB5F1495AFB8B92DEAB879D89A0058,
	U3CInitU3Ed__17_MoveNext_m564665D37149E8291EF61F9BA0F73EEAC3DA8A4D,
	U3CInitU3Ed__17_SetStateMachine_mA96BE34976F7466CC337181A09E10D6D5D4D1169,
	U3CStartModelTrainingU3Ed__20_MoveNext_m772784304C4A99A08730E52A3755C523056EE597,
	U3CStartModelTrainingU3Ed__20_SetStateMachine_m674DC58B684DFDB1EA92C7EA1EED44812DA5E566,
	U3CCapturePhotoU3Ed__22_MoveNext_mC0E8D3541C56083719C9C1CD127428D9881E65E3,
	U3CCapturePhotoU3Ed__22_SetStateMachine_m1ECC5810664E60F0C103D14F18CA1E8506218098,
	ObjectCardViewController_Awake_m66D77B236620F2DFB83FE2CDA26B4D816023F32C,
	ObjectCardViewController_OnDisable_mA7BE81F4CDFC487B33AFE9D8E04BA73E747B943D,
	ObjectCardViewController_Init_mE7F008F5EC7596FFEB1C5BEF9C91ECB31AC32479,
	ObjectCardViewController_StartComputerVisionDetection_mE34C8708BED6D9B76BD69488AE06743754766A50,
	ObjectCardViewController_StartFindLocation_m8653309E4A46FCED26A308C74DC9AF320AEF15B8,
	ObjectCardViewController_HandleOnAnchorFound_m4C093E111AE46D09C12474115E38E2304DDECE53,
	ObjectCardViewController_CloseCard_mBFC9B59C2AB8AE7DA92E80D0D3EB1F0BFC94941C,
	ObjectCardViewController_SearchWithComputerVision_mC4138AD7BA13D5B644B5EAE48D0620733E8CDB49,
	ObjectCardViewController_LoadThumbnailImage_mE37EEDDEF89FFDA2E310B0808AAD3BE938C38A01,
	ObjectCardViewController_SetButtonsInteractiveState_m0EF125BF4A65EB2F17B3679FAD17E1D38EEAF2B7,
	ObjectCardViewController__ctor_m62B3D1A47C5500BDAA66151DEAC9523E10C73163,
	ObjectCardViewController_U3CSearchWithComputerVisionU3Eb__17_0_m7E074DC5736CA8A50A5C6686482B97210FB782E1,
	U3CInitU3Ed__12_MoveNext_m6F9068BC7CD0CFCDF4E0E8DD04EE293E48279E42,
	U3CInitU3Ed__12_SetStateMachine_mC439122519743780B8E5205E111895D665E99134,
	U3CStartComputerVisionDetectionU3Ed__13_MoveNext_mAC4E0D875BCAE51C94D4AF3B11D3F65A40752159,
	U3CStartComputerVisionDetectionU3Ed__13_SetStateMachine_m45AB4FE13AEA39E60F55344E1A78876232C9BEB4,
	U3CSearchWithComputerVisionU3Ed__17_MoveNext_mA3ADFB2E4556288E51AA1F6023BBDEAACD3BE314,
	U3CSearchWithComputerVisionU3Ed__17_SetStateMachine_m1C63E2CF4E05B0FCD6148BAE033C8BADFAE0F8FB,
	U3CLoadThumbnailImageU3Ed__18_MoveNext_m3953192F759B21F12B04147EF688553834BC1F70,
	U3CLoadThumbnailImageU3Ed__18_SetStateMachine_m50A978F8F755E653AD24942B4554D91471C74AFE,
	ObjectEditController_Awake_m298B004C23699C85A9D7730BEB4D0CCB7CBF038F,
	ObjectEditController_Start_mBA687156C02490967633024D597F976FD0B0BEA0,
	ObjectEditController_Init_mE097D91AAA5BDB649B9D9B363A0287D284D907E3,
	ObjectEditController_TakeThumbnailPhoto_mE4E802ECF9175FFD021E126061A28551FCEC5FD8,
	ObjectEditController_DeleteThumbnailPhoto_m1CEDDB50339812797D0DC032959D6497F6777028,
	ObjectEditController_SaveChanges_mBB2214B96339B2A960861C4B64E2F52722056A9D,
	ObjectEditController_OpenComputerVisionFlow_mBCAF3690DD33CA2D6CE64BEE6C0B640B7656D3C9,
	ObjectEditController_OpenSpatialAnchorsFlow_m42F4153A4FBE413DB0BE87D2D732879C365AD628,
	ObjectEditController_HandleOnPointerClick_m5F58765FF4FBC912CB18177778D8508E552BF14A,
	ObjectEditController_CapturePhoto_m38A96A2B0B0B57E5865A19D6BB2D13971AD49868,
	ObjectEditController_HandleOnCreateAnchorSucceeded_m219D3D6EE75A06988A9C69A293E592CAF1782F30,
	ObjectEditController_LoadThumbnailImage_mEB6977AF1F33037AFDDAD6A4DFC0AE19ED246E99,
	ObjectEditController_SetButtonsInteractiveState_mC46E56F30362B8489896E6FCD82E0001CF7E7645,
	ObjectEditController__ctor_m30C876CE447EEB59FEA72CBB55D52738B7E80281,
	U3CInitU3Ed__14_MoveNext_mA3CA7720B916403A3643F8F75D3A4B3F8082B7D8,
	U3CInitU3Ed__14_SetStateMachine_m5A247ABD81740A7CC69B11E2D6AD92BFC5FF19B0,
	U3CDeleteThumbnailPhotoU3Ed__16_MoveNext_m9212698B7C0C3D9BDB1F9C89D127AAFACDE90D94,
	U3CDeleteThumbnailPhotoU3Ed__16_SetStateMachine_m6A3BAC8934E715428EC93195021E769541317001,
	U3CSaveChangesU3Ed__17_MoveNext_m88607E1A32FE1BAC3235A646B5BEC613469CD2CB,
	U3CSaveChangesU3Ed__17_SetStateMachine_m31775CB6BE2905AD4477A57AEFBDFFD78E290898,
	U3CCapturePhotoU3Ed__21_MoveNext_mDC92048B8293E18F846704A5E498F2BF72D3702D,
	U3CCapturePhotoU3Ed__21_SetStateMachine_m6BB7B5B993A8FC0CFDE187F9E6EA24C9E723CC2D,
	U3CHandleOnCreateAnchorSucceededU3Ed__22_MoveNext_m082A6F4DB234CFECB29A2DF4AB5A5ED6E5A61653,
	U3CHandleOnCreateAnchorSucceededU3Ed__22_SetStateMachine_mEDCA3ECFB52D5402CF8E6C6A37156C5A6DB4B2CF,
	U3CLoadThumbnailImageU3Ed__23_MoveNext_mA08007D052DA2FAEE9723B90566013DF24942F49,
	U3CLoadThumbnailImageU3Ed__23_SetStateMachine_m5314C7BBD0BBFC800434CC75B188A4D7B9A67DEC,
	ObjectEntryController_Awake_mC2D2ED6B926790B4778DE3EDA2F65E9729420590,
	ObjectEntryController_OnEnable_m610CC669D67FD9EEBB8A6B888D7A080C1902E7C8,
	ObjectEntryController_SetSearchMode_mDC3863941D52FCCFA97374A257F048BF242E5139,
	ObjectEntryController_SubmitQuery_m75870E932C00E52F9ABE4F1FE1DAC386021DDC8D,
	ObjectEntryController_FindObject_m5F4DD4E61C004801269FF8D3ADC8AC0F8E3A58DC,
	ObjectEntryController_CreateObject_mB31790598205AEC6FC8DBD08A5283220B7C2E8EE,
	ObjectEntryController_SetButtonsInteractiveState_mCA60C579EAC6252961E6AD970518F77EA790A9BD,
	ObjectEntryController__ctor_m31E9E61C5322DF69D4DB41FB1AB6D26E48BD244B,
	U3CSubmitQueryU3Ed__13_MoveNext_m17543901304155A13FB73367239EEDA73124483C,
	U3CSubmitQueryU3Ed__13_SetStateMachine_m76B07DB9F0E42E42BB396F819009392B81915CC3,
	U3CFindObjectU3Ed__14_MoveNext_m35CAC51C1D4884C68E48ABD6BF25B2A874894DC0,
	U3CFindObjectU3Ed__14_SetStateMachine_mAA8D0EB3D92A7093C8035BB269D14873A8091A95,
	U3CCreateObjectU3Ed__15_MoveNext_m9524FB8BDA7E0237E633CCA346D5420935B36E53,
	U3CCreateObjectU3Ed__15_SetStateMachine_m0A0891CE2BEAF1749257440380F54E0B66EA18E1,
	BotDirectLineManager_add_BotResponse_mDDA6FFBB0E0CECE42EAAE3218901D867EAA93F81,
	BotDirectLineManager_remove_BotResponse_mD5093BCE6AAC37B0EDCB6D21FC276EDA082F4BF1,
	BotDirectLineManager_get_Instance_mFA2C230ED0CD2019307207BC4577536C498D6346,
	BotDirectLineManager_get_IsInitialized_m21F9D92E5F4CF960A0D20AE28FB2FCE6A52BDF12,
	BotDirectLineManager_set_IsInitialized_m094CAADC334DA8FC8C0DB97082DD88671771756B,
	BotDirectLineManager_get_SecretKey_m3354C9DD6EC3BACB58220DFE3AB5A81A8AFD63E9,
	BotDirectLineManager_set_SecretKey_m374FC0F46381B3E39DB4A686EBD6D740D535BC2B,
	BotDirectLineManager__ctor_m0A681A6502F21DCF84764FB1B1593E3610430AB5,
	BotDirectLineManager_Initialize_m074F49C99224FADF360A0B28393EF49CCE1DE3DF,
	BotDirectLineManager_StartConversationCoroutine_m60C80ED82FE7ED25744CF96E627DEAB0F4BD4EB8,
	BotDirectLineManager_SendMessageCoroutine_m65784C01CB7C9B31B0C50CB68CE9831E913AD800,
	BotDirectLineManager_GetMessagesCoroutine_mB80599D9AB9C230303DC51D3D51F6CC49C41340D,
	BotDirectLineManager_CreateWebRequest_m082656659DB953557780F5A1E57B8C046A88A264,
	BotDirectLineManager_CreateBotResponseEventArgs_m345A7C7C639BB01FB77D886F8CC99F2DC5D18038,
	BotDirectLineManager_Utf8StringToByteArray_m424EA912B7E142B48B55FAEC033DD68EA100A48C,
	U3CStartConversationCoroutineU3Ed__21__ctor_mE66E0C6F842B5CF724F30E6B960AC6A5F693D97D,
	U3CStartConversationCoroutineU3Ed__21_System_IDisposable_Dispose_m675C7B2C01F5E8F87C432AED7CB665BBC9EF4C7C,
	U3CStartConversationCoroutineU3Ed__21_MoveNext_m048DB2E3EEA7999574F09DB596197FD1D32B48C1,
	U3CStartConversationCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE1662B13B1FF7BE87AE824B5040529CE3113241,
	U3CStartConversationCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m5941ADB6D7ADE577519B65ED6CFC2E4192B1550E,
	U3CStartConversationCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_mB010DDA6C03AFAC7A1887055B3314DF5A5318099,
	U3CSendMessageCoroutineU3Ed__22__ctor_m9A21274C214EC839F0F68DAA24AD99B4C91EE14C,
	U3CSendMessageCoroutineU3Ed__22_System_IDisposable_Dispose_m4AE86D26B440B860E5BF572953958616B98A3D16,
	U3CSendMessageCoroutineU3Ed__22_MoveNext_m1C4A3167BA715627E0957F5BD4B449A4722E335C,
	U3CSendMessageCoroutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9406F22EA5D27BFDF9890904560F9F0C1616B01F,
	U3CSendMessageCoroutineU3Ed__22_System_Collections_IEnumerator_Reset_mDE1A55EA026398E8402C73A1CD94B58461B0FE96,
	U3CSendMessageCoroutineU3Ed__22_System_Collections_IEnumerator_get_Current_m7AAA5907061EA77CA4FB803E609F79AC08DABB54,
	U3CGetMessagesCoroutineU3Ed__23__ctor_m1BB4B4DF8BB78CB56DCCC6F9F8769CE724DC115B,
	U3CGetMessagesCoroutineU3Ed__23_System_IDisposable_Dispose_m66B4019B9DE325F4957A891D50B04480727E1A5A,
	U3CGetMessagesCoroutineU3Ed__23_MoveNext_mFBE25A228E96C354E74E9D2B2503D6CC7F7D86C4,
	U3CGetMessagesCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD9D12A649FE899CC90D8A5C80D2718B534202BB,
	U3CGetMessagesCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_mFC1E7D11970BF765C12708FFE30AFC90B7F4DB30,
	U3CGetMessagesCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m6C18544D68791BC7DCF202678691CC8DDBCFE97E,
	BotJsonProtocol__ctor_mDF9EF054F224E5D2A42C7FFD8402C0C350D22495,
	BotResponseEventArgs_get_EventType_m25493DDDE06870680C4043C384C74F7C2DFF8C17,
	BotResponseEventArgs_set_EventType_m54129A8D8E98D40DD91E71937DB455DFE64C2BFD,
	BotResponseEventArgs_get_SentMessageId_mB9CD0FBD853525FED543B807ADE39037A703AAC4,
	BotResponseEventArgs_set_SentMessageId_m8536596B0AFB7EE528B5191320A2AA89FF5FFE8D,
	BotResponseEventArgs_get_ConversationId_m03DE6D59E357C84B7CBF73C536A7514CA3B42DD2,
	BotResponseEventArgs_set_ConversationId_m624262BED6F9CFA857A8F153D7611CDE87A783B7,
	BotResponseEventArgs_get_Code_m8ABF27E9D91C8A8B3469A72AC36D121BE825E232,
	BotResponseEventArgs_set_Code_mFF1A1B39F5681EE4820B5BA5CAB5D399B97D67E7,
	BotResponseEventArgs_get_Message_m9C13A7826D5DF73E5E100AC2BF13621E2D665C0F,
	BotResponseEventArgs_set_Message_mBFB9F4E9BFC8E540A2E30A3E3E2809AF7C521744,
	BotResponseEventArgs_get_Watermark_mCDCA680D26CE0AD026601320E164EA1F1158E577,
	BotResponseEventArgs_set_Watermark_m658BF4F21ECE1AD1ECEF3312FB15435A3DAEA7AE,
	BotResponseEventArgs_get_Messages_m58D435B5FAF026C4E6DFACB76F13DEDB6DB5B88D,
	BotResponseEventArgs_set_Messages_m45EC587BDF9732C1D35DAE205DEA5BFA06396F40,
	BotResponseEventArgs__ctor_mAFCDE1AF1E74A3639CE0AD8D85989DEC429F752C,
	BotResponseEventArgs_ToString_m342BCA1AB5672D37F5751A80B9F650BC8A5F987F,
	ConversationState_get_ConversationId_m9A39155CFA11EB421BAF1BEFCE85FA868FD3412D,
	ConversationState_set_ConversationId_m58F5C72CAC797CB6A985206793E968D4B87BAF8B,
	ConversationState_get_PreviouslySentMessageId_m65E412478DDD843EB7653E776316BA1398A4A45E,
	ConversationState_set_PreviouslySentMessageId_mB3C70E6E577BD8CBA2A4018E9EDD973761ED5317,
	ConversationState_get_Watermark_m47DFD940F2CC93ECFB0B158A8858232790B71179,
	ConversationState_set_Watermark_m555A200EDCEEA6D0D22ED63772B5C9E8CF2D62A1,
	ConversationState__ctor_mE9C448D88C9642BB5E3E536C7516B5270AF3E352,
	MessageActivity_get_Timestamp_mFC8B9107050E8BD148141904B656912B18EBB294,
	MessageActivity_set_Timestamp_m87A506C0189D11A856425758770690873C05E167,
	MessageActivity_get_Id_m9219957FB6DBAC7BF1FD55AA9C63C7073A05708F,
	MessageActivity_set_Id_m13B8968A045EA657E464D501FD3520D0718D455C,
	MessageActivity_get_ChannelId_m619D057773EA6ADEA0B0A2D4843FFCC8E074A50C,
	MessageActivity_set_ChannelId_m75913F27A0C269DD1F0F6C86C08B4743F30BD2AD,
	MessageActivity_get_Locale_m85A33FF9F8F87E615CB58B32AC65E08F8C1D18EA,
	MessageActivity_set_Locale_mFDF2ADE1CDAFBF334B606E0FE6570D91DCF71A1D,
	MessageActivity_get_FromId_m7A903C2D8AB2D1F84F459B81CB23E4AEFD5D9472,
	MessageActivity_set_FromId_m567954C11699ED6D9EF1B36A67240EBB994C4F27,
	MessageActivity_get_FromName_m184D9FF1A04CB442A1AB99B6CE0535C3A3FC2023,
	MessageActivity_set_FromName_m41973CA50E2A4391CB7AA290AD343B2EA7DDD47E,
	MessageActivity_get_ConversationId_m41334B4052434042965922710185289E85D994B4,
	MessageActivity_set_ConversationId_mDC61A9F119B76AEB7662993413056D032E934B15,
	MessageActivity_get_Text_m4F4EA7C5EE3F39A7257F4B06DAE383DF01950B9B,
	MessageActivity_set_Text_m4BD941BDD4213FC804663813ED80828A9DD5D255,
	MessageActivity_get_ReplyToId_m2BFED1A4CC86CAF82A5223834133509A01D55345,
	MessageActivity_set_ReplyToId_m40DF289F5F58A877413575BC1033C5107761B15F,
	MessageActivity__ctor_m73CD13E7FCD3CA5C34A1D9FFFB47EFB8E2738863,
	MessageActivity__ctor_m85B00C7BE26AD4CA7491DFEB6A922D38CB2F2EE5,
	MessageActivity_FromJson_m837F2BBA751807C91282BB03D728F998AA2ED8F8,
	MessageActivity_ToJsonString_mFE2347F3B921AE03F039F1C38FDBA3CDDCF67ADD,
	MessageActivity_ToString_m832BB1F35CBA339D9D486AC311BB3150757A714D,
};
extern void PointerData__ctor_m1C270542C6E8CA3F5536722DAC86B2EECC3B2AD0_AdjustorThunk (void);
extern void PointerData_get_IsNearPointer_mCEC5F5FC9A255DFAFA14CA5A4B5ED7251FCEE37F_AdjustorThunk (void);
extern void PointerData_get_GrabPoint_m6C4BF165B3C043AEB832099DE2033B8204473B61_AdjustorThunk (void);
extern void U3CU3CPlaySpeechU3Eb__0U3Ed_MoveNext_m604395E2D0545ABA13308F844EF82D1E3359B46E_AdjustorThunk (void);
extern void U3CU3CPlaySpeechU3Eb__0U3Ed_SetStateMachine_mFD91BAA9F1F1A2294AA3CCF16050C660FC13F7F0_AdjustorThunk (void);
extern void U3CHandleButtonClickU3Ed__14_MoveNext_mF720161ECDEF41103FA127C6530AD8437E8EA6A6_AdjustorThunk (void);
extern void U3CHandleButtonClickU3Ed__14_SetStateMachine_m5B82CD99AA2F16295DDEC5460785119016DA29FD_AdjustorThunk (void);
extern void U3COpenProgressIndicatorU3Ed__17_MoveNext_m1BD23385324B99F28EA5A32496561FA695B5F61A_AdjustorThunk (void);
extern void U3COpenProgressIndicatorU3Ed__17_SetStateMachine_mA9B346FEC4EADA24B0E0784703212DFFD1215ACF_AdjustorThunk (void);
extern void U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m4D4B2A2F50874C4C173492BC496BD868518AD19E_AdjustorThunk (void);
extern void U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m0B908A49592D587220B0D4F5EDF8DC82DCCC2431_AdjustorThunk (void);
extern void U3CCreateNewLogFileU3Ed__14_MoveNext_m51610C8380D963ECD36549D7396D660051DC203D_AdjustorThunk (void);
extern void U3CCreateNewLogFileU3Ed__14_SetStateMachine_mE9AF39239F16491AA8140EA2885F104078421DE0_AdjustorThunk (void);
extern void U3CLoadLogsU3Ed__21_MoveNext_mBFBDA1A9170A471CB4AB057033A5665AF89420E1_AdjustorThunk (void);
extern void U3CLoadLogsU3Ed__21_SetStateMachine_m52CD7696C92235AB638D02D5D50AD1F4E2FD8CE0_AdjustorThunk (void);
extern void U3CSaveLogsU3Ed__22_MoveNext_m2DBD29F6521D3A72E1288518B9ACE7E6BFCB9D3C_AdjustorThunk (void);
extern void U3CSaveLogsU3Ed__22_SetStateMachine_m08AA69B7E317EF2D112C6AD190A55B92EC99840D_AdjustorThunk (void);
extern void U3CUWP_LoadU3Ed__12_MoveNext_mF50633445CA445C15EB96BCBFEE6B0D0EAE5750B_AdjustorThunk (void);
extern void U3CUWP_LoadU3Ed__12_SetStateMachine_m591DA891C7D71B6BE2A2BEF8B43C3085CA166AD0_AdjustorThunk (void);
extern void U3CUWP_LoadNewFileU3Ed__13_MoveNext_mB09A0EF138A0B6388ACD97A9551D04E6A72180F2_AdjustorThunk (void);
extern void U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m0662CBA1196DF3F7A847D11CC714071670225D45_AdjustorThunk (void);
extern void U3CUWP_FileExistsU3Ed__14_MoveNext_m45168326C1D3F26DA0853F2BB2770F5A2175DA0C_AdjustorThunk (void);
extern void U3CUWP_FileExistsU3Ed__14_SetStateMachine_m0E7C98FD2BBBB9620A93E4779B89C2437C450B25_AdjustorThunk (void);
extern void U3CUWP_ReadDataU3Ed__15_MoveNext_mC1061B1A4D9E4ECF16413DCB85C41EC0C215C381_AdjustorThunk (void);
extern void U3CUWP_ReadDataU3Ed__15_SetStateMachine_m254C3E9028A12B79C84A1945BE7B7D5A04D7B183_AdjustorThunk (void);
extern void U3CLoadInUWPU3Ed__21_MoveNext_m43043EEB6C1601ECF81ABB1FD6933A79D82F134B_AdjustorThunk (void);
extern void U3CLoadInUWPU3Ed__21_SetStateMachine_m7F5529D96FD69AE974D6B8E3417740D88B06C266_AdjustorThunk (void);
extern void U3CStartU3Ed__9_MoveNext_m451334A42B884B78FAE2BBDCA8FB79FAD4BD20DF_AdjustorThunk (void);
extern void U3CStartU3Ed__9_SetStateMachine_m68B53E75DE39D39B25594881F84D029E91ACE7A6_AdjustorThunk (void);
extern void U3CWriteMessagesU3Ed__12_MoveNext_m1B7731FD9248E3A64D59366816A9963C272128DF_AdjustorThunk (void);
extern void U3CWriteMessagesU3Ed__12_SetStateMachine_m10ED05D0745B0BC452EC0BE04732A4E33BF8326F_AdjustorThunk (void);
extern void U3CStartProgressIndicatorSessionU3Ed__6_MoveNext_m5C3F80324352E494DC30D01C0BEC24A613F04C67_AdjustorThunk (void);
extern void U3CStartProgressIndicatorSessionU3Ed__6_SetStateMachine_m78BAD933A123E38DCD4EDF9744CCDFD87D9ACB06_AdjustorThunk (void);
extern void U3CCreateAsaAnchorEditorU3Ed__29_MoveNext_mF72D7A571AF2F5E9CB2719F72CCED585ED23CAF9_AdjustorThunk (void);
extern void U3CCreateAsaAnchorEditorU3Ed__29_SetStateMachine_m61AFA54E5F838432398C8D4AC8E44F34FF5075F0_AdjustorThunk (void);
extern void U3CCreateAsaAnchorU3Ed__30_MoveNext_m01F48D0EE30F81576103027AFB86F2454088B3C3_AdjustorThunk (void);
extern void U3CCreateAsaAnchorU3Ed__30_SetStateMachine_m159D78088F46C450CE0E21FFDBF6C0ABA8A7EB5B_AdjustorThunk (void);
extern void U3CFindAsaAnchorU3Ed__31_MoveNext_m7471F513BDE1BB5388A834CE009D3063AE8DC942_AdjustorThunk (void);
extern void U3CFindAsaAnchorU3Ed__31_SetStateMachine_m2DE169F9B79FCD78627B470D2C4682A2C469476A_AdjustorThunk (void);
extern void U3CFindAsaAnchorEditorU3Ed__32_MoveNext_m706A98BDD79F2E0C87675081DB12AF0D446C5B80_AdjustorThunk (void);
extern void U3CFindAsaAnchorEditorU3Ed__32_SetStateMachine_mF067B93045A8B49D35BD254868D8DB7AD322AB1A_AdjustorThunk (void);
extern void U3CStopAzureSessionU3Ed__33_MoveNext_m63FC6BEFC62458445D2C673734A04937AE211A53_AdjustorThunk (void);
extern void U3CStopAzureSessionU3Ed__33_SetStateMachine_m512A308EC6C9E9CB8A84D245069CBBE1FC1AE533_AdjustorThunk (void);
extern void U3CAwakeU3Ed__20_MoveNext_m3AD421FA6A7A4884310255D9AA87D2BF7A4B285B_AdjustorThunk (void);
extern void U3CAwakeU3Ed__20_SetStateMachine_mD715611BF40330195B4B6D43446F58DBC8D53981_AdjustorThunk (void);
extern void U3CGetOrCreateProjectU3Ed__21_MoveNext_m8A4493BBC01000EDB711FE161F6AA7AA01BE3453_AdjustorThunk (void);
extern void U3CGetOrCreateProjectU3Ed__21_SetStateMachine_m9B12E853B601A0591C6A8837C92F5BD739D595B4_AdjustorThunk (void);
extern void U3CUpdateProjectU3Ed__22_MoveNext_m5D02DF33E77BEB1166F8E4795C509AAA6A73A13C_AdjustorThunk (void);
extern void U3CUpdateProjectU3Ed__22_SetStateMachine_m4829BCBCBAD690F5C79D4AB6E91E52CFEDEB1094_AdjustorThunk (void);
extern void U3CUploadOrUpdateU3Ed__23_MoveNext_m97EAEAE6F3ED132508622ACB8FA7E36C76416657_AdjustorThunk (void);
extern void U3CUploadOrUpdateU3Ed__23_SetStateMachine_m44AACB808A3BC1A9D9B3AD96590EFE499D57DD07_AdjustorThunk (void);
extern void U3CGetAllTrackedObjectsU3Ed__24_MoveNext_m29F6EF2E0BE57F0A1CDF2CD592E11A82B8BD8396_AdjustorThunk (void);
extern void U3CGetAllTrackedObjectsU3Ed__24_SetStateMachine_m3632968E435819290A63D8F402C8DCDBBF1F4094_AdjustorThunk (void);
extern void U3CFindTrackedObjectByIdU3Ed__25_MoveNext_m123A428C131B1E6C966CF3102730AA332B6DD26E_AdjustorThunk (void);
extern void U3CFindTrackedObjectByIdU3Ed__25_SetStateMachine_m33AA8DFF1B28BEEDBA247995E19C7D2CA0A7F45A_AdjustorThunk (void);
extern void U3CFindTrackedObjectByNameU3Ed__26_MoveNext_m7C087B0BFA047015B30C1BAF9ECE6DF88124B63C_AdjustorThunk (void);
extern void U3CFindTrackedObjectByNameU3Ed__26_SetStateMachine_m785410F74E274E94EFC441418EAF3DCEC565839E_AdjustorThunk (void);
extern void U3CDeleteTrackedObjectU3Ed__27_MoveNext_m056A71511FD99FA70CD926B62B523EB1B5078AB0_AdjustorThunk (void);
extern void U3CDeleteTrackedObjectU3Ed__27_SetStateMachine_mBDB7E59EF871FB402C7A575D0EA212DDAB3C6A48_AdjustorThunk (void);
extern void U3CUploadBlobU3Ed__28_MoveNext_m67369901C66B76F01F0E7F14DA8E3F41CDA25CED_AdjustorThunk (void);
extern void U3CUploadBlobU3Ed__28_SetStateMachine_mD30FB69399F3DE0ECDF4B04386ECF2ACF49FEBAA_AdjustorThunk (void);
extern void U3CDownloadBlobU3Ed__29_MoveNext_mD33AC5DF8B290B4404804FD711CA1F15F51B43AE_AdjustorThunk (void);
extern void U3CDownloadBlobU3Ed__29_SetStateMachine_m6931AFC6D0CE2EA2A83EF31881469DCC10B787FD_AdjustorThunk (void);
extern void U3CDeleteBlobU3Ed__30_MoveNext_mFD9445A5EE67292CF8E920D243907AC9706F2CB9_AdjustorThunk (void);
extern void U3CDeleteBlobU3Ed__30_SetStateMachine_mCA39473460418F90AEEABA0C98F68CA59F2BA6C4_AdjustorThunk (void);
extern void U3CCreateTagU3Ed__6_MoveNext_m262D3BFCC6F7DE95508858D0AE759E8C36679691_AdjustorThunk (void);
extern void U3CCreateTagU3Ed__6_SetStateMachine_mE13F1D769729E4EE7399FF3C3AC5FBFC3407D1D7_AdjustorThunk (void);
extern void U3CUploadTrainingImageU3Ed__7_MoveNext_m64280BED4257534201728C7A53DB60BF099E664C_AdjustorThunk (void);
extern void U3CUploadTrainingImageU3Ed__7_SetStateMachine_mE12CB7D9ECCADB497F0DF92A83E5279147AA0EB3_AdjustorThunk (void);
extern void U3CTrainProjectU3Ed__8_MoveNext_m4309543749D82E359B04A4A87B685B1E684E304C_AdjustorThunk (void);
extern void U3CTrainProjectU3Ed__8_SetStateMachine_m8127814BE515070D4B1695ACD6269A26AD7DE841_AdjustorThunk (void);
extern void U3CGetTrainingStatusU3Ed__9_MoveNext_m5231D81B76D768CCE0069A4E6D63081F4268C5AC_AdjustorThunk (void);
extern void U3CGetTrainingStatusU3Ed__9_SetStateMachine_m68EB6F0F3AB7E0F2A09B5FCC644AA1E468BDEA1D_AdjustorThunk (void);
extern void U3CPublishTrainingIterationU3Ed__10_MoveNext_m97403A57DA6CC5895BB5D11BAC7FF96AA801601C_AdjustorThunk (void);
extern void U3CPublishTrainingIterationU3Ed__10_SetStateMachine_m809D97A970A63210B1B4891447D9C9D8D98372F2_AdjustorThunk (void);
extern void U3CDeleteTrainingIterationU3Ed__11_MoveNext_m64F345A69A88C6E46ADD9282B515F0B3E46F5FD4_AdjustorThunk (void);
extern void U3CDeleteTrainingIterationU3Ed__11_SetStateMachine_m73D10666246CC785EF9287162F7D96AA177FE7DC_AdjustorThunk (void);
extern void U3CDetectImageU3Ed__12_MoveNext_mF88473934D84B02005B7AE864694836F5CEDC84F_AdjustorThunk (void);
extern void U3CDetectImageU3Ed__12_SetStateMachine_mEE11B0C103C209B3D7505A47206DFF6FC5C2E015_AdjustorThunk (void);
extern void U3CInitU3Ed__22_MoveNext_m2153D52A515E4952E299C2AFAA57AAAAF472DEDE_AdjustorThunk (void);
extern void U3CInitU3Ed__22_SetStateMachine_mA7572AF7FDC75F69E8146D527BFCDCDAC583E03F_AdjustorThunk (void);
extern void U3CInitU3Ed__17_MoveNext_m564665D37149E8291EF61F9BA0F73EEAC3DA8A4D_AdjustorThunk (void);
extern void U3CInitU3Ed__17_SetStateMachine_mA96BE34976F7466CC337181A09E10D6D5D4D1169_AdjustorThunk (void);
extern void U3CStartModelTrainingU3Ed__20_MoveNext_m772784304C4A99A08730E52A3755C523056EE597_AdjustorThunk (void);
extern void U3CStartModelTrainingU3Ed__20_SetStateMachine_m674DC58B684DFDB1EA92C7EA1EED44812DA5E566_AdjustorThunk (void);
extern void U3CCapturePhotoU3Ed__22_MoveNext_mC0E8D3541C56083719C9C1CD127428D9881E65E3_AdjustorThunk (void);
extern void U3CCapturePhotoU3Ed__22_SetStateMachine_m1ECC5810664E60F0C103D14F18CA1E8506218098_AdjustorThunk (void);
extern void U3CInitU3Ed__12_MoveNext_m6F9068BC7CD0CFCDF4E0E8DD04EE293E48279E42_AdjustorThunk (void);
extern void U3CInitU3Ed__12_SetStateMachine_mC439122519743780B8E5205E111895D665E99134_AdjustorThunk (void);
extern void U3CStartComputerVisionDetectionU3Ed__13_MoveNext_mAC4E0D875BCAE51C94D4AF3B11D3F65A40752159_AdjustorThunk (void);
extern void U3CStartComputerVisionDetectionU3Ed__13_SetStateMachine_m45AB4FE13AEA39E60F55344E1A78876232C9BEB4_AdjustorThunk (void);
extern void U3CSearchWithComputerVisionU3Ed__17_MoveNext_mA3ADFB2E4556288E51AA1F6023BBDEAACD3BE314_AdjustorThunk (void);
extern void U3CSearchWithComputerVisionU3Ed__17_SetStateMachine_m1C63E2CF4E05B0FCD6148BAE033C8BADFAE0F8FB_AdjustorThunk (void);
extern void U3CLoadThumbnailImageU3Ed__18_MoveNext_m3953192F759B21F12B04147EF688553834BC1F70_AdjustorThunk (void);
extern void U3CLoadThumbnailImageU3Ed__18_SetStateMachine_m50A978F8F755E653AD24942B4554D91471C74AFE_AdjustorThunk (void);
extern void U3CInitU3Ed__14_MoveNext_mA3CA7720B916403A3643F8F75D3A4B3F8082B7D8_AdjustorThunk (void);
extern void U3CInitU3Ed__14_SetStateMachine_m5A247ABD81740A7CC69B11E2D6AD92BFC5FF19B0_AdjustorThunk (void);
extern void U3CDeleteThumbnailPhotoU3Ed__16_MoveNext_m9212698B7C0C3D9BDB1F9C89D127AAFACDE90D94_AdjustorThunk (void);
extern void U3CDeleteThumbnailPhotoU3Ed__16_SetStateMachine_m6A3BAC8934E715428EC93195021E769541317001_AdjustorThunk (void);
extern void U3CSaveChangesU3Ed__17_MoveNext_m88607E1A32FE1BAC3235A646B5BEC613469CD2CB_AdjustorThunk (void);
extern void U3CSaveChangesU3Ed__17_SetStateMachine_m31775CB6BE2905AD4477A57AEFBDFFD78E290898_AdjustorThunk (void);
extern void U3CCapturePhotoU3Ed__21_MoveNext_mDC92048B8293E18F846704A5E498F2BF72D3702D_AdjustorThunk (void);
extern void U3CCapturePhotoU3Ed__21_SetStateMachine_m6BB7B5B993A8FC0CFDE187F9E6EA24C9E723CC2D_AdjustorThunk (void);
extern void U3CHandleOnCreateAnchorSucceededU3Ed__22_MoveNext_m082A6F4DB234CFECB29A2DF4AB5A5ED6E5A61653_AdjustorThunk (void);
extern void U3CHandleOnCreateAnchorSucceededU3Ed__22_SetStateMachine_mEDCA3ECFB52D5402CF8E6C6A37156C5A6DB4B2CF_AdjustorThunk (void);
extern void U3CLoadThumbnailImageU3Ed__23_MoveNext_mA08007D052DA2FAEE9723B90566013DF24942F49_AdjustorThunk (void);
extern void U3CLoadThumbnailImageU3Ed__23_SetStateMachine_m5314C7BBD0BBFC800434CC75B188A4D7B9A67DEC_AdjustorThunk (void);
extern void U3CSubmitQueryU3Ed__13_MoveNext_m17543901304155A13FB73367239EEDA73124483C_AdjustorThunk (void);
extern void U3CSubmitQueryU3Ed__13_SetStateMachine_m76B07DB9F0E42E42BB396F819009392B81915CC3_AdjustorThunk (void);
extern void U3CFindObjectU3Ed__14_MoveNext_m35CAC51C1D4884C68E48ABD6BF25B2A874894DC0_AdjustorThunk (void);
extern void U3CFindObjectU3Ed__14_SetStateMachine_mAA8D0EB3D92A7093C8035BB269D14873A8091A95_AdjustorThunk (void);
extern void U3CCreateObjectU3Ed__15_MoveNext_m9524FB8BDA7E0237E633CCA346D5420935B36E53_AdjustorThunk (void);
extern void U3CCreateObjectU3Ed__15_SetStateMachine_m0A0891CE2BEAF1749257440380F54E0B66EA18E1_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[113] = 
{
	{ 0x0600003E, PointerData__ctor_m1C270542C6E8CA3F5536722DAC86B2EECC3B2AD0_AdjustorThunk },
	{ 0x0600003F, PointerData_get_IsNearPointer_mCEC5F5FC9A255DFAFA14CA5A4B5ED7251FCEE37F_AdjustorThunk },
	{ 0x06000040, PointerData_get_GrabPoint_m6C4BF165B3C043AEB832099DE2033B8204473B61_AdjustorThunk },
	{ 0x06000194, U3CU3CPlaySpeechU3Eb__0U3Ed_MoveNext_m604395E2D0545ABA13308F844EF82D1E3359B46E_AdjustorThunk },
	{ 0x06000195, U3CU3CPlaySpeechU3Eb__0U3Ed_SetStateMachine_mFD91BAA9F1F1A2294AA3CCF16050C660FC13F7F0_AdjustorThunk },
	{ 0x060002B8, U3CHandleButtonClickU3Ed__14_MoveNext_mF720161ECDEF41103FA127C6530AD8437E8EA6A6_AdjustorThunk },
	{ 0x060002B9, U3CHandleButtonClickU3Ed__14_SetStateMachine_m5B82CD99AA2F16295DDEC5460785119016DA29FD_AdjustorThunk },
	{ 0x060002BA, U3COpenProgressIndicatorU3Ed__17_MoveNext_m1BD23385324B99F28EA5A32496561FA695B5F61A_AdjustorThunk },
	{ 0x060002BB, U3COpenProgressIndicatorU3Ed__17_SetStateMachine_mA9B346FEC4EADA24B0E0784703212DFFD1215ACF_AdjustorThunk },
	{ 0x06000469, U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m4D4B2A2F50874C4C173492BC496BD868518AD19E_AdjustorThunk },
	{ 0x0600046A, U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m0B908A49592D587220B0D4F5EDF8DC82DCCC2431_AdjustorThunk },
	{ 0x06000481, U3CCreateNewLogFileU3Ed__14_MoveNext_m51610C8380D963ECD36549D7396D660051DC203D_AdjustorThunk },
	{ 0x06000482, U3CCreateNewLogFileU3Ed__14_SetStateMachine_mE9AF39239F16491AA8140EA2885F104078421DE0_AdjustorThunk },
	{ 0x06000483, U3CLoadLogsU3Ed__21_MoveNext_mBFBDA1A9170A471CB4AB057033A5665AF89420E1_AdjustorThunk },
	{ 0x06000484, U3CLoadLogsU3Ed__21_SetStateMachine_m52CD7696C92235AB638D02D5D50AD1F4E2FD8CE0_AdjustorThunk },
	{ 0x06000485, U3CSaveLogsU3Ed__22_MoveNext_m2DBD29F6521D3A72E1288518B9ACE7E6BFCB9D3C_AdjustorThunk },
	{ 0x06000486, U3CSaveLogsU3Ed__22_SetStateMachine_m08AA69B7E317EF2D112C6AD190A55B92EC99840D_AdjustorThunk },
	{ 0x060004D8, U3CUWP_LoadU3Ed__12_MoveNext_mF50633445CA445C15EB96BCBFEE6B0D0EAE5750B_AdjustorThunk },
	{ 0x060004D9, U3CUWP_LoadU3Ed__12_SetStateMachine_m591DA891C7D71B6BE2A2BEF8B43C3085CA166AD0_AdjustorThunk },
	{ 0x060004DA, U3CUWP_LoadNewFileU3Ed__13_MoveNext_mB09A0EF138A0B6388ACD97A9551D04E6A72180F2_AdjustorThunk },
	{ 0x060004DB, U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m0662CBA1196DF3F7A847D11CC714071670225D45_AdjustorThunk },
	{ 0x060004DC, U3CUWP_FileExistsU3Ed__14_MoveNext_m45168326C1D3F26DA0853F2BB2770F5A2175DA0C_AdjustorThunk },
	{ 0x060004DD, U3CUWP_FileExistsU3Ed__14_SetStateMachine_m0E7C98FD2BBBB9620A93E4779B89C2437C450B25_AdjustorThunk },
	{ 0x060004DE, U3CUWP_ReadDataU3Ed__15_MoveNext_mC1061B1A4D9E4ECF16413DCB85C41EC0C215C381_AdjustorThunk },
	{ 0x060004DF, U3CUWP_ReadDataU3Ed__15_SetStateMachine_m254C3E9028A12B79C84A1945BE7B7D5A04D7B183_AdjustorThunk },
	{ 0x060004E0, U3CLoadInUWPU3Ed__21_MoveNext_m43043EEB6C1601ECF81ABB1FD6933A79D82F134B_AdjustorThunk },
	{ 0x060004E1, U3CLoadInUWPU3Ed__21_SetStateMachine_m7F5529D96FD69AE974D6B8E3417740D88B06C266_AdjustorThunk },
	{ 0x0600066B, U3CStartU3Ed__9_MoveNext_m451334A42B884B78FAE2BBDCA8FB79FAD4BD20DF_AdjustorThunk },
	{ 0x0600066C, U3CStartU3Ed__9_SetStateMachine_m68B53E75DE39D39B25594881F84D029E91ACE7A6_AdjustorThunk },
	{ 0x06000673, U3CWriteMessagesU3Ed__12_MoveNext_m1B7731FD9248E3A64D59366816A9963C272128DF_AdjustorThunk },
	{ 0x06000674, U3CWriteMessagesU3Ed__12_SetStateMachine_m10ED05D0745B0BC452EC0BE04732A4E33BF8326F_AdjustorThunk },
	{ 0x06000682, U3CStartProgressIndicatorSessionU3Ed__6_MoveNext_m5C3F80324352E494DC30D01C0BEC24A613F04C67_AdjustorThunk },
	{ 0x06000683, U3CStartProgressIndicatorSessionU3Ed__6_SetStateMachine_m78BAD933A123E38DCD4EDF9744CCDFD87D9ACB06_AdjustorThunk },
	{ 0x060006B0, U3CCreateAsaAnchorEditorU3Ed__29_MoveNext_mF72D7A571AF2F5E9CB2719F72CCED585ED23CAF9_AdjustorThunk },
	{ 0x060006B1, U3CCreateAsaAnchorEditorU3Ed__29_SetStateMachine_m61AFA54E5F838432398C8D4AC8E44F34FF5075F0_AdjustorThunk },
	{ 0x060006B7, U3CCreateAsaAnchorU3Ed__30_MoveNext_m01F48D0EE30F81576103027AFB86F2454088B3C3_AdjustorThunk },
	{ 0x060006B8, U3CCreateAsaAnchorU3Ed__30_SetStateMachine_m159D78088F46C450CE0E21FFDBF6C0ABA8A7EB5B_AdjustorThunk },
	{ 0x060006B9, U3CFindAsaAnchorU3Ed__31_MoveNext_m7471F513BDE1BB5388A834CE009D3063AE8DC942_AdjustorThunk },
	{ 0x060006BA, U3CFindAsaAnchorU3Ed__31_SetStateMachine_m2DE169F9B79FCD78627B470D2C4682A2C469476A_AdjustorThunk },
	{ 0x060006BB, U3CFindAsaAnchorEditorU3Ed__32_MoveNext_m706A98BDD79F2E0C87675081DB12AF0D446C5B80_AdjustorThunk },
	{ 0x060006BC, U3CFindAsaAnchorEditorU3Ed__32_SetStateMachine_mF067B93045A8B49D35BD254868D8DB7AD322AB1A_AdjustorThunk },
	{ 0x060006BD, U3CStopAzureSessionU3Ed__33_MoveNext_m63FC6BEFC62458445D2C673734A04937AE211A53_AdjustorThunk },
	{ 0x060006BE, U3CStopAzureSessionU3Ed__33_SetStateMachine_m512A308EC6C9E9CB8A84D245069CBBE1FC1AE533_AdjustorThunk },
	{ 0x060006D9, U3CAwakeU3Ed__20_MoveNext_m3AD421FA6A7A4884310255D9AA87D2BF7A4B285B_AdjustorThunk },
	{ 0x060006DA, U3CAwakeU3Ed__20_SetStateMachine_mD715611BF40330195B4B6D43446F58DBC8D53981_AdjustorThunk },
	{ 0x060006DB, U3CGetOrCreateProjectU3Ed__21_MoveNext_m8A4493BBC01000EDB711FE161F6AA7AA01BE3453_AdjustorThunk },
	{ 0x060006DC, U3CGetOrCreateProjectU3Ed__21_SetStateMachine_m9B12E853B601A0591C6A8837C92F5BD739D595B4_AdjustorThunk },
	{ 0x060006DD, U3CUpdateProjectU3Ed__22_MoveNext_m5D02DF33E77BEB1166F8E4795C509AAA6A73A13C_AdjustorThunk },
	{ 0x060006DE, U3CUpdateProjectU3Ed__22_SetStateMachine_m4829BCBCBAD690F5C79D4AB6E91E52CFEDEB1094_AdjustorThunk },
	{ 0x060006DF, U3CUploadOrUpdateU3Ed__23_MoveNext_m97EAEAE6F3ED132508622ACB8FA7E36C76416657_AdjustorThunk },
	{ 0x060006E0, U3CUploadOrUpdateU3Ed__23_SetStateMachine_m44AACB808A3BC1A9D9B3AD96590EFE499D57DD07_AdjustorThunk },
	{ 0x060006E1, U3CGetAllTrackedObjectsU3Ed__24_MoveNext_m29F6EF2E0BE57F0A1CDF2CD592E11A82B8BD8396_AdjustorThunk },
	{ 0x060006E2, U3CGetAllTrackedObjectsU3Ed__24_SetStateMachine_m3632968E435819290A63D8F402C8DCDBBF1F4094_AdjustorThunk },
	{ 0x060006E3, U3CFindTrackedObjectByIdU3Ed__25_MoveNext_m123A428C131B1E6C966CF3102730AA332B6DD26E_AdjustorThunk },
	{ 0x060006E4, U3CFindTrackedObjectByIdU3Ed__25_SetStateMachine_m33AA8DFF1B28BEEDBA247995E19C7D2CA0A7F45A_AdjustorThunk },
	{ 0x060006E5, U3CFindTrackedObjectByNameU3Ed__26_MoveNext_m7C087B0BFA047015B30C1BAF9ECE6DF88124B63C_AdjustorThunk },
	{ 0x060006E6, U3CFindTrackedObjectByNameU3Ed__26_SetStateMachine_m785410F74E274E94EFC441418EAF3DCEC565839E_AdjustorThunk },
	{ 0x060006E7, U3CDeleteTrackedObjectU3Ed__27_MoveNext_m056A71511FD99FA70CD926B62B523EB1B5078AB0_AdjustorThunk },
	{ 0x060006E8, U3CDeleteTrackedObjectU3Ed__27_SetStateMachine_mBDB7E59EF871FB402C7A575D0EA212DDAB3C6A48_AdjustorThunk },
	{ 0x060006E9, U3CUploadBlobU3Ed__28_MoveNext_m67369901C66B76F01F0E7F14DA8E3F41CDA25CED_AdjustorThunk },
	{ 0x060006EA, U3CUploadBlobU3Ed__28_SetStateMachine_mD30FB69399F3DE0ECDF4B04386ECF2ACF49FEBAA_AdjustorThunk },
	{ 0x060006EB, U3CDownloadBlobU3Ed__29_MoveNext_mD33AC5DF8B290B4404804FD711CA1F15F51B43AE_AdjustorThunk },
	{ 0x060006EC, U3CDownloadBlobU3Ed__29_SetStateMachine_m6931AFC6D0CE2EA2A83EF31881469DCC10B787FD_AdjustorThunk },
	{ 0x060006ED, U3CDeleteBlobU3Ed__30_MoveNext_mFD9445A5EE67292CF8E920D243907AC9706F2CB9_AdjustorThunk },
	{ 0x060006EE, U3CDeleteBlobU3Ed__30_SetStateMachine_mCA39473460418F90AEEABA0C98F68CA59F2BA6C4_AdjustorThunk },
	{ 0x060006F7, U3CCreateTagU3Ed__6_MoveNext_m262D3BFCC6F7DE95508858D0AE759E8C36679691_AdjustorThunk },
	{ 0x060006F8, U3CCreateTagU3Ed__6_SetStateMachine_mE13F1D769729E4EE7399FF3C3AC5FBFC3407D1D7_AdjustorThunk },
	{ 0x060006F9, U3CUploadTrainingImageU3Ed__7_MoveNext_m64280BED4257534201728C7A53DB60BF099E664C_AdjustorThunk },
	{ 0x060006FA, U3CUploadTrainingImageU3Ed__7_SetStateMachine_mE12CB7D9ECCADB497F0DF92A83E5279147AA0EB3_AdjustorThunk },
	{ 0x060006FB, U3CTrainProjectU3Ed__8_MoveNext_m4309543749D82E359B04A4A87B685B1E684E304C_AdjustorThunk },
	{ 0x060006FC, U3CTrainProjectU3Ed__8_SetStateMachine_m8127814BE515070D4B1695ACD6269A26AD7DE841_AdjustorThunk },
	{ 0x060006FD, U3CGetTrainingStatusU3Ed__9_MoveNext_m5231D81B76D768CCE0069A4E6D63081F4268C5AC_AdjustorThunk },
	{ 0x060006FE, U3CGetTrainingStatusU3Ed__9_SetStateMachine_m68EB6F0F3AB7E0F2A09B5FCC644AA1E468BDEA1D_AdjustorThunk },
	{ 0x060006FF, U3CPublishTrainingIterationU3Ed__10_MoveNext_m97403A57DA6CC5895BB5D11BAC7FF96AA801601C_AdjustorThunk },
	{ 0x06000700, U3CPublishTrainingIterationU3Ed__10_SetStateMachine_m809D97A970A63210B1B4891447D9C9D8D98372F2_AdjustorThunk },
	{ 0x06000701, U3CDeleteTrainingIterationU3Ed__11_MoveNext_m64F345A69A88C6E46ADD9282B515F0B3E46F5FD4_AdjustorThunk },
	{ 0x06000702, U3CDeleteTrainingIterationU3Ed__11_SetStateMachine_m73D10666246CC785EF9287162F7D96AA177FE7DC_AdjustorThunk },
	{ 0x06000703, U3CDetectImageU3Ed__12_MoveNext_mF88473934D84B02005B7AE864694836F5CEDC84F_AdjustorThunk },
	{ 0x06000704, U3CDetectImageU3Ed__12_SetStateMachine_mEE11B0C103C209B3D7505A47206DFF6FC5C2E015_AdjustorThunk },
	{ 0x0600071A, U3CInitU3Ed__22_MoveNext_m2153D52A515E4952E299C2AFAA57AAAAF472DEDE_AdjustorThunk },
	{ 0x0600071B, U3CInitU3Ed__22_SetStateMachine_mA7572AF7FDC75F69E8146D527BFCDCDAC583E03F_AdjustorThunk },
	{ 0x060007D7, U3CInitU3Ed__17_MoveNext_m564665D37149E8291EF61F9BA0F73EEAC3DA8A4D_AdjustorThunk },
	{ 0x060007D8, U3CInitU3Ed__17_SetStateMachine_mA96BE34976F7466CC337181A09E10D6D5D4D1169_AdjustorThunk },
	{ 0x060007D9, U3CStartModelTrainingU3Ed__20_MoveNext_m772784304C4A99A08730E52A3755C523056EE597_AdjustorThunk },
	{ 0x060007DA, U3CStartModelTrainingU3Ed__20_SetStateMachine_m674DC58B684DFDB1EA92C7EA1EED44812DA5E566_AdjustorThunk },
	{ 0x060007DB, U3CCapturePhotoU3Ed__22_MoveNext_mC0E8D3541C56083719C9C1CD127428D9881E65E3_AdjustorThunk },
	{ 0x060007DC, U3CCapturePhotoU3Ed__22_SetStateMachine_m1ECC5810664E60F0C103D14F18CA1E8506218098_AdjustorThunk },
	{ 0x060007E9, U3CInitU3Ed__12_MoveNext_m6F9068BC7CD0CFCDF4E0E8DD04EE293E48279E42_AdjustorThunk },
	{ 0x060007EA, U3CInitU3Ed__12_SetStateMachine_mC439122519743780B8E5205E111895D665E99134_AdjustorThunk },
	{ 0x060007EB, U3CStartComputerVisionDetectionU3Ed__13_MoveNext_mAC4E0D875BCAE51C94D4AF3B11D3F65A40752159_AdjustorThunk },
	{ 0x060007EC, U3CStartComputerVisionDetectionU3Ed__13_SetStateMachine_m45AB4FE13AEA39E60F55344E1A78876232C9BEB4_AdjustorThunk },
	{ 0x060007ED, U3CSearchWithComputerVisionU3Ed__17_MoveNext_mA3ADFB2E4556288E51AA1F6023BBDEAACD3BE314_AdjustorThunk },
	{ 0x060007EE, U3CSearchWithComputerVisionU3Ed__17_SetStateMachine_m1C63E2CF4E05B0FCD6148BAE033C8BADFAE0F8FB_AdjustorThunk },
	{ 0x060007EF, U3CLoadThumbnailImageU3Ed__18_MoveNext_m3953192F759B21F12B04147EF688553834BC1F70_AdjustorThunk },
	{ 0x060007F0, U3CLoadThumbnailImageU3Ed__18_SetStateMachine_m50A978F8F755E653AD24942B4554D91471C74AFE_AdjustorThunk },
	{ 0x060007FF, U3CInitU3Ed__14_MoveNext_mA3CA7720B916403A3643F8F75D3A4B3F8082B7D8_AdjustorThunk },
	{ 0x06000800, U3CInitU3Ed__14_SetStateMachine_m5A247ABD81740A7CC69B11E2D6AD92BFC5FF19B0_AdjustorThunk },
	{ 0x06000801, U3CDeleteThumbnailPhotoU3Ed__16_MoveNext_m9212698B7C0C3D9BDB1F9C89D127AAFACDE90D94_AdjustorThunk },
	{ 0x06000802, U3CDeleteThumbnailPhotoU3Ed__16_SetStateMachine_m6A3BAC8934E715428EC93195021E769541317001_AdjustorThunk },
	{ 0x06000803, U3CSaveChangesU3Ed__17_MoveNext_m88607E1A32FE1BAC3235A646B5BEC613469CD2CB_AdjustorThunk },
	{ 0x06000804, U3CSaveChangesU3Ed__17_SetStateMachine_m31775CB6BE2905AD4477A57AEFBDFFD78E290898_AdjustorThunk },
	{ 0x06000805, U3CCapturePhotoU3Ed__21_MoveNext_mDC92048B8293E18F846704A5E498F2BF72D3702D_AdjustorThunk },
	{ 0x06000806, U3CCapturePhotoU3Ed__21_SetStateMachine_m6BB7B5B993A8FC0CFDE187F9E6EA24C9E723CC2D_AdjustorThunk },
	{ 0x06000807, U3CHandleOnCreateAnchorSucceededU3Ed__22_MoveNext_m082A6F4DB234CFECB29A2DF4AB5A5ED6E5A61653_AdjustorThunk },
	{ 0x06000808, U3CHandleOnCreateAnchorSucceededU3Ed__22_SetStateMachine_mEDCA3ECFB52D5402CF8E6C6A37156C5A6DB4B2CF_AdjustorThunk },
	{ 0x06000809, U3CLoadThumbnailImageU3Ed__23_MoveNext_mA08007D052DA2FAEE9723B90566013DF24942F49_AdjustorThunk },
	{ 0x0600080A, U3CLoadThumbnailImageU3Ed__23_SetStateMachine_m5314C7BBD0BBFC800434CC75B188A4D7B9A67DEC_AdjustorThunk },
	{ 0x06000813, U3CSubmitQueryU3Ed__13_MoveNext_m17543901304155A13FB73367239EEDA73124483C_AdjustorThunk },
	{ 0x06000814, U3CSubmitQueryU3Ed__13_SetStateMachine_m76B07DB9F0E42E42BB396F819009392B81915CC3_AdjustorThunk },
	{ 0x06000815, U3CFindObjectU3Ed__14_MoveNext_m35CAC51C1D4884C68E48ABD6BF25B2A874894DC0_AdjustorThunk },
	{ 0x06000816, U3CFindObjectU3Ed__14_SetStateMachine_mAA8D0EB3D92A7093C8035BB269D14873A8091A95_AdjustorThunk },
	{ 0x06000817, U3CCreateObjectU3Ed__15_MoveNext_m9524FB8BDA7E0237E633CCA346D5420935B36E53_AdjustorThunk },
	{ 0x06000818, U3CCreateObjectU3Ed__15_SetStateMachine_m0A0891CE2BEAF1749257440380F54E0B66EA18E1_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2152] = 
{
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	5978,
	4970,
	5978,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6154,
	6066,
	5978,
	6018,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	6072,
	2974,
	6018,
	6066,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5938,
	3630,
	4930,
	5938,
	6072,
	4970,
	4213,
	4213,
	6066,
	6066,
	5993,
	6025,
	5015,
	6025,
	5015,
	6025,
	5015,
	1782,
	5054,
	6072,
	6072,
	6072,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4930,
	6072,
	6072,
	4970,
	4970,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	1305,
	6072,
	1305,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	6072,
	4970,
	4970,
	2529,
	2529,
	1716,
	1716,
	1732,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	3917,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4930,
	6072,
	6072,
	6072,
	2529,
	2529,
	1716,
	1716,
	1732,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	4970,
	4970,
	4970,
	4970,
	4930,
	6072,
	6072,
	6072,
	6072,
	4930,
	6072,
	6072,
	4930,
	6072,
	6072,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	4970,
	3917,
	3917,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	5978,
	5978,
	6072,
	8716,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	4970,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	4970,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	4970,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	4970,
	5978,
	6072,
	6072,
	1928,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	3917,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	7934,
	7678,
	6990,
	7306,
	4970,
	2965,
	6072,
	4970,
	4970,
	5978,
	4970,
	5938,
	4930,
	6072,
	6072,
	5978,
	6072,
	4970,
	6072,
	6072,
	6072,
	4345,
	6072,
	5978,
	6072,
	5978,
	5978,
	4970,
	6018,
	5009,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	1735,
	1216,
	5054,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	5978,
	5978,
	6072,
	8716,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	4970,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	4970,
	4970,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	-1,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5009,
	6072,
	6072,
	4970,
	6072,
	6072,
	2965,
	6072,
	6072,
	5978,
	4970,
	5978,
	4970,
	5938,
	4930,
	6018,
	5009,
	5938,
	4930,
	5978,
	4970,
	6072,
	6072,
	2136,
	4970,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	5978,
	4970,
	4930,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5938,
	4930,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	-1,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	5978,
	4852,
	5978,
	4970,
	6072,
	4970,
	4970,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	4970,
	5978,
	4852,
	5978,
	4970,
	6072,
	4970,
	4970,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	1213,
	6072,
	4930,
	6072,
	6018,
	6072,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	4970,
	6072,
	6072,
	4970,
	6072,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	5978,
	6072,
	1613,
	3655,
	6072,
	6072,
	6072,
	6018,
	6072,
	6066,
	6072,
	5015,
	5015,
	6072,
	6072,
	5009,
	6072,
	6072,
	6072,
	6072,
	6072,
	5054,
	6072,
	5015,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	4970,
	5015,
	5015,
	6072,
	4607,
	6072,
	6072,
	3922,
	-1,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6018,
	6072,
	1613,
	3655,
	6072,
	6072,
	6072,
	6063,
	3008,
	6018,
	6072,
	5978,
	4970,
	6018,
	6072,
	5015,
	1613,
	6072,
	3655,
	6072,
	6072,
	3008,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	5978,
	5938,
	5938,
	5938,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6018,
	6018,
	6018,
	4612,
	6072,
	6072,
	6072,
	6072,
	5054,
	6025,
	4550,
	6018,
	5054,
	6072,
	6072,
	6072,
	5054,
	1594,
	4970,
	6072,
	8716,
	6072,
	4970,
	6072,
	6072,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4550,
	6072,
	6072,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	5978,
	1780,
	6072,
	6072,
	6072,
	5867,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	5978,
	6018,
	6072,
	6072,
	4856,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	2960,
	6072,
	2138,
	4970,
	6072,
	6072,
	5009,
	6072,
	5978,
	6072,
	6072,
	5054,
	3018,
	1784,
	3930,
	1477,
	1599,
	5978,
	5978,
	3222,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	5054,
	3223,
	2475,
	6072,
	4552,
	6072,
	6072,
	6072,
	6072,
	6072,
	8684,
	8565,
	6072,
	3917,
	4970,
	6072,
	6072,
	6072,
	5978,
	4970,
	4970,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5015,
	3004,
	1754,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4426,
	6072,
	6072,
	6072,
	6072,
	3027,
	3027,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	3917,
	6072,
	8716,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	1613,
	6072,
	6072,
	2965,
	6072,
	6072,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	6066,
	6072,
	1627,
	5015,
	1613,
	4970,
	4970,
	4970,
	4970,
	5978,
	4970,
	6072,
	8716,
	5978,
	4970,
	4970,
	8065,
	6072,
	6072,
	6072,
	6072,
	8684,
	8565,
	6072,
	6072,
	6072,
	8684,
	6072,
	6072,
	2968,
	6072,
	8430,
	8430,
	7763,
	7435,
	-1,
	7979,
	7935,
	8430,
	7774,
	7134,
	8074,
	7544,
	7544,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	6025,
	6025,
	6072,
	5015,
	5015,
	5015,
	4552,
	5883,
	5883,
	6072,
	6072,
	6072,
	2138,
	1299,
	1613,
	6072,
	8565,
	-1,
	5978,
	4970,
	2965,
	2965,
	6072,
	6072,
	5978,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	-1,
	-1,
	-1,
	-1,
	4970,
	4970,
	5978,
	5978,
	5978,
	6072,
	6072,
	6072,
	5978,
	3911,
	4345,
	6072,
	6072,
	5978,
	5978,
	5978,
	6072,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	1642,
	1642,
	6072,
	4840,
	4840,
	4930,
	583,
	2502,
	2502,
	1125,
	1125,
	496,
	1928,
	4993,
	3224,
	6072,
	6018,
	5938,
	4930,
	6072,
	6072,
	6072,
	5009,
	6072,
	5978,
	1435,
	6072,
	5978,
	5978,
	1435,
	6072,
	6072,
	6072,
	5978,
	3917,
	2138,
	3917,
	4970,
	1111,
	4550,
	6072,
	6072,
	6072,
	5978,
	5009,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	2958,
	6072,
	6072,
	6072,
	2728,
	4970,
	5978,
	3923,
	3923,
	3220,
	222,
	6018,
	4970,
	2965,
	2965,
	118,
	6072,
	6072,
	6072,
	8716,
	5978,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	8684,
	5978,
	5978,
	2138,
	5978,
	2136,
	8430,
	1735,
	4970,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	5009,
	6072,
	5009,
	6072,
	6072,
	2996,
	6072,
	6018,
	5009,
	6025,
	5015,
	6025,
	5015,
	6018,
	5009,
	6025,
	5015,
	6018,
	5009,
	6018,
	5009,
	5978,
	4970,
	6018,
	5009,
	5978,
	4970,
	5978,
	4970,
	5938,
	4930,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	3911,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	3286,
	6072,
	4930,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6018,
	5978,
	4970,
	5938,
	4930,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5938,
	4930,
	6072,
	6072,
	2968,
	6072,
	6072,
	6072,
	6018,
	6072,
	6072,
	6072,
	6072,
	6072,
	2965,
	2965,
	6072,
	6072,
	4970,
	4970,
	2965,
	6072,
	2965,
	3911,
	2762,
	3917,
	2965,
	5978,
	4970,
	5938,
	4970,
	3917,
	3911,
	3917,
	5978,
	5978,
	5978,
	3917,
	5938,
	4930,
	6025,
	5015,
	5883,
	4876,
	6018,
	5009,
	5978,
	5978,
	8430,
	8430,
	7883,
	7883,
	4345,
	5938,
	8430,
	8430,
	4970,
	4970,
	4970,
	4970,
	5978,
	4970,
	5978,
	8430,
	8430,
	8430,
	8430,
	8430,
	8430,
	8430,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	5978,
	5978,
	4930,
	6072,
	6018,
	6072,
	6072,
	5978,
	6072,
	5978,
	5978,
	5978,
	3911,
	2762,
	3917,
	2965,
	5938,
	2965,
	3911,
	3917,
	5978,
	5978,
	5978,
	3917,
	4970,
	6072,
	4930,
	6072,
	6018,
	6072,
	5978,
	6072,
	5978,
	5978,
	5978,
	4930,
	6072,
	6018,
	6072,
	5978,
	6072,
	5978,
	3917,
	2965,
	3911,
	2762,
	5938,
	2965,
	3917,
	3911,
	3917,
	5978,
	5978,
	5978,
	3917,
	4970,
	6072,
	6072,
	4091,
	4930,
	6072,
	6018,
	6072,
	5978,
	6072,
	5978,
	5978,
	5978,
	4930,
	6072,
	6018,
	6072,
	5978,
	6072,
	5978,
	5978,
	4970,
	4970,
	5015,
	4876,
	5009,
	4930,
	5978,
	3917,
	4970,
	4970,
	2965,
	4970,
	3911,
	2762,
	3917,
	2965,
	4970,
	2965,
	7883,
	7883,
	4345,
	5938,
	5978,
	3917,
	5938,
	4930,
	6025,
	5015,
	5883,
	4876,
	6018,
	5009,
	5978,
	5978,
	8430,
	6072,
	6072,
	5009,
	6072,
	6072,
	6072,
	6072,
	4970,
	4970,
	6072,
	2960,
	6072,
	2138,
	4970,
	5009,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	4970,
	4970,
	4970,
	4970,
	6072,
	2960,
	6072,
	2138,
	4970,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	5009,
	6072,
	6072,
	6072,
	4970,
	4970,
	6072,
	2960,
	6072,
	2138,
	4970,
	6072,
	4970,
	4970,
	3917,
	8704,
	8684,
	6072,
	6072,
	6072,
	8716,
	6072,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	5978,
	4970,
	1732,
	6072,
	6072,
	4970,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	4970,
	6072,
	6072,
	1732,
	6072,
	8430,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	2965,
	2965,
	6072,
	4970,
	5978,
	4970,
	5978,
	6072,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	6072,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	4345,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	2965,
	2965,
	2965,
	6072,
	6072,
	6072,
	8716,
	6072,
	2965,
	2965,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	4970,
	6072,
	6072,
	4970,
	1735,
	2965,
	6072,
	6018,
	5009,
	6072,
	5978,
	3917,
	3917,
	5978,
	3917,
	3917,
	3917,
	2138,
	3917,
	3917,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	3917,
	2138,
	5978,
	3917,
	2138,
	3917,
	2138,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	5009,
	6018,
	5978,
	4970,
	5978,
	5978,
	5978,
	6072,
	6072,
	6072,
	6072,
	6072,
	5978,
	5978,
	6072,
	6072,
	4970,
	5131,
	5131,
	5978,
	5978,
	6072,
	4970,
	8716,
	6072,
	3650,
	6072,
	6072,
	3039,
	6072,
	6072,
	3039,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	5978,
	4970,
	5880,
	4873,
	5938,
	4930,
	5938,
	4930,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5880,
	4873,
	5978,
	4970,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5880,
	4873,
	5978,
	4970,
	6072,
	6018,
	5009,
	5978,
	4970,
	6072,
	5883,
	4876,
	5978,
	4970,
	5978,
	4970,
	6072,
	5978,
	4970,
	5880,
	4873,
	5978,
	4970,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5938,
	4930,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5880,
	4873,
	5880,
	4873,
	5978,
	4970,
	6018,
	5009,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5939,
	4931,
	5978,
	4970,
	6018,
	6072,
	5978,
	4970,
	5978,
	4970,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6018,
	5009,
	6072,
	4970,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6018,
	5009,
	6018,
	5009,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	2965,
	2965,
	2965,
	3917,
	4970,
	3917,
	6072,
	4345,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4970,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	5009,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	6072,
	4970,
	6072,
	6072,
	2965,
	6072,
	5978,
	5978,
	5009,
	6072,
	4345,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	6072,
	4970,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	6072,
	2965,
	5978,
	5009,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	6072,
	6072,
	5009,
	6072,
	3917,
	3917,
	5009,
	6072,
	6072,
	4970,
	6072,
	4970,
	6072,
	4970,
	4970,
	4970,
	8684,
	6018,
	5009,
	5978,
	4970,
	6072,
	8565,
	5978,
	992,
	2138,
	1403,
	3917,
	3917,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	4930,
	6072,
	6018,
	5978,
	6072,
	5978,
	6072,
	5938,
	4930,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	5978,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	5878,
	4871,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	5978,
	4970,
	6072,
	213,
	8430,
	5978,
	5978,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x020000CE, { 10, 1 } },
	{ 0x020000CF, { 11, 5 } },
	{ 0x0600021E, { 0, 1 } },
	{ 0x06000258, { 1, 2 } },
	{ 0x060002F4, { 3, 3 } },
	{ 0x0600043A, { 6, 1 } },
	{ 0x0600045D, { 7, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[16] = 
{
	{ (Il2CppRGCTXDataType)3, 69294 },
	{ (Il2CppRGCTXDataType)3, 68089 },
	{ (Il2CppRGCTXDataType)2, 636 },
	{ (Il2CppRGCTXDataType)3, 21426 },
	{ (Il2CppRGCTXDataType)2, 6878 },
	{ (Il2CppRGCTXDataType)3, 21425 },
	{ (Il2CppRGCTXDataType)2, 18074 },
	{ (Il2CppRGCTXDataType)2, 2600 },
	{ (Il2CppRGCTXDataType)3, 355 },
	{ (Il2CppRGCTXDataType)3, 356 },
	{ (Il2CppRGCTXDataType)3, 66716 },
	{ (Il2CppRGCTXDataType)3, 23161 },
	{ (Il2CppRGCTXDataType)3, 56336 },
	{ (Il2CppRGCTXDataType)3, 55360 },
	{ (Il2CppRGCTXDataType)3, 66629 },
	{ (Il2CppRGCTXDataType)3, 55359 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2152,
	s_methodPointers,
	113,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	16,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
